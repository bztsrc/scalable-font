Scalable Screen Font
====================

--------------------------------------------------------------------------------
Check out the next generation of Scalable Screen Fonts: [SSFN 2.0](https://gitlab.com/bztsrc/scalable-font2)
offers even smaller font files, simpler API and high quality font rendering, comparable to professional engines!
--------------------------------------------------------------------------------

This is a single ANSI C/C++ header file, scalable bitmap and vector font renderer. It has only memory related
libc dependency and it does not use floating point numbers. It's extremely small (approx. 22k) and it is very
easy on memory, perfect for embedded systems and hobby OS kernels. It was a hobby project for me, so donations
and contributions would be much appreciated if it turns out to be useful to you.

<img alt="Scalable Screen Font Features" src="https://gitlab.com/bztsrc/scalable-font/raw/master/features.png">

SSFN renderer does not use existing font formats directly (because most formats are inefficient or just insane),
so you first have to compress those into [SSFN](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/ecosystem.md).
There're a handful of small ANSI C [utilities](https://gitlab.com/bztsrc/scalable-font/tree/master/sfnconv)
to do that (they support PS Type1, OpenType, TrueType, X11 Bitmap Distribution Format, Linux Console fonts, GNU
unifont and others). This means your fonts will require less space, and also the renderer can work
a lot faster than other renderer libraries. Check out [comparition](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/compare.md)
with other font formats.

 - [ssfn.h](https://gitlab.com/bztsrc/scalable-font/blob/master/ssfn.h) the SSFN renderer itself
 - [sfnconv](https://gitlab.com/bztsrc/scalable-font/tree/master/sfnconv) SSFN font converters / compressors
 - [sfnedit](https://gitlab.com/bztsrc/scalable-font/tree/master/sfnedit) SSFN font editor with a GUI
 - [sfntest](https://gitlab.com/bztsrc/scalable-font/tree/master/sfntest) test applications and [API](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/API.md) usage examples

The SSFN renderer comes in two flavours: there's the normal renderer with a few functions and libc dependency, and a
specialized renderer for OS kernel consoles with just one function and no dependencies at all.

Example Code
------------

### Normal Renderer

Very easy to use, here's an example without error handling:

```c
#include <ssfn.h>

ssfn_t ctx;                                         /* the renderer context */
ssfn_glyph_t *glyph;                                /* the returned rasterized bitmap */

/* you don't need to initialize the library, just make sure the context is zerod out */
memset(&ctx, 0, sizeof(ssfn_t));

/* add one or more fonts to the context. Fonts must be already in memory */
ssfn_load(&ctx, &_binary_times_sfn_start);          /* you can add different styles... */
ssfn_load(&ctx, &_binary_timesbold_sfn_start);
ssfn_load(&ctx, &_binary_timesitalic_sfn_start);
ssfn_load(&ctx, &_binary_emoji_sfn_start);          /* ...or different UNICODE ranges */
ssfn_load(&ctx, &_binary_cjk_sfn_start);

/* select the typeface to use */
ssfn_select(&ctx,
    SSFN_FAMILY_SERIF, NULL,                        /* family */
    SSFN_STYLE_REGULAR | SSFN_STYLE_UNDERLINE, 64,  /* style and size */
    SSFN_MODE_BITMAP                                /* rendering mode */
);

/* rasterize a glyph for the 0x41 UNICODE code point into a newly allocated bitmap */
glyph = ssfn_render(&ctx, 0x41);

/* display the bitmap on your screen */
my_draw_glyph(
    pen_x, pen_y - glyph->baseline,                 /* coordinates to draw to */
    glyph->w, glyph->h, glyph->pitch,               /* bitmap dimensions */
    &glyph->data                                    /* bitmap data */
);
pen_x += glyph->adv_x;                              /* adjust the cursor */
pen_y += glyph->adv_y;                              /* ssfn handles vertical fonts too */

/* free resources */
free(glyph);                                        /* no special treatment for freeing glyphs */
ssfn_free(&ctx);                                    /* free the renderer context's internal buffers */
```

There's more, you can select font by it's name and you can also query kerning info for example, read the
[API reference](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/API.md).

To keep the memory footprint of this renderer small, it can render glyphs up to 255 x 255 pixels. (Note
this is a limitation of this particular renderer implementation, the SSFN format is capable of storing
glyph data with 4096 x 4096 grid point precision just fine.)

An example `my_draw_glyph()` for SDL can be found [here](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/modes.md).

### Simple Renderer

```c
#define SSFN_NOIMPLEMENTATION               /* don't include the normal renderer implementation */
#define SSFN_CONSOLEBITMAP_HICOLOR          /* use the special renderer for hicolor packed pixels */
#include <ssfn.h>

/* set up context by global variables */
ssfn_font = &_binary_console_sfn_start;     /* the bitmap font to use */
ssfn_dst_ptr = 0xE0000000;                  /* framebuffer address and bytes per line */
ssfn_dst_pitch = 4096;
ssfn_fg = 0xFFFF;                           /* colors, white on black */
ssfn_bg = 0;
ssfn_x = 100;                               /* coordinates to draw to */
ssfn_y = 200;

/* render one glyph directly to the screen and then adjust ssfn_x and ssfn_y */
ssfn_putc(0x41);
```

As you see this renderer implementation is very simple, extremely small (less than 1k). It can only render
bitmap fonts. It does not allocate memory or need libc. It can't scale, but it can handle proportional fonts
(like 8x16 for Latin letters, and 16x16 for CJK ideograms), so you can implement a true UNICODE console with
this renderer.

Font Editor
-----------

Besides of the [converters](https://gitlab.com/bztsrc/scalable-font/tree/master/sfnconv) that can import various
font formats, there's also an [SSFN font editor](https://gitlab.com/bztsrc/scalable-font/tree/master/sfnedit) available:

<img alt="Scalable Screen Font Editor" src="https://gitlab.com/bztsrc/scalable-font/raw/master/docs/sfnedit4.png">

License
-------

Both the renderers, the converter utilities and the editor are licensed under the terms of MIT license in the
hope that they will be useful.

IMPORTANT NOTE: although the file format is licensed under MIT, it is possible that the font stored in a SSFN
file is NOT! Always consult the license field in the font's header! See `sfn2asc -h`.

Dependencies
------------

The simple renderer calls no functions at all and therefore has no dependencies whatsoever (not even libc
or compiler built-ins). Absolutely nothing save for it's global variables.

As for the normal renderer (all dependencies are provided as built-ins by gcc):
 - standard C integer defines from stdint.h
 - `realloc()` and `free()` from libc (stdlib.h)
 - `memcmp()` and `memset()` from libc (string.h)

The scalable font converter is built on the freetype2 library to read vector font files. The bitmap font
converter has no dependencies other than libc, but can be built optionally with zlib to read and write LZW
compressed (gzip deflate) files on-the-fly.

The editor uses SDL2 with an X11 fallback, and optionally needs zlib.

The test applications use SDL2 to create a window and display the rendered texts.

Known Bugs
----------

Not that I know of. But no programmer can test their own code properly. I've run SSFN renderer through valgrind
with many different font files with all the tests without problems. If you find a bug, please use the
[Issue Tracker](https://gitlab.com/bztsrc/scalable-font/issues) on gitlab and let me know.

Future Development (Whishlist)
------------------------------

The bitmap compressor works with the fragments as it reads them. It would be much better to read all fragments
first and only afterwards find the best matches to achieve a magnitude better compression rate. Now accents are
only stored separately if a plain letter comes first (like "Á" only splitted into "'" and an "A" if there was a
bitmap for "A" already).

The [autohinting](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/compare.md) code is very very basic
at the moment, it only uses one axis partitions instead of two axis partitioned boxes.

Converters only write the default glyph variants except for SSFN ASCII files where all variants are supported. The
editor is capable of handling all variant layers. It would be great to have that if I can figure out how to tell
the converters which variant they are converting.

The SSFN file format and the editor can handle colors and gradients in vector fonts, although the normal renderer
can only render to alpha channel without colors. I'm planning to introduce `SSFN_MODE_ARGB` [mode](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/modes.md)
or another version of the renderer to support colored typefaces. (Using colorless vector glyphs allows a much faster
and less memory eager algorithm to be used, which merges polygons before rasterization. With polygons of different
colors you can't merge in advance.)

Good Programmer Font
--------------------

A few thoughts on what makes a font good for programming:

 - it has to be limited proportinal or fixed (that is, it uses the same fixed width within a scripting system. That's okay
   if it has a single width for all Latin letters and the same double width for all CJK and other Asian scripts.)
 - numbers are clearly distinguishable from letters and ideographic numbers. Unlike uppercase O, 0 should have a dot in the
   middle or crossed with a slash. Unlike lowercase l, 1 should have a "head" in an angle, and probably as big "foot" as
   the 2. 3 should have a vertical line at the top, clearly different from Cyrillic letter Ze З and reversed Latin open E ɜ etc.
 - single quote ' and double quote " are clearly distinguishable from combining accent, apostrophe ʼ, single quotation mark ’
   and double prime ʺ, double apostrophe ˮ, double quotation mark ” etc.
 - parenthesis (, ) are clearly distinguishable from ideographic ones like ❨, ❪, ⟮, or （.
 - operators +, *, -, /, % etc. are clearly distinguishable from other similar symbols like ➕, ⁎, –, ⁄, ⁒ etc.
 - comparition operators are represented as is, using the same character count, for example =< is not replaced by the ≤ ligature.

Authors
-------

SSFN format, converters, editor and renderers: bzt

