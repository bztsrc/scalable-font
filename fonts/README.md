Scalable Screen Font - Example Fonts
====================================

 - FreeSans.sfn.gz - vector based font with cubic Bezier curves, converted from [FreeSans.otf](https://www.gnu.org/software/freefont/) of the GNU freefont project with maximum quality

 - FreeSans4.sfn.gz - same, but compressed at recommended quality level 4

 - FreeSerif.sfn.gz - vector based font, converted from FreeSerif.otf of the GNU freefont project

 - FreeSerif4.sfn.gz - same, but compressed at recommended quality level 4

 - VeraX.sfn - vector based fonts with quadratic Bezier curves, converted from [VeraX.ttf](https://www.gnome.org/fonts/) of the GNOME Bitstream Vera Fonts (variations on style)

 - Vera.sfn - an SSFN font collection of the above fonts

 - u_vga16.sfn - bitmap font, converted from [u_vga16.bdf](http://www.inp.nsk.su/~bolkhov/files/fonts/univga/), made by Dmitry Bolkhovityanov

 - unifont.sfn.gz bitmap font, converted from [unifont.hex](http://unifoundry.com/unifont/index.html) of the GNU unifont project

 - chrome.sfn - pixmap font, Retro Chrome looking, made after an image found by Google

 - emoji.sfn - pixmap font, created from a screenshot of unicode.org's emoji page

 - stoneage.sfn - pixmap font from an old DOS game, which I rewrote for Linux, [xstoneage](https://gitlab.com/bztsrc/xstoneage)

