/*
 * sfnedit/glyphs.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Glyphs table tab (View and Controller)
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ui.h"
#include "lang.h"
#include "util.h"
#include "file.h"
#include "unicode.h"
#include "copypaste.h"

int glyphactive = 2, glyphreent = 0, glyphra = -1, gsiz = 0, goff = 0, scroll = 0, insel = 0;
int selrs = -1, selre = -1, gmax = 0, oa = 0, clickx, clicky, glist[1024];
uint32_t stglyph = 0, grs, gre, srcgchr = 0, over = -1U, lastover = -1U;
char gsearch[256], gtmp[256], rangestr[64];
extern uint32_t ssfn_dst_w;
extern int numwin;
extern ui_win_t *wins;

/**
 * Fill in glyph list with search results
 */
void search_glyph(ui_win_t *win)
{
    uint32_t i, j, k;
    int u;

    if(glyphra == -1) {
        grs = 0; gre = UNICODE_LAST;
    } else {
        grs = ublocks[glyphra].start; gre = ublocks[glyphra].end;
    }
    sprintf(rangestr, "U+%06X ... U+%06X ", grs, gre);
    strncat(rangestr, glyphra != -1 ? ublocks[glyphra].name : lang[WHOLERANGE], 41);
    scroll = 0;
    if(!gsearch[0]) {
        gmax = ((win->h - 68) / (gsiz+1)) *16;
        if(stglyph + gmax > gre + 1) stglyph = ((int)gre + 1 > gmax ? gre + 1 - gmax : 0);
        if(stglyph < grs) stglyph = grs;
        for(i=0; (int)i < gmax; i++) {
            if(stglyph + i > gre) { gmax = i; break; }
            glist[i] = file_findchar(wins[0].v, stglyph + i);
        }
    } else {
        k = strlen(gsearch);
        if(gsearch[0]=='U' && gsearch[1]=='+') j = gethex(gsearch+2, 6);
        else {
            j = gethex(gsearch, 6);
            for(i=0;i<k;i++)
                if(!((gsearch[i]>='0'&&gsearch[i]<='9') ||
                    (gsearch[i]>='a'&&gsearch[i]<='f') ||
                    (gsearch[i]>='A'&&gsearch[i]<='F'))) { j = 0; break; }
        }
        for(i=grs; i<=gre && gmax < 1023; i++) {
            u = file_findchar(wins[0].v, i);
            if(u > -1 && ((j && i==j) || !memcmp(utf8(i), gsearch, k)))
                    glist[gmax++] = u;
        }
        if(!gmax)
            for(i=grs; i<=gre && gmax < 1023; i++) {
                u = file_findchar(wins[0].v, i);
                if(u > -1 && (!search_name(gsearch, uninames[uniname(i)].name, 0))) {
                    for(k=0;(int)k<gmax && glist[k]!=u;k++);
                    if((int)k==gmax) glist[gmax++] = u;
                }
            }
    }
}

/**
 * View for glyph table
 */
void view_glyphs(ui_win_t *win)
{
    int i, w, s, e;

    glyphreent++;
    if(over != lastover) {
        lastover = over;
        memset(gtmp, ' ', 63);
        gtmp[63]=0;
        if(selrs == -1 || glist[selre] == -1) {
            if(over != -1U) {
                glyphactive = 2;
                sprintf(gtmp, "U+%06X %s %s                                        ",
                    over, over ? utf8(over) : "\xC0\x80", uninames[
                    !win->v || file_findchar(0, over) != -1 ? uniname(over) : UNICODE_NUMNAMES].name);
            }
        } else if(glist[selre] != -1) {
            glyphactive = 2;
            sprintf(gtmp, "U+%06X ... U+%06X                                   ",
                chars[wins[0].v][glist[selrs]].unicode, chars[wins[0].v][glist[selre]].unicode);
        }
    }
redraw:
    gsiz = ((win->w - 20)/16);
    if(gsiz > 255) gsiz = 255;
    goff = (win->w - (gsiz+1)*16) >> 1;

    if(glyphactive < 2) { gmax = 0; selrs = selre = -1; }
    if(!gmax || !gsearch[0]) search_glyph(win);
    ssfn_x = 2; ssfn_y = win->h - 24;
    ssfn_fg = theme[THEME_TABU];
    ssfn_bg = theme[THEME_BG];
    ui_text(win, rangestr, ssfn_bg, ssfn_bg);

    ssfn_x = 2; ssfn_y = 44;
    ssfn_dst_w = win->w >> 1;
    ui_text(win, gtmp, ssfn_bg, ssfn_bg);
    ssfn_dst_w = win->w;
    ssfn_fg = theme[THEME_FG];

    ssfn_x = goff; ssfn_y = 68;
    s = selrs <= selre ? selrs : selre;
    e = selrs <= selre ? selre : selrs;
    for(i=scroll; i < gmax && ssfn_y + gsiz < (uint32_t)win->h; i++) {
        ui_glyph(win, glist[i], stglyph+i, gsiz, glist[i] == -1? 2 : ((i >= s && i <= e) ? 1 : (oa == i ? 3 : 0)));
        ssfn_x += gsiz+1;
        if(ssfn_x+gsiz >= (uint32_t)win->w) { ssfn_x = goff; ssfn_y += gsiz+1; }
    }

    ssfn_x = (win->w >> 1); ssfn_y = 44;
    ui_scale(win, lang[LOCAL0 + win->v], (win->w >> 2)-32, glyphactive==0);
    ssfn_x = (win->w >> 1) + 2 + (win->w >> 2) + (win->w & 1); ssfn_y = 44;
    w = ((win->w >> 1)-16-8-(win->w >> 2));
    ui_input(win, gsearch, w, 1, 0);
    ui_button(win, "⚲", 8, 2, glyphactive==1);
    ssfn_x = (win->w >> 1) + 2 + (win->w >> 2) + (win->w & 1); ssfn_y = 44;
    if((i=ui_input(win, gsearch, w, 5, glyphactive==1 && glyphreent==1))) {
        glyphactive += i;
        ui_resizewin(win, win->w, win->h);
        ui_tabs(win, ui_getwin(win->winid));
        gmax = 0;
        goto redraw;
    }
    glyphreent--;
}

/**
 * Controller for glyph table
 */
int ctrl_glyphs(ui_win_t *win __attribute__((unused)), ui_event_t *evt)
{
    int a, i, j, k;

    switch(evt->type) {
        case E_KEY:
            switch(evt->x) {
                case K_UP:
up:                 if(!glyphactive) { glyphactive = 2; return 1; }
                    if(!gsearch[0]) {
                        if(stglyph > grs) stglyph -= 16;
                        if(stglyph < grs) stglyph = grs;
                        gmax = 0;
                        return 1;
                    } else {
                        if(scroll > 0) {
                            scroll -= 16;
                            ui_resizewin(win, win->w, win->h);
                           return 1;
                        }
                    }
                    return 0;
                case K_DOWN:
down:               if(!glyphactive) { glyphactive = 1; return 1; }
                    if(!gsearch[0]) {
                        if(stglyph + gmax < gre) stglyph += 16;
                        gmax = 0;
                        return 1;
                    } else {
                        if(scroll + 16 < gmax) {
                            scroll += 16;
                            ui_resizewin(win, win->w, win->h);
                            return 1;
                        }
                    }
                    return 0;
                case K_LEFT:
left:               if(win->v > 0) {
                        win->v--;
                        gmax = 0;
                        lastover = selrs = selre = -1;
                        if(gsearch[0]) { over = -1U; lastover = -2U; }
                        ui_resizewin(win, win->w, win->h);
                        return 1;
                    }
                    return 0;
                case K_RIGHT:
right:              if(win->v + 1 < SSFN_NUMVARIANTS) {
                        win->v++;
                        gmax = 0;
                        lastover = selrs = selre = -1;
                        if(gsearch[0]) { over = -1U; lastover = -2U; }
                        ui_resizewin(win, win->w, win->h);
                        return 1;
                    }
                    return 0;
                default:
                    if(!(event.h & ~1)) {
                        gsearch[0] = 0;
                        if(event.x >= 32) memcpy(gsearch, &event.x, strlen((char*)&event.x)+1);
                        glyphactive = 1;
                        ui_resizewin(win, win->w, win->h);
                    } else if(event.h & 2) {
                        if(evt->x == 'c' && selrs != -1 && selre != -1) {
                            copypaste_clear();
                            for(i=selrs;i<selre;i++) {
                                copypaste_copyrange(win->v, file_findchar(win->v, glist[i]));
                            }
                        } else
                        if(evt->x == 'x' && selrs != -1 && selre != -1) {
                            copypaste_clear();
                            for(i=selrs;i<selre;i++) {
                                k = file_findchar(win->v, glist[i]);
                                if(k == -1) continue;
                                copypaste_copyrange(win->v, k);
                                file_delchar(win->v, k);
                                for(j=1; j < numwin; j++)
                                    if(wins[j].chr == k)
                                        ui_closewin(j);
                            }
                        } else
                        if(evt->x == 'z')
                            copypaste_pasterange(win->v, over);
                    }
            }
            return 1;
        case E_MOUSEMOVE:
            over = clickx = clicky = -1;
            if(evt->y > 68 && evt->x > goff && evt->x < goff + (gsiz+1)*16) {
                a = ((evt->y - 68) / (gsiz+1) *16) + ((evt->x - goff) / (gsiz+1)) + scroll;
                if(a < gmax) {
                    over = glist[a] != -1 ? chars[wins[0].v][glist[a]].unicode : stglyph + a;
                    oa = a;
                    if(glist[a] != -1 && insel) selre = a;
                }
            }
            return (lastover != over);
        case E_BTNPRESS:
            if(evt->y >= 44 && evt->y < 44+24 && evt->x > win->w >> 1 && evt->x < (win->w >> 1) + (win->w >> 2)) glyphactive = 0;
            else if(evt->y >= 44 && evt->y < 44+24 && evt->x > (win->w >> 1) + (win->w >> 2)) glyphactive = 1;
            else glyphactive = 2;
            if(evt->w & 32 || (evt->h & 1 && evt->w & 8)) goto left;
            if(evt->w & 64 || (evt->h & 1 && evt->w & 16)) goto right;
            if(evt->w & 8) goto up;
            if(evt->w & 16) goto down;
            if(evt->y > 68 && evt->x > goff && evt->x < goff + (gsiz+1)*16) {
                if(evt->w & 1) {
                    a = ((evt->y - 68) / (gsiz+1) *16) + ((evt->x - goff) / (gsiz+1)) + scroll;
                    if(a < gmax && glist[a] != -1) {
                        insel = 1;
                        selre = selrs = a;
                    }
                    clickx = event.x; clicky = event.y;
                }
            }
            if(evt->w & 6 && over != -1U) {
                printf("%s\n", gtmp);
            }
            return 1;
        case E_BTNRELEASE:
            if(evt->y > 68 && evt->x > goff && evt->x < goff + (gsiz+1)*16) {
                a = ((evt->y - 68) / (gsiz+1) *16) + ((evt->x - goff) / (gsiz+1)) + scroll;
                if(a < gmax && insel && glist[a] != -1) { if(a < selrs) { selre = selrs; selrs = a; } else selre = a; }
                else selrs = selre = -1;
            }
            if(over != -1U && clickx == event.x && clicky == event.y && (!wins[0].v || file_findchar(0, over) != -1)) {
                ui_openwin(wins[0].v, over);
                selrs = selre = -1;
            }
            insel = 0;
            return 1;
    }
    return 0;
}
