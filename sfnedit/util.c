/*
 * sfnedit/util.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Utility functions
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "lang.h"
#include "ui.h"
#include "file.h"
#if HAS_ZLIB
#include <zlib.h>
#else
#include "tinflate.h"
#endif
#ifndef tolower
#define tolower(X)  (((X) >= 'A') && ((X) <= 'Z') ? ('a'+((X)-'A')) : (X))
#endif

char ut[8], gz;

#ifdef __WIN32__
extern char binary_unicode_dat_start;
extern char binary_unicode_dat_end;
#define unicode_start binary_unicode_dat_start
#define unicode_end binary_unicode_dat_end
#else
extern char _binary_unicode_dat_start;
extern char _binary_unicode_dat_end;
#define unicode_start _binary_unicode_dat_start
#define unicode_end _binary_unicode_dat_end
#endif

/**
 * Convert hex string to binary number
 */
uint32_t gethex(char *ptr, int len)
{
    register uint32_t ret = 0;
    for(;len--;ptr++) {
        if(*ptr>='0' && *ptr<='9') {          ret <<= 4; ret += (uint32_t)(*ptr-'0'); }
        else if(*ptr >= 'a' && *ptr <= 'f') { ret <<= 4; ret += (uint32_t)(*ptr-'a'+10); }
        else if(*ptr >= 'A' && *ptr <= 'F') { ret <<= 4; ret += (uint32_t)(*ptr-'A'+10); }
        else break;
    }
    return ret;
}

/**
 * Convert UNICODE code point into UTF-8 sequence
 */
char *utf8(int i)
{
    if(i<0x80) { ut[0]=i; ut[1]=0;
    } else if(i<0x800) {
        ut[0]=((i>>6)&0x1F)|0xC0;
        ut[1]=(i&0x3F)|0x80;
        ut[2]=0;
    } else if(i<0x10000) {
        ut[0]=((i>>12)&0x0F)|0xE0;
        ut[1]=((i>>6)&0x3F)|0x80;
        ut[2]=(i&0x3F)|0x80;
        ut[3]=0;
    } else {
        ut[0]=((i>>18)&0x07)|0xF0;
        ut[1]=((i>>12)&0x3F)|0x80;
        ut[2]=((i>>6)&0x3F)|0x80;
        ut[3]=(i&0x3F)|0x80;
        ut[4]=0;
    }
    return ut;
}

/**
 * Block name comparition according to UNICODE Inc.
 */
int unicmp(char *a, char *b, int sep)
{
    for(;*a && *b;a++,b++) {
        if(sep) {
            while(*a==' ' || *a=='-' || *a=='_') a++;
            while(*b==' ' || *b=='-' || *b=='_') b++;
        }
        if(tolower(*a) != tolower(*b)) return 1;
    }
    return 0;
}

/**
 * Search name for match
 */
int search_name(char *n, char *h, int sep)
{
    register int i, j=strlen(n), k=strlen(h);

    if(j <= k)
        for(i=0; i<k-j+1; i++)
            if(!unicmp(n, h+i, sep)) return 0;
    return 1;
}

/**
 * report an error and exit
 */
void error(char *subsystem, int fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    fprintf(stderr, "sfnedit: %s: ", subsystem);
    vfprintf(stderr, lang[fmt], args);
    fprintf(stderr, "\n");
    exit(fmt + 1);
}

/**
 * Add a color map entry
 */
uint8_t cpal_add(int r, int g, int b, int a)
{
    int i, dr, dg, db, m, dm, q;

    if(!r && !g && !b && (!a || a==0xFF)) return 240;

    for(q=1; q<=8; q++) {
        m=-1; dm=256;
        for(i=0; i<240 && (cpal[i*4+0] || cpal[i*4+1] || cpal[i*4+2]); i++) {
            if(a==cpal[i*4+3] && b==cpal[i*4+0] && g==cpal[i*4+1] && r==cpal[i*4+2]) return i;
            if(b>>q==cpal[i*4+0]>>q && g>>q==cpal[i*4+1]>>q && r>>q==cpal[i*4+2]>>q) {
                dr = r > cpal[i*4+2] ? r - cpal[i*4+2] : cpal[i*4+2] - r;
                dg = g > cpal[i*4+1] ? g - cpal[i*4+1] : cpal[i*4+1] - g;
                db = b > cpal[i*4+0] ? b - cpal[i*4+0] : cpal[i*4+0] - b;
                if(dg > dr) dr = dg;
                if(db > dr) dr = db;
                if(dr < dm) { dm = dr; m = i; }
                if(!dm) break;
            }
        }
        if(dm > 3 && i<240) {
            cpal[i*4+3] = a;
            cpal[i*4+2] = r;
            cpal[i*4+1] = g;
            cpal[i*4+0] = b;
            return i;
        }
        if(m>=0) {
            cpal[m*4+3] = ((cpal[m*4+3] + a) >> 1);
            cpal[m*4+2] = ((cpal[m*4+2] + r) >> 1);
            cpal[m*4+1] = ((cpal[m*4+1] + g) >> 1);
            cpal[m*4+0] = ((cpal[m*4+0] + b) >> 1);
            return m;
        }
    }
    error("cpal_add",ERR_COLOR);
    return 240;
}

/**
 * Load a (compressed) file
 */
unsigned char *load_file(char *filename, int *size)
{
    unsigned char *data = NULL, *data2 = NULL;
    int r, s;
    FILE *f;
#if HAS_ZLIB
    z_stream strm  = {0};
#else
    unsigned char *ptr, c;
    TINF_DATA d;
#endif

    if(filename == (char*)&font_start) {
        data = (unsigned char*)&font_start;
        *size = (int)(&font_end - &font_start);
    } else
    if(filename == (char*)&unicode_start) {
        data = (unsigned char*)&unicode_start;
        *size = (int)(&unicode_end - &unicode_start);
    } else {
        f = fopen(filename, "rb");
        if(!f) return NULL;
        fseek(f, 0, SEEK_END);
        *size = (int)ftell(f);
        fseek(f, 0, SEEK_SET);
        if(!*size) return NULL;
        data = (unsigned char*)malloc(*size);
        if(!data) error("load_file", ERR_MEM);
        fread(data, *size, 1, f);
        fclose(f);
    }

    gz = 0;
    if(data[0] == 0x1f && data[1] == 0x8b) {
        gz = 1;
        s = *(unsigned int*)(data + *size - 4);
        data2 = (unsigned char*)malloc(s);
        if(!data2) error("inflate", ERR_MEM);
#if HAS_ZLIB
        strm.total_in  = strm.avail_in  = *size;
        strm.total_out = strm.avail_out = s;
        strm.next_in   = (Bytef *) data;
        strm.next_out  = (Bytef *) data2;

        strm.zalloc = Z_NULL;
        strm.zfree = Z_NULL;
        strm.opaque = Z_NULL;
        r = inflateInit2(&strm, (15+32));
        if (r == Z_OK) {
            r = inflate(&strm, Z_FINISH);
            inflateEnd(&strm);
        }
        if (r != Z_OK && r != Z_STREAM_END)
            error("zlib", ERR_MEM);
#else
        ptr = data + 2;
        if(*ptr++ != 8) goto gzerr;
        c = *ptr++; ptr += 6;
        if(c & 4) { r = *ptr++; r += (*ptr++ << 8); ptr += r; }
        if(c & 8) { while(*ptr++ != 0); }
        if(c & 16) { while(*ptr++ != 0); }
        d.source = ptr;
        d.bitcount = 0;
        d.bfinal = 0;
        d.btype = -1;
        d.dict_size = 0;
        d.dict_ring = NULL;
        d.dict_idx = 0;
        d.curlen = 0;
        d.dest = data2;
        d.destSize = s;
        do { r = uzlib_uncompress(&d); } while (!r);
        if (r != TINF_DONE) {
gzerr:      error("tinflate", ERR_MEM);
        }
#endif
        if(data != (unsigned char*)&font_start && data != (unsigned char*)&unicode_start)
            free(data);
        *size = s;
        return(data2);
    }

    return data;
}
