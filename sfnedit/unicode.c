/*
 * sfnedit/unicode.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief UNICODE lookup function
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "lang.h"
#include "util.h"
#define _UNICODE_BLOCKSDATA
#define _UNICODE_NAMESDATA
#include "unicode.h"

#ifdef __WIN32__
extern char binary_unicode_dat_start;
#define unicode_start binary_unicode_dat_start
#else
extern char _binary_unicode_dat_start;
#define unicode_start _binary_unicode_dat_start
#endif
char *unicodedb;

/**
 * Initialize UNICODE names data
 */
void uniname_init()
{
    int i, j;
    char *ptr, *end;

    ptr = unicodedb = (char*)load_file((char*)&unicode_start, &i);
    if(!ptr) error("uniname_init", ERR_MEM);
    end = ptr + i;

    for(i=j=0;i < UNICODE_NUMNAMES && ptr < end;i++) {
        while(*((uint16_t*)ptr) & 0x8000) {
            j += -(*((int16_t*)ptr));
            ptr += 2;
        }
        uninames[i].unicode = j++;
        uninames[i].rtl = *ptr++;
        uninames[i].name = ptr;
        while(*ptr && ptr < end) ptr++;
        ptr++;
    }

    uninames[0].name = lang[NOGLYPH];
    uninames[UNICODE_NUMNAMES].name = lang[UNDEFINED];
}

/**
 * Return the UNICODE name data index for UNICODE
 */
int uniname(int unicode)
{
    register int i=0, j=UNICODE_NUMNAMES-1, k, l=22;

    if(!unicode) return 0;
    if(unicode > 0x10FFFF) return UNICODE_NUMNAMES;
    while(l--) {
        k = i + ((j-i) >> 1);
        if(uninames[k].unicode == unicode) return k;
        if(i >= j) break;
        if(uninames[k].unicode < unicode) i = k + 1; else j = k;
    }
    return UNICODE_NUMNAMES;
}

/**
 * Free resources
 */
void uniname_free()
{
    free(unicodedb);
}
