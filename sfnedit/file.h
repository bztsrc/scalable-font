/*
 * sfnedit/file.h
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief SSFN file definitons
 *
 */

#ifndef _SSFN_H_
# define SSFN_NOIMPLEMENTATION
# include <ssfn.h>
#endif

typedef struct {
    int type;
    int len;
    cont_t *data;
    int prod;
    uint8_t color[4];
} frag_t;

typedef struct {
    uint32_t unicode;
    int rtl;
    int w;
    int h;
    int adv_x;
    int adv_y;
    int bear_l;
    int bear_t;
    int len;
    int *frags;
    int kgrp;
    int klen;
    int *kern;
} char_t;

extern int numchars[SSFN_NUMVARIANTS], numfrags, numkerns, modified;
extern char_t *chars[SSFN_NUMVARIANTS];
extern frag_t *frags;
extern uint32_t cmap[241];
extern uint8_t *cpal;
extern ssfn_font_t *font;
extern char *fontfilebn;
extern char *strtable[];

int file_findchar(int v, uint32_t unicode);
int file_addchar(int v, uint32_t unicode, int check);
void file_delchar(int v, int chr);
void file_delfrag(int idx);
int file_addemptyfrag(char_t *chr, int type, int fragidx);
void file_removefrag(char_t *chr, int idx);
void file_contadd(char_t *chr, int fragidx, int type, int px, int py, int c1x, int c1y, int c2x, int c2y);
void file_load(char *filename);
void file_savesfn(int idx);
void file_saveasc(int idx);
void file_setquality(int q);
void file_setbaseline();
void file_free();
