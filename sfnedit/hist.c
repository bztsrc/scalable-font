/*
 * sfnedit/hist.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Edit history
 *
 */

#include <string.h>
#include <stdlib.h>
#include "ui.h"
#include "hist.h"
#include "file.h"
#include "lang.h"
#include "util.h"

extern int minx, miny, maxx, maxy, movex, movey;

/**
 * Undo
 */
void hist_undo(ui_win_t *win)
{
    int i,j,k;
    frag_t *f;
    char_t *chr = &chars[win->v][win->chr];
    hist_t *his;

    if(win->hist && win->histmin) {
        win->histmin--;
        his = &win->hist[win->histmin];
        switch(his->type) {
            case HIST_DELLAYER: win->frag = file_addemptyfrag(chr, -1, his->frag); win->tab = 0; win->menu = 5; break;
            case HIST_ADDLAYER:
                for(i=0;i<chr->len;i++) if(chr->frags[i] == his->frag) break;
                file_removefrag(chr, i);
                win->frag = 0;
                win->tab = 0; win->menu = 5;
            break;
            case HIST_SETPIXEL: ((uint8_t*)frags[his->frag].data)[his->idx] = his->data.pixel.oldpix;
                win->tab = 0; win->menu = 5; break;
            case HIST_SETADV: chr->adv_x = his->data.adv.oldx; chr->adv_y = his->data.adv.oldy;
                win->tab = 0; win->menu = chr->adv_y ? 1: 0; break;
            case HIST_SETBLINE: font->baseline = his->data.adv.oldy; win->tab = 0; win->menu = 0; break;
            case HIST_SETULINE: font->underline = his->data.adv.oldy; win->tab = 0; win->menu = 0; break;
            case HIST_DELCONT: file_contadd(chr, his->frag, his->data.cont.type, his->data.cont.px, his->data.cont.py,
                his->data.cont.c1x, his->data.cont.c1y, his->data.cont.c2x, his->data.cont.c2y); win->tab = 0; win->menu = 5; break;
            case HIST_ADDCONT: if(frags[his->frag].len) { frags[his->frag].len--; } win->tab = 0; win->menu = 5; break;
            case HIST_MOVEPT: memcpy(frags[his->frag].data, his->data.contlist.oldc, frags[his->frag].len*sizeof(cont_t));
                win->tab = 0; win->menu = 5; break;
            case HIST_HINT:
                for(i=k=0; i < chr->len; i++) {
                    f = &frags[chars[win->v][win->chr].frags[i]];
                    if(f->type == SSFN_FRAG_CONTOUR)
                        for(j=0; j < f->len; j++,k++)
                            f->data[j].px = win->hist[win->histmin].data.hintlist.oldx[k];
                }
                win->tab = 0; win->menu = 2;
            break;
            case HIST_KERN:
                chr = &chars[0][his->frag];
                for(i=0;i<chr->klen;i++) {
                    if(chr->kern[i*3] == his->idx) {
                        chr->kern[i*3+1] = his->data.adv.oldx;
                        chr->kern[i*3+2] = his->data.adv.oldy;
                        break;
                    }
                }
                win->tab = 1;
            break;
        }
    }
}

/**
 * Redo
 */
void hist_redo(ui_win_t *win)
{
    int i,j,k;
    frag_t *f;
    char_t *chr = &chars[win->v][win->chr];
    hist_t *his = win->hist ? &win->hist[win->histmin] : NULL;

    if(win->histmin < win->histmax && his) {
        switch(his->type) {
            case HIST_ADDLAYER: win->frag = file_addemptyfrag(chr, -1, his->frag); win->tab = 0; win->menu = 5; break;
            case HIST_DELLAYER:
                for(i=0;i<chr->len;i++) if(chr->frags[i] == his->frag) break;
                file_removefrag(chr, i); if(win->frag > chr->len) { win->frag--; }
                win->tab = 0; win->menu = 5;
            break;
            case HIST_SETPIXEL: ((uint8_t*)frags[his->frag].data)[his->idx] = his->data.pixel.newpix;
                win->tab = 0; win->menu = 5; break;
            case HIST_SETADV: chr->adv_x = his->data.adv.newx; chr->adv_y = his->data.adv.newy;
                win->tab = 0; win->menu = chr->adv_y ? 1 : 0; break;
            case HIST_SETBLINE: font->baseline = his->data.adv.newy; win->tab = 0; win->menu = 0; break;
            case HIST_SETULINE: font->underline = his->data.adv.newy; win->tab = 0; win->menu = 0; break;
            case HIST_DELCONT: if(frags[his->frag].len) { frags[his->frag].len--; } win->tab = 0; win->menu = 5; break;
            case HIST_ADDCONT: file_contadd(chr, his->frag, his->data.cont.type, his->data.cont.px, his->data.cont.py,
                his->data.cont.c1x, his->data.cont.c1y, his->data.cont.c2x, his->data.cont.c2y); win->tab = 0; win->menu = 5; break;
            case HIST_MOVEPT: memcpy(frags[his->frag].data, his->data.contlist.newc, frags[his->frag].len*sizeof(cont_t));
                win->tab = 0; win->menu = 5; break;
            case HIST_HINT:
                for(i=k=0; i < chr->len; i++) {
                    f = &frags[chars[win->v][win->chr].frags[i]];
                    if(f->type == SSFN_FRAG_CONTOUR)
                        for(j=0; j < f->len; j++,k++)
                            f->data[j].px = win->hist[win->histmin].data.hintlist.newx[k];
                }
                win->tab = 0; win->menu = 5;
            break;
            case HIST_KERN:
                chr = &chars[0][his->frag];
                for(i=0;i<chr->klen;i++) {
                    if(chr->kern[i*3] == his->idx) {
                        chr->kern[i*3+1] = his->data.adv.newx;
                        chr->kern[i*3+2] = his->data.adv.newy;
                        break;
                    }
                }
                win->tab = 1;
            break;
        }
        win->histmin++;
    }
}

/**
 * Cleanup things stuck in history
 */
void hist_cleanup(ui_win_t *win, int idx)
{
    int i;

    if(win->hist && win->histmax) {
        for(i=idx;i<win->histmax;i++)
            switch(win->hist[i].type) {
                case HIST_DELLAYER: file_delfrag(win->hist[i].frag); break;
                case HIST_MOVEPT:
                    if(win->hist[i].data.contlist.oldc)
                        free(win->hist[i].data.contlist.oldc);
                    win->hist[i].data.contlist.oldc = NULL;
                    if(win->hist[i].data.contlist.newc)
                        free(win->hist[i].data.contlist.newc);
                    win->hist[i].data.contlist.newc = NULL;
                break;
                case HIST_HINT:
                    if(win->hist[i].data.hintlist.oldx)
                        free(win->hist[i].data.hintlist.oldx);
                    win->hist[i].data.hintlist.oldx = NULL;
                    if(win->hist[i].data.hintlist.newx)
                        free(win->hist[i].data.hintlist.newx);
                    win->hist[i].data.hintlist.newx = NULL;
                break;
            }
    }
}

/**
 * Allocate memory for a new history element
 */
int hist_add(ui_win_t *win, int type)
{
    hist_cleanup(win, win->histmin);
    win->histmax = win->histmin + 1;
    win->hist = (hist_t*)realloc(win->hist, win->histmax*sizeof(ui_win_t));
    if(!win->hist) error("hist_add", ERR_MEM);
    memset(&win->hist[win->histmin], 0, sizeof(hist_t));
    win->hist[win->histmin].type = type;
    return win->histmin;
}

/**
 * Add a new layer
 */
void hist_addlayer(ui_win_t *win, int type)
{
    int i;
    char_t *chr = &chars[win->v][win->chr];
    if(type != SSFN_FRAG_CONTOUR) {
        for(i=0;i<chr->len;i++)
            if(frags[chr->frags[i]].type == type) { win->frag = i; return; }
    }
    i = hist_add(win, HIST_ADDLAYER);
    win->histmin++;
    win->frag = file_addemptyfrag(&chars[win->v][win->chr], type, -1);
    win->hist[i].frag = chars[win->v][win->chr].frags[win->frag];
}

/**
 * Delete a layer
 */
void hist_dellayer(ui_win_t *win)
{
    int i;
    char_t *chr = &chars[win->v][win->chr];

    if(!chars[win->v][win->chr].len) return;
    i = hist_add(win, HIST_DELLAYER);
    win->hist[i].frag = chr->frags[win->frag];
    hist_redo(win);
    modified = 1;
}

/**
 * Change a pixel on a bitmap or pixmap glyph
 */
void hist_setpixel(ui_win_t *win, int idx, uint8_t pixel)
{
    int i;
    char_t *chr = &chars[win->v][win->chr];

    if(!chars[win->v][win->chr].len) return;
    i = hist_add(win, HIST_SETPIXEL);
    win->histmin++;
    win->hist[i].frag = chr->frags[win->frag];
    win->hist[i].idx = idx;
    win->hist[i].data.pixel.oldpix = ((uint8_t*)frags[chr->frags[win->frag]].data)[idx];
    win->hist[i].data.pixel.newpix = ((uint8_t*)frags[chr->frags[win->frag]].data)[idx] = pixel;
    modified = 1;
}

/**
 * Delete the last contour command
 */
void hist_delcont(ui_win_t *win)
{
    int i, j;
    char_t *chr = &chars[win->v][win->chr];

    if(!chr->len || !frags[chr->frags[win->frag]].len) return;
    j = --frags[chr->frags[win->frag]].len;

    i = hist_add(win, HIST_DELCONT);
    win->histmin++;
    win->hist[i].frag = chr->frags[win->frag];
    win->hist[i].idx = j;
    memcpy(&win->hist[i].data.cont, &frags[chr->frags[win->frag]].data[j], sizeof(cont_t));
    modified = 1;
}

/**
 * Add a contour command to contour
 */
void hist_addcont(ui_win_t *win, int type, int px, int py)
{
    int i = hist_add(win, HIST_ADDCONT), j, dx, dy;
    char_t *chr = &chars[win->v][win->chr];
    cont_t *c;

    if(!chr->len) return;
    if(!frags[chr->frags[win->frag]].len) type = SSFN_CONTOUR_MOVE;
    j = frags[chr->frags[win->frag]].len;
    win->hist[i].frag = chr->frags[win->frag];
    win->hist[i].idx = j;
    win->hist[i].data.cont.type = type;
    win->hist[i].data.cont.px = px;
    win->hist[i].data.cont.py = py;
    if(type == SSFN_CONTOUR_QUAD) {
        c = &frags[chr->frags[win->frag]].data[j-1];
        win->hist[i].data.cont.c1x = (px + c->px) >> 1;
        win->hist[i].data.cont.c1y = (py + c->py) >> 1;
    } else
    if(type == SSFN_CONTOUR_CUBIC) {
        c = &frags[chr->frags[win->frag]].data[j-1];
        dx = px > c->px ? px - c->px : c->px - px;
        dy = py > c->py ? py - c->py : c->py - py;
        win->hist[i].data.cont.c1x = (px > c->px ? c->px + dx/3 : px + 2*dx/3);
        win->hist[i].data.cont.c1y = (py > c->py ? c->py + dy/3 : py + 2*dy/3);
        win->hist[i].data.cont.c2x = (px > c->px ? c->px + 2*dx/3 : px + dx/3);
        win->hist[i].data.cont.c2y = (py > c->py ? c->py + 2*dy/3 : py + dy/3);
    } else {
        win->hist[i].data.cont.c1x = win->hist[i].data.cont.c1y = win->hist[i].data.cont.c2x = win->hist[i].data.cont.c2y = 0;
    }
    hist_redo(win);
    modified = 1;
}

/**
 * Save fragment coordinates
 */
void hist_savecoords(ui_win_t *win)
{
    frag_t *f = &frags[chars[win->v][win->chr].frags[win->frag]];

    hist_add(win, HIST_MOVEPT);

    win->hist[win->histmin].frag = chars[win->v][win->chr].frags[win->frag];
    win->hist[win->histmin].idx = win->hist[win->histmin].data.contlist.newidx = f->len;

    win->hist[win->histmin].data.contlist.oldc = malloc(f->len*sizeof(cont_t));
    if(!win->hist[win->histmin].data.contlist.oldc) error("hist_savecoords", ERR_MEM);
    memcpy(win->hist[win->histmin].data.contlist.oldc, f->data, f->len*sizeof(cont_t));

    win->hist[win->histmin].data.contlist.newc = malloc(f->len*sizeof(cont_t));
    if(!win->hist[win->histmin].data.contlist.newc) error("hist_savecoords", ERR_MEM);
    memcpy(win->hist[win->histmin].data.contlist.newc, f->data, f->len*sizeof(cont_t));
}

/**
 * Set coordinate on contour
 */
void hist_setcoord(ui_win_t *win, int px, int py)
{
    int i, j, s = 12 - font->quality;
    frag_t *f;
    cont_t *c1 = &frags[chars[win->v][win->chr].frags[win->frag]].data[win->cont];
    cont_t *c2 = &win->hist[win->histmin].data.contlist.newc[win->cont];

    if(!win->histmax || win->hist[win->histmin].type != HIST_MOVEPT ||
        win->hist[win->histmin].frag != chars[win->v][win->chr].frags[win->frag])
            return;

    if(win->pt) {
        if(px < minx >> s) px = minx >> s;
        if(py < miny >> s) py = miny >> s;
        if(px > maxx >> s) px = maxx >> s;
        if(py > maxy >> s) py = maxy >> s;
    }
    switch(win->pt) {
        case 0: c1->px = c2->px = px; c1->py = c2->py = py; break;
        case 1: c1->c1x = c2->c1x =px; c1->c1y = c2->c1y = py; break;
        case 2: c1->c2x = c2->c2x =px; c1->c2y = c2->c2y = py; break;
    }
    if(!win->pt) {
        f = &frags[chars[win->v][win->chr].frags[win->frag]];
        c1 = win->hist[win->histmin].data.contlist.oldc;
        c2 = win->hist[win->histmin].data.contlist.newc;
        for(i=0; i < f->len; i++) {
            if(f->data[i].type == SSFN_CONTOUR_QUAD || f->data[i].type == SSFN_CONTOUR_CUBIC) {
                f->data[i].c1x = c1[i].c1x; f->data[i].c1y = c1[i].c1y;
                if(f->data[i].c1x < minx >> s) f->data[i].c1x = c2[i].c1x = minx >> s;
                if(f->data[i].c1y < miny >> s) f->data[i].c1y = c2[i].c1y = miny >> s;
                if(f->data[i].c1x > maxx >> s) f->data[i].c1x = c2[i].c1x = maxx >> s;
                if(f->data[i].c1y > maxy >> s) f->data[i].c1y = c2[i].c1y = maxy >> s;
            }
            if(f->data[i].type == SSFN_CONTOUR_CUBIC) {
                f->data[i].c2x = c1[i].c2x; f->data[i].c2y = c1[i].c2y;
                if(f->data[i].c2x < minx >> s) f->data[i].c2x = c2[i].c2x = minx >> s;
                if(f->data[i].c2y < miny >> s) f->data[i].c2y = c2[i].c2y = miny >> s;
                if(f->data[i].c2x > maxx >> s) f->data[i].c2x = c2[i].c2x = maxx >> s;
                if(f->data[i].c2y > maxy >> s) f->data[i].c2y = c2[i].c2y = maxy >> s;
            }
        }
    }
    movex = movey = -1;
    for(j=0; j < chars[win->v][win->chr].len; j++)
        for(i=0; i < frags[chars[win->v][win->chr].frags[j]].len; i++) {
            if(j == win->frag && i == win->cont) continue;
            f = &frags[chars[win->v][win->chr].frags[j]];
            if(f->data[i].px == px) movex = px;
            if(f->data[i].py == py) movey = py;
            if(f->data[i].type == SSFN_CONTOUR_QUAD || f->data[i].type == SSFN_CONTOUR_CUBIC) {
                if(f->data[i].c1x == px) movex = px;
                if(f->data[i].c1y == py) movey = py;
            }
            if(f->data[i].type == SSFN_CONTOUR_CUBIC) {
                if(f->data[i].c2x == px) movex = px;
                if(f->data[i].c2y == py) movey = py;
            }
            if(movex != -1 && movey != -1) break;
        }
    modified = 1;
}

/**
 * Set advance values
 */
void hist_setadv(ui_win_t *win, int x, int y, int conv)
{
    int i, j;
    char_t *chr = &chars[win->v][win->chr];

    if(!conv) {
        j = file_findchar(0, chr->unicode);
        if(j != -1 && chars[0][j].rtl) {
            if(x && chr->w && x < chr->w) x = chr->w - x; else x = 0;
        } else {
            if(x && chr->bear_l != 65536) x -= chr->bear_l;
        }
        if(y && chr->bear_t != 65536) y -= chr->bear_t;
    }
    if(x < 0) x = 0;
    if(y < 0) y = 0;
    if(win->histmin && win->hist[win->histmin-1].type == HIST_SETADV &&
        win->hist[win->histmin-1].data.adv.newx - x >= -1 && win->hist[win->histmin-1].data.adv.newx - x <= 1 &&
        win->hist[win->histmin-1].data.adv.newy - y >= -1 && win->hist[win->histmin-1].data.adv.newy - y <= 1)
            i = win->histmin-1;
    else {
        i = hist_add(win, HIST_SETADV);
        win->histmin++;
        win->hist[i].data.adv.oldx = chr->adv_x;
        win->hist[i].data.adv.oldy = chr->adv_y;
    }
    win->hist[i].data.adv.newx = chr->adv_x = x;
    win->hist[i].data.adv.newy = chr->adv_y = y;
    modified = 1;
}

/**
 * Set baseline value
 */
void hist_setbline(ui_win_t *win, int y)
{
    int i;

    if(win->histmin && win->hist[win->histmin-1].type == HIST_SETBLINE &&
        win->hist[win->histmin-1].data.adv.newy - y >= -1 && win->hist[win->histmin-1].data.adv.newy - y <= 1)
            i = win->histmin-1;
    else {
        i = hist_add(win, HIST_SETBLINE);
        win->histmin++;
        win->hist[i].data.adv.oldy = font->baseline;
    }
    win->hist[i].data.adv.newy = font->baseline = y;
    if(font->underline < font->baseline) font->underline = font->baseline;
    modified = 1;
}

/**
 * Set underline value
 */
void hist_setuline(ui_win_t *win, int y)
{
    int i;

    if(win->histmin && win->hist[win->histmin-1].type == HIST_SETULINE &&
        win->hist[win->histmin-1].data.adv.newy - y >= -1 && win->hist[win->histmin-1].data.adv.newy - y <= 1)
            i = win->histmin-1;
    else {
        i = hist_add(win, HIST_SETULINE);
        win->histmin++;
        win->hist[i].data.adv.oldy = font->underline;
    }
    win->hist[i].data.adv.newy = font->underline = y;
    if(font->underline < font->baseline) font->underline = font->baseline;
    modified = 1;
}

/**
 * Save fragment coordinates before paste
 */
void hist_savepaste(ui_win_t *win)
{
    frag_t *f = &frags[chars[win->v][win->chr].frags[win->frag]];

    hist_add(win, HIST_PASTE);

    win->hist[win->histmin].idx = f->len;
    win->hist[win->histmin].data.contlist.oldc = malloc(f->len*sizeof(cont_t));
    if(!win->hist[win->histmin].data.contlist.oldc) error("hist_savepaste", ERR_MEM);
    memcpy(win->hist[win->histmin].data.contlist.oldc, f->data, f->len*sizeof(cont_t));
}

/**
 * Save fragment coordinates after paste
 */
void hist_addpaste(ui_win_t *win)
{
    frag_t *f = &frags[chars[win->v][win->chr].frags[win->frag]];

    win->hist[win->histmin].data.contlist.newidx = f->len;
    win->hist[win->histmin].data.contlist.newc = malloc(f->len*sizeof(cont_t));
    if(!win->hist[win->histmin].data.contlist.newc) error("hist_addpaste", ERR_MEM);
    memcpy(win->hist[win->histmin].data.contlist.newc, f->data, f->len*sizeof(cont_t));
    win->histmin++;
}

/**
 * Save hinting coordinates
 */
void hist_savehint(ui_win_t *win)
{
    int i, j, k;
    frag_t *f;
    char_t *chr = &chars[win->v][win->chr];

    hist_add(win, HIST_HINT);

    win->hist[win->histmin].idx = 0;
    for(i=k=0; i < chr->len; i++) {
        f = &frags[chars[win->v][win->chr].frags[i]];
        if(f->type == SSFN_FRAG_CONTOUR)
            win->hist[win->histmin].idx += f->len;
    }
    win->hist[win->histmin].data.hintlist.oldx = malloc(win->hist[win->histmin].idx*sizeof(int));
    if(!win->hist[win->histmin].data.hintlist.oldx) error("hist_savehint", ERR_MEM);
    win->hist[win->histmin].data.hintlist.newx = malloc(win->hist[win->histmin].idx*sizeof(int));
    if(!win->hist[win->histmin].data.hintlist.newx) error("hist_savehint", ERR_MEM);

    for(i=k=0; i < chr->len; i++) {
        f = &frags[chars[win->v][win->chr].frags[i]];
        if(f->type == SSFN_FRAG_CONTOUR)
            for(j=0; j < f->len; j++,k++)
                win->hist[win->histmin].data.hintlist.oldx[k] = win->hist[win->histmin].data.hintlist.newx[k] = f->data[j].px;
    }
}

/**
 * Move hinting grid
 */
void hist_sethint(ui_win_t *win, int ox, int nx)
{
    int i, j, k;
    frag_t *f;
    char_t *chr = &chars[win->v][win->chr];

    for(i=k=0; i < chr->len; i++) {
        f = &frags[chars[win->v][win->chr].frags[i]];
        if(f->type == SSFN_FRAG_CONTOUR)
            for(j=0; j < f->len; j++,k++)
                if(f->data[j].px == ox) {
                    f->data[j].px = win->hist[win->histmin].data.hintlist.newx[k] = nx;
                    modified = 1;
                }
    }
}

/**
 * Change kerning
 */
void hist_setkern(ui_win_t *win, int chr, int unicode, int dx, int dy)
{
    int i, j;

    if(chr < 0 || chr > numchars[0] || unicode < 33 || unicode > UNICODE_LAST || (!dx && !dy)) return;

    for(i=-1,j=0;j<chars[0][chr].klen && chars[0][chr].kern[j*3]<=unicode;j++)
        if(chars[0][chr].kern[j*3]==unicode) { i = j; break; }
    if(i == -1) {
        i = chars[0][chr].klen++;
        chars[0][chr].kern = (int*)realloc(chars[0][chr].kern, chars[0][chr].klen*3*sizeof(int));
        if(!chars[0][chr].kern) error("hist_setkern", ERR_MEM);
        for(;i>j;i--) {
            chars[0][chr].kern[i*3] = chars[0][chr].kern[i*3-3];
            chars[0][chr].kern[i*3+1] = chars[0][chr].kern[i*3-2];
            chars[0][chr].kern[i*3+2] = chars[0][chr].kern[i*3-1];
        }
        chars[0][chr].kern[i*3] = unicode;
        chars[0][chr].kern[i*3+1] = chars[0][chr].kern[i*3+2] = 0;
    }
    if(win->histmin && win->hist[win->histmin-1].type == HIST_KERN && win->hist[win->histmin-1].frag == chr &&
        win->hist[win->histmin-1].idx == unicode)
            j = win->histmin-1;
    else {
        j = hist_add(win, HIST_KERN);
        win->histmin++;
        win->hist[j].frag = chr;
        win->hist[j].idx = unicode;
        win->hist[j].data.adv.oldx = chars[0][chr].kern[i*3+1];
        win->hist[j].data.adv.oldy = chars[0][chr].kern[i*3+2];
    }
    chars[0][chr].kern[i*3+1] += dx;
    chars[0][chr].kern[i*3+2] += dy;
    win->hist[j].data.adv.newx = chars[0][chr].kern[i*3+1];
    win->hist[j].data.adv.newy = chars[0][chr].kern[i*3+2];
    modified = 1;
}

