/*
 * sfnedit/color.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Color picker (View and Controller)
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include "ui.h"
#include "file.h"

void view_layers(ui_win_t *win);
void view_coords(ui_win_t *win);
int selcolor = 0, hue = 0, sat = 0, val = 0, incolorsel = 0;

/**
 * Color conversion RGB to HSV
 */
void rgb2hsv(uint32_t c)
{
    int r = (int)(((uint8_t*)&c)[2]), g = (int)(((uint8_t*)&c)[1]), b = (int)(((uint8_t*)&c)[0]), m, d;

    m = r < g? r : g; if(b < m) m = b;
    val = r > g? r : g; if(b > val) val = b;
    d = val - m;
    if(!val) { sat = 0; return; }
    sat = d * 255 / val;
    if(!sat) { return; }

    if(r == val) hue = 43*(g - b) / d;
    else if(g == val) hue = 85 + 43*(b - r) / d;
    else hue = 171 + 43*(r - g) / d;
    if(hue < 0) hue += 256;
}

/**
 * Color conversion HSV to RGB
 */
uint32_t hsv2rgb(int a, int h, int s, int v)
{
    int i, f, p, q, t;
    uint32_t c = (a & 255) << 24;

    if(!s) { ((uint8_t*)&c)[2] = ((uint8_t*)&c)[1] = ((uint8_t*)&c)[0] = v; }
    else {
        if(h >= 255) i = 0; else i = h / 43;
        f = (h - i * 43) * 6;
        p = (v * (255 - s) + 127) >> 8;
        q = (v * (255 - ((s * f + 127) >> 8)) + 127) >> 8;
        t = (v * (255 - ((s * (255 - f) + 127) >> 8)) + 127) >> 8;
        switch(i) {
            case 0:  ((uint8_t*)&c)[2] = v; ((uint8_t*)&c)[1] = t; ((uint8_t*)&c)[0] = p; break;
            case 1:  ((uint8_t*)&c)[2] = q; ((uint8_t*)&c)[1] = v; ((uint8_t*)&c)[0] = p; break;
            case 2:  ((uint8_t*)&c)[2] = p; ((uint8_t*)&c)[1] = v; ((uint8_t*)&c)[0] = t; break;
            case 3:  ((uint8_t*)&c)[2] = p; ((uint8_t*)&c)[1] = q; ((uint8_t*)&c)[0] = v; break;
            case 4:  ((uint8_t*)&c)[2] = t; ((uint8_t*)&c)[1] = p; ((uint8_t*)&c)[0] = v; break;
            default: ((uint8_t*)&c)[2] = v; ((uint8_t*)&c)[1] = p; ((uint8_t*)&c)[0] = q; break;
        }
    }
    return c;
}

/**
 * View for color picker
 */
void view_color(ui_win_t *win, uint8_t sc)
{
    char tmp[4];
    int i, j, p, w = (win->w - 52) / 30;
    uint32_t h, c = cmap[sc > 240 ? 240 : sc];
    uint8_t *d, *a = (uint8_t*)&theme[THEME_INACT], *b = (uint8_t*)&theme[THEME_INPBG], *C = (uint8_t*)&c;

    if(w < 5) w = 5;
    for(j = 0; j < 8; j++)
        for(i = 0; i < 30; i++) {
            ui_box(win, 52 + i*w, j*w + 26, w, w, theme[selcolor == j*30+i ? THEME_INPUT : THEME_INPBG]);
            ui_argb(win, 52 + i*w+1, j*w + 26+1, w-2, w-2, cmap[j*30+i]);
        }
    ui_box(win, 52, 8*w + 26, w, w, theme[selcolor == 240 ? THEME_INPUT : THEME_INPBG]);
    ui_argb(win, 53, 8*w + 27, w-2, w-2, 0);

    ssfn_x = 52; ssfn_y = (9*w + 26) + 8;
    if(selcolor >= 240) {
        ui_box(win, ssfn_x, ssfn_y - 8, 256+32+17+17+40, 260, ssfn_bg);
    } else {
        ssfn_fg = theme[THEME_FG];
        ui_text(win, "A ", ssfn_bg, ssfn_bg);
        sprintf(tmp, "%02X", C[3]);
        ui_scale(win, tmp, 16, 0);
        ssfn_x = 52; ssfn_y += 24;
        ssfn_fg = theme[THEME_FG];
        ui_text(win, "R ", ssfn_bg, ssfn_bg);
        sprintf(tmp, "%02X", C[2]);
        ui_scale(win, tmp, 16, 0);
        ssfn_x = 52; ssfn_y += 24;
        ssfn_fg = theme[THEME_FG];
        ui_text(win, "G ", ssfn_bg, ssfn_bg);
        sprintf(tmp, "%02X", C[1]);
        ui_scale(win, tmp, 16, 0);
        ssfn_x = 52; ssfn_y += 24;
        ssfn_fg = theme[THEME_FG];
        ui_text(win, "B ", ssfn_bg, ssfn_bg);
        sprintf(tmp, "%02X", C[0]);
        ui_scale(win, tmp, 16, 0);
        ssfn_y = (9*w + 26) + 2; ssfn_x += 8;
        ui_box(win, ssfn_x - 4, ssfn_y - 2, 4, 260, ssfn_bg);
        ui_box(win, ssfn_x +17+256+17, ssfn_y - 2, 4, 260, ssfn_bg);
        for(p = ssfn_y * win->p + ssfn_x, j=0;j<256 && (int)ssfn_y + j < win->h;j++, p += win->p) {
            if(C[3] == 255-j && (int)ssfn_x < win->w && (int)ssfn_y + j + 2< win->h ) {
                win->data[p - 4] = theme[THEME_INPUT];
                win->data[p - 3] = theme[THEME_INPUT];
                win->data[p - 2] = theme[THEME_INPUT];
                win->data[p - 3 - win->p] = theme[THEME_INPUT];
                win->data[p - 3 + win->p] = theme[THEME_INPUT];
                win->data[p - 4 - 2*win->p] = theme[THEME_INPUT];
                win->data[p - 4 - win->p] = theme[THEME_INPUT];
                win->data[p - 4 + win->p] = theme[THEME_INPUT];
                win->data[p - 4 + 2*win->p] = theme[THEME_INPUT];
                for(i=0;i<15 && (int)ssfn_x + i < win->w;i++)
                    win->data[p+i] = theme[THEME_INPUT];
            } else
                for(i=0;i<15 && (int)ssfn_x + i < win->w;i++) {
                    d = (j & 8) ^ (i & 8) ? a : b;
                    ((uint8_t*)&win->data[p+i])[0] = (d[0]*j + (256 - j)*C[0])>>8;
                    ((uint8_t*)&win->data[p+i])[1] = (d[1]*j + (256 - j)*C[1])>>8;
                    ((uint8_t*)&win->data[p+i])[2] = (d[2]*j + (256 - j)*C[2])>>8;
                }
        }
        rgb2hsv(c);
        for(p = ssfn_y * win->p + ssfn_x + 16, j=0;j<256 && (int)ssfn_y + j < win->h;j++, p += win->p)
            for(i=0;i<256 && (int)ssfn_x + 16 + i < win->w;i++)
                win->data[p + i] = sat == i || 255-j == val ? theme[i<96 && j<96 ?THEME_INPBG:THEME_INPUT] :
                    hsv2rgb(255, hue,i,255-j);
        for(p = ssfn_y * win->p + ssfn_x + 17+256, j=0;j<255 && (int)ssfn_y + j < win->h;j++, p += win->p) {
            h = hsv2rgb(255, j, 255, 255);
            if(hue == j && (int)ssfn_x + 19 < win->w && (int)ssfn_y + j + 2< win->h ) {
                h = theme[THEME_INPUT];
                win->data[p + 19] = h;
                win->data[p + 18] = h;
                win->data[p + 17] = h;
                win->data[p + 18 - win->p] = h;
                win->data[p + 18 + win->p] = h;
                win->data[p + 19 - 2*win->p] = h;
                win->data[p + 19 - win->p] = h;
                win->data[p + 19 + win->p] = h;
                win->data[p + 19 + 2*win->p] = h;
            }
            for(i=0;i<15 && (int)ssfn_x + 17+256 + i < win->w;i++)
                win->data[p + i] = h;
        }
    }
}

/**
 * Controller for color picker
 */
int ctrl_color(ui_win_t *win, ui_event_t *evt, uint8_t *c)
{
    int w = (win->w - 52) / 30;
    uint8_t *a;

    switch(evt->type) {
        case E_MOUSEMOVE:
            if(incolorsel && selcolor < 240 && evt->y >= 30 + w*9 && evt->y < 30 + 256 + w*9) {
                a = (uint8_t*)&cmap[selcolor];
                switch(incolorsel) {
                    case 1: a[3] = 255 - (evt->y - w*9 - 30); break;
                    case 2:
                        if(evt->x >= 136 && evt->x < 392) {
                            if(!cmap[selcolor]) a[3] = 255;
                            sat = evt->x - 136;
                            val = 255 - (evt->y - w*9 - 30);
                            cmap[selcolor] = hsv2rgb(a[3], hue, sat, val);
                        }
                    break;
                    case 3:
                        if(!cmap[selcolor]) a[3] = 255;
                        hue = evt->y - w*9 - 30;
                        if(hue >= 255) hue = 0;
                        cmap[selcolor] = hsv2rgb(a[3], hue, sat, val);
                    break;
                }
                *c = selcolor;
                view_color(win, selcolor);
                view_coords(win);
                ui_flushwin(win, 0, 0, win->w, win->h);
            }
        break;
        case E_BTNPRESS:
            if(evt->x > 52 && evt->y > 26) {
                if(evt->y < 26 + w * 9) {
                    selcolor = (evt->y - 26) / w * 30 + (evt->x - 52) / w;
                    if(selcolor > 239) { *c = selcolor = 240; }
                    else *c = selcolor;
                }
                if(selcolor < 240 && evt->y >= 30 + w*9 && evt->y < 30 + 256 + w*9) {
                    a = (uint8_t*)&cmap[selcolor];
                    if(evt->x > 69 && evt->x < 88) {
                        if(evt->y >= 36 + w*9 && evt->y < 36 + 18 + w*9 && a[3] > 0) a[3]--;
                        if(evt->y >= 60 + w*9 && evt->y < 60 + 18 + w*9 && a[2] > 0) a[2]--;
                        if(evt->y >= 84 + w*9 && evt->y < 84 + 18 + w*9 && a[1] > 0) a[1]--;
                        if(evt->y >= 108 + w*9 && evt->y < 108 + 18 + w*9 && a[0] > 0) a[0]--;
                    } else
                    if(evt->x > 88 && evt->x < 112) {
                        if(evt->y >= 36 + w*9 && evt->y < 36 + 18 + w*9 && a[3] < 255) a[3]++;
                        if(evt->y >= 60 + w*9 && evt->y < 60 + 18 + w*9 && a[2] < 255) a[2]++;
                        if(evt->y >= 84 + w*9 && evt->y < 84 + 18 + w*9 && a[1] < 255) a[1]++;
                        if(evt->y >= 108 + w*9 && evt->y < 108 + 18 + w*9 && a[0] < 255) a[0]++;
                    } else {
                        if(evt->x > 117 && evt->x < 136) {
                            a[3] = 255 - (evt->y - w*9 - 30);
                            incolorsel = 1;
                        } else
                        if(evt->x >= 136 && evt->x < 392) {
                            if(!cmap[selcolor]) a[3] = 255;
                            sat = evt->x - 136;
                            val = 255 - (evt->y - w*9 - 30);
                            cmap[selcolor] = hsv2rgb(a[3], hue, sat, val);
                            incolorsel = 2;
                        } else
                        if(evt->x >= 392 && evt->x < 412) {
                            if(!cmap[selcolor]) a[3] = 255;
                            hue = evt->y - w*9 - 30;
                            if(hue >= 255) hue = 0;
                            cmap[selcolor] = hsv2rgb(a[3], hue, sat, val);
                            incolorsel = 3;
                        }
                    }
                    *c = selcolor;
                }
                view_color(win, selcolor);
                view_layers(win);
                view_coords(win);
                ui_flushwin(win, 0, 0, win->w, win->h);
            }
        break;
        case E_BTNRELEASE:
            incolorsel = 0;
        break;
    }
    return 0;
}
