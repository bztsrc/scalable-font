/*
 * sfnedit/ranges.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Character ranges tab (View and Controller)
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ui.h"
#include "lang.h"
#include "util.h"
#include "unicode.h"

/* start range pun intended :-) */
int rangeactive = 1, currange = 0, numrange = 0, strange = 0, morerange = 0, rsel[128], srcchr = 0, rangereent = 0;
int emptytoo = 0;
char search[256], rtmp[24];
extern int glyphra;

/**
 * View for ranges
 */
void view_ranges(ui_win_t *win)
{
    int i, j, k, w;

    rangereent++;
redraw:
    ssfn_x = 2; ssfn_y = 68;
    ui_button(win, lang[COVERAGE], 128, 0, rangeactive==1);
    ssfn_x += 2;
    i = (win->w-8-ssfn_x);
    ui_button(win, lang[RANGENAME], i, 0, rangeactive==1);
    ssfn_y += 2;
    ssfn_bg = theme[THEME_BG];
    if(!rangeactive) strange = currange = 0;
    srcchr = gethex(search, 6);
    for(i=0;i<(int)strlen(search);i++)
        if(!((search[i]>='0'&&search[i]<='9') ||
            (search[i]>='a'&&search[i]<='f') ||
            (search[i]>='A'&&search[i]<='F'))) { srcchr = 0; break; }
    for(i = strange, morerange = j = 0; i < UNICODE_NUMBLOCKS && j < (int)(sizeof(rsel)/sizeof(rsel[0])); i++) {
        if((!emptytoo && !ublocks[i].cnt) || (search[0] && search_name(search, ublocks[i].name, 1) &&
            (!srcchr || srcchr < ublocks[i].start || srcchr > ublocks[i].end))) continue;
        ssfn_fg = theme[rangeactive==1 && j==currange ? THEME_INPUT : THEME_FG];
        ssfn_x = 2; ssfn_y += 18;
        if((int)ssfn_y >= win->h - 24) { morerange = 1; break; }
        rsel[j] = i;
        k = ublocks[i].cnt * 1000 / (ublocks[i].end - ublocks[i].start + 1 - ublocks[i].undef);
        sprintf(rtmp, "%3d.%d%%",k/10,k%10);
        ui_text(win, rtmp, ssfn_bg, ssfn_bg);
        ssfn_x += 4;
        ui_progress(win,k,1000,64,12,rangeactive==1 && j==currange);
        ssfn_x = 138;
        sprintf(rtmp, "U+%06X U+%06X ", ublocks[i].start, ublocks[i].end);
        ui_text(win, rtmp, ssfn_bg, ssfn_bg);
        ui_text(win, ublocks[i].name, ssfn_bg, ssfn_bg);
        j++;
    }
    numrange = j ? j-1 : 0;
    if(currange && currange > numrange) { currange = numrange; goto redraw; }

    ssfn_x = 2; ssfn_y = 44;
    ssfn_fg = theme[THEME_TABU];
    ui_bool(win, lang[EMPTYTOO], emptytoo, 0);
    ssfn_fg = theme[THEME_FG];

    ssfn_x = (win->w >> 1)+2;
    w = ((win->w >> 1)-16-8);
    ui_input(win, search, w, 1, 0);
    ui_button(win, "⚲", 8, 2, rangeactive==0);
    ssfn_x = (win->w >> 1) + 2 + (win->w & 1); ssfn_y = 44;
    if(ui_input(win, search, w, 5, rangeactive==0 && rangereent==1)) {
        rangeactive = 1;
        ui_resizewin(win, win->w, win->h);
        ui_tabs(win, ui_getwin(win->winid));
        goto redraw;
    }
    rangereent--;
}

/**
 * Controller for ranges
 */
int ctrl_ranges(ui_win_t *win __attribute__((unused)), ui_event_t *evt)
{
    switch(evt->type) {
        case E_KEY:
            switch(evt->x) {
                case K_UP:
                    if(rangeactive==1) {
                        if(currange > 0) currange--; else
                        if(strange >0) { strange--; ui_resizewin(win, win->w, win->h); }
                    }
                    return 1;
                case K_DOWN:
                    if(rangeactive==1) {
                        if(currange < numrange) currange++; else
                        if(morerange) { strange++; ui_resizewin(win, win->w, win->h); }
                    }
                    return 1;
                case K_ENTER:
sel:                if(rangeactive==1 && currange <= numrange) {
                        glyphra = rsel[currange];
                        gsearch[0] = 0;
                        win->tab = MAIN_TAB_GLYPHS;
                        ui_resizewin(win, win->w, win->h);
                    }
                    return 1;
                default:
                    if(!search[0] && event.x == ' ') { emptytoo ^= 1; ui_resizewin(win, win->w, win->h); }
                    else if(!(event.h & ~1)) {
                        search[0] = 0;
                        if(event.x >= 32) memcpy(search, &event.x, strlen((char*)&event.x)+1);
                        rangeactive = 0;
                        ui_resizewin(win, win->w, win->h);
                    }
            }
            return 1;
        case E_BTNPRESS:
            if(evt->y >= 44 && evt->y < 44+24) {
                if(evt->x < win->w >> 1) { emptytoo ^= 1; ui_resizewin(win, win->w, win->h); }
                else rangeactive = 0;
            } else {
                rangeactive = 1;
                if(evt->w & 8) {
                    if(strange >0) { strange--; ui_resizewin(win, win->w, win->h); }
                } else
                if(evt->w & 16) {
                    if(morerange) { strange++; ui_resizewin(win, win->w, win->h); }
                } else
                if(evt->w & 1) {
                    currange = (evt->y - 68 - 24) / 18;
                    goto sel;
                }
            }
            return 1;
        case E_BTNRELEASE:
            return 1;
    }
    return 0;
}
