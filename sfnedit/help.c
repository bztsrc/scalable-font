/*
 * sfnedit/help.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Help tab (View)
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include "ui.h"
#include "lang.h"

void help_text(ui_win_t *win, char *str)
{
    int u;

    ssfn_x = 2; ssfn_y += 16;
    while(*str) {
        u = ssfn_utf8(&str);
        if(u == '\n') { ssfn_x = 2; ssfn_y += 16; }
        else {
            if((int)ssfn_x >= win->w || ssfn_putc(u))
                while(*str != '\n' && *str) str++;
        }
    }
}

/**
 * View for help
 */
void view_help(ui_win_t *win)
{
    int t;

    ssfn_x = ssfn_y = 2;
    ssfn_fg = theme[THEME_INPUT];
    ssfn_bg = theme[THEME_BG];
    ui_text(win, lang[HELP], ssfn_bg, ssfn_bg);
    ui_text(win, " - ", ssfn_bg, ssfn_bg);
    t = win->tab + (ui_getwin(win->winid) ? GMENU_EDIT - MMENU_PROPS : 0);
    ui_text(win, lang[MMENU_PROPS + t], ssfn_bg, ssfn_bg);
    ssfn_x = 2; ssfn_y += 20;
    ssfn_fg = theme[THEME_FG];
    help_text(win, lang[ui_getwin(win->winid) ? HELP_GLYPHWIN : HELP_MAINWIN]);
    help_text(win, lang[HELP_PROPS + t]);
    if(t == 3)
        help_text(win, lang[win->menu < 2 ? HELP_ADV : win->menu == 2 ? HELP_HINT : HELP_MODIFY]);
}
