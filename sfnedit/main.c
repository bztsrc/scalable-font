/*
 * sfnedit/main.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Scalable Screen Font Editor main function
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include "lang.h"
#include "util.h"
#include "ui.h"
#include "file.h"
#include "render.h"

/*
 * wrapper for Windows SDL support. This partialy came from SDL_windows_main.c
 */
#ifdef __WIN32__
#include <SDL.h>
#include <windows.h>

static void UnEscapeQuotes(char *arg)
{
    char *last = NULL;

    while (*arg) {
        if (*arg == '"' && (last != NULL && *last == '\\')) {
            char *c_curr = arg;
            char *c_last = last;

            while (*c_curr) {
                *c_last = *c_curr;
                c_last = c_curr;
                c_curr++;
            }
            *c_last = '\0';
        }
        last = arg;
        arg++;
    }
}

/* Parse a command line buffer into arguments */
static int ParseCommandLine(char *cmdline, char **argv)
{
    char *bufp;
    char *lastp = NULL;
    int argc, last_argc;

    argc = last_argc = 0;
    for (bufp = cmdline; *bufp;) {
        /* Skip leading whitespace */
        while (SDL_isspace(*bufp)) {
            ++bufp;
        }
        /* Skip over argument */
        if (*bufp == '"') {
            ++bufp;
            if (*bufp) {
                if (argv) {
                    argv[argc] = bufp;
                }
                ++argc;
            }
            /* Skip over word */
            lastp = bufp;
            while (*bufp && (*bufp != '"' || *lastp == '\\')) {
                lastp = bufp;
                ++bufp;
            }
        } else {
            if (*bufp) {
                if (argv) {
                    argv[argc] = bufp;
                }
                ++argc;
            }
            /* Skip over word */
            while (*bufp && !SDL_isspace(*bufp)) {
                ++bufp;
            }
        }
        if (*bufp) {
            if (argv) {
                *bufp = '\0';
            }
            ++bufp;
        }

        /* Strip out \ from \" sequences */
        if (argv && last_argc != argc) {
            UnEscapeQuotes(argv[last_argc]);
        }
        last_argc = argc;
    }
    if (argv) {
        argv[argc] = NULL;
    }
    return (argc);
}

int APIENTRY WinMain(__attribute__((unused)) HINSTANCE hInstance, __attribute__((unused)) HINSTANCE hPrevInstance,
    __attribute__((unused)) LPSTR lpCmdLine, __attribute__((unused)) int nCmdShow)
{
    OPENFILENAME  ofn;
    char *cmdline = GetCommandLine();
    int ret, argc = ParseCommandLine(cmdline, NULL);
    char **argv = SDL_stack_alloc(char*, argc+2);
    char fn[1024];
    ParseCommandLine(cmdline, argv);
    if(!argv[1]) {
        memset(&fn,0,sizeof(fn));
        memset(&ofn,0,sizeof(ofn));
        ofn.lStructSize     = sizeof(ofn);
        ofn.hwndOwner       = NULL;
        ofn.hInstance       = hInstance;
        ofn.lpstrFilter     = "Font Files (*.sfn, *.asc)\0*.sfn;*.sfn.gz;*.asc;*.asc.gz\0All Files\0*\0\0";
        ofn.lpstrFile       = fn;
        ofn.nMaxFile        = sizeof(fn)-1;
        ofn.lpstrTitle      = "Please Enter a File Name";
        if (GetOpenFileName(&ofn)) {
            argc++;
            argv[1] = fn;
        }
    }
    SDL_SetMainReady();
    ret = main(argc, argv);
    SDL_stack_free(argv);
    exit(ret);
    return ret;
}
#endif

/**
 * Main sfnedit function
 */
int main(int argc, char **argv)
{
    lang_init();

    if(argc < 2) {
        printf("Scalable Screen Font Editor by bzt Copyright (C) 2019 MIT license\n\n%s <.sfn|.asc"
#if HAS_ZLIB
            "|.sfn.gz|.asc.gz"
#endif
            ">\n\n", argv[0]);
        exit(1);
    }

#ifndef __WIN32__
    signal(SIGQUIT, ui_forcequit);
    signal(SIGINT, ui_forcequit);
#endif
    memset(nchr, 0, 256);
    memset(search, 0, 256);
    memset(gsearch, 0, 256);
    memset(ksearch, 0, 256);
    memset(gtmp, ' ', 64);
    memset(nr, 0, 256*sizeof(int));
    memset(raster, 0, 256*sizeof(uint16_t*));

    ui_init();
    ui_main(argv[1]);
    ui_fini();

    render_free();
    file_free();

    return 0;
}
