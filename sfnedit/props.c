/*
 * sfnedit/props.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Font properties tab (View and Controller)
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include "ui.h"
#include "lang.h"
#include "file.h"
#include "util.h"

unsigned int nmaxx;
int active = 0, savebtn = 0;
char nchr[256];
char *copyright[] = {
    "Copyright (C) 2019 bzt (bztsrc@gitlab)", "",
    "Permission is hereby granted, free of charge, to any person",
    "obtaining a copy of this software and associated documentation",
    "files (the \"Software\"), to deal in the Software without",
    "restriction, including without limitation the rights to use, copy,",
    "modify, merge, publish, distribute, sublicense, and/or sell copies",
    "of the Software, and to permit persons to whom the Software is",
    "furnished to do so, subject to the following conditions:","",
    "The above copyright notice and this permission notice shall be",
    "included in all copies or substantial portions of the Software.",
    NULL
};

/**
 * View for properties
 */
void view_props(ui_win_t *win)
{
    unsigned int i, sy;
    int j, na;
    char num[16], *s;

    nmaxx = 0;
    for(i=PROP_FAMILY; i<=PROP_LICENSE; i++) {
        ssfn_x = 2; ssfn_y += 4;
        ui_text(win, lang[i], ssfn_bg, ssfn_bg);
        if(ssfn_x > nmaxx) nmaxx = ssfn_x;
        ssfn_y += 20;
    }
    nmaxx += 16;

    ssfn_x = nmaxx; ssfn_y = 44;
    ui_scale(win, lang[FAM_SERIF + font->family], 128, !active);

    ssfn_x = nmaxx; ssfn_y += 24;
    ui_bool(win, lang[STYL_BOLD], font->style & SSFN_STYLE_BOLD, active==1);

    ssfn_x = nmaxx + 128;
    ui_bool(win, lang[STYL_ITALIC], font->style & SSFN_STYLE_ITALIC, active==2);

    ssfn_x = nmaxx; ssfn_y += 24;
    sprintf(num, "%4d x %4d", 16<<font->quality, 16<<font->quality);
    ui_scale(win, num, 88, active==3);

    ssfn_x = nmaxx; ssfn_y += 24;
    sprintf(num, "%4d", font->baseline);
    ui_scale(win, num, 32, active==4);
    ssfn_x += 8;
    ui_button(win, lang[RECALIBRATE], 128, 3, active==5);

    sy = ssfn_y;
    na = active;
    do {
        ssfn_y = sy;
        active = na;
        ssfn_x = nmaxx; ssfn_y += 24;
        sprintf(num, "%4d", font->underline);
        ui_scale(win, num, 32, active==6);

        for(i=0; i<6; i++) {
            ssfn_x = nmaxx; ssfn_y += 24;
            ui_input(win, strtable[i], win->w-nmaxx-8, 3, 0);
        }
        ssfn_x = 2; ssfn_y += 48;
        ssfn_bg = theme[THEME_BG];
        ui_text(win, "U+", ssfn_bg, ssfn_bg);
        ui_input(win, nchr, 48, 1, 0);
        ui_button(win, lang[PROP_NEWCHAR], 0, 2, active == 14);
        ssfn_x += 16; savebtn = ssfn_x;
        i = (win->w - savebtn - 40) >> 1;
        ui_button(win, lang[PROP_SAVEASC], i, 3, active == 15 ? 2 : 3);
        ssfn_x += 16;
        ui_button(win, lang[PROP_SAVESFN], i, 3, active == 16 ? 2 : 3);

        ssfn_x = 2; ssfn_y += 32;
        ssfn_fg = theme[THEME_INACT];
        ssfn_bg = theme[THEME_BG];
        for(i = 0; copyright[i] && (int)ssfn_y < win->h - 24; i++) {
            ssfn_x = 2; ssfn_y += 16;
            for(s = copyright[i]; *s && (int)ssfn_x < win->w && !ssfn_putc(ssfn_utf8(&s)););
        }

        if(active >= 7 && active < 13) {
            ssfn_x = nmaxx; ssfn_y = sy + (active-5)*24;
            j = ui_input(win, strtable[active-7], win->w-nmaxx-8, 3, 1);
            if(j != 0) na = active + j;
        }
        if(active == 13) {
            ssfn_x = 18; ssfn_y = sy + 9*24;
            j = ui_input(win, nchr, 48, 1, 1);
            if(j != 0) na = active + j;
        }
    } while(na != active);
}

/**
 * Controller for properties
 */
int ctrl_props(ui_win_t *win, ui_event_t *evt)
{
    int a;

    switch(evt->type) {
        case E_KEY:
            switch(evt->x) {
                case K_UP:
                    if(active > 0) active--; else active = 16;
                    return 1;
                case K_DOWN:
                    if(active < 16) active++; else active = 0;
                    return 1;
                case K_ENTER:
do1:                if(active==5 || active > 13) {
                        active = -active;
                        ui_refreshwin(evt->win, 0, 0, win->w, win->h);
                        switch(-active) {
                            case 5: file_setbaseline(); break;
                            case 14:
                                a = gethex(nchr, 6);
                                ui_openwin(wins[0].v, (uint32_t)a);
                                a++; sprintf(nchr, "%X", a);
                            break;
                            case 15: file_saveasc(evt->win); break;
                            case 16: file_savesfn(evt->win); break;
                        }
                        active = -active;
                        ui_refreshall();
                    }
                    return 1;
                break;
                case K_RIGHT:
                case '+':
                case '=':
do2:                switch(active) {
                        case 0: if(font->family < SSFN_FAMILY_HAND) { font->family++; modified = 1; } break;
                        case 1: font->style ^= SSFN_STYLE_BOLD; modified = 1; break;
                        case 2: font->style ^= SSFN_STYLE_ITALIC; modified = 1; break;
                        case 3:
                            active = -1;
                            ui_refreshwin(evt->win, 0, 0, win->w, win->h);
                            file_setquality(+1);
                            active = 3;
                            ui_refreshall();
                            break;
                        case 4:
                            if(font->baseline < (16<<font->quality)) { font->baseline++; modified = 1; }
                            if(font->underline < font->baseline) { font->underline = font->baseline; modified = 1; }
                        break;
                        case 6: if(font->underline < (16<<font->quality)) { font->underline++; modified = 1; } break;
                    }
                    return 1;
                case K_LEFT:
                case '-':
                case ' ':
do3:                switch(active) {
                        case 0: if(font->family > SSFN_FAMILY_SERIF) { font->family--; modified = 1; } break;
                        case 1: font->style ^= SSFN_STYLE_BOLD; modified = 1; break;
                        case 2: font->style ^= SSFN_STYLE_ITALIC; modified = 1; break;
                        case 3:
                            active = -1;
                            ui_refreshwin(evt->win, 0, 0, win->w, win->h);
                            file_setquality(-1);
                            active = 3;
                            ui_refreshall();
                        break;
                        case 4: if(font->baseline > 0) { font->baseline--; modified = 1; } break;
                        case 6:
                            if(font->underline > 0) { font->underline--; modified = 1; }
                            if(font->underline < font->baseline) { font->baseline = font->underline; modified = 1; }
                        break;
                    }
            }
            return 1;
        break;
        case E_BTNPRESS:
            a = (evt->y - 40) / 24;
            switch(a) {
                case 0:
                    active = 0;
                    if(evt->x >= (int)nmaxx && evt->x < (int)nmaxx+12) goto do3;
                    if(evt->x >= (int)nmaxx+128+16 && evt->x < (int)nmaxx+128+32) goto do2;
                    return 1;
                case 1:
                    if(evt->x >= (int)nmaxx && evt->x < (int)nmaxx+128) active = 1; else active = 2;
                    goto do2;
                case 2:
                    active = 3;
                    if(evt->x >= (int)nmaxx && evt->x < (int)nmaxx+12) goto do3;
                    if(evt->x >= (int)nmaxx+88+16 && evt->x < (int)nmaxx+88+32) goto do2;
                    return 1;
                case 3:
                    active = 4;
                    if(evt->x >= (int)nmaxx && evt->x < (int)nmaxx+12) goto do3;
                    if(evt->x >= (int)nmaxx+32+16 && evt->x < (int)nmaxx+32+28) goto do2;
                    if(evt->x >= (int)nmaxx+32+28+8 && evt->x < (int)nmaxx+32+28+8+128+8) { active = 5; goto do1; }
                    return 1;
                case 4:
                    active = 6;
                    if(evt->x >= (int)nmaxx && evt->x < (int)nmaxx+12) goto do3;
                    if(evt->x >= (int)nmaxx+32+16 && evt->x < (int)nmaxx+32+28) goto do2;
                    return 1;
                case 12:
                    active = 13;
                    if(evt->x >= 16+48+8 && evt->x < savebtn-16) { active = 14; goto do1; }
                    a = (win->w - savebtn - 40) >> 1;
                    if(evt->x >= savebtn && evt->x <= savebtn+a+12) { active = 15; goto do1; }
                    if(evt->x >= savebtn+a+12+16 && evt->x <= savebtn+12+16+a+a+12) { active = 16; goto do1; }
                    return 1;
                default: if(a < 13) active = a + (a>1? 1 : 0) + (a>3? 1 : 0) + (a>11 ? -1 : 0); break;
            }
            return 1;
        case E_BTNRELEASE:
            return 1;
    }
    return 0;
}

