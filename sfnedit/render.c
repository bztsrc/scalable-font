/*
 * sfnedit/render.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Glyph renderer
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "lang.h"
#include "ui.h"
#include "file.h"
#include "util.h"
#include "render.h"

extern int showlines, insl, selminx, selmaxx, selminy, selmaxy, selfrt, sellst;
frag_t *of = NULL;
int ox, oy, ow, oh, opw, oph, opp, opx, opy, x0, y0, op = 0, np = 0, mp = 0, nr[512], minx, miny, maxx, maxy;
int sminx, smaxx, sminy, smaxy, rx = 0, ry = 0;
uint16_t *points = NULL, *raster[512];
uint32_t ofg, obg, ofc, *opd;

/**
 * Mix colors to get a linear gradient
 */
uint32_t render_mkcolor(int x, int y)
{
    uint32_t c1, c2, tc, bc;
    uint8_t *a=(uint8_t*)&c1, *b=(uint8_t*)&c2, *c=(uint8_t*)&tc, *d=(uint8_t*)&bc;

    if(of->color[0] == 240 && of->color[1] == 240 && of->color[2] == 240 && of->color[3] == 240)
        return ofg;

    c1 = of->color[0] == 240 ? ofg : cmap[of->color[0]]; if(!a[3]) c1 = ofg;
    c2 = of->color[1] == 240 ? ofg : cmap[of->color[1]]; if(!b[3]) c2 = ofg;
    if(c1 == c2) tc = c1;
    else {
        c[0] = (b[0]*x + (maxx - x)*a[0]) / maxx;
        c[1] = (b[1]*x + (maxx - x)*a[1]) / maxx;
        c[2] = (b[2]*x + (maxx - x)*a[2]) / maxx;
    }

    c1 = of->color[2] == 240 ? ofg : cmap[of->color[2]]; if(!a[3]) c1 = ofg;
    c2 = of->color[3] == 240 ? ofg : cmap[of->color[3]]; if(!b[3]) c2 = ofg;
    if(c1 == c2) bc = c1;
    else {
        d[0] = (b[0]*x + (maxx - x)*a[0]) / maxx;
        d[1] = (b[1]*x + (maxx - x)*a[1]) / maxx;
        d[2] = (b[2]*x + (maxx - x)*a[2]) / maxx;
    }

    if(tc == bc) c1 = tc;
    else {
        a[0] = (d[0]*y + (maxy - y)*c[0]) / maxy;
        a[1] = (d[1]*y + (maxy - y)*c[1]) / maxy;
        a[2] = (d[2]*y + (maxy - y)*c[2]) / maxy;
    }
    return c1;
}

/**
 * Rasterize a glyph with antialiasing
 */
void render_raster()
{
    int i, k, l, m, n, o, x, y, Y, X0, Y0, X1, Y1, sx, sy, dx, dy;
    uint8_t *b = (uint8_t*)&obg, *c = (uint8_t*)&ofc;
    uint8_t e;
    uint16_t *r;

    if(op != 1 || np < 3) return;

    maxx -= minx; maxy -= miny;
    for(y = 0; y < oh; y++) {
        Y = ((y)? (((y) << 16) - (1 << 15)) / oh : 0); r = raster[y];
        for(n = i = 0; i < np - 3; i += 2) {
            if( (points[i+1] < Y && points[i+3] >= Y) ||
                (points[i+3] < Y && points[i+1] >= Y)) {
                    if(((int)points[i+1] * oh + (1 << 15)) >> 16 == ((int)points[i+3] * oh + (1 << 15)) >> 16)
                        x = (((int)points[i]+(int)points[i+2])>>1);
                    else
                        x = ((int)points[i]) + ((Y - (int)points[i+1])*
                            ((int)points[i+2] - (int)points[i])/
                            ((int)points[i+3] - (int)points[i+1]));
                    x = (x * ow + (1 << 7)) >> 8;
                    for(k=0; k < n && x > r[k]; k++);
                    if(n >= nr[y]) {
                        l = nr[y] << 1;
                        nr[y] = (n < np) ? np : (n+1) << 1;
                        raster[y] = (uint16_t *)realloc(raster[y], (nr[y] << 1));
                        if(!raster[y]) error("render_raster", ERR_MEM);
                        r = raster[y];
                    }
                    for(l = n; l > k; l--) r[l] = r[l-1];
                    r[k] = x;
                    n++;
            }
        }
        if(n>1 && n&1) { r[n-2] = r[n-1]; n--; }
        nr[y] = n;
        if(n && op != 2 && oy + y < oph) {
            k = (oy + y) * opp + ox;
            for(i=0; i < n-1; i += 2) {
                l = (((uint16_t)r[i] + 128) >> 8);
                m = (((uint16_t)r[i + 1] + 128) >> 8);
                for(; l < m && ox + l < opw; l++)
                    opd[k + l] = (opd[k + l] == obg ? 0xFF000000 | render_mkcolor(l - minx, y - miny) : obg);
            }
        }
    }

    for(y = 0, o = oy * opp; y < oh && oy + y < oph; y++, o += opp) {
        if(nr[y] && (r = raster[y])) {
            for(i = 0; i < nr[y] - 1; i += 2) {
                x = (((uint16_t)r[i] + 128) >> 8) + ox;
                if(x < opw) {
                    e = ~(((uint16_t)r[i] + 128) & 0xFF);
                    if(e == 127) e = 255;
                    else {
                        if(x && (opd[o + x - 1] >> 24) > e) e = opd[o + x - 1] >> 24;
                        if(y && (opd[o + x - opp] >> 24) > e) e = opd[o + x - opp] >> 24;
                    }
                    if(opd[o + x] == obg || e > (opd[o + x] >> 24)) {
                        ofc = opd[o + x] == obg ? obg : render_mkcolor(x-ox-minx, y-miny);
                        opd[o + x] = e << 24;
                        ((uint8_t*)&opd[o+x])[0] = (c[0]*e + (256 - e)*b[0])>>8;
                        ((uint8_t*)&opd[o+x])[1] = (c[1]*e + (256 - e)*b[1])>>8;
                        ((uint8_t*)&opd[o+x])[2] = (c[2]*e + (256 - e)*b[2])>>8;
                    }
                    e = (((uint16_t)r[i + 1] + 128) & 0xFF);
                    if(e == 128) e = 255;
                    x = (((uint16_t)r[i + 1] + 128) >> 8) + ox;
                    if(opd[o + x] == obg || e > (opd[o + x] >> 24)) {
                        ofc = render_mkcolor(x-ox-minx, y-miny);
                        opd[o + x] = e << 24;
                        ((uint8_t*)&opd[o+x])[0] = (c[0]*e + (256 - e)*b[0])>>8;
                        ((uint8_t*)&opd[o+x])[1] = (c[1]*e + (256 - e)*b[1])>>8;
                        ((uint8_t*)&opd[o+x])[2] = (c[2]*e + (256 - e)*b[2])>>8;
                    }
                }
            }
        }
    }

    for(i = 0; i < np - 3; i += 2) {
        X0 = points[i]; Y0 = points[i+1]; X1 = points[i+2]; Y1 = points[i+3];
        sx = X1 >= X0? 1 : -1; sy = Y1 >= Y0? 1 : -1; dx = X1 - X0; dy = Y1 - Y0;
        if(sx * dx >= sy * dy && dx) {
            for(x = X0, m = sx * dx; m > 0; m -= 16, x += sx * 16) {
                y = ((Y0 + ((x - X0) * dy / dx)) * oh + (1 << 7)) >> 8;
                y += 128; e = ~(y & 0xFF); y >>= 8; e >>= 1;
                l = ((x * ow + (1 << 7)) >> 16);
                o = (oy + y) * opp + ox + l;
                if(opd[o] == obg || e > opd[o] >> 24) {
                    ofc = render_mkcolor(ox+x-minx, y-miny);
                    opd[o] = e << 24;
                    ((uint8_t*)&opd[o])[0] = (c[0]*e + (256 - e)*b[0])>>8;
                    ((uint8_t*)&opd[o])[1] = (c[1]*e + (256 - e)*b[1])>>8;
                    ((uint8_t*)&opd[o])[2] = (c[2]*e + (256 - e)*b[2])>>8;
                }
            }
        }
    }
}

/**
 * Add a dot to contour
 */
void render_dot(int x1, int y1, int th)
{
    int x, y;

    if(op != 2) return;
    if(x1 > (4096<<4) - 16) x1 = (4096<<4) - 16;
    if(y1 > (4096<<4) - 16) y1 = (4096<<4) - 16;
    y = ((y1 * oh + (1 << 15)) >> 16) - opy;
    x = ((x1 * ow + (1 << 15)) >> 16) - opx;
    if(y >= 0 && y + oy < oph && x >= 0 && x + ox < opw) {
        if(y + oy - 1 >= 0) {
            if(x + ox - 1 >= 0)
                opd[(y + oy - 1) * opp + x + ox - 1] = theme[th];
            if(x + ox + 1 < opw)
                opd[(y + oy - 1) * opp + x + ox + 1] = theme[th];
        }
        if(y + oy + 1 < oph) {
            if(x + ox - 1 >= 0)
                opd[(y + oy + 1) * opp + x + ox - 1] = theme[th];
            if(x + ox + 1 < opw)
                opd[(y + oy + 1) * opp + x + ox + 1] = theme[th];
        }
        if(th == THEME_ONCURVE) {
            if(y + oy - 1 >= 0)
                opd[(y + oy - 1) * opp + x + ox] = theme[th];
            if(x + ox - 1 >= 0)
                opd[(y + oy) * opp + x + ox - 1] = theme[th];
            if(x + ox + 1 < opw)
                opd[(y + oy) * opp + x + ox + 1] = theme[th];
            if(y + oy + 1 < opw)
                opd[(y + oy + 1) * opp + x + ox] = theme[th];
        }
        opd[(y + oy) * opp + x + ox] = theme[th];
    }
}

/**
 * Add a line to contour
 */
void render_line(int x1, int y1, int l)
{
    int x, y, sx, sy, dx, dy, px, py, m, n, d = l == -1? 1024*512/ow : 16;

    if(x1 > (4096<<4) - 16) x1 = (4096<<4) - 16;
    if(y1 > (4096<<4) - 16) y1 = (4096<<4) - 16;
    if(x1 < -1 || y1 < -1 || (x0 == x1 && y0 == y1)) return;

    if(op && op != 2) {
        if(np+2 >= mp) {
            mp += 512;
            points = (uint16_t *)realloc(points, mp*sizeof(uint16_t));
            if(!points) error("render_line", ERR_MEM);
        }
        px = (x1 * ow + (1 << 15)) >> 16;
        py = (y1 * oh + (1 << 15)) >> 16;
        if(!np || !l || ((int)points[np-2] * ow + (1 << 15)) >> 16 != px ||
            ((int)points[np-1] * oh + (1 << 15)) >> 16 != py) {
                if(px < minx) minx = px;
                if(px > maxx) maxx = px;
                if(py < miny) miny = py;
                if(py > maxy) maxy = py;
                points[np++] = x1;
                points[np++] = y1;
                x0 = x1; y0 = y1;
        }
    } else if(x1 != -1 && x0 != -1) {
        if(x1 < minx) minx = x1;
        if(x1 > maxx) maxx = x1;
        if(y1 < miny) miny = y1;
        if(y1 > maxy) maxy = y1;
        sx = x1 >= x0? 1 : -1; sy = y1 >= y0? 1 : -1; dx = x1 - x0; dy = y1 - y0;
        if(sx * dx >= sy * dy && dx) {
            for(x = x0, m = sx * dx; m > 0; m -= d, x += sx*d) {
                y = (((y0 + ((x - x0) * dy / dx)) * oh + (1 << 15)) >> 16) - opy;
                n = ((x * ow + (1 << 15)) >> 16) - opx;
                if(y >= 0 && y + oy < oph && n >= 0 && n + ox < opw) opd[(y + oy) * opp + n + ox] = ofg;
            }
        } else {
            for(y = y0, m = sy * dy; m > 0; m -= d, y += sy*d) {
                x = (((x0 + ((y - y0) * dx / dy)) * ow + (1 << 15)) >> 16) - opx;
                n = ((y * oh + (1 << 15)) >> 16) - opy;
                if(x >= 0 && x + ox < opw && n >= 0 && n + oy < oph) opd[(n + oy) * opp + x + ox] = ofg;
            }
        }
        x0 = x1; y0 = y1;
    }
}

/**
 * Add a curve to contour
 */
void render_curve(int x0,int y0, int x1,int y1, int x2,int y2, int x3,int y3, int l)
{
    int m0x, m0y, m1x, m1y, m2x, m2y, m3x, m3y, m4x, m4y,m5x, m5y;
    if(l<8 && (x0!=x3 || y0!=y3) && x0 > 0 && y0 > 0) {
        m0x = ((x1-x0)/2) + x0;     m0y = ((y1-y0)/2) + y0;
        m1x = ((x2-x1)/2) + x1;     m1y = ((y2-y1)/2) + y1;
        m2x = ((x3-x2)/2) + x2;     m2y = ((y3-y2)/2) + y2;
        m3x = ((m1x-m0x)/2) + m0x;  m3y = ((m1y-m0y)/2) + m0y;
        m4x = ((m2x-m1x)/2) + m1x;  m4y = ((m2y-m1y)/2) + m1y;
        m5x = ((m4x-m3x)/2) + m3x;  m5y = ((m4y-m3y)/2) + m3y;
        render_curve(x0,y0, m0x,m0y, m3x,m3y, m5x,m5y, l+1);
        render_curve(m5x,m5y, m4x,m4y, m2x,m2y, x3,y3, l+1);
    }
    render_line(x3, y3, l);
}

/**
 * Render a contour
 */
void render_contour(frag_t *frag, uint32_t *pix, int pw, int ph, int pp, int px, int py, int x, int y, int w, int h,
    uint32_t fg, uint32_t bg, int oper)
{
    int i, mx = 0, my = 0, a, b, c, d, s = (12 - font->quality);
    cont_t *C;

    if(!frag) return;

    of = frag; ox = x; oy = y; ow = w; oh = h; obg = bg & 0xFFFFFF; op = oper;
    opw = pw, oph = ph; opp = pp; opx = px; opy = py; opd = pix;
    if(insl) { sminx = selminx << s; smaxx = selmaxx << s; sminy = selminy << s; smaxy = selmaxy << s; selfrt = sellst = -1; }
    if(op == 2 && showlines) {
        ofg = theme[THEME_CONTROL] >> 1;
        ofg &= 0x7F7F7F7F;
        for(i=0; i < frag->len; i++) {
            C = &frag->data[i];
            x = C->px << s; y = C->py << s; a = C->c1x << s; b = C->c1y << s; c = C->c2x << s; d = C->c2y << s;
            switch(C->type) {
                case SSFN_CONTOUR_QUAD:
                    render_line(a, b, -1);
                    render_line(x, y, -1);
                break;
                case SSFN_CONTOUR_CUBIC:
                    render_line(a, b, -1);
                    render_line(c, d, -1);
                    render_line(x, y, -1);
                break;
            }
            x0 = x; y0 = y;
        }
    }
    minx = miny = 65536; maxx = maxy = 0;
    x0 = mx = y0 = my = -1; np = 0;
    for(i=0; i < frag->len; i++) {
        C = &frag->data[i];
        x = C->px << s; y = C->py << s; a = C->c1x << s; b = C->c1y << s; c = C->c2x << s; d = C->c2y << s;
        ofg = fg;
        if(insl && x0 >= sminx && x0 <= smaxx && x >= sminx && x <= smaxx &&
            y0 >= sminy && y0 <= smaxy && y >= sminy && y <= smaxy) {
                if(selfrt == -1) selfrt = i;
                if(sellst == -1) ofg = theme[THEME_INPUT];
        } else {
            if(selfrt != -1 && sellst == -1) sellst = i;
        }
        switch(C->type) {
            case SSFN_CONTOUR_MOVE: render_line(x, y, 0); mx = x0 = x; my = y0 = y; break;
            case SSFN_CONTOUR_LINE: render_line(x, y, 0); break;
            case SSFN_CONTOUR_QUAD:
                render_curve(x0, y0, ((a - x0) >> 1) + x0, ((b - y0) >> 1) + y0,
                    ((x - a) >> 1) + a, ((y - b) >> 1) + b, x, y, 0);
            break;
            case SSFN_CONTOUR_CUBIC:
                render_curve(x0, y0, a, b, c, d, x, y, 0);
            break;
        }
        x0 = x; y0 = y;
    }
    if(op != 2) render_line(mx, my, 0);
    if(op == 1) render_raster();
    if(op == 2) {
        for(i=0; i < frag->len; i++) {
            C = &frag->data[i];
            x = C->px << s; y = C->py << s; a = C->c1x << s; b = C->c1y << s; c = C->c2x << s; d = C->c2y << s;
            switch(C->type) {
                case SSFN_CONTOUR_QUAD:
                    render_dot(a, b, THEME_CONTROL);
                break;
                case SSFN_CONTOUR_CUBIC:
                    render_dot(a, b, THEME_CONTROL);
                    render_dot(c, d, THEME_CONTROL);
                break;
            }
            render_dot(x, y, THEME_ONCURVE);
        }
    }
}

/**
 * Render a bitmap
 */
void render_bitmap(frag_t *frag, uint32_t *pix, int pw, int ph, int pp, int px, int py, int x, int y, int w, int h, uint32_t fg)
{
    int i, j, m, c, o, s = (16 << font->quality);
    uint8_t *raw = (uint8_t*)frag->data;

    if(!frag) return;

    ox = x; oy = y;
    minx = miny = 65536; maxx = maxy = 0;

    c = (oy - (py < 0? py : 0)) * pp + ox - px;
    for(j = (py > 0 ? py : 0); j < h && j + oy - py < ph; j++, c += pp) {
        y = (((long int)(j<<8) * (s<<8) / (h<<8)) >> 8);
        o = y * (s >> 3);
        for(i = (px > 0 ? px : 0); i < w && i + ox - px < pw; i++) {
            m = ((long int)(i<<8) * (s<<8) / (w<<8)) >> 8;
            if(m >= 0 && m < s && raw[o + (m >> 3)] & (1 << (m & 7))) {
                if((m & ~7) < minx) minx = (m & ~7);
                if(((m+8) & ~7) > maxx) maxx = ((m+8) & ~7);
                if(y < miny) miny = y;
                if(y > maxy) maxy = y;
                pix[c + i] = fg;
            }
        }
    }
    minx <<= (12 - font->quality);
    miny <<= (12 - font->quality);
    maxx <<= (12 - font->quality);
    maxy <<= (12 - font->quality);
}

/**
 * Render a pixmap
 */
void render_pixmap(frag_t *frag, uint32_t *pix, int pw, int ph, int pp, int px, int py, int x, int y, int w, int h, uint32_t bg)
{
    int i, j, m, c, o, s = (16 << font->quality);
    uint32_t color;
    uint8_t *a = (uint8_t*)&color, *b = (uint8_t*)&bg;
    uint8_t *raw = (uint8_t*)frag->data;

    if(!frag) return;

    ox = x; oy = y;
    minx = miny = 65536; maxx = maxy = 0;

    c = (oy - (py < 0? py : 0)) * pp + ox - px;
    for(j = (py > 0 ? py : 0); j < h && j - py < ph; j++, c += pp) {
        y = (((long int)(j<<8) * (s<<8) / (h<<8)) >> 8);
        o = y * s;
        for(i = (px > 0 ? px : 0); i < w && i + ox < pw && i + ox - px < pw; i++) {
            m = ((long int)(i<<8) * (s<<8) / (w<<8)) >> 8;
            if(m >= 0 && m < s && raw[o + m] < 0xF0) {
                if(m < minx) minx = m;
                if((m+1) > maxx) maxx = (m+1);
                if(y < miny) miny = y;
                if(y > maxy) maxy = y;
                color = cmap[raw[o + m]];
                if(insl) { color >>= 1; color &= 0x7F7F7F; a[3] = ((uint8_t*)&cmap[raw[o + m]])[3]; }
                ((uint8_t*)&pix[c + i])[0] = (a[0]*a[3] + (256 - a[3])*b[0])>>8;
                ((uint8_t*)&pix[c + i])[1] = (a[1]*a[3] + (256 - a[3])*b[1])>>8;
                ((uint8_t*)&pix[c + i])[2] = (a[2]*a[3] + (256 - a[3])*b[2])>>8;
            }
        }
    }
    minx <<= (12 - font->quality);
    miny <<= (12 - font->quality);
    maxx <<= (12 - font->quality);
    maxy <<= (12 - font->quality);
}

/**
 * Render an entire glyph
 */
void render_glyph(int v, int chr, ui_win_t *win, int x, int y, int w, int h, uint32_t fg, uint32_t bg, int op)
{
    int i;

    if(v < 0 || v >= SSFN_NUMVARIANTS || chr < 0 || chr > numchars[v]) return;

    for(i=0; i<chars[v][chr].len; i++)
        if(chars[v][chr].frags[i] >= 0 && chars[v][chr].frags[i] < numfrags)
            switch(frags[chars[v][chr].frags[i]].type) {
                case SSFN_FRAG_BITMAP:
                    render_bitmap(&frags[chars[v][chr].frags[i]], win->data, win->w, win->h, win->p, rx, ry, x, y, w, h, fg);
                break;
                case SSFN_FRAG_PIXMAP:
                    render_pixmap(&frags[chars[v][chr].frags[i]], win->data, win->w, win->h, win->p, rx, ry, x, y, w, h, bg);
                break;
                case SSFN_FRAG_CONTOUR:
                    render_contour(&frags[chars[v][chr].frags[i]], win->data, win->w, win->h, win->p, rx, ry, x, y, w, h,
                        fg, bg, op);
                break;
            }
}

/**
 * Free resources
 */
void render_free()
{
    int i;

    for(i=0; i<512; i++)
        if(raster[i]) free(raster[i]);
    free(points);
}
