/*
 * sfnedit/ui.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Common user interface routines
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#define SSFN_CONSOLEBITMAP_TRUECOLOR
#define SSFN_CONSOLEBITMAP_CLEARBG
#include <ssfn.h>
#include "lang.h"
#include "ui.h"
#include "hist.h"
#include "file.h"
#include "util.h"
#include "icon.h"
#include "render.h"
#include "unicode.h"
#include "copypaste.h"

#ifdef __WIN32__
extern unsigned char binary_logofont_sfn_start;
#define logofont binary_logofont_sfn_start
#else
extern unsigned char _binary_logofont_sfn_start;
#define logofont _binary_logofont_sfn_start
#endif
extern int glyphra, gmax, selall, kernactive;
ssfn_font_t *ui_font = NULL;
int saving = 0, savingmax = 0, saveact = 2, mainloop = 1;

uint32_t theme[] = { 0xFFF0F0F0, 0xFFF4F4F4, 0xFFBEBEBE, 0xFF808080, 0xFF5C5C5C, 0xFF4C4C4C, 0xFF454545, 0xFF383838, 0xFF303030,
    0xFFFF0000, 0xFF800000, 0xFF004040, 0xFF0000FF, 0xFF00FF00, 0xFFFF0000, 0xFF00FFFF };
uint32_t u=0x20;
int tabedge[] = { 5, 4, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 0 };
int gw = 36+16+512, gh = 24+8+512, gotevt = 0;

int numwin = 0;
ui_win_t *wins = NULL;
ui_event_t event;

/**
 * Open a window
 */
void ui_openwin(int v, uint32_t unicode)
{
    char title[278];
    int w, h, i, j=0, chr=-1;

    if(!numwin) {
        memcpy(title, "sfnedit ", 8);
        strncpy(title + 8, fontfilebn, 256);
        w = MAIN_W; h = MAIN_H; unicode = WINTYPE_MAIN;
    } else {
        sprintf(title, "sfnedit - U+%06X - ", unicode);
        strncpy(title + 21, fontfilebn, 256);
        w = gw; h = gh;
        chr = file_addchar(v, unicode, 1);
    }
    for(i=0; i < numwin; i++) {
        if(wins[i].winid) {
            if(wins[i].unicode == unicode) { ui_focuswin(&wins[i]); return; }
        } else {
            if(!j) j = i;
        }
    }
    if(!j) {
        j = numwin++;
        wins = (ui_win_t*)realloc(wins, numwin*sizeof(ui_win_t));
        if(!wins) error("openwin", ERR_MEM);
    }
    memset(&wins[j], 0, sizeof(ui_win_t));
    wins[j].v = v;
    wins[j].unicode = unicode;
    wins[j].uniname = uninames[uniname(unicode)].name;
    wins[j].chr = chr;
    wins[j].winid = ui_createwin(title, w, h);
    wins[j].menu = 5;
    wins[j].zoom = 512;
    wins[j].cont = -1;
    ui_resizewin(&wins[j], w, h);
    ui_focuswin(&wins[j]);
    ui_refreshwin(j, 0, 0, w, h);
}

/**
 * Close a window
 */
void ui_closewin(int idx)
{
    int i;

    selall = 0;
    ui_cursorwin(&wins[idx], CURSOR_PTR);
    ui_cursorwin(&wins[0], CURSOR_PTR);
    if(idx < 0 || idx > numwin || !wins[idx].winid) return;
    if(wins[idx].bg) free(wins[idx].bg);
    wins[idx].bg = NULL;
    if(wins[idx].hist) {
        hist_cleanup(&wins[idx], 0);
        free(wins[idx].hist);
    }
    wins[idx].hist = NULL;
    if(!idx) {
        for(i=1; i < numwin; i++)
            if(wins[i].winid)
                ui_closewin(i);
        numwin = 1;
        if(modified && mainloop == 1) {
            mainloop = 2;
            ui_resizewin(&wins[0], wins[0].w, wins[0].h);
            ui_refreshwin(0, 0, 0, wins[0].w, wins[0].h);
        } else
            mainloop = 0;
        return;
    }
    ui_destroywin(&wins[idx]);
    wins[idx].winid = NULL;
    wins[idx].surface = NULL;
    wins[idx].data = NULL;
    wins[idx].unicode = -1;
    wins[idx].histmin = wins[idx].histmax = 0;
    while(numwin && !wins[numwin-1].winid) numwin--;
}

/**
 * Get common window id for driver specific window id
 */
int ui_getwin(void *winid)
{
    int i;

    for(i=0; i < numwin; i++)
        if(wins[i].winid == winid)
            return i;
    return -1;
}

int ui_textwidth(char *s, int len)
{
    uint32_t i, j, n, t, u, w = 0;
    uint8_t *rg;

    t = (ssfn_font->quality < 5 && ssfn_font->characters_offs[0] < 65536) ? 4 : (ssfn_font->characters_offs[0] < 1048576 ? 5 : 6);
    if(len < 1) len = 65536;
    while((u = ssfn_utf8(&s)) && len--) {
        for(rg = (uint8_t*)ssfn_font + ssfn_font->characters_offs[0],j=i=0;i<=UNICODE_LAST;i++) {
            if(rg[0] & 0x80) {
                n = (rg[0] & 0x3f);
                if(rg[0] & 0x40) { i += rg[1] | (n << 8); rg++; } else { i += n; }
                rg++;
            } else {
                if(i == u) {
                    w += rg[6];
                    break;
                }
                rg += rg[0] * t + 10;
                j++;
            }
        }
    }
    return w;
}

/**
 * Blit an icon on window
 */
void ui_icon(ui_win_t *win, uint32_t *icon, int x, int y, int ih)
{
    int i, j, m, k, w, h;
    uint8_t *a, *b;

    if(x < 0 || y < 0) return;
    w = win->w > x + 32 ? 32 : win->w - x;
    h = win->h > y + 32 ? 32 : win->h - y;
    for(k = y*win->p + x, m = ((32 - h) << 8) + (32 - w), j = 0; j < ih && y + j < win->h; j++, k += win->p, m += 32)
        for(i = 0; i < w; i++)
            if(x + i < win->w && icon[m+i]>>24) {
                a = (uint8_t*)&win->data[k+i];
                b = (uint8_t*)&icon[m + i];
                a[0] = (b[0]*b[3] + (256 - b[3])*a[0])>>8;
                a[1] = (b[1]*b[3] + (256 - b[3])*a[1])>>8;
                a[2] = (b[2]*b[3] + (256 - b[3])*a[2])>>8;
            }
}

/**
 * Display a string on window
 */
void ui_text(ui_win_t *win, char *s, uint32_t a, uint32_t u)
{
    int i, p = (ssfn_y-1)*win->p + ssfn_x, q = (ssfn_y+16)*win->p + ssfn_x;

    if(s)
        while(*s && ssfn_x < ssfn_dst_w) {
            ssfn_putc(ssfn_utf8(&s));
            if(ssfn_x > ssfn_dst_w) ssfn_x = ssfn_dst_w;
            for(i = 0; i < (int)ssfn_adv_x && ssfn_x - ssfn_adv_x + i < ssfn_dst_w; i++) {
                if((int)ssfn_y-1 < win->h)  win->data[p - win->p] = u;
                if((int)ssfn_y+0 < win->h)  win->data[p++] = ssfn_bg;
                if((int)ssfn_y+17 < win->h) win->data[q + win->p] = a;
                if((int)ssfn_y+16 < win->h) win->data[q++] = ssfn_bg;
            }
        }
}

/**
 * Display a scale input box
 */
void ui_scale(ui_win_t *win, char *s, int w, int th)
{
    unsigned int i, j, a, border = theme[th ? THEME_FG : THEME_INACT], p;

    ssfn_fg = theme[THEME_BG];
    ssfn_bg = border;
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y+1)*win->p + ssfn_x, i=0; i<14; i++, p += win->p)
        win->data[p] = border;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y-1)*win->p + ssfn_x, i=0; i<18; i++, p += win->p)
        win->data[p] = border;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;
    ui_text(win, "-", border, border);
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y-2)*win->p + ssfn_x, i=0; i<20; i++, p += win->p)
        win->data[p] = border;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y-2)*win->p + ssfn_x, i=0; i<20; i++, p += win->p)
        win->data[p] = border;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;

    ssfn_fg = theme[th ? THEME_INPUT : THEME_FG];
    ssfn_bg = theme[th ? THEME_INPBG : THEME_TABBG];
    j = ssfn_x + w;
    for(i=0;i<2; ssfn_x++) {
        if((int)ssfn_x >= win->w) return;
        p = (ssfn_y-1)*win->p + ssfn_x;
        win->data[p-win->p] = border;
        for(i=0; i<18; i++, p += win->p)
            win->data[p] = ssfn_bg;
        win->data[p] = border;
    }
    a = ssfn_dst_w;
    ssfn_dst_w = j;
    ui_text(win, s, border, border);
    ssfn_dst_w = a;
    if(j < ssfn_x + 2) j = ssfn_x + 2;
    for(;ssfn_x < j; ssfn_x++) {
        if((int)ssfn_x >= win->w) return;
        p = (ssfn_y-1)*win->p + ssfn_x;
        win->data[p-win->p] = border;
        for(i=0; i<18; i++, p += win->p)
            win->data[p] = ssfn_bg;
        win->data[p] = border;
    }

    ssfn_fg = theme[THEME_BG];
    ssfn_bg = border;
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y-2)*win->p + ssfn_x, i=0; i<20; i++, p += win->p)
        win->data[p] = border;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y-2)*win->p + ssfn_x, i=0; i<20; i++, p += win->p)
        win->data[p] = border;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;
    ui_text(win, "+", border, border);
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y-1)*win->p + ssfn_x, i=0; i<18; i++, p += win->p)
        win->data[p] = border;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y+1)*win->p + ssfn_x, i=0; i<14; i++, p += win->p)
        win->data[p] = border;
    ssfn_x++;
    ssfn_bg = theme[THEME_BG];
}

/**
 * Display a checkbox
 */
void ui_bool(ui_win_t *win, char *s, int state, int th)
{
    int i, p;

    ssfn_fg = theme[th == 1 ? THEME_INPUT : THEME_INACT];
    ssfn_bg = theme[th ? THEME_INPBG : THEME_TABBG];
    for(p = (ssfn_y-1)*win->p + ssfn_x, i=0; i<18; i++, p += win->p)
        win->data[p] = ssfn_fg;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;
    ui_text(win, state? "✘" : "  ", ssfn_fg, ssfn_fg);
    /*ui_text(win, state? "✓" : "✗", ssfn_fg, ssfn_fg);*/
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y-1)*win->p + ssfn_x, i=0; i<18; i++, p += win->p)
        win->data[p] = ssfn_fg;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;
    ssfn_bg = theme[THEME_BG];
    ssfn_x += 2;
    ssfn_fg = theme[th == -1 ? THEME_INACT : THEME_FG];
    ui_text(win, s, ssfn_bg, ssfn_bg);
}

/**
 * Display a button
 */
void ui_button(ui_win_t *win, char *s, int w, int b, int th)
{
    unsigned int i, j, p;

    ssfn_fg = theme[th==2? THEME_INPUT : ( th==3 ? THEME_TABU : (th==5? THEME_FG : THEME_BG))];
    ssfn_bg = theme[th? (th==2? THEME_SAVEACT : ( th==3 ? THEME_SAVEINACT : (th==5? THEME_BG : THEME_FG))) : THEME_INACT];
    if(b&1) {
        if((int)ssfn_x >= win->w) return;
        for(p = (ssfn_y+1)*win->p + ssfn_x, i=0; i<14 && ssfn_y+1+i < (unsigned int)win->h; i++, p += win->p)
            win->data[p] = ssfn_bg;
        ssfn_x++;
        if((int)ssfn_x >= win->w) return;
        for(p = (ssfn_y-1)*win->p + ssfn_x, i=0; i<18 && ssfn_y-1+i < (unsigned int)win->h; i++, p += win->p)
            win->data[p] = ssfn_bg;
        ssfn_x++;
    }
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y-2)*win->p + ssfn_x, i=0; i<20 && ssfn_y-2+i < (unsigned int)win->h; i++, p += win->p)
        win->data[p] = ssfn_bg;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;
    for(p = (ssfn_y-2)*win->p + ssfn_x, i=0; i<20 && ssfn_y-2+i < (unsigned int)win->h; i++, p += win->p)
        win->data[p] = ssfn_bg;
    ssfn_x++;
    if((int)ssfn_x >= win->w) return;

    j = ui_textwidth(s, 0);
    if(w < (int)j) w = j;
    w = ((w + 4) - j) >> 1;
    for(j=0;(int)j<w; j++,ssfn_x++) {
        if((int)ssfn_x >= win->w) return;
        for(p = (ssfn_y-2)*win->p + ssfn_x, i=0; i<20 && ssfn_y-2+i < (unsigned int)win->h; i++, p += win->p)
            win->data[p] = ssfn_bg;
    }
    ui_text(win, s, ssfn_bg, ssfn_bg);
    for(j=0;(int)j<w; j++,ssfn_x++) {
        if((int)ssfn_x >= win->w) return;
        for(p = (ssfn_y-2)*win->p + ssfn_x, i=0; i<20 && ssfn_y-2+i < (unsigned int)win->h; i++, p += win->p)
            win->data[p] = ssfn_bg;
    }

    if(b&2) {
        if((int)ssfn_x >= win->w) return;
        for(p = (ssfn_y-1)*win->p + ssfn_x, i=0; i<18 && ssfn_y-1+i < (unsigned int)win->h; i++, p += win->p)
            win->data[p] = ssfn_bg;
        ssfn_x++;
        if((int)ssfn_x >= win->w) return;
        for(p = (ssfn_y+1)*win->p + ssfn_x, i=0; i<14 && ssfn_y+1+i < (unsigned int)win->h; i++, p += win->p)
            win->data[p] = ssfn_bg;
        ssfn_x++;
    }
    ssfn_bg = theme[THEME_BG];
}

/**
 * Display a text input box
 */
int ui_input(ui_win_t *win, char *s, int w, int b, int th)
{
    unsigned int i, j, k, border = theme[th ? THEME_FG : THEME_INACT], p, q, tx, cx = 65536, mx, my =ssfn_y;
    char *str = s, *pos;
    uint32_t bg = theme[th ? THEME_INPBG : THEME_TABBG];

    ssfn_fg = theme[THEME_BG];
    ssfn_bg = border;
    if((int)ssfn_x >= win->w) return 0;
    tx = ssfn_x;
    my = ssfn_y;
redraw:
    ssfn_x = tx;
    ssfn_y = my;
    if(b&1) {
        for(p = (ssfn_y+1)*win->p + ssfn_x, i=0; i<14; i++, p += win->p)
            win->data[p] = border;
        ssfn_x++;
        if((int)ssfn_x >= win->w) return 0;
        p=(ssfn_y-1)*win->p + ssfn_x;
        win->data[p] = border; p+=win->p;
        win->data[p] = border; p+=win->p;
        for(i=0; i<14; i++, p += win->p)
            win->data[p] = bg;
        win->data[p] = border; p+=win->p;
        win->data[p] = border;
        ssfn_x++;
        if((int)ssfn_x >= win->w) return 0;
    }
    ssfn_fg = theme[th? THEME_INPUT : THEME_FG];
    ssfn_bg = bg;
    for(i=0;i<2; ssfn_x++) {
        if((int)ssfn_x >= win->w) return 0;
        p = (ssfn_y-1)*win->p + ssfn_x;
        win->data[p-win->p] = border;
        for(i=0; i<18; i++, p += win->p)
            win->data[p] = bg;
        win->data[p] = border;
    }
    ssfn_dst_w = ssfn_x + w;
    for(str = s, mx = 0; ssfn_utf8(&str); mx++);
    if(cx > mx) cx = mx;
    k = 0;
    p = (ssfn_y-1)*win->p + ssfn_x;
    q = (ssfn_y+16)*win->p + ssfn_x;
    str = s;
    if(th) {
        while(cx) {
            i = ui_textwidth(str, cx);
            if((int)i > w) { ssfn_utf8(&str); k++; }
            else break;
        }
        if(!cx) {
            for(i=p-1+win->p;i<q-1-win->p;i+=win->p)
                win->data[i] = theme[THEME_INPUT];
            pos = s;
        }
    }
    while(*str && ssfn_x < ssfn_dst_w) {
        ssfn_putc(ssfn_utf8(&str));
        for(i = 0; i < ssfn_adv_x && ssfn_x - ssfn_adv_x + i < ssfn_dst_w; i++) {
            win->data[p - win->p] = border;
            win->data[p++] = ssfn_bg;
            win->data[q + win->p] = border;
            win->data[q++] = ssfn_bg;
        }
        if(th && ++k == cx) {
            pos = str;
            for(i=p-1+win->p;i<q-1-win->p;i+=win->p)
                win->data[i] = theme[THEME_INPUT];
        }
    }
    if(ssfn_x > ssfn_dst_w) ssfn_x = ssfn_dst_w;
    for(;ssfn_x < ssfn_dst_w; ssfn_x++) {
        if((int)ssfn_x >= win->w) break;
        p = (ssfn_y-1)*win->p + ssfn_x;
        win->data[p-win->p] = border;
        for(i=0; i<18; i++, p += win->p)
            win->data[p] = ssfn_bg;
        win->data[p] = border;
    }
    ssfn_dst_w = win->w;
    if(b&2) {
        p=(ssfn_y-1)*win->p + ssfn_x;
        win->data[p] = border; p+=win->p;
        win->data[p] = border; p+=win->p;
        for(i=0; i<14; i++, p += win->p)
            win->data[p] = bg;
        win->data[p] = border; p+=win->p;
        win->data[p] = border;
        ssfn_x++;
        if((int)ssfn_x >= win->w) return 0;
        for(p = (ssfn_y+1)*win->p + ssfn_x, i=0; i<14; i++, p += win->p)
            win->data[p] = border;
        ssfn_x++;
        if((int)ssfn_x >= win->w) return 0;
    }
    if(th) {
        while(1) {
            ui_flushwin(win, 0, 0, win->w, win->h);
            memset(&event, 0, sizeof(ui_event_t));
            ui_getevent();
            if(event.type == E_CLOSE || (event.type == E_KEY && event.x == K_ESC)) { ui_closewin(event.win); return 0; }
            if(event.type == E_NONE) continue;
            if(event.type != E_KEY || event.win != ui_getwin(win->winid)) { gotevt = 1; return 0; }
            switch(event.x) {
                case K_F1:
                case K_TAB: gotevt = 1; return 0;
                case K_UP: return -1;
                case K_ENTER:
                case K_DOWN: return 1;
                case K_LEFT: if(cx>0) cx--; break;
                case K_RIGHT: if(cx<mx) cx++; break;
                case K_DEL:
                    if(pos[0] & 0x80) {
                        if((pos[0] & 0xE0) == 0xC0) j = 2; else
                        if((pos[0] & 0xF0) == 0xE0) j = 3; else
                        if((pos[0] & 0xF8) == 0xF0) j = 4; else break;
                    } else j = 1;
delete:             for(i=(long int)pos-(long int)s;i+j<256;i++) s[i] = s[i+j];
                    if(win->unicode == WINTYPE_MAIN && !win->tab) modified = 1;
                break;
                case K_BACKSPC:
                    if(cx) {
                        cx--; j = 0;
                        do { pos--; j++; } while(pos > s && (pos[0] & 128) && !(pos[0] & 64));
                        goto delete;
                    }
                break;
                default:
                    if((long int)pos-(long int)s < 255) {
                        j = strlen((char*)&event.x);
                        for(i=255; i >= (long int)pos-(long int)s+j;i--) s[i] = s[i-j];
                        memcpy(pos, &event.x, j);
                        cx++;
                        if(win->unicode == WINTYPE_MAIN && !win->tab) modified = 1;
                    }
                break;
            }
            if(b&4) {
                ui_resizewin(win, win->w, win->h);
                ui_refreshwin(ui_getwin(win->winid),0,0,0,0);
            }
            goto redraw;
        }
    }
    return 0;
}

/**
 * Display a progress bar
 */
void ui_progress(ui_win_t *win, int val, int max, int w, int h, int th)
{
    unsigned int i, j, k, p;
    uint32_t c;

    c = theme[th ? (th==2? THEME_SAVEINACT : THEME_INPUT) : THEME_FG];
    if(val > max) val = max;
    k = val*w/(max > 0 ? max : 1);
    for(i=0;(int)i<w; i++,ssfn_x++) {
        if((int)ssfn_x >= win->w) return;
        for(p = (ssfn_y+(h<16?2:0))*win->p + ssfn_x, j=0; (int)j<h; j++, p += win->p)
            win->data[p] = i<k ? c : theme[THEME_TABBG];
    }
}

/**
 * Display a glyph (undefined, no glyph in font or render)
 */
void ui_glyph(ui_win_t *win, int chr, uint32_t unicode, int u, int th)
{
    int i, j, p;

    if(win->v && file_findchar(0, unicode) == -1) unicode = -1U;
    ssfn_fg = theme[th ? THEME_INPUT : THEME_FG];
    ssfn_bg = (theme[uniname(unicode)==UNICODE_NUMNAMES ? THEME_BG :
        (chr < 0 || chr >= numchars[win->v] || th == 2 ? THEME_BGLOGO : (th == 1 ? THEME_INACT :
        (th == 3 ? THEME_INPBG : THEME_TABBG)))]) & 0xFFFFFF;
    p = ssfn_y*win->p + ssfn_x;
    for(j=0;j<u;j++,p+=win->p) {
        for(i=0;i<u;i++)
            win->data[p+i] = ssfn_bg;
    }
    if(chr < 0 || chr >= numchars[win->v] || th == 2) {
        if(unicode != -1U && ssfn_bg != theme[THEME_BG]) {
            ssfn_fg = theme[THEME_TABBG];
            p = ssfn_x;
            ssfn_putc(unicode);
            ssfn_x = p;
        }
    } else {
        render_glyph(win->v, chr, win, ssfn_x, ssfn_y, u, u, ssfn_fg, ssfn_bg, 1);
    }
}

/**
 * Make color brighter in an area
 */
void ui_bright(ui_win_t *win, int x, int y, int w, int h, int all)
{
    int i, j, p = y * win->p + x;
    uint32_t c;

    for(j=0; j < h && y + j < win->h; j++, p += win->p)
        for(i=0; i < w && x + i < win->w; i++) {
            c = win->data[p + i];
            if(all || c != theme[THEME_INACT]) {
                c <<= 1;
                c &= 0xFEFEFEFE;
                win->data[p + i] = c;
            }
        }

}

/**
 * Display a filled box
 */
void ui_box(ui_win_t *win, int x, int y, int w, int h, uint32_t c)
{
    int i, j, p = y * win->p + x;

    c &= 0xFFFFFF;
    for(j=0; j < h && y + j < win->h; j++, p += win->p)
        for(i=0; i < w && x + i < win->w; i++)
            win->data[p + i] = c;

}

/**
 * Display a box with color
 */
void ui_argb(ui_win_t *win, int x, int y, int w, int h, uint32_t c)
{
    int i, j, k = w < 8 ? (w < 4 ? 2 : 4) : 8, p = y * win->p + x;
    uint8_t *d, *a = (uint8_t*)&theme[THEME_INACT], *b = (uint8_t*)&theme[THEME_INPBG], *C = (uint8_t*)&c;

    for(j=0; j < h && y + j < win->h; j++, p += win->p)
        for(i=0; i < w && x + i < win->w; i++) {
            d = (j & k) ^ (i & k) ? a : b;
            ((uint8_t*)&win->data[p+i])[0] = (C[0]*C[3] + (256 - C[3])*d[0])>>8;
            ((uint8_t*)&win->data[p+i])[1] = (C[1]*C[3] + (256 - C[3])*d[1])>>8;
            ((uint8_t*)&win->data[p+i])[2] = (C[2]*C[3] + (256 - C[3])*d[2])>>8;
        }
}

/**
 * Display a number with extra small glyphs
 */
void ui_number(ui_win_t *win, int n, int x, int y, uint32_t c)
{
    int i, j, k, m, d = 0, p = y * win->p + x;

    if(!n) { p += 12; goto zero; }
    if(n < 0) {
        win->data[p + 2*win->p ] = win->data[p + 2*win->p + 1] = win->data[p + 2*win->p + 2] = c;
        n = -n;
    }
    for(m = 1000; m > 0; m /= 10, p += 4)
        if(n >= m) {
            d = (n/m)%10;
zero:
            for(k = 1<<19, j = 0; j < 5 && y + j < win->h; j++)
                for(i = 0; i < 4; i++, k >>= 1)
                    if((numbers[d] & k) && x + i < win->w) win->data[p + j*win->p +i] = c;
        }
}

/**
 * Display header with tabs on a window
 */
void ui_tabs(ui_win_t *win, int idx)
{
    char *s, *title = "Scalable Screen Font Editor v0.0.1";
    int i, j, k, e = idx? GMENU_KERN : MMENU_GLYPHS, m, p, q;
    uint8_t *b = (uint8_t*)&theme[THEME_BG], *c = (uint8_t*)&theme[THEME_BGLOGO];
    uint32_t a, d, u;

    if(!idx) {
        p = win->w > 256 ? 256 : win->w;
        q = win->h > 256 ? 256 : win->h;
        for(k = (win->h-q+1)*win->p - p, m = ((256 - q) << 8) + (256 - p), j = 0; j < q; j++, k += win->p, m += 256)
            for(i = 0; i < p; i++)
                if(logo[m+i]) {
                    ((uint8_t*)&win->data[k+i])[0] = (c[0]*logo[m+i] + (256 - logo[m+i])*b[0])>>8;
                    ((uint8_t*)&win->data[k+i])[1] = (c[1]*logo[m+i] + (256 - logo[m+i])*b[1])>>8;
                    ((uint8_t*)&win->data[k+i])[2] = (c[2]*logo[m+i] + (256 - logo[m+i])*b[2])>>8;
                }
    }
    for(p=0,j=0;j<(int)(idx? 20 : 38);j++,p+=win->p)
        for(i=0;i<win->w;i++)
            win->data[p+i] = theme[THEME_TABBG];
    if(!idx) {
        ssfn_font = (ssfn_font_t*)&logofont;
        ssfn_x = ssfn_y = 2;
        ssfn_fg = theme[THEME_INPUT];
        ssfn_bg = theme[THEME_TABBG];
        for(s = title; *s; s++)
            ssfn_putc(*s);
        ui_icon(win, &wm_icon[2+16*16+2], win->w - 32, 4, 32);
    }
    ssfn_font = ui_font;
    ssfn_x = 0; ssfn_y = idx? 2 : 20;
    ssfn_fg = theme[THEME_FG];
    ssfn_bg = theme[THEME_TABBG];
    for(q=(ssfn_y + 18)*win->p, j = 0; j < 10; j++, ssfn_x++, q++)
        win->data[q] = theme[THEME_FG];
    if(idx) {
        if(win->unicode < 33) e = GMENU_EDIT;
        ssfn_putc(win->unicode);
        for(j=0;j<(int)ssfn_adv_x+16;j++)
            win->data[q++] = theme[THEME_FG];
        ssfn_x += 16;
        s = (char*)win->uniname;
    } else {
        s = strrchr(fontfilebn, '/');
        if(s) s++; else {
            s = strrchr(fontfilebn, '\\');
            if(s) s++;
            else s = fontfilebn;
        }
    }
    if((int)ssfn_x >= win->w) goto end;
    while(*s) {
        if(ssfn_putc(ssfn_utf8(&s)) != SSFN_OK) goto end;
        for(j=0;j<(int)ssfn_adv_x && (int)ssfn_x - (int)ssfn_adv_x + j < win->w;j++)
            win->data[q++] = theme[THEME_FG];
    }
    for(j = 0; j < 10 && (int)ssfn_x < win->w; j++, ssfn_x++, q++)
        win->data[q] = theme[THEME_FG];
    if((int)ssfn_x >= win->w || mainloop > 1) goto end;
    p = (ssfn_y-2)*win->p + ssfn_x;
    for(m = 0, i = idx? GMENU_EDIT : MMENU_PROPS; i <= e && (int)ssfn_x < win->w; i++, m++) {
        if(win->tab == m) {
            ssfn_fg = theme[THEME_TABA];
            ssfn_bg = u = theme[THEME_BG];
            u = theme[THEME_BG];
            a = theme[THEME_TABA];
            d = theme[THEME_TABU];
        } else {
            ssfn_bg = theme[THEME_TABBG];
            ssfn_fg = a = d = theme[THEME_INACT];
            u = theme[THEME_FG];
        }
        for(j = 0; j < 5 && (int)ssfn_x + j < win->w; j++, q++) win->data[q] = u;
        win->tabpos[m] = ssfn_x;
        for(j=0;j<20;j++) {
            if((int)ssfn_x + tabedge[j] - 1 < win->w)
                win->data[p+j*win->p+tabedge[j]-1] = a;
            for(k = tabedge[j]; k < 10 && (int)ssfn_x + k < win->w; k++)
                win->data[p+j*win->p+k] = ssfn_bg;
        }
        p += 5;
        for(j = 0; j < 5 && (int)ssfn_x + 5 + j < win->w; j++) { win->data[p++] = a; win->data[q++] = u; }
        ssfn_x += 10;
        for(s = lang[i]; *s && (int)ssfn_x < win->w;) {
            ssfn_putc(ssfn_utf8(&s));
            for(j=0;j<(int)ssfn_adv_x && (int)ssfn_x - (int)ssfn_adv_x + j < win->w; j++) {
                for(k=1;k<3;k++)
                    win->data[q - k*win->p] = ssfn_bg;
                win->data[p + win->p] = ssfn_bg;
                win->data[p++] = a;
                win->data[q++] = u;
            }
        }
        for(j=0;j<20;j++) {
            if(j)
                for(k=0; k<10-tabedge[j] && (int)ssfn_x + k < win->w; k++)
                    win->data[p+j*win->p+k] = ssfn_bg;
            if((int)ssfn_x + 10 - tabedge[j] < win->w)
                win->data[p + j*win->p + 10 - tabedge[j]] = d;
        }
        for(j = 0; j < 5 && (int)ssfn_x + j < win->w; j++) { win->data[p++] = a; win->data[q++] = u; }
        for(j = 0; j < 5 && (int)ssfn_x + 5 + j < win->w; j++, q++) win->data[q] = u;
        ssfn_x += 10; p += 5;
        if((int)ssfn_x >= win->w) break;
        for(j = 0; j < 10 && (int)ssfn_x + j < win->w; j++, q++)
            win->data[q] = theme[THEME_FG];
        ssfn_x += 10; p += 10;
    }
end:
    for(;(int)ssfn_x < win->w; ssfn_x++,q++) win->data[q] = theme[THEME_FG];
    ssfn_x = 0; ssfn_y = !idx ? 40 : 20;
    ssfn_fg = theme[THEME_FG];
    ssfn_bg = theme[THEME_BG];
}

/**
 * Set saving progress bar and cursor
 */
void ui_saving(int idx, int s)
{
    int m = savingmax + 1;

    if(!s || saving * 100 / m != s * 100 / m) {
        saving = s;
        ui_cursorwin(&wins[idx], s > 0 ? CURSOR_LOADING : CURSOR_PTR);
        if(!s) ui_resizewin(&wins[idx], wins[idx].w, wins[idx].h);
        ui_refreshwin(idx, 0, wins[idx].h - 18, wins[idx].w, 18);
    }
}

/**
 * Redraw window
 */
void ui_refreshwin(int idx, int wx, int wy, int ww, int wh)
{
    ui_win_t *win = &wins[idx];
    int i;

    if(win->w < 34 || win->h < 34) return;
    ssfn_dst_ptr = (uint8_t*)win->data;
    ssfn_dst_pitch = win->p*4;
    ssfn_dst_w = win->w;
    ssfn_dst_h = win->h;
    switch(mainloop) {
        case 3:
            ui_tabs(win, idx);
            i = ui_textwidth(lang[LOADING], 64);
            ssfn_x = (win->w - i) >> 1; ssfn_y = (win->h - 80) >> 1;
            ssfn_fg = theme[THEME_FG]; ssfn_bg = theme[THEME_BG];
            ui_text(win, lang[LOADING], ssfn_bg, ssfn_bg);
        break;
        case 2:
            ui_tabs(win, idx);
            i = ui_textwidth(lang[ERR_UNSAVED], 64);
            ssfn_x = (win->w - i) >> 1; ssfn_y = (win->h - 80) >> 1;
            ssfn_fg = theme[THEME_FG]; ssfn_bg = theme[THEME_BG];
            ui_text(win, lang[ERR_UNSAVED], ssfn_bg, ssfn_bg);
            ssfn_x = 8; ssfn_y = (win->h) >> 1;
            i = (win->w - 80) / 3;
            ui_button(win, lang[PROP_NOQUIT], i, 3, saveact == 0);
            ssfn_x += 16;
            ui_button(win, lang[PROP_SAVEASC], i, 3, saveact == 1 ? 2 : 3);
            ssfn_x += 16;
            ui_button(win, lang[PROP_SAVESFN], i, 3, saveact == 2 ? 2 : 3);
        break;
        default:
            if(win->help) view_help(win);
            else {
                /* draw header */
                ui_tabs(win, idx);
                /* draw tab */
                if(!idx) {
                    switch(win->tab) {
                        case MAIN_TAB_PROPS: view_props(win); break;
                        case MAIN_TAB_RANGES: view_ranges(win); break;
                        case MAIN_TAB_GLYPHS: view_glyphs(win); break;
                    }
                } else {
                    switch(win->tab) {
                        case GLYPH_TAB_EDIT: view_edit(win); break;
                        case GLYPH_TAB_KERN: view_kern(win); break;
                    }
                }
            }
        break;
    }
    if(saving) {
        ssfn_x = 0; ssfn_y = win->h - 18;
        if(saving < 0) {
            ui_progress(win, 100, 100, win->w, 18, 2);
            ssfn_x = 0; ssfn_y += 2;
            ssfn_fg = theme[THEME_INPUT];
            ssfn_bg = theme[THEME_SAVEINACT];
            ui_text(win, lang[ERR_SAVE], ssfn_bg, ssfn_bg);
            ui_text(win, " (", ssfn_bg, ssfn_bg);
            ui_text(win, fontfilebn, ssfn_bg, ssfn_bg);
            ui_text(win, ")", ssfn_bg, ssfn_bg);
        } else {
            ui_progress(win, saving, savingmax + 1, win->w, 18, 1);
        }
    }
    ui_flushwin(win, wx, wy, ww, wh);
}

/**
 * Refresh all windows
 */
void ui_refreshall()
{
    int i;

    for(i=0; i < numwin; i++)
        if(wins[i].winid) {
            if(wins[i].bg) free(wins[i].bg);
            wins[i].bg = NULL;
            if(wins[i].hist) {
                hist_cleanup(&wins[i], 0);
                free(wins[i].hist);
            }
            ui_resizewin(&wins[i], wins[i].w, wins[i].h);
            ui_refreshwin(i, 0, 0, wins[i].w, wins[i].h);
        }
}

/**
 * Update char_t indeces in windows
 */
void ui_updatewin(int v, uint32_t unicode, int diff)
{
    int i;

    for(i=1; i < numwin; i++)
        if(wins[i].v == v && wins[i].unicode > unicode)
            wins[i].chr += diff;
}

/**
 * Quit via Ctrl-C signal handler
 */
void ui_forcequit(int sig __attribute__((unused)))
{
    ui_fini();
    exit(1);
}

/**
 * Main user interface event handler
 */
void ui_main(char *fn)
{
    int m;

    /* get user interface font */
    ui_font = (ssfn_font_t*)load_file((char*)&font_start, &m);
    if(!ui_font) error("ui_main", ERR_MEM);
    /* get UNICODE names database */
    uniname_init();
    copypaste_init();

    /* fake a view to display "Loading..." message and read font */
    mainloop = 3;
    for(fontfilebn = fn + strlen(fn); fontfilebn > fn && *(fontfilebn-1) != '/'; fontfilebn--);
    ui_openwin(0, WINTYPE_MAIN);
    file_load(fn);
    /* set normal operation and refresh window */
    mainloop = 1;
    ui_resizewin(&wins[0], wins[0].w, wins[0].h);
    ui_refreshwin(0, 0, 0, wins[0].w, wins[0].h);

    /* main event loop */
    while(mainloop) {
        if(!gotevt) {
            memset(&event, 0, sizeof(ui_event_t));
            ui_getevent();
        } else gotevt = 0;
        if(!event.type || event.win < 0 || event.win >= numwin) continue;
        if(saving < 0) {
            saving = 0;
            ui_resizewin(&wins[event.win], wins[event.win].w, wins[event.win].h);
            ui_refreshwin(event.win, 0, wins[event.win].h-18, wins[event.win].w, 18);
        }
/*printf("event %d x %ld y %d w %d h %d\n",event.type,event.x,event.y,event.w,event.h);fflush(stdout);*/
        switch(event.type) {
            case E_CLOSE: ui_closewin(event.win); break;
            case E_RESIZE:
                if(wins[event.win].bg) free(wins[event.win].bg);
                wins[event.win].bg = NULL;
                ui_resizewin(&wins[event.win], event.w, event.h);
                ui_refreshwin(event.win, 0, 0, event.w, event.h);
                if(event.win) { gw = event.w; gh = event.h; }
            break;
            case E_REFRESH: ui_refreshwin(event.win, event.x, event.y, event.w, event.h); break;
            case E_KEY:
                if(event.x == 's' && event.h & 2) {
                    if(event.h & 1) file_saveasc(event.win);
                    else file_savesfn(event.win);
                    break;
                }
                if(event.x == K_ESC) { ui_closewin(event.win); break; }
                if(mainloop == 1) {
                    if(event.x == K_F1) {
help:                   wins[event.win].help ^= 1;
                        ui_resizewin(&wins[event.win], wins[event.win].w, wins[event.win].h);
                        ui_refreshwin(event.win, 0, 0, wins[event.win].w, wins[event.win].h);
                        break;
                    }
                    if(wins[event.win].help) goto help;
                    switch(event.x) {
                        case K_TAB:
                            m = !event.win ? MMENU_GLYPHS-MMENU_PROPS : GMENU_KERN-GMENU_EDIT;
                            if(event.h & 1) {
                                if(!wins[event.win].tab)
                                    wins[event.win].tab = m;
                                else
                                    wins[event.win].tab--;
                            } else {
                                wins[event.win].tab++;
                                if(wins[event.win].tab > m)
                                    wins[event.win].tab = 0;
                            }
swtab:                      if(!event.win) { glyphra = -1; gmax = 0; gsearch[0] = 0; }
                            if(event.win && wins[event.win].unicode < 33)
                                wins[event.win].tab = 0;
                            ui_resizewin(&wins[event.win], wins[event.win].w, wins[event.win].h);
                            ui_refreshwin(event.win, 0, 0, wins[event.win].w, wins[event.win].h);
                            kernactive = 10;
                        break;
                        default: goto def; break;
                    }
                } else {
                    switch(event.x) {
                        case K_UP:
                        case K_LEFT: if(saveact > 0) saveact--; else saveact=2; break;
                        case K_DOWN:
                        case K_RIGHT: if(saveact < 2) saveact++; else saveact=0; break;
                        case K_ENTER:
save:                       if(saveact==1) file_saveasc(0);
                            if(saveact==2) file_savesfn(0);
                            if(!saving) mainloop = 0;
                        break;
                    }
                    ui_refreshwin(event.win, 0, 0, wins[event.win].w, wins[event.win].h);
                }
            break;
            case E_BTNPRESS:
                if(mainloop == 1) {
                    if(wins[event.win].help) goto help;
                    if((event.w & 3) && event.y < (!event.win ? 38 : 20)) {
                        for(m=0; m < MAX_TAB && wins[event.win].tabpos[m] && wins[event.win].tabpos[m] <= event.x; m++);
                        wins[event.win].tab = (m ? m-1 : 0);
                        goto swtab;
                    }
                } else {
                    if(event.y > (wins[0].h >> 1) && event.y < (wins[0].h >> 1) + 20) {
                        saveact = event.x * 3 / wins[0].w;
                        goto save;
                    }
                }
            goto def;
            default:
def:            if(mainloop == 1 && !wins[event.win].help) {
                    m = 0;
                    if(!event.win) {
                        selall = 0;
                        switch(wins[0].tab) {
                            case MAIN_TAB_PROPS: m = ctrl_props(&wins[0], &event); break;
                            case MAIN_TAB_RANGES: m = ctrl_ranges(&wins[0], &event); break;
                            case MAIN_TAB_GLYPHS: m = ctrl_glyphs(&wins[0], &event); break;
                        }
                    } else {
                        if(wins[event.win].tab != GLYPH_TAB_EDIT || wins[event.win].menu != 5) selall = 0;
                        switch(wins[event.win].tab) {
                            case GLYPH_TAB_EDIT: m = ctrl_edit(&wins[event.win], &event); break;
                            case GLYPH_TAB_KERN: m = ctrl_kern(&wins[event.win], &event); break;
                        }
                    }
                    if(m) ui_refreshwin(event.win, 0, 0, wins[event.win].w, wins[event.win].h);
                }
            break;
        }
    }
    /* close main window (others are already closed) */
    ui_closewin(0);
    /* free resources */
    free(wins);
    free(ui_font);
    uniname_free();
    copypaste_free();
}
