/*
 * sfnedit/render.h
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Glyph renderer prototypes
 *
 */

extern int nr[512];
extern uint16_t *raster[512];

void render_bitmap(frag_t *frag, uint32_t *pix, int pw, int ph, int pp, int px, int py, int x, int y, int w, int h, uint32_t fg);
void render_pixmap(frag_t *frag, uint32_t *pix, int pw, int ph, int pp, int px, int py, int x, int y, int w, int h, uint32_t bg);
void render_contour(frag_t *frag, uint32_t *pix, int pw, int ph, int pp, int px, int py, int x, int y, int w, int h, uint32_t fg,
    uint32_t bg, int oper);
void render_glyph(int v, int chr, ui_win_t *win, int x, int y, int w, int h, uint32_t fg, uint32_t bg, int op);
void render_free();
