#!/usr/bin/php
<?php
/*
 * sfnedit/misc/icon.php
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * @brief small tool to generate icon.h
 */

$f=fopen("../icon.h","w");
fprintf($f,"/*
 * sfnedit/icon.h
 *
 * --- Generated from misc data by icon.php ---
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the \"Software\"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief various icons (window, tools, background logo etc.)
 *
 */

uint32_t wm_icon[] = {");
foreach([16,32] as $size){
    system("convert sfnedit".$size.".png icon.rgba");
    $data=file_get_contents("icon.rgba");
    unlink("icon.rgba");
    fprintf($f,"\n\t%d, %d,\n",$size,$size);
    for($i=0;$i<strlen($data);$i+=4) {
        $p = unpack("V",substr($data,$i,4))[1];
        $p = (0xFF00FF00 & $p) | (($p >> 16) & 0xFF) | (($p & 0xFF) << 16);
        fprintf($f,"0x%x,",$p);
    }
}
fprintf($f,"\n\t0, 0\n};\n\n");
fprintf($f,"uint32_t tool_icons[] = {\n");
system("convert sfnedittool.png icon.rgba");
$data=file_get_contents("icon.rgba");
unlink("icon.rgba");
for($i=0;$i<strlen($data);$i+=4) {
    $p = unpack("V",substr($data,$i,4))[1];
    $p = (0xFF00FF00 & $p) | (($p >> 16) & 0xFF) | (($p & 0xFF) << 16);
    fprintf($f,"0x%x,",$p);
}
fprintf($f,"\n};\n\nuint32_t numbers[] = {0x4AAA4,0x26222,0xc248e,0xc2c2c,0x24ae2,0xe8c2c,0x68eac,0xe2444,0x4a4a4,0x4a62c};\n\n");
fprintf($f,"uint8_t logo[] = {\n");
system("convert sfneditlogo.png icon.rgba");
$data=file_get_contents("icon.rgba");
unlink("icon.rgba");
for($i=0;$i<strlen($data);$i+=4)
    fprintf($f,"0x%x,",(unpack("V",substr($data,$i,4))[1])>>24);
fprintf($f,"\n};\n\n");
fclose($f);
