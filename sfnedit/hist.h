/*
 * sfnedit/hist.h
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Edit history
 *
 */

enum {
    HIST_ADDLAYER,
    HIST_DELLAYER,
    HIST_SETPIXEL,
    HIST_DELCONT,
    HIST_ADDCONT,
    HIST_MOVEPT,
    HIST_SETADV,
    HIST_SETBLINE,
    HIST_SETULINE,
    HIST_HINT,
    HIST_KERN,
    HIST_PASTE
};

void hist_cleanup(ui_win_t *win, int idx);
void hist_undo(ui_win_t *win);
void hist_redo(ui_win_t *win);
void hist_addlayer(ui_win_t *win, int type);
void hist_dellayer(ui_win_t *win);
void hist_setpixel(ui_win_t *win, int idx, uint8_t pixel);
void hist_delcont(ui_win_t *win);
void hist_addcont(ui_win_t *win, int type, int px, int py);
void hist_savecoords(ui_win_t *win);
void hist_setcoord(ui_win_t *win, int px, int py);
void hist_setadv(ui_win_t *win, int x, int y, int conv);
void hist_setbline(ui_win_t *win, int y);
void hist_setuline(ui_win_t *win, int y);
void hist_savepaste(ui_win_t *win);
void hist_addpaste(ui_win_t *win);
void hist_savehint(ui_win_t *win);
void hist_sethint(ui_win_t *win, int ox, int nx);
void hist_setkern(ui_win_t *win, int chr, int unicode, int dx, int dy);
