/*
 * sfnedit/kern.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Kerning (View and Controller)
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ui.h"
#include "file.h"
#include "lang.h"
#include "render.h"
#include "hist.h"
#include "util.h"
#include "unicode.h"

int kernactive = 0, kernreent = 0, kernvar[7] = {0}, kernchr[7] = {-1}, kernsel = 0, kmw, kmh, kw;
int klist[256], kmax = 0, ksmax = 0, kscroll = 0;
char ksearch[256];
extern uint32_t ssfn_dst_w;
extern int rx, ry;

/**
 * Fill in glyph list with search results
 */
void search_kern()
{
    uint32_t i, j, k;
    int l, u;

    if(!ksearch[0]) {
        if(kscroll + 1 > chars[0][kernchr[0]].klen) kscroll = chars[0][kernchr[0]].klen - 1;
        if(kscroll < 0) kscroll = 0;
        for(kmax=0;kmax + kscroll < chars[0][kernchr[0]].klen && kmax < 256;kmax++)
            klist[kmax] = chars[0][kernchr[0]].kern[(kmax+kscroll)*3];
    } else {
        k = strlen(ksearch);
        if(ksearch[0]=='U' && ksearch[1]=='+') j = gethex(ksearch+2, 6);
        else {
            j = gethex(ksearch, 6);
            for(i=0;i<k;i++)
                if(!((ksearch[i]>='0'&&ksearch[i]<='9') ||
                    (ksearch[i]>='a'&&ksearch[i]<='f') ||
                    (ksearch[i]>='A'&&ksearch[i]<='F'))) { j = 0; break; }
        }
        for(i=kmax=0;(int)i<chars[0][kernchr[0]].klen && kmax < 256;i++) {
            if((j && chars[0][kernchr[0]].kern[i*3]==(int)j) || !memcmp(utf8(chars[0][kernchr[0]].kern[i*3]), ksearch, k))
                    klist[kmax++] = chars[0][kernchr[0]].kern[i*3];
        }
        for(i=0;(int)i<chars[0][kernchr[0]].klen && kmax < 256;i++) {
            if(!search_name(ksearch, uninames[uniname(chars[0][kernchr[0]].kern[i*3])].name, 0)) {
                for(l=0;l<kmax && klist[l]!=chars[0][kernchr[0]].kern[i*3];l++);
                if(l==kmax) klist[kmax++] = chars[0][kernchr[0]].kern[i*3];
            }
        }
        for(i=0;i<UNICODE_LAST && kmax < 256;i++) {
            u = file_findchar(0, i);
            if(u != -1 && ((j && i==j) || !memcmp(utf8(i), ksearch, k))) {
                for(l=0;l<kmax && klist[l]!=(int)i;l++);
                if(l==kmax) klist[kmax++] = i;
            }
        }
        for(i=0;i<UNICODE_LAST && kmax < 256;i++) {
            u = file_findchar(0, i);
            if(u != -1 && !search_name(ksearch, uninames[uniname(i)].name, 0)) {
                for(l=0;l<kmax && klist[l]!=(int)i;l++);
                if(l==kmax) klist[kmax++] = i;
            }
        }
        if(kscroll + 1 > kmax) kscroll = kmax - 1;
        if(kscroll < 0) kscroll = 0;
    }
}

/**
 * View for kerning tab
 */
void view_kern(ui_win_t *win)
{
    int i, j, k, x, y, s = 16 << font->quality, kx, ky;
    char tmp[9];

    kernreent++;
redraw:
    for(i=x=0;i<7;i++) x += kernvar[i];
    if(!x) kernvar[0] = 1;
    ssfn_y = 24;
    x = (win->w - 4) / 8;
    kmw = kmh = 0;
    for(i=0;i<7;i++) {
        ssfn_x = 2 + i*x;
        ssfn_dst_w = ssfn_x + x - 2;
        kernchr[i] = file_findchar(i, win->unicode);
        ui_bool(win, lang[LOCAL0 + i], kernvar[i], kernchr[i] == -1 ? -1 : kernactive==i);
        if(kernchr[i] != -1) {
            if(chars[i][kernchr[i]].adv_x > kmw) kmw = chars[i][kernchr[i]].adv_x;
            if(chars[i][kernchr[i]].adv_y > kmh) kmh = chars[i][kernchr[i]].adv_y;
            if(chars[i][kernchr[i]].w-chars[i][kernchr[i]].bear_l > kmw) kmw = chars[i][kernchr[i]].w-chars[i][kernchr[i]].bear_l;
            if(chars[i][kernchr[i]].h-chars[i][kernchr[i]].bear_t > kmh) kmh = chars[i][kernchr[i]].h-chars[i][kernchr[i]].bear_t;
        }
    }
    ssfn_x = 2 + 7*x;
    ssfn_dst_w = win->w;
    ui_bool(win, "RTL", chars[0][kernchr[0]].rtl, chars[0][kernchr[0]].adv_y ? -1 : kernactive==7);
    if(kernactive == 8 && kernreent == 2) kernsel = kscroll = kmax = 0;
    if(!kmax || !ksearch[0]) search_kern();
    ssfn_y = 72;
    ssfn_bg = theme[THEME_BG];
    for(i=!ksearch[0]?0:kscroll,ksmax=0;i<kmax && (int)ssfn_y < win->h - 16; i++,ksmax++) {
        ssfn_x = 2;
        ssfn_fg = theme[i==kernsel && kernactive==9?THEME_INPUT:THEME_INACT];
        ui_box(win, ssfn_x, ssfn_y, 16, 16, ssfn_bg);
        ssfn_putc(klist[i]);
        ssfn_x = 18;
        sprintf(tmp,"U+%06X",klist[i]);
        ui_text(win, tmp, ssfn_bg, ssfn_bg);
        ssfn_y += 16;
    }
    if((int)ssfn_y < win->h - 16) ui_box(win, 2, ssfn_y, 96, win->h-ssfn_y, ssfn_bg); else ksmax--;
    for(kx=ky=i=0;i<chars[0][kernchr[0]].klen;i++) {
        if(chars[0][kernchr[0]].kern[i*3] == klist[kernsel]) {
            kx = chars[0][kernchr[0]].kern[i*3+1];
            ky = chars[0][kernchr[0]].kern[i*3+2];
            break;
        }
    }
    ssfn_fg = ssfn_bg = theme[THEME_INACT];
    x = 128; y = 54;
    ui_box(win, x, y-6, win->w - x, 6, theme[THEME_BG]);
    ui_box(win, x-16, y, 16, win->h-y, theme[THEME_BG]);
    if(chars[0][kernchr[0]].adv_y) {
        kw = (win->h - y);
        if(win->w - x < kw) kw = win->w - x;
        ui_box(win, x, y, kw, 2*kw, theme[kernactive==10?THEME_INPBG:THEME_INPBG]);
        ui_box(win, x+kw/2, y, 1, 2*kw, theme[THEME_BASELINE]);
        ui_number(win, kmh, x-16, y+(kmh*kw+(s/2))/s, theme[THEME_ADVANCE]);
        ui_number(win, kx, x+4+kw/2-16, y-6, theme[THEME_FG]);
        ui_number(win, ky, x-16, y+6+(kmh*kw+(s/2))/s, theme[THEME_FG]);
        ui_box(win, x, y+4+(kmh*kw+(s/2))/s, kw, 1, theme[THEME_ADVANCE]);
        for(i=0;i<7;i++)
            if(kernchr[i] != -1 && kernvar[i]) {
                render_glyph(i, kernchr[i], win, x+kw/2-(((chars[i][kernchr[i]].w - chars[i][kernchr[i]].bear_l)/2 +
                    chars[i][kernchr[i]].bear_l)*kw + (s/2))/s,
                    y + 4 + (kmh*kw+(s/2))/s - ((chars[i][kernchr[i]].adv_y+chars[i][kernchr[i]].bear_t)*kw+(s/2))/s,
                    kw, kw, ssfn_fg, ssfn_bg, 0);
            }
        if(kmax && (j = file_findchar(0, klist[kernsel])) != -1) {
            if(kernactive==10) ssfn_fg = ssfn_bg = theme[THEME_INPUT];
            render_glyph(0, j, win, x+kw/2-(((chars[0][j].w - chars[0][j].bear_l)/2 + chars[0][j].bear_l)*kw + (s/2))/s +
                (((kx << (16 - (4+font->quality))) * kw + (1 << 16) - 1) >> 16),
                y + 4 + (kmh*kw+(s/2))/s - ((chars[0][j].bear_t)*kw+(s/2))/s +
                (((ky << (16 - (4+font->quality))) * kw + (1 << 16) - 1) >> 16), kw, kw, ssfn_fg, ssfn_bg, 0);
        }
    } else {
        kw = (win->w - x);
        if(win->h - y < kw) kw = win->h - y;
        ui_box(win, x, y, win->w - x, kw, theme[kernactive==10?THEME_INPBG:THEME_INPBG]);
        ui_box(win, x, y+(font->baseline*kw+(s/2))/s, win->w - x, 1, theme[THEME_BASELINE]);
        ui_number(win, kmw, chars[0][kernchr[0]].rtl?win->w-15-(kmw*kw+(s/2))/s:x-14+(kmw*kw+(s/2))/s, y-6,
            theme[THEME_ADVANCE]);
        ui_number(win, kx, chars[0][kernchr[0]].rtl?win->w-31-(kmw*kw+(s/2))/s:x+2+(kmw*kw+(s/2))/s, y-6, theme[THEME_FG]);
        ui_number(win, ky, x-16, y-6+kw/2+(0*kw+(s/2))/s, theme[THEME_FG]);
        ui_box(win, chars[0][kernchr[0]].rtl?win->w-1-(kmw*kw+(s/2))/s:x+(kmw*kw+(s/2))/s, y, 1, kw, theme[THEME_ADVANCE]);
        for(i=0;i<7;i++)
            if(kernchr[i] != -1 && kernvar[i]) {
                if(chars[0][kernchr[0]].rtl) {
                    render_glyph(i, kernchr[i], win, win->w - 1 - (chars[i][kernchr[i]].w*kw + (s/2))/s, y,
                        kw, kw, ssfn_fg, ssfn_bg, 0);
                } else {
                    render_glyph(i, kernchr[i], win, x + (kmw*kw + (s/2))/s -
                        (chars[i][kernchr[i]].adv_x*kw+(s/2))/s - (chars[i][kernchr[i]].bear_l*kw+(s/2))/s,
                        y, kw, kw, ssfn_fg, ssfn_bg, 0);
                }
            }
        if(kmax && (j = file_findchar(0, klist[kernsel])) != -1) {
            if(kernactive==10) ssfn_fg = ssfn_bg = theme[THEME_INPUT];
            if(chars[0][kernchr[0]].rtl) {
                k = win->w-1-(kmw*kw+(s/2))/s - ((chars[0][j].w)*kw + (s/2))/s -
                    (((kx << (16 - (4+font->quality))) * kw + (1 << 16) - 1) >> 16);
                if(k < x) { rx = x-k; k=x; }
                render_glyph(0, j, win, k, y+(((ky << (16 - (4+font->quality))) * kw + (1 << 16) - 1) >> 16), kw, kw,
                    ssfn_fg, ssfn_bg, 0);
                rx = 0;
            } else
                render_glyph(0, j, win, x+(kmw*kw + (s/2))/s - ((chars[0][j].bear_l)*kw+(s/2))/s +
                    (((kx << (16 - (4+font->quality))) * kw + (1 << 16) - 1) >> 16), y +
                    (((ky << (16 - (4+font->quality))) * kw + (1 << 16) - 1) >> 16), kw, kw,
                    ssfn_fg, ssfn_bg, 0);
        }
    }

    ssfn_x = 2; ssfn_y = 48;
    ui_input(win, ksearch, 72, 1, 0);
    ui_button(win, "⚲", 8, 2, kernactive == 8);
    ssfn_x = 2;
    if((i=ui_input(win, ksearch, 72, 5, kernactive == 8 && kernreent == 1))) {
        kernactive += i;
        while(i == -1 && kernactive < 7 && kernchr[kernactive] == -1) kernactive--;
        ui_resizewin(win, win->w, win->h);
        ui_tabs(win, ui_getwin(win->winid));
        kmax = 0;
        goto redraw;
    }
    kernreent--;
}

/**
 * Controller for kerning tab
 */
int ctrl_kern(ui_win_t *win, ui_event_t *evt)
{
    switch(evt->type) {
        case E_KEY:
            switch(evt->x) {
                case K_LEFT:
                    if(kernactive == 10 && !evt->h) {
                        hist_setkern(win, kernchr[0], klist[kernsel], -1, 0);
                    }
                break;
                case K_RIGHT:
                    if(kernactive == 10 && !evt->h) {
                        hist_setkern(win, kernchr[0], klist[kernsel], 1, 0);
                    }
                break;
                case K_UP:
                    if(kernactive == 10 && !evt->h) {
                        hist_setkern(win, kernchr[0], klist[kernsel], 0, -1);
                    } else
                    if(kernactive == 9 && !evt->h) {
                        if((!kernsel && !kscroll) || !kmax) kernactive = 8;
                        else {
                            if(!ksearch[0]) {
                                if(!kernsel) kscroll--;
                                else kernsel--;
                            } else {
                                if(kernsel == kscroll) kscroll--;
                                kernsel--;
                            }
                            if(kernsel < 0) kernsel = 0;
                            if(kscroll < 0) kscroll = 0;
                            if(!ksearch[0]) kmax = 0;
                        }
                    } else {
                        if(kernactive > 0) kernactive--; else kernactive = 10;
                        if(kernactive == 7 && chars[0][kernchr[0]].adv_y) kernactive--;
                        while(kernactive < 7 && kernchr[kernactive] == -1) kernactive--;
                    }
                break;
                case K_DOWN:
                    if(kernactive == 10 && !evt->h) {
                        hist_setkern(win, kernchr[0], klist[kernsel], 0, 1);
                    } else
                    if(kernactive == 9 && !evt->h) {
                        if(!ksearch[0]) {
                            if(kernsel >= ksmax) kscroll++;
                            else kernsel++;
                        } else {
                            if(kernsel-kscroll >= ksmax) kscroll++;
                            kernsel++;
                        }
                        if(kernsel + 1 > kmax) kernsel = kmax ? kmax - 1 : 0;
                        if(kscroll + ksmax > kmax) kscroll = kmax - ksmax;
                        if(kscroll < 0) kscroll = 0;
                        if(!ksearch[0]) kmax = 0;
                    } else {
                        if(kernactive < 10) kernactive++; else kernactive = 0;
                        while(kernactive < 7 && kernchr[kernactive] == -1) kernactive++;
                        if(kernactive == 7 && chars[0][kernchr[0]].adv_y) kernactive++;
                    }
                break;
                case K_ENTER:
                case ' ':
                    if(kernactive < 7) kernvar[kernactive] ^= 1;
                    if(kernactive == 7) chars[0][kernchr[0]].rtl ^= 1;
                    if(kernactive == 9) kernactive = 10;
                break;
                case 'z':
                    hist_undo(win);
                    if(win->bg) free(win->bg);
                    win->bg = NULL;
                    ui_resizewin(win, win->w, win->h);
                break;
                case 'y':
                    hist_redo(win);
                    if(win->bg) free(win->bg);
                    win->bg = NULL;
                    ui_resizewin(win, win->w, win->h);
                break;
                case 'r':
                    chars[0][kernchr[0]].rtl ^= 1;
                break;
                default:
                    if(!(evt->h & ~1)) {
                        ksearch[0] = 0;
                        if(evt->x >= 32) memcpy(ksearch, &evt->x, strlen((char*)&evt->x)+1);
                    }
                break;
            }
            return 1;
        case E_MOUSEMOVE:
        break;
        case E_BTNPRESS:
            if(evt->y > 24 && evt->y < 48) {
                kernactive = evt->x / ((win->w - 4) / 8);
                if(kernactive == 7 && chars[0][kernchr[0]].adv_y) kernactive--;
                while(kernactive < 7 && kernchr[kernactive] == -1) kernactive++;
                if(kernactive < 7) kernvar[kernactive] ^= 1;
                if(kernactive == 7) chars[0][kernchr[0]].rtl ^= 1;
                return 1;
            } else if(evt->y > 48) {
                if(evt->x < 96) {
                    if(evt->y < 72)
                        kernactive = 8;
                    else {
                        kernactive = 9;
                        if(evt->w & 1) {
                            kernsel = (evt->y-72)/16;
                            if(kernsel>ksmax) kernsel = ksmax;
                            if(ksearch[0]) kernsel += kscroll;
                        } else
                        if(evt->w & 8) {
                            if(kscroll) {
                                kscroll--;
                                if(ksearch[0]) kernsel--;
                            }
                            if(!ksearch[0]) kmax = 0;
                        } else
                        if(evt->w & 16) {
                            if(kmax && kscroll + ksmax < kmax) {
                                kscroll++;
                                if(ksearch[0]) kernsel++;
                            }
                            if(!ksearch[0]) kmax = 0;
                        }
                    }
                } else if(kernactive != 10)
                    kernactive = 10;
                else
                if(evt->x > 128 && evt->y > 54) {
                }
                return 1;
            }
        break;
        case E_BTNRELEASE:
        break;
    }
    return 0;
}
