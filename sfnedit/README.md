Scalable Screen Font Editor
===========================

A little application to read and modify [SSFN](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/sfn_format.md) or
[ASC](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/asc_format.md) files with GUI.

Read the user manual on this tool in the [documentation](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/editor.md).

Compilation Dependencies
------------------------

Uses SDL2 by default, so it should be easy to port. If SDL not found, it fallbacks to X11. It
autodetects zlib too. If found, then the editor can read and write gzip compressed files. If not,
then decompression will be still available by a built-in tiny inflate library (also used for
several internal data), but no gzip compression will be possible for writes. Other than those,
requires libc only.

### Under Windows

You'll need a couple of tools, here's a step-by-step how to:
1. install [MinGW](https://osdn.net/projects/mingw/releases), this will give you gcc and zlib under Windows
2. download [SDL2-devel-X-mingw.tar.gz](http://libsdl.org/download-2.0.php) under the section Development Libraries
3. extract SDL2 into a directory under MinGW's home directory
4. open Makefile in Notepad, and edit MINGWSDL to the path where you've extracted the tarball, add the last SDL2-X part too
5. copy $(MINGWSDL)/i686-w64-mingw32/bin/SDL2.dll into C:\Windows
6. run `make`

On Windows, if command line argument is not specified, the editor will fire up an Open File dialog. There'll be no Save File
dialog when you press \[Ctrl\]+\[S\], sfnedit will save the font to that file.
