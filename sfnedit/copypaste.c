/*
 * sfnedit/copypaste.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Copy'n'paste functions
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include "lang.h"
#include "ui.h"
#include "file.h"
#include "util.h"

char *clipboardfn = NULL;

/**
 * Get clipboard file name
 */
void copypaste_init()
{
    char *home = getenv("HOME");
    char *name = getenv("LOGNAME");
    struct stat st;

    if(clipboardfn) return;
    clipboardfn = malloc((home?strlen(home):(name?strlen(name):4))+32);
    if(!clipboardfn) error("copypaste_init", ERR_MEM);
    if(!home) {
        sprintf(clipboardfn, "/home/%s", name);
        if(stat(clipboardfn, &st))
            sprintf(clipboardfn, "/tmp/sfnclipbrd.%s", name?name:"UNKNOWN");
        else {
            sprintf(clipboardfn, "/home/%s/.cache", name);
            goto mdir;
        }
    } else {
        sprintf(clipboardfn, "%s/.cache", home);
mdir:
#ifdef __WIN32__
        mkdir(clipboardfn);
#else
        mkdir(clipboardfn, 0700);
#endif
        strcat(clipboardfn, "/sfnclipbrd");
    }
}

/**
 * Clear clipboard
 */
void copypaste_clear()
{
    FILE *f;
    f = fopen(clipboardfn, "w");
    if(f) fclose(f);
}

/**
 * Save character range to clipboard
 */
void copypaste_copyrange(int v, int chr)
{
    FILE *f;
    f = fopen(clipboardfn, "a");
    if(f) {
        fclose(f);
    }
}

/**
 * Paste character range from clipboard
 */
void copypaste_pasterange(int v, int pos)
{
    FILE *f;
    f = fopen(clipboardfn, "a");
    if(f) {
        fclose(f);
    }
}

/**
 * Free resources
 */
void copypaste_free()
{
    /* do not delete the clipboard file as another sfnedit instance might be using it */
    if(clipboardfn) free(clipboardfn);
}
