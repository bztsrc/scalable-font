/*
 * sfnedit/file.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief SSFN file functions (Model)
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lang.h"
#include "ui.h"
#include "file.h"
#include "util.h"
#include "unicode.h"
#if HAS_ZLIB
#include <zlib.h>
#endif
#ifndef va_start
#define va_start __builtin_va_start
#define va_list __builtin_va_list
#define va_end(x)
#endif

extern char gz;
ssfn_font_t *font = NULL;
char *fontfile = NULL, *fontfilebn = NULL, *fontfileext = NULL, gzipped = 0;
char *strtable[6];
uint32_t cmap[241];
uint8_t *cpal = (uint8_t*)&cmap, cpalidx[241], cpalrev[241];
int modified = 0;
#if HAS_ZLIB
gzFile sg;
#endif
FILE *sf;

int numchars[SSFN_NUMVARIANTS] = {0};
char_t *chars[SSFN_NUMVARIANTS] = {NULL};

int numfrags = 0;
frag_t *frags = NULL;

int numkerns = 0;

/**
 * Sort characters by UNICODE
 */
int file_chrsort(const void *a, const void *b)
{
    return ((char_t*)a)->unicode - ((char_t*)b)->unicode;
}

/**
 * Sort color map entries
 */
int file_cpalsrt(const void *a, const void *b)
{
    /* zero means undefined color, send to back */
    if(!cpal[*((uint8_t*)a)*4+0] && !cpal[*((uint8_t*)a)*4+1] && !cpal[*((uint8_t*)a)*4+2]) return 1;
    if(!cpal[*((uint8_t*)b)*4+0] && !cpal[*((uint8_t*)b)*4+1] && !cpal[*((uint8_t*)b)*4+2]) return -1;
    /* green is most significant, alpha is the least */
    return
        ((((int)cpal[*((uint8_t*)a)*4+1]<<3) + ((int)cpal[*((uint8_t*)a)*4+0]<<1) +
          ((int)cpal[*((uint8_t*)a)*4+2]<<1) + (int)cpal[*((uint8_t*)a)*4+3])>>2) -
        ((((int)cpal[*((uint8_t*)b)*4+1]<<3) + ((int)cpal[*((uint8_t*)b)*4+0]<<1) +
          ((int)cpal[*((uint8_t*)b)*4+2]<<1) + (int)cpal[*((uint8_t*)b)*4+3])>>2);
}

/**
 * Reorder color map and replace index references
 */
void file_sortcmap()
{
    int i, j, s = 16 << font->quality;
    uint32_t cmap2[240];

    for(i=0;i<241;i++) cpalidx[i] = i;
    qsort(cpalidx, 240, 1, file_cpalsrt);
    for(i=0;i<241;i++) cpalrev[cpalidx[i]]=i;
    memcpy(cmap2, cmap, sizeof(cmap) - 4);
    for(i=0;i<240;i++) cmap[i] = cmap2[cpalidx[i]];

    for(i=0;i<numfrags;i++) {
        if(frags[i].type == SSFN_FRAG_CONTOUR) {
            for(j=0;j<frags[i].len;j++) {
                if(frags[i].data[j].type == SSFN_CONTOUR_COLOR)
                    frags[i].data[j].px = cpalrev[frags[i].data[j].px];
            }
        }
        if(frags[i].type == SSFN_FRAG_PIXMAP) {
            for(j=0;j<s*s;j++)
                ((uint8_t*)frags[i].data)[j] = cpalrev[((uint8_t*)frags[i].data)[j]];
        }
    }
}

/**
 * Add a fragment to list
 */
int file_fragadd(int type)
{
    int i = -1, j, s = 16 << font->quality;
    for(j=0; modified && j < numfrags; j++)
        if(frags[j].type == -1) { i = j; break; }
    if(i == -1) {
        i = numfrags++;
        frags = (frag_t*)realloc(frags, numfrags*sizeof(frag_t));
        if(!frags) error("file_fragadd", ERR_MEM);
    }
    memset(&frags[i], 0, sizeof(frag_t));
    frags[i].type = type;
    frags[i].color[0] = frags[i].color[1] = frags[i].color[2] = frags[i].color[3] = 240;
    if(type == SSFN_FRAG_BITMAP) {
        frags[i].data = (cont_t*)malloc(s*s/8);
        if(!frags[i].data) error("file_fragadd", ERR_MEM);
        memset(frags[i].data, 0, s*s/8);
    } else
    if(type == SSFN_FRAG_PIXMAP) {
        frags[i].data = (cont_t*)malloc(s*s);
        if(!frags[i].data) error("file_fragadd", ERR_MEM);
        memset(frags[i].data, 0xF0, s*s);
    }
    return i;
}

/**
 * Delete a fragment from list
 */
void file_delfrag(int idx)
{
    if(idx < 0 || idx >= numfrags || frags[idx].type == -1) return;

    frags[idx].type = -1;
    if(frags[idx].data)
        free(frags[idx].data);
    frags[idx].len = 0;
    frags[idx].data = NULL;
}

/**
 * Return a character for UNICODE
 */
int file_findchar(int v, uint32_t unicode)
{
    register int i=0, j, k, l=22;

    if(v < 0 || v >= SSFN_NUMVARIANTS || !numchars[v] || unicode > UNICODE_LAST) return -1;
    j = numchars[v]-1;
    while(l--) {
        k = i + ((j-i) >> 1);
        if(chars[v][k].unicode == unicode) return k;
        if(i >= j) break;
        if(chars[v][k].unicode < unicode) i = k + 1; else j = k;
    }
    return -1;
}

/**
 * Find a character for UNICODE and add a new one if not found
 */
int file_addchar(int v, uint32_t unicode, int check)
{
    int i, j;

    if(v < 0 || v >= SSFN_NUMVARIANTS || unicode > UNICODE_LAST) return -1;

    if(check) {
        i = file_findchar(v, unicode);
        if(i != -1) return i;
    }

    if(!v)
        for(i=0; i < UNICODE_NUMBLOCKS; i++)
            if(ublocks[i].start <= (int)unicode && ublocks[i].end >= (int)unicode) { ublocks[i].cnt++; break; }

    i = numchars[v]++;
    chars[v] = (char_t*)realloc(chars[v], numchars[v]*sizeof(char_t));
    if(!chars[v]) error("file_addchar", ERR_MEM);
    memset(&chars[v][i], 0, sizeof(char_t));
    chars[v][i].unicode = unicode;
    j = uniname(unicode);
    if(!v && j != -1) chars[0][i].rtl = uninames[j].rtl;
    chars[v][i].bear_t = chars[v][i].bear_l = 65536;
    if(check) {
        qsort(chars[v], numchars[v], sizeof(char_t), file_chrsort);
        ui_updatewin(v, unicode, +1);
        return file_findchar(v, unicode);
    }
    return i;
}

/**
 * Remove a character
 */
void file_delchar(int v, int chr)
{
    int i;

    if(v < 0 || v >= SSFN_NUMVARIANTS || chr < 0 || chr >= numchars[v]) return;

    if(chars[v][chr].len) {
        for(i = 0; i < chars[v][chr].len; i++)
            file_delfrag(chars[v][chr].frags[i]);
        modified = 1;
    }
    if(chars[v][chr].frags)
        free(chars[v][chr].frags);
    if(chars[v][chr].kern)
        free(chars[v][chr].kern);

    if(!v)
        for(i=0; i < UNICODE_NUMBLOCKS; i++)
            if(ublocks[i].start <= (int)chars[0][chr].unicode && ublocks[i].end >= (int)chars[0][chr].unicode)
                { ublocks[i].cnt--; break; }
    ui_updatewin(v, chars[v][chr].unicode, -1);
    chars[v][chr].unicode = UNICODE_LAST + 1;
    qsort(chars[v], numchars[v], sizeof(char_t), file_chrsort);
    numchars[v]--;
    chars[v] = (char_t*)realloc(chars[v], numchars[v]*sizeof(char_t));
    if(!chars[v]) error("file_delchar", ERR_MEM);
    modified = 1;
}

/**
 * Add a contour fragment
 */
void file_contadd(char_t *chr, int fragidx, int type, int px, int py, int c1x, int c1y, int c2x, int c2y)
{
    int i;
    frag_t *f = &frags[fragidx];
    cont_t *c;

    i = f->len++;
    f->data = (cont_t*)realloc(f->data, f->len*sizeof(cont_t));
    if(!f->data) error("file_contadd", ERR_MEM);
    c = &f->data[i];
    memset(c, 0, sizeof(cont_t));
    c->type = type;
    c->px = px;
    c->py = py;
    c->c1x = c1x;
    c->c1y = c1y;
    c->c2x = c2x;
    c->c2y = c2y;
    if(type != SSFN_CONTOUR_COLOR && chr) {
        if(px < chr->bear_l) chr->bear_l = px;
        if(py < chr->bear_t) chr->bear_t = py;
        if(px > chr->w) chr->w = px;
        if(py > chr->h) chr->h = py;
        if(type > 1) {
            if(c1x < chr->bear_l) chr->bear_l = c1x;
            if(c1y < chr->bear_t) chr->bear_t = c1y;
            if(c1x > chr->w) chr->w = c1x;
            if(c1y > chr->h) chr->h = c1y;
            if(type > 2) {
                if(c2x < chr->bear_l) chr->bear_l = c2x;
                if(c2y < chr->bear_t) chr->bear_t = c2y;
                if(c2x > chr->w) chr->w = c2x;
                if(c2y > chr->h) chr->h = c2y;
            }
        }

    }
}

/**
 * Add a new and empty fragment to character
 */
int file_addemptyfrag(char_t *chr, int type, int fragidx)
{
    int i;

    for(i=0;i<chr->len;i++)
        if(chr->frags[i] == fragidx || (type != SSFN_FRAG_CONTOUR && frags[chr->frags[i]].type == type))
            return i;

    i = chr->len; chr->len++;
    chr->frags = (int*)realloc(chr->frags, (chr->len)*sizeof(int));
    if(!chr->frags) error("file_addemptyfrag", ERR_MEM);
    chr->frags[i] = fragidx >= 0 ? fragidx : file_fragadd(type);
    return i;
}

/**
 * Remove a fragment from a character
 */
void file_removefrag(char_t *chr, int idx)
{
    int i;

    if(idx < 0 || idx >= chr->len || chr->len == 0) return;

    for(i = idx; i + 1 < chr->len; i++)
        chr->frags[i] = chr->frags[i+1];
    chr->len--;
    if(!chr->len) {
        free(chr->frags);
        chr->frags = NULL;
    } else {
        chr->frags = (int*)realloc(chr->frags, (chr->len)*sizeof(int));
        if(!chr->frags) error("file_removefrag", ERR_MEM);
    }
}

/**
 * Read and parse a fragment from compressed SSFN data
 */
void file_addrawfrag(char_t *chr, uint8_t* raw, int ox, int oy)
{
    int i, j, l, n, t, x, y, a, b, c, d, s = 16 << font->quality;
    unsigned char *ra, *re, *data;

    if(raw[0] & 0x80) {
        switch((raw[0] & 0x60)>>5) {
            case SSFN_FRAG_LBITMAP:
                x = ((((raw[0]>>2)&3)<<8)+raw[1])+1;
                y = (((raw[0]&3)<<8)|raw[2])+1;
                raw += 3;
                goto bitmap;

            case SSFN_FRAG_BITMAP:
                x = (raw[0] & 0x1F)+1;
                y = raw[1]+1;
                raw += 2;
bitmap:         for(i=0,l=-1;i<chr->len;i++)
                    if(frags[chr->frags[i]].type == SSFN_FRAG_BITMAP) { l = i; break; }
                if(l == -1) {
                    l = chr->len; chr->len++;
                    chr->frags = (int*)realloc(chr->frags, (chr->len)*sizeof(int));
                    if(!chr->frags) error("file_addrawfrag", ERR_MEM);
                    chr->frags[l] = file_fragadd(SSFN_FRAG_BITMAP);
                }
                data = (unsigned char*)frags[chr->frags[l]].data;
                if((ox & ~7) < chr->bear_l) chr->bear_l = ox & ~7;
                if(oy < chr->bear_t) chr->bear_t = oy;
                if(oy+y-1 > chr->h) chr->h = oy+y-1;
                b = s >> 3;
                for(j=0; j < y && oy + j < s; j++)
                    for(i=0; i < x && i < b; i++) {
                            if(*raw && (ox & ~7)+(x<<3)-1 > chr->w) chr->w = (ox & ~7)+(x<<3)-1;
                            data[(oy + j) * b + i] = *raw++;
                    }
            break;

            case SSFN_FRAG_PIXMAP:
                x = (((raw[0] & 12) << 6) | raw[1])+1;
                y = (((raw[0] & 3) << 8) | raw[2])+1;
                n = ((raw[4]<<8) | raw[3])+1;
                raw += 5;
                if(raw[-5] & 0x10) {
                    /* todo: direct ARGB values in pixmap fragment */
                }
                a = x * y;
                ra = (unsigned char *)malloc(a);
                if(!ra) error("file_addrawfrag", ERR_MEM);
                for(re = raw+n, i=0; i < a && raw < re;) {
                    c = (raw[0] & 0x7F)+1;
                    if(raw[0] & 0x80) { for(j=0; j < c; j++) ra[i++] = raw[1]; raw += 2; }
                    else { raw++; for(j=0; j < c; j++) ra[i++] = *raw++; }
                }
                for(i=0,l=-1;i<chr->len;i++)
                    if(frags[chr->frags[i]].type == SSFN_FRAG_PIXMAP) { l = i; break; }
                if(l == -1) {
                    l = chr->len; chr->len++;
                    chr->frags = (int*)realloc(chr->frags, (chr->len)*sizeof(int));
                    if(!chr->frags) error("file_addrawfrag", ERR_MEM);
                    chr->frags[l] = file_fragadd(SSFN_FRAG_PIXMAP);
                }
                data = (unsigned char*)frags[chr->frags[l]].data;
                re = ra;
                for(j=0; j < y && oy + j < s; j++)
                    for(i=0; i < x && ox + i < s; i++) {
                        if(*re < 240) {
                            if(ox+i < chr->bear_l) chr->bear_l = ox+i;
                            if(oy+j < chr->bear_t) chr->bear_t = oy+j;
                            if(ox+i > chr->w) chr->w = ox+i;
                            if(oy+j > chr->h) chr->h = oy+j;
                        }
                        data[(oy + j) * s + ox + i] = *re++;
                    }
                free(ra);
            break;

            case SSFN_FRAG_HINTING:
/*
                if(raw[0] & 0x10) { n = ((raw[0]&0xF)<<8) | raw[1]; raw += 2; } else { n = raw[0] & 0xF; raw++; }
                y = 4096; x = ((ox >> s) - 1) << (s-4);
                chr->h[y++] = x;
                for(n++;n-- && x < 4096;) {
                    x = raw[0]; raw++;
                    if(font->features & SSFN_FEAT_HBIGCRD) { x |= (raw[0] << 8); raw++; }
                    x <<= (s-4);
                    chr->h[y++] = x;
                }
                if(y < 4096) chr->h[y++] = 65535;
*/
            break;
        }
    } else {
        if(raw[0] & 0x40) {
            l = ((raw[0] & 0x3F) << 8) | raw[1];
            raw += 2;
        } else {
            l = raw[0] & 0x3F;
            raw++;
        }
        l++;
        if(font->quality < 5) {
            x = raw[0] & 0xFF; y = raw[1] & 0xFF;
            raw += 2;
        } else {
            x = ((raw[0] & 15) << 8) | raw[1]; y = ((raw[0] & 0xF0) << 4) | raw[2];
            raw += 3;
        }
        j=chr->len; chr->len++;
        chr->frags = (int*)realloc(chr->frags, (chr->len)*sizeof(int));
        if(!chr->frags) error("file_addrawfrag", ERR_MEM);
        chr->frags[j] = file_fragadd(SSFN_FRAG_CONTOUR);
        file_contadd(chr, chr->frags[j], SSFN_CONTOUR_MOVE,  ox+x, oy+y, 0,0, 0,0);
        for(i=0;i<l;i++) {
            t = font->quality < 4 ? (raw[0] >> 7) | ((raw[1] >> 6) & 2) : raw[0] & 3;
            x = y = a = b = c = d = 0;
            switch(font->quality) {
                case 0:
                case 1:
                case 2:
                case 3:
                    x = raw[0] & 0x7F; y = raw[1] & 0x7F;
                    switch(t) {
                        case 0: a = ((x & 1) << 7) | y;
                            if(raw[0] & 4) { b = raw[2]; c = raw[3]; d = raw[4]; raw += 5; } else { b=c=d=a; raw += 2; } break;
                        case 1: raw += 2; break;
                        case 2: a = raw[2] & 0x7F; b = raw[3] & 0x7F; raw += 4; break;
                        case 3: a = raw[2] & 0x7F; b = raw[3] & 0x7F; c = raw[4] & 0x7F; d = raw[5] & 0x7F; raw += 6; break;
                    }
                break;

                case 4:
                    x = raw[1]; y = raw[2];
                    switch(t) {
                        case 0: a = raw[1];
                            if(raw[0] & 4) { b = raw[2]; c = raw[3]; d = raw[4]; raw += 5; } else { b=c=d=a; raw += 2; } break;
                        case 1: raw += 3; break;
                        case 2: a = raw[3]; b = raw[4]; raw += 5; break;
                        case 3: a = raw[3]; b = raw[4]; c = raw[5]; d = raw[6]; raw += 7; break;
                    }
                break;

                case 5:
                    x = ((raw[0] & 4) << 6) | raw[1]; y = ((raw[0] & 8) << 5) | raw[2];
                    switch(t) {
                        case 0: a = raw[1];
                            if(raw[0] & 4) { b = raw[2]; c = raw[3]; d = raw[4]; raw += 5; } else { b=c=d=a; raw += 2; } break;
                        case 1: raw += 3; break;
                        case 2: a = ((raw[0] & 16) << 4) | raw[3]; b = ((raw[0] & 32) << 3) | raw[4]; raw += 5; break;
                        case 3: a = ((raw[0] & 16) << 4) | raw[3]; b = ((raw[0] & 32) << 3) | raw[4];
                            c = ((raw[0] & 64) << 2) | raw[5]; d = ((raw[0] & 128) << 1) | raw[6]; raw += 7; break;
                    }
                break;

                default:
                    x = ((raw[0] & 12) << 6) | raw[1]; y = ((raw[0] & 48) << 4) | raw[2];
                    switch(t) {
                        case 0: a = raw[1];
                            if(raw[0] & 4) { b = raw[2]; c = raw[3]; d = raw[4]; raw += 5; } else { b=c=d=a; raw += 2; } break;
                        case 1: raw += 3; break;
                        case 2: a = ((raw[3] & 3) << 8) | raw[4]; b = ((raw[3] & 12) << 6) | raw[5]; raw += 6; break;
                        case 3: a = ((raw[3] & 3) << 8) | raw[4]; b = ((raw[3] & 12) << 6) | raw[5];
                            c = ((raw[3] & 48) << 4) | raw[6]; d = ((raw[3] & 192) << 2) | raw[7]; raw += 8; break;
                    }
                break;
            }
            switch(t) {
                case 0: file_contadd(chr, chr->frags[j], SSFN_CONTOUR_COLOR, a, b, c, d, 0,0); break;
                case 1: file_contadd(chr, chr->frags[j], SSFN_CONTOUR_LINE,  ox+x, oy+y, 0,0, 0,0); break;
                case 2: file_contadd(chr, chr->frags[j], SSFN_CONTOUR_QUAD,  ox+x,oy+y,ox+a,oy+b, 0,0); break;
                case 3: file_contadd(chr, chr->frags[j], SSFN_CONTOUR_CUBIC, ox+x,oy+y,ox+a,oy+b,ox+c,oy+d); break;
            }
        } /* for numfrags */
    }
}

/**
 * Load a font file
 */
#define findandskip(x) do{while(ptr < end && *ptr!=x){ptr++;};while(ptr < end && *ptr==x){ptr++;};}while(0)
void file_load(char *filename)
{
    unsigned int size;
    int i, j, k, l, n, x, y, v, L, T, par[6], p, s;
    char *str;
    uint8_t *ptr, *end, *e, *bitmap, cmd;
    char_t *c;

    i = strlen(filename);
    fontfile = malloc(i+1);
    if(!fontfile) error("file_load", ERR_MEM);
    memcpy(fontfile, filename, i+1);
    for(fontfilebn = fontfile + i; fontfilebn > fontfile && *(fontfilebn-1) != '/'; fontfilebn--);
    for(fontfileext = fontfile + i; fontfileext > fontfilebn && (*(fontfileext-1) != '.' || *fontfileext == 'g'); fontfileext--);

    ptr = load_file(fontfile, (int*)&size);
    /* new file */
    if(!ptr || !size) {
        font = (ssfn_font_t*)malloc(sizeof(ssfn_font_t));
        if(!font) error("file_load", ERR_MEM);
        memset(font, 0, sizeof(ssfn_font_t));
        memcpy(font->magic, SSFN_MAGIC, 4);
        font->quality = 4;
        for(i = 0; i < 6; i++) {
            strtable[i] = (char*)malloc(256);
            if(!strtable[i]) error("file_load", ERR_MEM);
            memset(strtable[i], 0, 256);
        }
        return;
    }
    gzipped = gz;
    memset(cmap, 0, sizeof(cmap));

    /* if it's an SSFN ASCII */
    if(!memcmp((char*)ptr, "## S", 4)) {
        font = (ssfn_font_t*)malloc(sizeof(ssfn_font_t));
        if(!font) error("file_load", ERR_MEM);
        memset(font, 0, sizeof(ssfn_font_t));
        memcpy(font->magic, SSFN_MAGIC, 4);
        font->bbox_top = font->bbox_left = 65535;
        end = ptr + size;
        /* parse header */
        while(ptr < end && *ptr) {
            if(*ptr=='+') {
                if(ptr[1]=='#') {
                    if(ptr[2]=='f' && ptr[3]=='a') { ptr += 10; font->family = atoi((const char*)ptr); }
                    if(ptr[2]=='q') { ptr += 11; font->quality = atoi((const char*)ptr); }
                    if(ptr[2]=='b' && ptr[3]=='a') { ptr += 12; if(!font->baseline) font->baseline = atoi((const char*)ptr); }
                    if(ptr[2]=='u') { ptr += 13; font->underline = atoi((const char*)ptr); }
                    if(ptr[2]=='s') {
                        for(ptr += 9;*ptr!='\n';ptr++) {
                            if(*ptr=='b') font->style |= SSFN_STYLE_BOLD;
                            if(*ptr=='i') font->style |= SSFN_STYLE_ITALIC;
                        }
                    }
                } else
                if(ptr[1]=='$') {
                    k = ptr[2];
                    findandskip(' ');
                    for(e=ptr;*e && *e!='\r' && *e!='\n';e++);
                    str = malloc(256);
                    if(!str) error("file_load", ERR_MEM);
                    memcpy(str, ptr, e-ptr);
                    str[e-ptr]=0;
                    switch(k) {
                        case 'n': strtable[0] = str; break;
                        case 'f': strtable[1] = str; break;
                        case 's': strtable[2] = str; break;
                        case 'r': strtable[3] = str; break;
                        case 'm': strtable[4] = str; break;
                        case 'l': strtable[5] = str; break;
                        default: free(str); str = NULL; break;
                    }
                } else
                if(ptr[1]!='@') break;
            }
            ptr++;
        }
        s = 16 << font->quality;

        /* parse glyphs */
        while(ptr < end) {
            while(ptr < end && !(ptr[0]=='+' && (ptr[1]=='+' || ptr[1]=='=' || ptr[1]=='%' || ptr[1]=='!' || ptr[1]=='@') &&
                ptr[2]=='-' && ptr[4]=='-')) ptr++;
            if(ptr[0]!='+') break;
            if(ptr[1]=='@') { if(ptr[5]=='K' || ptr[5]=='E') break; else { ptr++; continue; } }
            x = y = -1;
            cmd = ptr[1];
            switch(ptr[3]) {
                case '1': case 'i': v = 1; break;
                case '2': case 'm': v = 2; break;
                case '3': case 'f': v = 3; break;
                case '4': v = 4; break;
                case '5': v = 5; break;
                case '6': v = 6; break;
                default: v = 0; break;
            }
            ptr += 5;
            j = atoi((char*)ptr);
            findandskip('-');
            if(cmd!='+') {
                x = atoi((char*)ptr);
                findandskip('-');
                y = atoi((char*)ptr);
            }
            findandskip('\n');
            k = file_addchar(v, j, 0);
            if(k < 0) continue;
            c = &chars[v][k];
            if(cmd!='!') {
                i = c->len; c->len++;
                c->frags = (int*)realloc(c->frags, (c->len)*sizeof(int));
                if(!c->frags) error("file_load", ERR_MEM);
                c->frags[i] = file_fragadd(cmd=='%' ? SSFN_FRAG_PIXMAP : SSFN_FRAG_BITMAP);
                bitmap = (uint8_t*)frags[c->frags[i]].data;
                j = 0; n = s; if(cmd!='%') n >>= 3;
                while(ptr < end && *ptr!='+') {
                    for(i=0;ptr < end && *ptr!='\r' && *ptr!='\n';i++) {
                        if(cmd == '%') {
                            if(*ptr=='.') { while(ptr < end && *ptr=='.') ptr++; }
                            else {
                                if(j < s && i < s)
                                    bitmap[j*s + i] = cpal_add(
                                        gethex((char*)ptr+2, 2), gethex((char*)ptr+4, 2),
                                        gethex((char*)ptr+6,2), gethex((char*)ptr,2));
                                ptr += 8;
                            }
                            if(*ptr==' ') ptr++;
                        } else {
                            if(*ptr!=' ' && *ptr!='.' && *ptr!='+' && j < s && i < s)
                                bitmap[j*n + (i>>3)] |= 1 << (i & 7);
                            ptr++;
                        }
                    }
                    j++;
                    while(*ptr=='\r' || *ptr=='\n') ptr++;
                }
            } else {
                while(ptr < end && *ptr!='+') {
                    cmd = *ptr; par[0]=par[1]=par[2]=par[3]=par[4]=par[5]=0;
                    if(cmd == 'H' || cmd == 'V') {
                        findandskip('\n');
                        continue;
                    } else {
                        for(ptr+=2,p=0;ptr < end && *ptr!='\n' && *ptr!='+' && p<6;ptr++) {
                            if(cmd=='c' || cmd=='g') {
                                par[p++] = *ptr=='.' ? 240 : cpal_add(
                                    gethex((char*)ptr+2, 2), gethex((char*)ptr+4, 2),
                                    gethex((char*)ptr+6,2), gethex((char*)ptr,2));
                            } else
                                par[p++] = atoi((char*)ptr);
                            while(*ptr!=' ' && *ptr!=',' && ptr[1] && ptr[1]!='\n' && ptr[1]!='+') ptr++;
                        }
                    }
                    while(*ptr=='\n') ptr++;
                    switch(cmd) {
                        case 'm':
                            if(p<2) { fprintf(stderr,"Too few move arguments for U+%06X\n",j); exit(3); }
                            i = c->len; c->len++;
                            c->frags = (int*)realloc(c->frags, (c->len)*sizeof(int));
                            if(!c->frags) error("file_load", ERR_MEM);
                            c->frags[i] = file_fragadd(SSFN_FRAG_CONTOUR);
                            file_contadd(c, c->frags[i], SSFN_CONTOUR_MOVE,  par[0],par[1], 0,0, 0,0);
                        break;

                        case 'l':
                            if(p<2) { fprintf(stderr,"Too few line arguments for U+%06X\n",j); exit(3); }
                            file_contadd(c, c->frags[i], SSFN_CONTOUR_LINE,  par[0],par[1], 0,0, 0,0);
                        break;

                        case 'q':
                            if(p<4) { fprintf(stderr,"Too few quadratic curve arguments for U+%06X\n",j); exit(3); }
                            file_contadd(c, c->frags[i], SSFN_CONTOUR_QUAD,  par[0],par[1], par[2],par[3], 0,0);
                        break;

                        case 'b':
                            if(p<6) { fprintf(stderr,"Too few bezier curve arguments for U+%06X\n",j); exit(3); }
                            file_contadd(c, c->frags[i], SSFN_CONTOUR_QUAD,  par[0],par[1], par[2],par[3], par[4],par[5]);
                        break;

                        case 'c':
                            if(p<1) { fprintf(stderr,"Too few color arguments for U+%06X\n",j); exit(3); }
                            file_contadd(c, c->frags[i], SSFN_CONTOUR_COLOR,  par[0],par[0],par[0],par[0], 0,0);
                        break;

                        case 'g':
                            if(p<4) { fprintf(stderr,"Too few gradient arguments for U+%06X\n",j); exit(3); }
                            file_contadd(c, c->frags[i], SSFN_CONTOUR_COLOR,  par[0],par[1],par[2],par[3], 0,0);
                        break;
                    }
                }
            }
        }

        /* parse kerning (if any) */
        while(ptr < end) {
            while(ptr < end && !(ptr[0]=='+' && ptr[1]=='@' && ptr[2]=='-' && ptr[3]=='-')) ptr++;
            cmd = ptr[5];
            findandskip('\n');
            if(cmd == 'K') {
                while(ptr < end && *ptr!='\n' && ptr[1]==':') {
                    j = ssfn_utf8((char**)&ptr);
                    if(!j || j == 0xA || j == 0x20 || ptr[0]!=':') break;
                    i = file_findchar(0, j);
                    if(i == -1) break;
                    ptr += 2;
                    while(ptr < end && *ptr!='\n') {
                        while(*ptr == ' ') ptr++;
                        x = ssfn_utf8((char**)&ptr);
                        ptr++;
                        l = atoi((char*)ptr);
                        while(ptr < end && *ptr!='v' && *ptr!='h') ptr++;
                        if(chars[0][i].klen > 32767)
                            fprintf(stderr,"Too many kerning pairs for U+%06x, truncated to 32768\n", chars[0][i].unicode);
                        else if(l) {
                            for(k=j=0;k<chars[0][i].klen;k+=3) {
                                if(chars[0][i].kern[k] == x) { j=1; break; }
                            }
                            if(!j) {
                                k = chars[0][i].klen*3;
                                chars[0][i].klen++;
                                chars[0][i].kern = (int*)realloc(chars[0][i].kern, (k+3)*sizeof(int));
                                if(!chars[0][i].kern) error("file_load", ERR_MEM);
                                chars[0][i].kern[k+0] = x;
                                chars[0][i].kern[k+1] = chars[0][i].kern[k+2] = 0;
                            }
                            chars[0][i].kern[k + (*ptr != 'v'? 1 : 2)] = l;
                        }
                        ptr++;
                        if(*ptr == ',') ptr++;
                    }
                    if(*ptr == '\n') ptr++;
                }
            }
            if(cmd == 'E') break;
            ptr++;
        }
    } else {
        /* if it's an SSFN font */
        font = (ssfn_font_t*)ptr;
        if(!memcmp(font->magic, SSFN_COLLECTION, 4))
            error("file_load", ERR_COLLECTION, filename);

        if(memcmp(font->magic, SSFN_MAGIC, 4) || size != font->size ||
            memcmp((uint8_t*)font + font->size - 4, SSFN_ENDMAGIC, 4) ||
            font->family > SSFN_FAMILY_HAND || font->fragments_offs > font->size || font->characters_offs[0] > font->size ||
            font->kerning_offs > font->size || font->fragments_offs > font->characters_offs[0]) {
                error("file_load", ERR_BADFILE, filename);
        }
        /* parse header */
        if(font->features & SSFN_FEAT_HASCMAP)
            memcpy(cmap, (uint8_t*)font + font->size - sizeof(cmap), sizeof(cmap) - 4);

        for(i = 0, str = (char*)font + sizeof(ssfn_font_t); i < 6; i++) {
            strtable[i] = (char*)malloc(256);
            if(!strtable[i]) error("file_load", ERR_MEM);
            memcpy(strtable[i], str, strlen(str) + 1);
            str += strlen(str) + 1;
        }

        for(v = 0; v < SSFN_NUMVARIANTS; v++) {
            if(!font->characters_offs[v]) continue;
            ptr = (uint8_t*)font + font->characters_offs[v];
            end = (uint8_t*)font + (v + 1 < SSFN_NUMVARIANTS && font->characters_offs[v + 1] ?
                font->characters_offs[v + 1] : font->size);
            l = (font->quality < 5 && font->characters_offs[v] < 65536) ? 4 : (font->characters_offs[v] < 1048576 ? 5 : 6);

            /* parse character table and construct glyphs from fragments */
            for(j=i=0;i<0x110000 && ptr < end;i++) {
                if(ptr[0] & 0x80) {
                    if(ptr[0] & 0x40) { i += ptr[1] | ((ptr[0] & 0x3f) << 8); ptr += 2; }
                    else { i += ptr[0] & 0x3f; ptr++; }
                } else {
                    k =file_addchar(v, i, 0);
                    if(k != -1) {
                        c = &chars[v][k];
                        n = (ptr[0] & 0x7F);
                        c->adv_x = ((ptr[2]&0x0F)<<8)|ptr[6];
                        c->adv_y = ((ptr[2]&0xF0)<<4)|ptr[7];
                        c->bear_l = L = ((ptr[3]&0x0F)<<8)|ptr[8];
                        c->bear_t = T = ((ptr[3]&0xF0)<<4)|ptr[9];
                        ptr += 10;
                        for(;n--;ptr += l) {
                            x = (((ptr[1] >> 4) & 3) << 8) | ptr[4];
                            y = (((ptr[1] >> 6) & 3) << 8) | ptr[5];
                            switch(l) {
                                case 4: k=(ptr[1] << 8) | ptr[0]; x = ptr[2]; y = ptr[3]; break;
                                case 5: k=((ptr[2] & 0xF) << 16) | (ptr[1] << 8) | ptr[0];
                                    x = (((ptr[2] >> 4) & 3) << 8) | ptr[3]; y = (((ptr[2] >> 6) & 3) << 8) | ptr[4]; break;
                                default: k=(ptr[2] << 16) | (ptr[1] << 8) | ptr[0];
                                    x = ((ptr[3] & 0xF) << 8) | ptr[4]; y = (((ptr[3] >> 4) & 0xF) << 8) | ptr[5]; break;
                            }
                            file_addrawfrag(c, (uint8_t*)font + k, L+x, T+y);
                        }
                    } else
                        ptr += ptr[0] * l + 10;
                    j++;
                }
            }
        }

        /* parse kerning (if any) */
        if(font->kerning_offs) {
            ptr = (uint8_t*)font + font->kerning_offs;
            L = font->features & SSFN_FEAT_KBIGLKP ? 4 : 3;
            T = font->features & SSFN_FEAT_KBIGCHR;
            p = font->features & SSFN_FEAT_KBIGCRD;
            k = 0;
            for(i=0;i<0x110000;i++) {
                if(ptr[0] & 0x80) {
                    if(ptr[0] & 0x40) { i += ptr[1] | ((ptr[0] & 0x3f) << 8); ptr += 2; }
                    else { i += ptr[0] & 0x3f; ptr++; }
                } else {
                    y = i + (ptr[0] & 0x7F);
                    for(; i <= y; i++) {
                        k = file_findchar(0, i);
                        if(k != -1) {
                            e = (uint8_t*)font + font->kerning_offs + ((L==4?(ptr[3]<<16):0)|(ptr[2]<<8)|ptr[1]);
                            if(e[0] & 0x80) { chars[0][k].klen = (e[1] | ((e[0] & 0x7f) << 8)) + 1; e += 2; }
                            else { chars[0][k].klen = (e[0] & 0x7F) + 1; e++; }
                            chars[0][k].kern = (int*)malloc(chars[0][k].klen*3*sizeof(int));
                            if(!chars[0][k].kern) error("file_load", ERR_MEM);
                            for(x = 0; x < chars[0][k].klen; x++) {
                                if(T) { j = e[0] | (e[1] << 8) | ((e[2] & 0x7F) << 16); l = e[2] & 0x80; e += 3; }
                                else { j = e[0] | ((e[1] & 0x7F) << 8); l = e[1] & 0x80; e += 2; }
                                if(p) { n = (short)(e[0] | (e[1] << 8)); e += 2; } else { n = (signed char)e[0]; e++; }
                                chars[0][k].kern[x*3+0] = j;
                                chars[0][k].kern[x*3+1+l] = n;
                                chars[0][k].kern[x*3+2-l] = 0;
                            }
                        }
                    }
                    ptr += L;
                    i--;
                }
            }
        }

        font = (ssfn_font_t*)realloc(font, sizeof(ssfn_font_t));
    }
    /* sort color map */
    file_sortcmap();
    qsort(chars[0], numchars[0], sizeof(char_t), file_chrsort);
}

/**
 * Save font as SSFN
 */
void file_savesfn(int idx)
{
    if(!fontfile || !fontfileext) return;
    memcpy(fontfileext, "sfn", 3);
    file_sortcmap();
    ui_saving(idx, 1000);
    /*sleep(1);*/
    ui_saving(idx, -1);
    modified = 0;
}

/**
 * Save content to ASC
 */
void file_printf(char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
#ifdef HAS_ZLIB
    if(gzipped)
        gzvprintf(sg,fmt,ap);
    else
#endif
        vfprintf(sf,fmt,ap);
    va_end(ap);
}

/**
 * Save ASC character header
 */
void file_charhdr(char type, int v, int chr)
{
    char var[7] = { '-', 'i', 'm', 'f', '4', '5', '6' };

    file_printf("\n+%c-%c-%d-%d-%d---U+%06X-", type, var[v], chars[v][chr].unicode,
        chars[v][chr].adv_x, chars[v][chr].adv_y, chars[v][chr].unicode);
    if(chars[v][chr].unicode>=32) file_printf("'%s'", utf8(chars[v][chr].unicode));
    file_printf("-'%s'---\n", !chars[v][chr].unicode? "UNDEF" : uninames[uniname(chars[v][chr].unicode)].name);
}

/**
 * Save font as ASC
 */
void file_saveasc(int idx)
{
    char *fam[] = { "Serif", "Sans", "Decorative", "Monospace", "Handwriting" };
    char *dump_str[] = { "name", "family", "subfamily", "revision", "manufacturer", "license" };
    cont_t *cont;
    int a, b, c, d, i, j, k, m, p=1, v, chr, hdr, x, y;

    if(!fontfile || !fontfileext) return;
    memcpy(fontfileext, "asc", 3);

    font->bbox_left = font->bbox_top = 65535;
    font->bbox_right = font->bbox_bottom = 0;
    for(v=savingmax=0;v<SSFN_NUMVARIANTS;v++) {
        savingmax += numchars[v];
        for(i=0; i < numchars[v]; i++) {
            if(chars[v][i].bear_l < font->bbox_left) font->bbox_left = chars[v][i].bear_l;
            if(chars[v][i].bear_t < font->bbox_top) font->bbox_top = chars[v][i].bear_t;
            if(chars[v][i].w > font->bbox_right) font->bbox_right = chars[v][i].w;
            if(chars[v][i].h > font->bbox_bottom) font->bbox_bottom = chars[v][i].h;
        }
    }
    for(i=0;i<numchars[0];i++) {
        if(chars[0][i].klen && chars[0][i].unicode > 32) savingmax++;
    }

#ifdef HAS_ZLIB
    if(gzipped) {
        sg = gzopen(fontfile,"w9");
        if(!sg) { ui_saving(idx, -1); return; }
    } else 
#endif
    {
        sf = fopen(fontfile,"w");
        if(!sf) { ui_saving(idx, -1); return; }
    }

    file_printf("## Scalable Screen Font (revision %d)\n\n+@---Header---\n", font->revision);
    file_printf("+#family: %d (%s)\n", font->family, fam[font->family]);
    file_printf("+#style:");
    if(!font->style) file_printf(" regular");
    else {
        if(font->style & SSFN_STYLE_BOLD) file_printf(" bold");
        if(font->style & SSFN_STYLE_ITALIC) file_printf(" italic");
    }
    file_printf("\n+#quality: %d (grid %d x %d)\n", font->quality, 16<<font->quality, 16<<font->quality);
    file_printf("+#baseline: %d\n+#underline: %d\n", font->baseline, font->underline);
    file_printf("+#boundingbox: %d,%d %d,%d (informational)\n",font->bbox_left,font->bbox_top,font->bbox_right,font->bbox_bottom);
    for(i=0;i<6;i++) {
        file_printf("+$%s: %s\n", dump_str[i], strtable[i]);
    }
    file_printf("\n+@---Glyphs---\n");
    for(i=0; i < numchars[0]; i++) {
        for(v=hdr=0; v < SSFN_NUMVARIANTS; v++) {
            chr = !v? i : file_findchar(v, chars[0][i].unicode);
            if(chr < 0) continue;
            ui_saving(idx, p++);
            if(!chars[v][chr].len && (chars[v][chr].adv_x || chars[v][chr].adv_y))
                file_charhdr('!', v, chr);
            for(j=0; j < chars[v][chr].len; j++) {
                if(chars[v][chr].frags[j] >= 0 && chars[v][chr].frags[j] < numfrags)
                    switch(frags[chars[v][chr].frags[j]].type) {
                        case SSFN_FRAG_BITMAP:
                            file_charhdr('=', v, chr);
                            a = (16<<font->quality);
                            b = ((chars[v][chr].w+7)/8)*8;
                            for(y=0; y < a; y++) {
                                k = y*(2<<font->quality);
                                for(x=0,m=1; x < b; x++, m<<=1) {
                                    if(m > 0x80) { k++; m = 1; }
                                    file_printf("%c", ((uint8_t*)frags[chars[v][chr].frags[j]].data)[k] & m ? 'X' : '.');
                                }
                                file_printf("\n");
                            }
                        break;
                        case SSFN_FRAG_PIXMAP:
                            file_charhdr('%', v, chr);
                            a = (16<<font->quality);
                            b = ((chars[v][chr].w+7)/8)*8;
                            for(y=0; y < chars[v][chr].h; y++) {
                                k = y*a;
                                for(x=0; x < b; x++, k++) {
                                    m = ((uint8_t*)frags[chars[v][chr].frags[j]].data)[k];
                                    if(m > 239 || cmap[m]==0 || cmap[m]==0xFF000000)
                                        file_printf("%s........", x?" ":"");
                                    else
                                        file_printf("%s%02x%02x%02x%02x", x?" ":"", cpal[m*4+3],cpal[m*4+2],cpal[m*4+1],cpal[m*4]);
                                }
                                file_printf("\n");
                            }
                        break;
                        case SSFN_FRAG_CONTOUR:
                            if(!hdr) { hdr = 1; file_charhdr('!', v, chr); }
                            for(k=0; k < frags[chars[v][chr].frags[j]].len; k++) {
                                cont = &frags[chars[v][chr].frags[j]].data[k];
                                switch(cont->type) {
                                    case SSFN_CONTOUR_COLOR:
                                        a=cont->px; b=cont->py; c=cont->c1x; d=cont->c2x;
                                        if(a<240 && b==a && c==a && d==a)
                                            file_printf("c %02X%02X%02X%02X",cpal[a*4+3],cpal[a*4+2],cpal[a*4+1],cpal[a*4+0]);
                                        else if(a<240 || b<240 || c<240 || d<240) {
                                            file_printf("g");
                                            if(a<240)
                                                file_printf(" %02X%02X%02X%02X",cpal[a*4+3],cpal[a*4+2],cpal[a*4+1],cpal[a*4+0]);
                                            else
                                                file_printf(" .");
                                            if(b<240)
                                                file_printf(" %02X%02X%02X%02X",cpal[b*4+3],cpal[b*4+2],cpal[b*4+1],cpal[b*4+0]);
                                            else
                                                file_printf(" .");
                                            if(c<240)
                                                file_printf(" %02X%02X%02X%02X",cpal[c*4+3],cpal[c*4+2],cpal[c*4+1],cpal[c*4+0]);
                                            else
                                                file_printf(" .");
                                            if(d<240)
                                                file_printf(" %02X%02X%02X%02X",cpal[d*4+3],cpal[d*4+2],cpal[d*4+1],cpal[d*4+0]);
                                            else
                                                file_printf(" .");
                                            file_printf("\n");
                                        }
                                    break;
                                    case SSFN_CONTOUR_MOVE:
                                        file_printf("m %d,%d\n", cont->px, cont->py);
                                    break;
                                    case SSFN_CONTOUR_LINE:
                                        file_printf("l %d,%d\n", cont->px, cont->py);
                                    break;
                                    case SSFN_CONTOUR_QUAD:
                                        file_printf("q %d,%d %d,%d\n", cont->px, cont->py, cont->c1x, cont->c1y);
                                    break;
                                    case SSFN_CONTOUR_CUBIC:
                                        file_printf("b %d,%d %d,%d %d,%d\n", cont->px, cont->py, cont->c1x, cont->c1y,
                                            cont->c2x, cont->c2y);
                                    break;
                                }
                            }
                        break;
                    }
            }
        }
    }

    for(i=hdr=0;i<numchars[0];i++) {
        if(!chars[0][i].klen || chars[0][i].unicode <= 32) continue;
        ui_saving(idx, p++);
        if(!hdr) { hdr = 1; file_printf("\n+@---Kerning Table---\n"); }
        for(j=k=0; j < chars[0][i].klen; j++) {
            if(chars[0][i].kern[j*3+1]) {
                if(!k) file_printf("%s:",utf8(chars[0][i].unicode));
                file_printf("%s %s %dh", k?",":"", utf8(chars[0][i].kern[j*3]), chars[0][i].kern[j*3+1]);
                k = 1;
            }
            if(chars[0][i].kern[j*3+2]) {
                if(!k) file_printf("%s:",utf8(chars[0][i].unicode));
                file_printf("%s %s %dh", k?",":"", utf8(chars[0][i].kern[j*3]), chars[0][i].kern[j*3+2]);
                k = 1;
            }
        }
        if(k) file_printf("\n");
    }

    file_printf("\n+@----End---\n\n");
#ifdef HAS_ZLIB
    if(gzipped)
        gzclose(sg);
    else
#endif
        fclose(sf);

    ui_saving(idx, 0);
    modified = 0;
}

/**
 * Change quality of font
 */
void file_setquality(int q)
{
    int i, j, o, s, p, x, y;
    uint8_t *data;

    if((q != -1 && q != 1) || (q < 0 && font->quality == 0) || (q > 0 && font->quality == 6)) return;
    modified = 1;
    o = 16 << font->quality;
    font->quality += q;
    s = 16 << font->quality;
    if(q<0) {
        font->baseline >>= 1;
        font->underline >>= 1;
    } else {
        font->baseline <<= 1;
        font->underline <<= 1;
    }
    for(i=0;i<numfrags;i++) {
        switch(frags[i].type) {
            case SSFN_FRAG_BITMAP:
                data = (uint8_t*)malloc(s*s/8);
                if(!data) error("file_setquality", ERR_MEM);
                memset(data, 0, s*s/8);
                if(q<0) {
                    for(y=p=0;y<s;y++)
                        for(x=0;x<s;x++,p++)
                            data[p>>3] |= ((uint8_t*)frags[i].data)[2*y*o/8+(x/4)] & (1 << ((2*x) & 7)) ? (1 << (p & 7)) : 0;
                } else {
                    for(y=p=0;y<o;y++)
                        for(x=0;x<o;x++,p++)
                            if(((uint8_t*)frags[i].data)[p>>3] & (1 << (p & 7))) {
                                data[2*y*s/8+(x/4)] |= (3 << ((2*x) & 7));
                                data[(2*y+1)*s/8+(x/4)] |= (3 << ((2*x) & 7));
                            }
                }
                free(frags[i].data);
                frags[i].data = (cont_t*)data;
            break;
            case SSFN_FRAG_PIXMAP:
                data = (uint8_t*)malloc(s*s);
                if(!data) error("file_setquality", ERR_MEM);
                if(q<0) {
                    for(y=p=0;y<s;y++)
                        for(x=0;x<s;x++,p++) {
                            data[p] = ((uint8_t*)frags[i].data)[2*y*o+2*x];
                        }
                } else {
                    for(y=p=0;y<o;y++)
                        for(x=0;x<o;x++,p++) {
                            data[2*y*s+2*x] = data[2*y*s+2*x+1] = data[(2*y+1)*s+2*x] = data[(2*y+1)*s+2*x+1] =
                                ((uint8_t*)frags[i].data)[p];
                        }
                }
                free(frags[i].data);
                frags[i].data = (cont_t*)data;
            break;
            case SSFN_FRAG_CONTOUR:
                for(j=0;j<frags[i].len;j++) {
                    if(frags[i].data[j].type == SSFN_CONTOUR_COLOR) continue;
                    if(q<0) {
                        frags[i].data[j].px >>= 1;
                        frags[i].data[j].py >>= 1;
                        frags[i].data[j].c1x >>= 1;
                        frags[i].data[j].c1y >>= 1;
                        frags[i].data[j].c2x >>= 1;
                        frags[i].data[j].c2y >>= 1;
                    } else {
                        frags[i].data[j].px <<= 1;
                        frags[i].data[j].py <<= 1;
                        frags[i].data[j].c1x <<= 1;
                        frags[i].data[j].c1y <<= 1;
                        frags[i].data[j].c2x <<= 1;
                        frags[i].data[j].c2y <<= 1;
                    }
                }
            break;
        }
    }
    for(j=0;j<SSFN_NUMVARIANTS;j++)
        for(i=0;i<numchars[j];i++) {
            if(!j && chars[j][i].klen) {
                for(x=0;x<chars[j][i].klen;x++)
                    if(q<0) {
                        chars[j][i].kern[x*3+1] >>= 1;
                        chars[j][i].kern[x*3+2] >>= 1;
                    } else {
                        chars[j][i].kern[x*3+1] <<= 1;
                        chars[j][i].kern[x*3+2] <<= 1;
                    }
            }
            if(q<0) {
                chars[j][i].bear_l >>= 1;
                chars[j][i].bear_t >>= 1;
                chars[j][i].w >>= 1;
                chars[j][i].h >>= 1;
                chars[j][i].adv_x >>= 1;
                chars[j][i].adv_y >>= 1;
            } else {
                chars[j][i].bear_l <<= 1;
                chars[j][i].bear_t <<= 1;
                chars[j][i].w <<= 1;
                chars[j][i].h <<= 1;
                chars[j][i].adv_x <<= 1;
                chars[j][i].adv_y <<= 1;
            }
        }
}

/**
 * Autodetect baseline
 */
void file_setbaseline()
{
    int i,j,k,hs[4096], s=(16<<font->quality);

    memset(hs, 0, sizeof(hs));
    for(j=0;j<SSFN_NUMVARIANTS;j++)
        for(i=0;i<numchars[j];i++)
            hs[chars[j][i].h]++;
    for(i=j=k=0;i<s;i++)
        if(hs[i] > k) { k = hs[i]; j = i + 1; }
    if(j < s && font->baseline != j) {
        modified = 1;
        font->underline += j-font->baseline;
        font->baseline = j;
        if(font->underline < font->baseline) font->underline = font->baseline;
    }
}

/**
 * Free resources
 */
void file_free()
{
    int i, v;

    for(i = 0; i < 6; i++) free(strtable[i]);
    for(i = 0; i < numfrags; i++)
        if(frags[i].data) free(frags[i].data);
    free(frags);
    for(v = 0; v < SSFN_NUMVARIANTS; v++) {
        if(chars[v]) {
            for(i = 0; i < numchars[v]; i++) {
                if(chars[v][i].frags) free(chars[v][i].frags);
                if(chars[v][i].kern) free(chars[v][i].kern);
            }
            free(chars[v]);
        }
    }
    free(font);
}
