/*
 * sfnedit/edit.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Glyph edit tab (View and Controller)
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ui.h"
#include "lang.h"
#include "util.h"
#include "file.h"
#include "hist.h"
#include "render.h"
#include "unicode.h"
#include "copypaste.h"

extern int selcolor, clickx, clicky, minx, miny, maxx, maxy;
int overmenu=0, showgrid=0, showlines=1, mousex=0, mousey=0, mousebtn = 0, inmove=0, prevx = 0, prevy = 0;
int insl = 0, inhint = 0, selminx, selminy, selmaxx, selmaxy, selfrt = 0, sellst = 0, selall = 0, movex=-1, movey=-1;
int zooms[] = { 16,24,32,40,48,56,64,80,96,128,160,192,224,256,320,384,448,512,768,1024,1536,2048,3072,4096,5120,6144,7168,8192 };
uint16_t hints[8192];

/**
 * Display coordinates on left bottom corner
 */
void view_coords(ui_win_t *win)
{
    char coord[8];
    int i = 0;

    ssfn_fg = theme[THEME_TABU];
    ssfn_bg = theme[THEME_BG];
    if(mousex != -1 && mousey != -1) {
        sprintf(coord, "%4d", mousex);
        ssfn_x = 2; ssfn_y = win->h - 34;
        ui_text(win, coord, ssfn_bg, ssfn_bg);
        sprintf(coord, "%4d", mousey);
        ssfn_x = 2; ssfn_y = win->h - 18;
        ui_text(win, coord, ssfn_bg, ssfn_bg);
    } else {
        ui_box(win, 2, win->h - 34, 34, 34, ssfn_bg);
    }
    if(chars[win->v][win->chr].len) {
        switch(frags[chars[win->v][win->chr].frags[win->frag]].type) {
            case SSFN_FRAG_PIXMAP:
                ui_box(win, 2, win->h - 50, 34, 1, ssfn_bg);
                ui_box(win, 2, win->h - 49, 9, 15, ssfn_bg);
                if(win->menu == 9) { ui_box(win, 10, win->h - 49, 15, 15, theme[THEME_INPUT]); i = 1; } else i = 0;
                ui_argb(win, 10+i, win->h - 49 + i, 15 - 2*i, 15 - 2*i,
                    cmap[frags[chars[win->v][win->chr].frags[win->frag]].color[0]]);
                ui_box(win, 25, win->h - 49, 9, 15, ssfn_bg);
            break;
            case SSFN_FRAG_CONTOUR:
                ui_box(win, 2, win->h - 66, 34, 1, ssfn_bg);
                if(win->menu == 9) { ui_box(win, 2, win->h - 65, 15, 15, theme[THEME_INPUT]); i = 1; } else i = 0;
                    ui_argb(win, 2+i, win->h - 65+i, 15-2*i, 15-2*i,
                        cmap[frags[chars[win->v][win->chr].frags[win->frag]].color[0]]);
                ui_box(win, 17, win->h - 65, 2, 15, ssfn_bg);
                if(win->menu == 10) { ui_box(win, 19, win->h - 65, 15, 15, theme[THEME_INPUT]); i = 1; } else i = 0;
                    ui_argb(win, 19+i, win->h - 65+i, 15-2*i, 15-2*i,
                        cmap[frags[chars[win->v][win->chr].frags[win->frag]].color[1]]);
                ui_box(win, 2, win->h - 50, 34, 1, ssfn_bg);
                if(win->menu == 11) { ui_box(win, 2, win->h - 49, 15, 15, theme[THEME_INPUT]); i = 1; } else i = 0;
                    ui_argb(win, 2+i, win->h - 49+i, 15-2*i, 15-2*i,
                        cmap[frags[chars[win->v][win->chr].frags[win->frag]].color[2]]);
                ui_box(win, 17, win->h - 49, 2, 15, ssfn_bg);
                if(win->menu == 12) { ui_box(win, 19, win->h - 49, 15, 15, theme[THEME_INPUT]); i = 1; } else i = 0;
                ui_argb(win, 19+i, win->h - 49+i, 15-2*i, 15-2*i,
                    cmap[frags[chars[win->v][win->chr].frags[win->frag]].color[3]]);
            break;
        }
    }
}

/**
 * Display the toolbar
 */
void view_tools(ui_win_t *win)
{
    int i, y;

    for(i=0, y=24; i < (win->menu >= 6 && win->menu < 9? 9 : 7) && y < win->h; i++, y += 36)
        ui_box(win, 2, y, 32, 32, theme[i==win->menu ? THEME_TABU : THEME_BGLOGO]);
    ui_icon(win, tool_icons, 2, 22, 320-(win->menu >= 6 && win->menu < 9? 0 : 36+36));
}

/**
 * Display the layers
 */
void view_layers(ui_win_t *win)
{
    int i, y;
    uint32_t fg, bg;

    if(!insl)
        for(i=win->fscroll, y=276; i < chars[win->v][win->chr].len + 1 && y < win->h - 50; i++, y += 33) {
            fg = theme[selall || i == win->frag? THEME_INPUT : THEME_INACT];
            bg = theme[i == chars[win->v][win->chr].len ? THEME_BG : (selall || i == win->frag? THEME_INPBG : THEME_TABBG)];
            ui_box(win, 2, y, 32, 1, theme[THEME_BG]);
            ui_box(win, 2, y+1, 32, 32, bg);
            if(i < chars[win->v][win->chr].len)
                switch(frags[chars[win->v][win->chr].frags[i]].type) {
                    case SSFN_FRAG_BITMAP:
                        render_bitmap(&frags[chars[win->v][win->chr].frags[i]], win->data, win->w, win->h, win->p, 0, 0, 2, y+1,
                            32, 32, fg);
                    break;
                    case SSFN_FRAG_PIXMAP:
                        render_pixmap(&frags[chars[win->v][win->chr].frags[i]], win->data, win->w, win->h, win->p, 0, 0, 2, y+1,
                            32, 32, bg);
                    break;
                    case SSFN_FRAG_CONTOUR:
                        render_contour(&frags[chars[win->v][win->chr].frags[i]], win->data, win->w, win->h, win->p, 0, 0, 2, y+1,
                            32, 32, fg, bg, 1);
                    break;
                }
        }
    view_coords(win);
}

/**
 * Display the main edit area
 */
void view_editarea(ui_win_t *win)
{
    int i, j, s, p, q, w, h, a, b, c, d, x, y;

    switch(win->menu) {
        case 3:
            i = ui_textwidth(lang[AREYOUSURE], 64);
            ssfn_x = 40 + ((win->w - 40 - i) >> 1); ssfn_y = (win->h - 80) >> 1;
            ssfn_fg = theme[THEME_FG]; ssfn_bg = theme[THEME_BG];
            ui_text(win, lang[AREYOUSURE], ssfn_bg, ssfn_bg);
            ssfn_x = 40; ssfn_y = (win->h) >> 1;
            i = (win->w - 80) >> 1;
            ui_button(win, lang[NO], i, 3, 0);
            ssfn_x += 16;
            ui_button(win, lang[YES], i, 3, 3);
        break;
        case 9:
        case 10:
        case 11:
        case 12:
            if(chars[win->v][win->chr].len)
                view_color(win, frags[chars[win->v][win->chr].frags[win->frag]].color[win->menu-9]);
        break;
        default:
            w = win->w - 52; h = win->h - 32;
            if(!win->bg || win->menu < 3) {
                win->bg = (uint32_t*)malloc(w*h*sizeof(uint32_t));
                if(!win->bg) error("view_editarea", ERR_MEM);
                ui_box(win, 36, 24, win->w - 36, 8, theme[THEME_BG]);
                ui_box(win, 36, 32, 16, win->h - 32, theme[THEME_BG]);
                if(chars[win->v][win->chr].len || win->menu < 2) {
                    b = (16 << font->quality);
                    s = (win->zoom + (8 << font->quality)) / b;
                    q = win->zoom >> 1;
                    if(win->zx < 0)
                        ui_number(win, 0, 38 - win->zx, 24, theme[THEME_INACT]);
                    else
                        ui_number(win, (win->zx * b) / win->zoom, 38, 24, theme[THEME_INACT]);
                    if(win->zy < 0)
                        ui_number(win, 0, 36, 32 - win->zy, theme[THEME_INACT]);
                    else
                        ui_number(win, (win->zy * b) / win->zoom, 36, 32, theme[THEME_INACT]);
                    a = 36 - win->zx + win->zoom;
                    if(a <= win->w - 16)
                        ui_number(win, b, a, 24, theme[THEME_INACT]);
                    else
                        ui_number(win, ((win->zoom - (a - win->w +16)) * b) / win->zoom, win->w -16, 24, theme[THEME_INACT]);
                    a = 26 - win->zy + win->zoom;
                    if(a <= win->h - 6)
                        ui_number(win, b, 36, a, theme[THEME_INACT]);
                    else
                        ui_number(win, ((win->zoom - (a - win->h + 6)) * b) / win->zoom, 36, win->h - 6, theme[THEME_INACT]);
                    a = win->zoom-win->zx;
                    b = win->zoom-win->zy;
                    if(showgrid && s > 2) {
                        c = win->zx-(q%s);
                        d = win->zy-(q%s);
                        for(j=p=0;j<h;j++)
                            for(i=0;i<w;i++,p++)
                                win->bg[p] = theme[i < -win->zx || j < -win->zy || i >= a || j >= b ? THEME_BG :
                                ((!((i+c)%s) || !((j+d)%s))? THEME_TABBG : THEME_INPBG)];
                    } else {
                        c = q-win->zx;
                        d = q-win->zy;
                        for(j=p=0;j<h;j++)
                            for(i=0;i<w;i++,p++)
                                win->bg[p] = theme[i < -win->zx || j < -win->zy || i >= a || j >= b ? THEME_BG :
                                (i == c || j == d ? THEME_TABBG : THEME_INPBG)];
                    }
                    p = ((font->baseline * win->zoom + (8 << font->quality)) / (16 << font->quality) - win->zy);
                    if(p>=0 && p<win->h-32 && win->menu != 1 && win->menu != 2) {
                        if(!win->menu) ui_number(win, font->baseline, 36, p + 28, theme[THEME_FG]);
                        p = p * w - (win->zx < 0 ? win->zx : 0);
                        for(i=(win->zx < 0 ? -win->zx : 0);p>=0 && p<w*h && i<a && i<win->w - 52;i++,p++)
                            win->bg[p] = theme[!win->menu ? THEME_FG : THEME_BASELINE];
                    }
                    switch(win->menu) {
                        case 0:
                            p = ((font->underline * win->zoom + (8 << font->quality)) / (16 << font->quality) - win->zy);
                            if(p>=0 && p<win->h-32) {
                                ui_number(win, font->underline, 36, p + 28, theme[THEME_FG]);
                                p = p * w - (win->zx < 0 ? win->zx : 0);
                                for(i=(win->zx < 0 ? -win->zx : 0);p>=0 && p<w*h && i<a && i<win->w - 52;i++,p++)
                                    win->bg[p] = theme[THEME_FG];
                            }
                            c = chars[win->v][win->chr].bear_l != 65536 ? chars[win->v][win->chr].bear_l : 0;
                            p = (c * win->zoom + (8 << font->quality)) / (16 << font->quality) - win->zx;
                            if(p >= 0 && p < win->w - 52) {
                                ui_number(win, c, p + 38, 24, theme[THEME_BASELINE]);
                                if(win->zy<0) p -= win->zy*w;
                                for(i=(win->zy < 0? -win->zy : 0);i<b && i<win->h - 32;i++,p+=w)
                                    win->bg[p] = theme[THEME_BASELINE];
                            }
                            c = chars[win->v][win->chr].w;
                            p = (c * win->zoom + (8 << font->quality)) / (16 << font->quality) - win->zx;
                            if(p >= 0 && p < win->w - 52) {
                                ui_number(win, c, p + 38, 24, theme[THEME_BASELINE]);
                                if(win->zy<0) p -= win->zy*w;
                                for(i=(win->zy < 0? -win->zy : 0);i<b && i<win->h - 32;i++,p+=w)
                                    win->bg[p] = theme[THEME_BASELINE];
                            }
                            i = file_findchar(0, chars[win->v][win->chr].unicode);
                            if(i != -1 && chars[0][i].rtl) {
                                c = chars[win->v][win->chr].adv_x && chars[win->v][win->chr].w ?
                                    chars[win->v][win->chr].w - chars[win->v][win->chr].adv_x : 0;
                            } else {
                                c = chars[win->v][win->chr].adv_x ?
                                    ((chars[win->v][win->chr].bear_l != 65536 ? chars[win->v][win->chr].bear_l : 0) +
                                    chars[win->v][win->chr].adv_x) : 0;
                            }
                            p = ((c < 0 ? 0 : c) * win->zoom + (8 << font->quality)) / (16 << font->quality) - win->zx;
                            if(p >= 0 && p < win->w - 52) {
                                ui_number(win, c, p + 38, 24, theme[THEME_ADVANCE]);
                                if(win->zy<0) p -= win->zy*w;
                                for(i=(win->zy < 0? -win->zy : 0);i<b && i<win->h - 32;i++,p+=w)
                                    win->bg[p] = theme[THEME_ADVANCE];
                            }
                        break;
                        case 1:
                            if(chars[win->v][win->chr].bear_l != 65536) {
                                c = ((chars[win->v][win->chr].w - chars[win->v][win->chr].bear_l) >> 1) +
                                    chars[win->v][win->chr].bear_l;
                                p = (c * win->zoom + (8 << font->quality)) / (16 << font->quality) - win->zx;
                                if(p >= 0 && p < win->w - 52) {
                                    ui_number(win, c, p + 38, 24, theme[THEME_FG]);
                                    if(win->zy<0) p -= win->zy*w;
                                    for(i=(win->zy < 0? -win->zy : 0);i<b && i<win->h - 32;i++,p+=w)
                                        win->bg[p] = theme[THEME_FG];
                                }
                            }
                            c = chars[win->v][win->chr].bear_t != 65536 ? chars[win->v][win->chr].bear_t : 0;
                            p = ((c * win->zoom + (8 << font->quality)) / (16 << font->quality) - win->zy);
                            if(p >= 0 && p < win->h - 32) {
                                ui_number(win, c, 36, p + 28, theme[THEME_BASELINE]);
                                p = p * w - (win->zx < 0 ? win->zx : 0);
                                for(i=(win->zx < 0 ? -win->zx : 0);p>0 && i<win->zoom && i<win->w - 52;i++,p++)
                                    win->bg[p] = theme[THEME_BASELINE];
                            }
                            c = chars[win->v][win->chr].h;
                            p = ((c * win->zoom + (8 << font->quality)) / (16 << font->quality) - win->zy);
                            if(p >= 0 && p < win->h - 32) {
                                ui_number(win, c, 36, p + 28, theme[THEME_BASELINE]);
                                p = p * w - (win->zx < 0 ? win->zx : 0);
                                for(i=(win->zx < 0 ? -win->zx : 0);p>0 && i<win->zoom && i<win->w - 52;i++,p++)
                                    win->bg[p] = theme[THEME_BASELINE];
                            }
                            c = chars[win->v][win->chr].adv_y ?
                                ((chars[win->v][win->chr].bear_t != 65536 ? chars[win->v][win->chr].bear_t : 0) +
                                chars[win->v][win->chr].adv_y) : 0;
                            p = ((c * win->zoom + (8 << font->quality)) / (16 << font->quality) - win->zy);
                            if(p >= 0 && p < win->h - 32) {
                                ui_number(win, c, 36, p + 28, theme[THEME_ADVANCE]);
                                p = p * w - (win->zx < 0 ? win->zx : 0);
                                for(i=(win->zx < 0 ? -win->zx : 0);p>0 && i<win->zoom && i<win->w - 52;i++,p++)
                                    win->bg[p] = theme[THEME_ADVANCE];
                            }
                        break;
                        case 2:
                            memset(&hints, 0, sizeof(hints));
                            for(i=0; i < chars[win->v][win->chr].len; i++)
                                if(frags[chars[win->v][win->chr].frags[i]].type == SSFN_FRAG_CONTOUR) {
                                    for(j=0;j<frags[chars[win->v][win->chr].frags[i]].len;j++) {
                                        x = frags[chars[win->v][win->chr].frags[i]].data[j].px << (12 - font->quality);
                                        y = frags[chars[win->v][win->chr].frags[i]].data[j].py << (12 - font->quality);
                                        if(frags[chars[win->v][win->chr].frags[i]].data[j].type == SSFN_CONTOUR_LINE) {
                                            a = ((c < x) ? x - c : c - x) >> 4;
                                            b = ((d < y) ? y - d : d - y) >> 4;
                                            c = (((c + x) >> 1)*win->zoom) >> 16;
                                            if(a < 2) hints[(!hints[c] && c && hints[c - 1]? c - 1 : c)] += b;
                                        }
                                        c = x; d = y;
                                    }
                                }
                            for(d=1,j=0;j<win->zoom;j++) if(hints[j] > d) d = hints[j];
                            if(d > 4096/SSFN_HINTING_THRESHOLD) d = 4096/SSFN_HINTING_THRESHOLD;
                            b = win->zoom-win->zy;
                            for(j=(win->zx < 0? 0 : win->zx); j - win->zx + 52 < win->w;j++) {
                                if(hints[j] >= 4096/SSFN_HINTING_THRESHOLD) {
                                    p = j - win->zx;
                                    if(p >= 0 && p < win->w - 52) {
                                        ui_number(win, ((j << 16) / win->zoom) >> (12 - font->quality), p + 38, 24, theme[THEME_HINTING]);
                                        if(win->zy<0) p -= win->zy*w;
                                        for(i=(win->zy < 0? -win->zy : 0);i<b && i<win->h - 32;i+=2,p+=w+w)
                                            win->bg[p] = theme[THEME_HINTING];
                                    }
                                }
                                a = hints[j] >= 4096/SSFN_HINTING_THRESHOLD ? 64 : (hints[j] << 6) / d;
                                p = ((b <= win->h - 32 ? b : win->h-32)-a)*w - win->zx + j;
                                for(i=0;i<b && i<a;i++,p+=w)
                                    win->bg[p] = theme[THEME_INPUT];
                            }
                        break;
                        default:
                            chars[win->v][win->chr].bear_l = chars[win->v][win->chr].bear_t = 65536;
                            chars[win->v][win->chr].w = chars[win->v][win->chr].h = 0;
                        break;
                    }
                    for(i=0; i < chars[win->v][win->chr].len; i++) {
                        if(win->menu >= 3 && i == win->frag) continue;
                        switch(frags[chars[win->v][win->chr].frags[i]].type) {
                            case SSFN_FRAG_BITMAP:
                                render_bitmap(&frags[chars[win->v][win->chr].frags[i]], win->bg, w, h, w, win->zx, win->zy,
                                    0, 0, win->zoom, win->zoom, theme[selall ? THEME_INPUT : THEME_INACT]);
                            break;
                            case SSFN_FRAG_PIXMAP:
                                render_pixmap(&frags[chars[win->v][win->chr].frags[i]], win->bg, w, h, w, win->zx, win->zy,
                                    0, 0, win->zoom, win->zoom, theme[THEME_BG]);
                            break;
                            case SSFN_FRAG_CONTOUR:
                                render_contour(&frags[chars[win->v][win->chr].frags[i]], win->bg, w, h, w, win->zx, win->zy,
                                    0, 0, win->zoom, win->zoom, theme[selall ? THEME_INPUT : THEME_INACT], theme[THEME_TABBG], 0);
                            break;
                        }
                    }
                } else {
                    for(i=0;i<w*h;i++)
                        win->bg[i] = theme[THEME_BG];
                }
            }
            for(j=0,p=32*win->p; j < h && 32 + j < win->h; j++, p += win->p)
                for(i=0;i<w && 52+i < win->w;i++)
                    win->data[p + 52 + i] = win->bg[j*w+i];
            if(win->menu >= 3) {
                if(win->cont != -1) {
                    if(movex != -1) {
                        a = (movex * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        ui_box(win, a-win->zx+52, 32, 1, win->h - 32, theme[THEME_FG]);
                    }
                    if(movey != -1) {
                        a = (movey * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        ui_box(win, 52, a-win->zy+32, win->w-52, 1, theme[THEME_FG]);
                    }
                }
                if(chars[win->v][win->chr].len && win->frag < chars[win->v][win->chr].len) {
                    switch(frags[chars[win->v][win->chr].frags[win->frag]].type) {
                        case SSFN_FRAG_BITMAP:
                            render_bitmap(&frags[chars[win->v][win->chr].frags[win->frag]], win->data, win->w, win->h, win->p,
                                win->zx, win->zy, 52, 32, win->zoom, win->zoom, theme[insl ? THEME_INACT : THEME_INPUT]);
                        break;
                        case SSFN_FRAG_PIXMAP:
                            render_pixmap(&frags[chars[win->v][win->chr].frags[win->frag]], win->data, win->w, win->h, win->p,
                                win->zx, win->zy, 52, 32, win->zoom, win->zoom, theme[THEME_BG]);
                        break;
                        case SSFN_FRAG_CONTOUR:
                            render_contour(&frags[chars[win->v][win->chr].frags[win->frag]], win->data, win->w, win->h, win->p,
                                win->zx, win->zy, 52, 32, win->zoom, win->zoom, theme[insl ? THEME_INACT : THEME_INPUT],
                                theme[THEME_TABBG], 2);
                        break;
                    }
                    if(insl) {
                        a = (selminx * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        b = (selminy * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        c = ((selmaxx+1) * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        d = ((selmaxy+1) * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        ui_bright(win, a-win->zx+52, b-win->zy+32, c-a, d-b, frags[chars[win->v][win->chr].frags[win->frag]].type
                            != SSFN_FRAG_CONTOUR);
                    }
                    a = minx >> (12 - font->quality);
                    b = miny >> (12 - font->quality);
                    if(chars[win->v][win->chr].bear_l > a) chars[win->v][win->chr].bear_l = a;
                    if(chars[win->v][win->chr].bear_t > b) chars[win->v][win->chr].bear_t = b;
                    a = maxx >> (12 - font->quality);
                    b = maxy >> (12 - font->quality);
                    if(chars[win->v][win->chr].w < a) chars[win->v][win->chr].w = a;
                    if(chars[win->v][win->chr].h < b) chars[win->v][win->chr].h = b;
                    if(selall) {
                        a = (chars[win->v][win->chr].bear_l * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        b = (chars[win->v][win->chr].bear_t * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        c = ((chars[win->v][win->chr].w+1) * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        d = ((chars[win->v][win->chr].h+1) * win->zoom + (8 << font->quality)) / (16 << font->quality);
                        ui_bright(win, a-win->zx+52, b-win->zy+32, c-a, d-b, 1);
                    }
                }
            }
    }
}

/**
 * Zoom in at mouse coordinates x, y
 */
void view_zoomin(ui_win_t *win, int x, int y)
{
    int i;

    if(win->zoom < zooms[(int)(sizeof(zooms)/sizeof(zooms[0]))-1]) {
        for(i=0;i<(int)(sizeof(zooms)/sizeof(zooms[0])) && win->zoom != zooms[i];i++);
        i = zooms[i+1];
        win->zx = ((win->zx + x) * i + (win->zoom >> 1)) / win->zoom - x;
        win->zy = ((win->zy + y) * i + (win->zoom >> 1)) / win->zoom - y;
        win->zoom = i;
        mousex = ((win->zx + x) * (16 << font->quality) + (i >> 1)) / i;
        mousey = ((win->zy + y) * (16 << font->quality) + (i >> 1)) / i;
        if(win->bg) free(win->bg);
        win->bg = NULL;
        ui_resizewin(win, win->w, win->h);
    }
}

/**
 * Zoom out, retaining mouse coordinates x, y
 */
void view_zoomout(ui_win_t *win, int x, int y)
{
    int i;

    if(win->zoom > zooms[0]) {
        for(i=1;i<(int)(sizeof(zooms)/sizeof(zooms[0])) && win->zoom != zooms[i];i++);
        i = zooms[i-1];
        win->zx = ((win->zx + x) * i + (win->zoom >> 1)) / win->zoom - x;
        win->zy = ((win->zy + y) * i + (win->zoom >> 1)) / win->zoom - y;
        win->zoom = i;
        mousex = ((win->zx + x) * (16 << font->quality) + (i >> 1)) / i;
        mousey = ((win->zy + y) * (16 << font->quality) + (i >> 1)) / i;
        if(win->bg) free(win->bg);
        win->bg = NULL;
        ui_resizewin(win, win->w, win->h);
    }
}

/**
 * Get the closest point to mouse pointer
 */
int view_getpoint(ui_win_t *win, int *pt)
{
    int i, s = ((32 << font->quality) / win->zoom);
    frag_t *f;

    if(!chars[win->v][win->chr].len) return -1;
    if(pt) *pt = 0;
    f = &frags[chars[win->v][win->chr].frags[win->frag]];
    for(i=0; i < f->len; i++) {
        if( f->data[i].px >= mousex - s && f->data[i].px <= mousex + s &&
            f->data[i].py >= mousey - s && f->data[i].py <= mousey + s) {
                return i;
            }
        if( (f->data[i].type == SSFN_CONTOUR_QUAD || f->data[i].type == SSFN_CONTOUR_CUBIC) &&
            f->data[i].c1x >= mousex - s && f->data[i].c1x <= mousex + s &&
            f->data[i].c1y >= mousey - s && f->data[i].c1y <= mousey + s) {
                if(pt) *pt = 1;
                return i;
            }
        if( f->data[i].type == SSFN_CONTOUR_CUBIC &&
            f->data[i].c2x >= mousex - s && f->data[i].c2x <= mousex + s &&
            f->data[i].c2y >= mousey - s && f->data[i].c2y <= mousey + s) {
                if(pt) *pt = 2;
                return i;
            }
    }
    return -1;
}

/**
 * View for edit tab
 */
void view_edit(ui_win_t *win)
{
    view_layers(win);
    view_tools(win);
    view_editarea(win);
}

/**
 * Controller for edit tab
 */
int ctrl_edit(ui_win_t *win, ui_event_t *evt)
{
    int i, j;

    switch(evt->type) {
        case E_KEY:
            switch(evt->x) {
                case 'g':
                    showgrid ^= 1;
                    if(win->bg) free(win->bg);
                    win->bg = NULL;
                break;
                case 'l':
                    showlines ^= 1;
                break;
                case 'z':
                    hist_undo(win);
                    if(win->bg) free(win->bg);
                    win->bg = NULL;
                    ui_resizewin(win, win->w, win->h);
                break;
                case 'y':
                    hist_redo(win);
                    if(win->bg) free(win->bg);
                    win->bg = NULL;
                    ui_resizewin(win, win->w, win->h);
                break;
                case 'r':
                    i = file_findchar(0, chars[win->v][win->chr].unicode);
                    if(i != -1) chars[0][i].rtl ^= 1;
                    if(win->bg) free(win->bg);
                    win->bg = NULL;
                    ui_refreshwin(evt->win, 0, 0, win->w, win->h);
                break;
                case 'a':
                    if(win->menu == 5) {
                        selall = 1;
                        if(win->bg) free(win->bg);
                        win->bg = NULL;
                        return 1;
                    }
                break;
                case 'c':
                    if(win->menu == 5 && evt->h & 2) {
                    }
                break;
                case 'x':
                    if(win->menu == 5 && evt->h & 2) {
                    }
                break;
                case 'p':
                    if(win->menu == 5 && evt->h & 2) {
                    }
                break;
                case K_DEL:
                case K_BACKSPC:
                    if(win->menu == 5 && chars[win->v][win->chr].len && mousex != -1 && mousey != -1) {
                        mousebtn = 2;
                        goto modify;
                    }
                break;
                case K_LEFT:
                    if(win->tab == 0) {
                        if(win->menu == 0) hist_setadv(win, chars[win->v][win->chr].adv_x-1, 0, 1);
                    }
                break;
                case K_RIGHT:
                    if(win->tab == 0) {
                        if(win->menu == 0) hist_setadv(win, chars[win->v][win->chr].adv_x+1, 0, 1);
                    }
                break;
                case K_UP:
                    if(win->tab == 0) {
                        switch(win->menu) {
                            case 0:
                                if(evt->h & 1)
                                    hist_setuline(win, font->underline-1);
                                else
                                    hist_setbline(win, font->baseline-1);
                            break;
                            case 1:
                                hist_setadv(win, 0, chars[win->v][win->chr].adv_y-1, 1);
                            break;
                        }
                    }
                break;
                case K_DOWN:
                    if(win->tab == 0) {
                        switch(win->menu) {
                            case 0:
                                if(evt->h & 1)
                                    hist_setuline(win, font->underline+1);
                                else
                                    hist_setbline(win, font->baseline+1);
                            break;
                            case 1:
                                hist_setadv(win, 0, chars[win->v][win->chr].adv_y+1, 1);
                            break;
                        }
                    }
                break;
                default: break;
            }
            selall = 0;
            return 1;
        case E_MOUSEMOVE:
            if(!overmenu && win->menu != 3 && win->menu < 9 && evt->y > 32 && evt->x > 52) {
                mousex = ((evt->x - 52 + win->zx) * (16 << font->quality)) / win->zoom;
                mousey = ((evt->y - 32 + win->zy) * (16 << font->quality)) / win->zoom;
                if((evt->x - 52 + win->zx) < 0 || (evt->y - 32 + win->zy) < 0 ||
                    mousex >= (16 << font->quality) || mousey >= (16 << font->quality)) {
                    ui_cursorwin(win, inmove ? CURSOR_MOVE : CURSOR_PTR);
                    mousex = mousey = -1;
                } else {
                    if(win->menu == 5)
                        ui_cursorwin(win, inmove ? CURSOR_MOVE : (!insl && (win->cont != -1 ||
                            view_getpoint(win, NULL) != -1) ? CURSOR_GRAB : CURSOR_CROSS));
                    else if(win->menu == 2) {
                        j = evt->x - 52 + win->zx;
                        ui_cursorwin(win, inmove ? CURSOR_MOVE : (j >= 0 && j < win->zoom && hints[j] > 1 ?
                            CURSOR_GRAB : CURSOR_CROSS));
                    }
                }
                if(insl) {
                    if(mousex < prevx) { selminx = mousex; selmaxx = prevx; } else { selminx = prevx; selmaxx = mousex; }
                    if(mousey < prevy) { selminy = mousey; selmaxy = prevy; } else { selminy = prevy; selmaxy = mousey; }
                    return 1;
                }
                if(inmove) {
                    win->zx = clickx - evt->x + prevx;
                    win->zy = clicky - evt->y + prevy;
                    if(win->bg) free(win->bg);
                    win->bg = NULL;
                    return 1;
                }
                if(inhint) {
                    if(mousex != -1) {
                        hist_sethint(win, prevx, mousex);
                        prevx = mousex;
                    }
                    if(win->bg) free(win->bg);
                    win->bg = NULL;
                    return 1;
                }
                if(win->cont != -1) {
                    if(mousex != -1 && mousey != -1)
                        hist_setcoord(win, mousex, mousey);
                    return 1;
                }
                view_coords(win);
                ui_flushwin(win, 0, win->h - 34, 36, 32);
            } else {
                mousex = mousey = -1;
                ui_cursorwin(win, CURSOR_PTR);
                if(chars[win->v][win->chr].len && win->menu >= 9)
                    ctrl_color(win, evt, &frags[chars[win->v][win->chr].frags[win->frag]].color[win->menu-9]);
                else {
                    view_coords(win);
                    if(evt->x < 36 && win->menu < 9) {
                        if(overmenu) win->menu = (evt->y - 24) / 36;
                        if(win->menu > 8) win->menu = 5;
                        view_layers(win);
                        view_tools(win);
                    }
                    ui_flushwin(win, 0, 22, 36, win->h-22);
                }
            }
            return 0;
        case E_BTNPRESS:
            insl = inhint = selall = 0;
            if(chars[win->v][win->chr].len && (evt->w & 32 || (evt->h & 1 && evt->w & 8))) {
                if(frags[chars[win->v][win->chr].frags[win->frag]].color[0])
                    frags[chars[win->v][win->chr].frags[win->frag]].color[0]--;
                view_coords(win);
                return 1;
            }
            if(evt->w & 64 || (evt->h & 1 && evt->w & 16)) {
                if(frags[chars[win->v][win->chr].frags[win->frag]].color[0] < 241)
                    frags[chars[win->v][win->chr].frags[win->frag]].color[0]++;
                view_coords(win);
                return 1;
            }
            if(evt->x < 36) {
                if(win->menu == 3 || win->menu > 8) {
                    win->menu = 5;
                    ui_resizewin(win, win->w, win->h);
                    ui_refreshwin(evt->win, 0, 0, win->w, win->h);
                }
                if(evt->y < 276) {
                    win->menu = (evt->y - 24) / 36;
                    overmenu = 1;
                    view_layers(win);
                    view_tools(win);
                    ui_flushwin(win, 0, 22, 36, win->h-22);
                } else
                if(chars[win->v][win->chr].len && evt->y > win->h - 66) {
                    switch(frags[chars[win->v][win->chr].frags[win->frag]].type) {
                        case SSFN_FRAG_PIXMAP:
                            if(evt->y >= win->h - 50 && evt->y < win->h - 34) win->menu = 9;
                        break;
                        case SSFN_FRAG_CONTOUR:
                            if(evt->y >= win->h - 65 && evt->y < win->h - 50) win->menu = evt->x < 17 ? 9 : 10;
                            if(evt->y >= win->h - 50 && evt->y < win->h - 34) win->menu = evt->x < 17 ? 11 : 12;
                        break;
                    }
                    selcolor = win->menu >= 9 ? frags[chars[win->v][win->chr].frags[win->frag]].color[win->menu-9] : 0;
                    ui_resizewin(win, win->w, win->h);
                    return 1;
                } else
                if(evt->y >= 276 && evt->y < win->h - 50) {
                    if(evt->w & 8) { if(win->fscroll > 0) win->fscroll--; } else
                    if(evt->w & 16) { if(win->fscroll + 1 < chars[win->v][win->chr].len) win->fscroll++; } else
                    if(evt->w & 1) {
                        win->frag = win->fscroll + (evt->y - 276) / 33;
                        if(win->frag >= chars[win->v][win->chr].len) win->frag = chars[win->v][win->chr].len - 1;
                        if(win->bg) free(win->bg);
                        win->bg = NULL;
                    }
                    ui_resizewin(win, win->w, win->h);
                    return 1;
                }
            } else
            if(evt->y >= 32 && evt->x >= 52) {
                mousex = ((evt->x - 52 + win->zx) * (16 << font->quality)) / win->zoom;
                mousey = ((evt->y - 32 + win->zy) * (16 << font->quality)) / win->zoom;
                win->cont = -1;
                if(win->menu == 5 && evt->h & 1) {
                    prevx = mousex; prevy = mousey;
                    insl = 1;
                } else {
                    if(win->menu < 9) {
                        clickx = evt->x; clicky = evt->y;
                        mousebtn = evt->w;
                        if(evt->w & 8) {
                            view_zoomin(win, evt->x - 52, evt->y - 32);
                            return 1;
                        }
                        if(evt->w & 16) {
                            view_zoomout(win, evt->x - 52, evt->y - 32);
                            return 1;
                        }
                        if(evt->w & 1) {
                            prevx = win->zx;
                            prevy = win->zy;
                            inmove = 1;
                        }
                    }
                    switch(win->menu) {
                        case 2:
                            j = evt->x - 52 + win->zx;
                            inhint = j >= 0 && j < win->zoom && hints[j] > 1;
                            if(inhint && mousex != -1) {
                                hist_savehint(win);
                                inmove = 0;
                                prevx = mousex;
                            }
                        break;
                        case 5:
                            if(inmove && chars[win->v][win->chr].len && frags[chars[win->v][win->chr].frags[win->frag]].type ==
                                SSFN_FRAG_CONTOUR) {
                                if(mousex < 0 || mousey < 0 || mousex >= (16 << font->quality) || mousey >= (16 << font->quality)) {
                                    ui_cursorwin(win, CURSOR_PTR);
                                    mousex = mousey = -1;
                                } else {
                                    win->cont = view_getpoint(win, &win->pt);
                                    if(win->cont != -1) { inmove = 0; hist_savecoords(win); hist_setcoord(win, mousex, mousey); }
                                    ui_cursorwin(win, win->cont == -1 ? CURSOR_MOVE : CURSOR_GRAB);
                                    return 1;
                                }
                            }
                        break;
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                            if(chars[win->v][win->chr].len)
                                ctrl_color(win, evt, &frags[chars[win->v][win->chr].frags[win->frag]].color[win->menu-9]);
                        break;
                    }
                } /* if not shift click */
            }
            return 0;
        case E_BTNRELEASE:
            mousex = ((evt->x - 52 + win->zx) * (16 << font->quality)) / win->zoom;
            mousey = ((evt->y - 32 + win->zy) * (16 << font->quality)) / win->zoom;
            if((evt->x - 52 + win->zx) < 0 || (evt->y - 32 + win->zy) < 0 ||
                mousex >= (16 << font->quality) || mousey >= (16 << font->quality)) {
                ui_cursorwin(win, CURSOR_PTR);
                mousex = mousey = -1;
            } else
                ui_cursorwin(win, CURSOR_CROSS);
            if(win->cont != -1) {
                if(mousex != -1 && mousey != -1)
                    hist_setcoord(win, mousex, mousey);
                win->histmin++;
                win->cont = -1;
                clickx = clicky = -1;
                return 1;
            }
            if(insl)
                insl = 0;
            if(inmove)
                inmove = 0;
            if(inhint) {
                inhint = 0;
                win->histmin++;
                return 1;
            }
            if(overmenu) {
                overmenu = 0;
                if(evt->x > 36 && (win->menu > 5 || win->menu == 4)) win->menu = 5;
                switch(win->menu) {
                    case 4: hist_dellayer(win); break;
                    case 6: hist_addlayer(win, SSFN_FRAG_CONTOUR); break;
                    case 7: hist_addlayer(win, SSFN_FRAG_BITMAP); break;
                    case 8: hist_addlayer(win, SSFN_FRAG_PIXMAP); break;
                }
                if(win->menu == 4 || win->menu > 5) win->menu = 5;
                if(win->bg) free(win->bg);
                win->bg = NULL;
                ui_resizewin(win, win->w, win->h);
                return 1;
            } else
            if(chars[win->v][win->chr].len && win->menu >= 9)
                ctrl_color(win, evt, &frags[chars[win->v][win->chr].frags[win->frag]].color[win->menu-9]);
            else
            if(win->menu == 3) {
                if(evt->y > (win->h >> 1) && evt->y < (win->h >>1) + 20 && evt->x >= 68 + ((win->w - 80) >> 1)) {
                    file_delchar(win->v, win->chr);
                    ui_closewin(evt->win);
                } else {
                    win->menu = 5;
                    ui_resizewin(win, win->w, win->h);
                    return 1;
                }
            } else
            if(evt->y > 32 && evt->x > 52 && clickx == evt->x && clicky == evt->y) {
                if(win->menu == 0) {
                    if(evt->h & 1)
                        hist_setbline(win, mousey);
                    else if(evt->h & 2)
                        hist_setuline(win, mousey);
                    else
                        hist_setadv(win, mousex, 0, 0);
                    clickx = clicky = -1;
                    return 1;
                } else
                if(win->menu == 1) {
                    hist_setadv(win, 0, mousey, 0);
                    clickx = clicky = -1;
                    return 1;
                } else
                if(win->menu == 5 && chars[win->v][win->chr].len) {
modify:             switch(frags[chars[win->v][win->chr].frags[win->frag]].type) {
                        case SSFN_FRAG_BITMAP:
                            i = mousey * ((16 << font->quality)>>3) + (mousex >> 3);
                            j = ((uint8_t*)frags[chars[win->v][win->chr].frags[win->frag]].data)[i];
                            if((mousebtn & 1) || (mousebtn & 6))
                                hist_setpixel(win, i, mousebtn & 1 ? j | (1<<(mousex&7)) : j & ~(1<<(mousex&7)));
                        break;
                        case SSFN_FRAG_PIXMAP:
                            i = mousey * (16 << font->quality) + mousex;
                            if(evt->h) {
                                frags[chars[win->v][win->chr].frags[win->frag]].color[0] =
                                    ((uint8_t*)frags[chars[win->v][win->chr].frags[win->frag]].data)[i];
                            } else if((mousebtn & 1) || (mousebtn & 6))
                                hist_setpixel(win, i, mousebtn & 1? frags[chars[win->v][win->chr].frags[win->frag]].color[0]:240);
                        break;
                        default:
                            if(mousebtn & 1) {
                                if(evt->h & ~1) {
                                    hist_addcont(win, (evt->h & 8) || (evt->h & 6) == 6 ? SSFN_CONTOUR_QUAD :
                                        (evt->h & 4 ? SSFN_CONTOUR_CUBIC : SSFN_CONTOUR_LINE), mousex, mousey);
                                }
                            } else
                            if(mousebtn & 6) {
                                hist_delcont(win);
                            }
                        break;
                    }
                    clickx = clicky = -1;
                    return 1;
                }
            }
            clickx = clicky = -1;
        break;
    }
    return 0;
}
