/*
 * sfnconv/sfn2asc.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Scalable Screen Font to ASCII text converter
 *
 * Note: this tools handles memory allocation in a lazy fashion, but
 * that's okay as it's a command line tool which exists immediately
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SSFN_NOIMPLEMENTATION
#include "../ssfn.h"
#if HAS_ZLIB
# include <zlib.h>
#endif
#if HAS_UNICODE
# define _UNICODE_BLOCKSDATA
# include "../sfnedit/unicode.h"
#endif

/*** variables ***/
ssfn_font_t *font;
unsigned char *cmap;
char u[5], dot = 1, bit[65536];
int dy;

/**
 * Convert hex string to binary number. Use this lightning fast implementation
 * instead of the unbeliveably crap, slower than a pregnant snail sscanf...
 */
uint32_t gethex(char *ptr, int len)
{
    uint32_t ret = 0;
    for(;len--;ptr++) {
        if(*ptr>='0' && *ptr<='9') {          ret <<= 4; ret += (uint32_t)(*ptr-'0'); }
        else if(*ptr >= 'a' && *ptr <= 'f') { ret <<= 4; ret += (uint32_t)(*ptr-'a'+10); }
        else if(*ptr >= 'A' && *ptr <= 'F') { ret <<= 4; ret += (uint32_t)(*ptr-'A'+10); }
        else break;
    }
    return ret;
}

/**
 * Convert UNICODE code point into UTF-8 sequence
 */
char *utf8(int i)
{
    if(i<0x80) { u[0]=i; u[1]=0;
    } else if(i<0x800) {
        u[0]=((i>>6)&0x1F)|0xC0;
        u[1]=(i&0x3F)|0x80;
        u[2]=0;
    } else if(i<0x10000) {
        u[0]=((i>>12)&0x0F)|0xE0;
        u[1]=((i>>6)&0x3F)|0x80;
        u[2]=(i&0x3F)|0x80;
        u[3]=0;
    } else {
        u[0]=((i>>18)&0x07)|0xF0;
        u[1]=((i>>12)&0x3F)|0x80;
        u[2]=((i>>6)&0x3F)|0x80;
        u[3]=(i&0x3F)|0x80;
        u[4]=0;
    }
    return u;
}

/**
 * Parse a fragment
 */
void frag(FILE *f, unsigned char *raw, int w, int h, int ox, int oy)
{
    int i, l, t, x, y, a, b, c, d;
    unsigned char *re;

    if(raw[0] & 0x80) {
        switch((raw[0] & 0x60)>>5) {
            case SSFN_FRAG_LBITMAP:
                x = ((((raw[0]>>2)&3)<<8)+raw[1])+1;
                y = (((raw[0]&3)<<8)|raw[2])+1;
                raw += 3;
                goto bitmap;

            case SSFN_FRAG_BITMAP:
                x = (raw[0] & 0x1F)+1;
                y = raw[1]+1;
                raw += 2;
bitmap:         if(x*y > 65535) fprintf(f, "bitmap %d x %d too large\n", x, y);
                else {
                    if(oy>dy) {
                        memset(bit, (dot ? '.' : ' '), x*8);
                        bit[x*8]=0;
                        for(;oy>dy && dy<h;dy++)
                            fprintf(f,"%s\n",bit);
                    }
                    for(l=0;l<y && l<h;l++) {
                        for(i=t=0;t<x;t++,raw++) {
                            for(a=1;a<256;a<<=1,i++)
                                bit[i] = raw[0] & a ? 'X' : (dot ? '.' : ' ');
                        }
                        bit[i]=0;
                        fprintf(f,"%s\n",bit);
                    }
                    dy+=y;
                }
            break;

            case SSFN_FRAG_PIXMAP:
                x = (((raw[0] & 12) << 6) | raw[1])+1;
                y = (((raw[0] & 3) << 8) | raw[2])+1;
                l = ((raw[4]<<8) | raw[3])+1;
                if(raw[0]&0x10) {
                    /* todo: direct ARGB values in pixmap fragment */
                }
                raw += 5;
                if(x*y > 65535) fprintf(f, "pixmap %d x %d too large\n", x, y);
                else {
                    for(a=x*y,re=raw+l,i=0;i<a && raw<re;) {
                        c = (raw[0] & 0x7F)+1;
                        if(raw[0] & 0x80) {
                            for(t=0; t < c; t++) bit[i++] = raw[1];
                            raw += 2;
                        } else {
                            raw++;
                            for(t=0; t < c; t++) bit[i++] = *raw++;
                        }
                    }
                    if(oy>dy) {
                        for(;oy>dy && dy<h;dy++) {
                            for(i=0;i<ox+x;i++)
                                fprintf(f,"%s........",i?" ":"");
                            fprintf(f,"\n");
                        }
                    }
                    for(re=(unsigned char*)bit,l=0;l<y && l<h;l++) {
                        for(i=0;i<ox;i++)
                            fprintf(f,"%s........",i?" ":"");
                        for(t=0;t<x;t++,i++,re++) {
                            if(re[0] >= 0xF0)
                                fprintf(f,"%s........",i?" ":"");
                            else
                                fprintf(f,"%s%02x%02x%02x%02x",i?" ":"", cmap[re[0]*4+3], cmap[re[0]*4+2],
                                    cmap[re[0]*4+1], cmap[re[0]*4+0]);
                        }
                        for(;i<w;i++)
                            fprintf(f," ........");
                        fprintf(f,"\n");
                    }
                    dy+=y;
                }
            break;

            case SSFN_FRAG_HINTING:
                if(raw[0]&0x10) {
                    x = (((raw[0] & 0xF)<<8) | raw[1]) + 1;
                    raw += 2;
                } else {
                    x = (raw[0] & 0xF) + 1;
                    raw++;
                }
                if(ox) { y=ox-1; } else { y=oy-1; }
                fprintf(f,"%c %d", ox?'H':'V', y);
                for(i=0;i<x;i++) {
                    if(font->features & SSFN_FEAT_HBIGCRD) {
                        y += ((raw[1]<<8)|raw[0]);
                        raw += 2;
                    } else {
                        y += raw[0];
                        raw++;
                    }
                    fprintf(f," %d", y);
                }
                fprintf(f,"\n");
            break;
        }
    } else {
        if(raw[0] & 0x40) {
            l = ((raw[0] & 0x3F) << 8) | raw[1];
            raw += 2;
        } else {
            l = raw[0] & 0x3F;
            raw++;
        }
        l++;
        if(font->quality < 5) {
            x = raw[0] & 0xFF; y = raw[1] & 0xFF;
            raw += 2;
        } else {
            x = ((raw[0] & 15) << 8) | raw[1]; y = ((raw[0] & 0xF0) << 4) | raw[2];
            raw += 3;
        }
        fprintf(f,"m %d,%d\n",ox+x,oy+y);
        for(i=0;i<l;i++) {
            t = font->quality < 4 ? (raw[0] >> 7) | ((raw[1] >> 6) & 2) : raw[0] & 3;
            x = y = a = b = c = d = 0;
            switch(font->quality) {
                case 0:
                case 1:
                case 2:
                case 3:
                    x = raw[0] & 0x7F; y = raw[1] & 0x7F;
                    switch(t) {
                        case 0: a = ((x & 1) << 7) | y;
                            if(raw[0] & 4) { b = raw[2]; c = raw[3]; d = raw[4]; raw += 5; } else { b=c=d=a; raw += 2; } break;
                        case 1: raw += 2; break;
                        case 2: a = raw[2] & 0x7F; b = raw[3] & 0x7F; raw += 4; break;
                        case 3: a = raw[2] & 0x7F; b = raw[3] & 0x7F; c = raw[4] & 0x7F; d = raw[5] & 0x7F; raw += 6; break;
                    }
                break;

                case 4:
                    x = raw[1]; y = raw[2];
                    switch(t) {
                        case 0: a = raw[1];
                            if(raw[0] & 4) { b = raw[2]; c = raw[3]; d = raw[4]; raw += 5; } else { b=c=d=a; raw += 2; } break;
                        case 1: raw += 3; break;
                        case 2: a = raw[3]; b = raw[4]; raw += 5; break;
                        case 3: a = raw[3]; b = raw[4]; c = raw[5]; d = raw[6]; raw += 7; break;
                    }
                break;

                case 5:
                    x = ((raw[0] & 4) << 6) | raw[1]; y = ((raw[0] & 8) << 5) | raw[2];
                    switch(t) {
                        case 0: a = raw[1];
                            if(raw[0] & 4) { b = raw[2]; c = raw[3]; d = raw[4]; raw += 5; } else { b=c=d=a; raw += 2; } break;
                        case 1: raw += 3; break;
                        case 2: a = ((raw[0] & 16) << 4) | raw[3]; b = ((raw[0] & 32) << 3) | raw[4]; raw += 5; break;
                        case 3: a = ((raw[0] & 16) << 4) | raw[3]; b = ((raw[0] & 32) << 3) | raw[4];
                            c = ((raw[0] & 64) << 2) | raw[5]; d = ((raw[0] & 128) << 1) | raw[6]; raw += 7; break;
                    }
                break;

                default:
                    x = ((raw[0] & 12) << 6) | raw[1]; y = ((raw[0] & 48) << 4) | raw[2];
                    switch(t) {
                        case 0: a = raw[1];
                            if(raw[0] & 4) { b = raw[2]; c = raw[3]; d = raw[4]; raw += 5; } else { b=c=d=a; raw += 2; } break;
                        case 1: raw += 3; break;
                        case 2: a = ((raw[3] & 3) << 8) | raw[4]; b = ((raw[3] & 12) << 6) | raw[5]; raw += 6; break;
                        case 3: a = ((raw[3] & 3) << 8) | raw[4]; b = ((raw[3] & 12) << 6) | raw[5];
                            c = ((raw[3] & 48) << 4) | raw[6]; d = ((raw[3] & 192) << 2) | raw[7]; raw += 8; break;
                    }
                break;
            }
            switch(t) {
                case 0:
                    if(a<240 && b==a && c==a && d==a)
                        fprintf(f,"c %02X%02X%02X%02X",cmap[a*4+3],cmap[a*4+2],cmap[a*4+1],cmap[a*4+0]);
                    else if(a<240 || b<240 || c<240 || d<240) {
                        fprintf(f,"g");
                        if(a<240)
                            fprintf(f," %02X%02X%02X%02X",cmap[a*4+3],cmap[a*4+2],cmap[a*4+1],cmap[a*4+0]);
                        else
                            fprintf(f," .");
                        if(b<240)
                            fprintf(f," %02X%02X%02X%02X",cmap[b*4+3],cmap[b*4+2],cmap[b*4+1],cmap[b*4+0]);
                        else
                            fprintf(f," .");
                        if(c<240)
                            fprintf(f," %02X%02X%02X%02X",cmap[c*4+3],cmap[c*4+2],cmap[c*4+1],cmap[c*4+0]);
                        else
                            fprintf(f," .");
                        if(d<240)
                            fprintf(f," %02X%02X%02X%02X",cmap[d*4+3],cmap[d*4+2],cmap[d*4+1],cmap[d*4+0]);
                        else
                            fprintf(f," .");
                        fprintf(f,"\n");
                    }
                break;
                case 1: fprintf(f,"l %d,%d\n",ox+x,oy+y); break;
                case 2: fprintf(f,"q %d,%d %d,%d\n",ox+x,oy+y,ox+a,oy+b); break;
                case 3: fprintf(f,"b %d,%d %d,%d %d,%d\n",ox+x,oy+y,ox+a,oy+b,ox+c,oy+d); break;
            }
        } /* for numfrags */
    }
}

/**
 * Main procedure
 */
int main(int argc, char **argv)
{
    int i = 0, j, k, l, t, T, L, m, n, x, y, a, b, c, d, w, h, v, map, range = 0, dump = 0, *frgoffs, gs, fragnum;
    char *infile, *outfile, *fam[] = { "Serif", "Sans", "Decorative", "Monospace", "Handwriting", "?" };
    char *uninamesdb[] = { "/usr/share/unicode", "~/.config/ssfn", "~/.ssfn", ".", NULL }, **uninames = NULL;
    char *dump_fam[] = { "SERIF", "SANS", "DECOR", "MONO", "HAND", "?" };
    char *dump_cmd[] = { "COLOR", "LINE ", "QUAD ", "CUBIC" };
    char *dump_frg[] = { "BITMAP ", "LBITMAP", "PIXMAP ", "HINTING" };
    char *dump_var[] = { "DEFAULT", "INITIAL", "MEDIAL", "FINAL", "LOCAL4", "LOCAL5", "LOCAL6" };
    char *dump_str[] = { "name", "family", "subfamily", "revision", "manufacturer", "license" };
    char var[7] = { '-', 'i', 'm', 'f', '4', '5', '6' };
    unsigned char *ptr, *ptr2, *ptr3;
    long int origsize;
    FILE *f;
#if HAS_ZLIB
    unsigned char hdr[2];
    gzFile g;
#endif

    /* parse flags */
    while(argv[i+1] && argv[i+1][0] == '-') {
        for(j=1;argv[i+1][j];j++) {
            switch(argv[i+1][j]) {
                case 's': dot = 0; break;
                case 'd': dump++; break;
                case 'D': dump = 99; break;
                case 'h': dump = 98; break;
                case 'r': range = 1; break;
                default: fprintf(stderr, "unknown flag '%c'\n", argv[i+1][j]); return 1;
            }
        }
        i++;
    }

    /* get input and output file names, print help if not found */
    if(argc<i+3-(range|(dump?1:0))) {
        printf("Scalable Screen Font by bzt Copyright (C) 2019 MIT license\n\n"
               "%s [-s|-r|-d*|-D|-h] <in> [out]\n\n", argv[0]);
        printf(" -s:    use spaces instead of dots in bitmaps*\n"
               " -r:    UNICODE code point range coverage report\n"
               " -h:    dump the SSFN header and string table\n"
               " -d:    dump header and all tables, check for errors\n"
               " -dd:   dump only the fragments table\n");
        printf(" -ddd:  dump only the characters table\n"
               " -dddd: dump only the kerning table\n"
               " -D:    dump everything without integrity checks\n"
               " in:    input SSFN font filename\n out:   output ASC filename (if no -r or -d given)\n\n"
               "* - don't use if your text editor removes or replaces spaces with tabs\n");
        return 1;
    }
    infile = argv[i+1];
    outfile = argv[i+2];

    f = fopen(infile,"rb");
    if(!f) { fprintf(stderr,"unable to load %s\n", infile); return 3; }
    origsize = 0;
#if HAS_ZLIB
    fread(&hdr, 2, 1, f);
    if(hdr[0]==0x1f && hdr[1]==0x8b) {
        fseek(f, -4L, SEEK_END);
        fread(&origsize, 4, 1, f);
    } else {
        fseek(f, 0, SEEK_END);
        origsize = ftell(f);
    }
    fclose(f);
    g = gzopen(infile,"r");
#else
    fseek(f, 0, SEEK_END);
    origsize = ftell(f);
    fseek(f, 0, SEEK_SET);
#endif
    font = (ssfn_font_t*)malloc(origsize);
    if(!font) { fprintf(stderr,"memory allocation error\n"); return 2; }
#if HAS_ZLIB
    gzread(g, font, origsize);
    gzclose(g);
#else
    fread(font, origsize, 1, f);
    fclose(f);
#endif

    if(memcmp(font->magic, SSFN_MAGIC, 4) || origsize != font->size || memcmp((uint8_t*)font + font->size - 4, SSFN_ENDMAGIC, 4) ||
        font->family > SSFN_FAMILY_HAND || font->fragments_offs > font->size || font->characters_offs[0] > font->size ||
        font->kerning_offs > font->size || font->fragments_offs > font->characters_offs[0]) {
            fprintf(stderr,"bad file format %s\n", infile);
            if(dump) {
                if(memcmp(font->magic, SSFN_MAGIC, 4)) {
                    if(!memcmp(font->magic, SSFN_COLLECTION, 4))
                        fprintf(stderr, "  This is a collection, extract it first with sfn2sfn\n");
                    fprintf(stderr,"  bad magic '%c%c%c%c' != 'SSFN'\n",
                        font->magic[0] < 32 ? '?' : font->magic[0], font->magic[1] < 32 ? '?' : font->magic[1],
                        font->magic[2] < 32 ? '?' : font->magic[2], font->magic[3] < 32 ? '?' : font->magic[3]);
                } else {
                    if(origsize != font->size)
                        fprintf(stderr,"  bad font size, %d != %ld\n", font->size, origsize);
                    else {
                        ptr = (uint8_t*)font + font->size - 4;
                        if(memcmp(ptr, SSFN_ENDMAGIC, 4))
                            fprintf(stderr,"  bad end magic '%c%c%c%c' != 'NFSS'\n", ptr[0], ptr[1], ptr[2], ptr[3]);
                        if(font->family > SSFN_FAMILY_HAND)
                            fprintf(stderr,"  bad font family, %d > %d\n", font->family, SSFN_FAMILY_HAND);
                        if(font->fragments_offs > font->size)
                            fprintf(stderr,"  bad fragments offset, 0x%x > 0x%x\n", font->fragments_offs, font->size);
                        if(font->characters_offs[0] > font->size)
                            fprintf(stderr,"  bad characters offset, 0x%x > 0x%x\n", font->characters_offs[0], font->size);
                        if(font->kerning_offs > font->size)
                            fprintf(stderr,"  bad kerning offset, 0x%x > 0x%x\n", font->kerning_offs, font->size);
                        if(font->fragments_offs > font->characters_offs[0])
                            fprintf(stderr,"  bad fragments offset, 0x%x > 0x%x\n", font->fragments_offs, font->characters_offs[0]);
                    }
                }
            }
            return 2;
    }

    if(dump) {
        printf("---Header---");
        gs = 16<<font->quality;
        if(dump==1||dump==98||dump==99) {
            printf("\nmagic: '%c%c%c%c'\n", font->magic[0], font->magic[1], font->magic[2], font->magic[3]);
            printf("size: %d\n", font->size);
            printf("family: %d (SSFN_FAMILY_%s)\n", font->family, dump_fam[font->family<5?font->family:5]);
            printf("style: %d %s%s\n", font->style,
                font->style & SSFN_STYLE_BOLD ? "SSFN_STYLE_BOLD " : "",
                font->style & SSFN_STYLE_ITALIC ? "SSFN_STYLE_ITALIC" : "");
            printf("quality: %d (grid %d x %d)\n", font->quality, gs, gs);
            printf("features: 0x%02x %s%s%s%s%s%s%s\n", font->features,
                font->features & SSFN_FEAT_HASBMAP ? "SSFN_FEAT_HASBMAP " : "",
                font->features & SSFN_FEAT_HASCMAP ? "SSFN_FEAT_HASCMAP " : "",
                font->features & SSFN_FEAT_HASHINT ? "SSFN_FEAT_HASHINT " : "",
                font->features & SSFN_FEAT_KBIGLKP ? "SSFN_FEAT_KBIGLKP " : "",
                font->features & SSFN_FEAT_KBIGCHR ? "SSFN_FEAT_KBIGCHR " : "",
                font->features & SSFN_FEAT_KBIGCRD ? "SSFN_FEAT_KBIGCRD " : "",
                font->features & SSFN_FEAT_HBIGCRD ? "SSFN_FEAT_HBIGCRD " : "");
            printf("revision: %d\n", font->revision);
            printf("baseline: %d\n", font->baseline);
            printf("undeline: %d\n", font->underline);
            printf("bbox_left: %d\nbbox_top: %d\nbbox_right: %d\nbbox_bottom: %d\n",
                font->bbox_left, font->bbox_top, font->bbox_right, font->bbox_bottom);
            printf("fragments_offs: 0x%08x\n", font->fragments_offs);
            printf("characters_offs[]:\n");
            for(i=0;i<7;i++)
                if(font->characters_offs[i])
                    printf("  0x%08x SSFN_VARIANT_LOCAL%d SSFN_VARIANT_%s\n", font->characters_offs[i], i, dump_var[i]);
            printf("kerning_offs: 0x%08x\n", font->kerning_offs);
            if(dump!=99) {
                if(font->family > SSFN_FAMILY_HAND) { printf("Corrupt file, bad family\n"); return 3; }
                if(font->style & ~3) { printf("Corrupt file, bad style bits\n"); return 3; }
                if(font->quality > 8) { printf("Corrupt file, bad quality value\n"); return 3; }
                if((font->features & ~0x7F) || (font->quality<5 && (font->features & SSFN_FEAT_KBIGCRD ||
                    font->features & SSFN_FEAT_HBIGCRD))) { printf("Corrupt file, bad feature bits\n"); return 3; }
                if(font->revision) printf("\nWARNING unknown file format revision\n");
                if(font->reserved0 || font->reserved1) printf("\nWARNING reserved bytes not zeros\n");
                if(font->baseline >= gs) { printf("Corrupt file, bad baseline value\n"); return 3; }
                if(font->baseline < gs/2 || font->baseline >= gs - gs/8) printf("\nWARNING unlikely baseline value\n");
                if(font->underline >= gs || font->underline < font->baseline) { printf("\nWARNING bad underline value\n"); }
            }
            printf("\n---String Table---\n");
            ptr = (unsigned char *)font + sizeof(ssfn_font_t);
            for(i=0;i<6;i++) { printf("%d. %-12s \"%s\"\n", i, dump_str[i], ptr); ptr += strlen((char*)ptr)+1; }
            if(dump==98) return 0;
        } else ptr = (unsigned char *)font + font->fragments_offs;
        cmap = (unsigned char *)font + font->size - 964;

        printf("\n---Fragments Table---\n");
        if(dump!=99 && ((uint64_t)ptr-(uint64_t)font)!=font->fragments_offs) {
            printf("Corrupt file, wrong fragments offset\n"); return 3;
        }
        frgoffs = NULL;
        for(i=0;((uint64_t)ptr-(uint64_t)font) < font->characters_offs[0];i++) {
            frgoffs = (int*)realloc(frgoffs,(i+1)*sizeof(int));
            if(!frgoffs) { fprintf(stderr,"memory allocation error\n"); return 2; }
            frgoffs[i] = ((uint64_t)ptr-(uint64_t)font);
            if(dump<3||dump==99) printf("%06lx: %02x ", ((uint64_t)ptr-(uint64_t)font), ptr[0]);
            if(ptr[0] & 0x80) {
                switch((ptr[0]&0x60)>>5) {
                    case SSFN_FRAG_BITMAP:
                        if(dump<3||dump==99)
                            printf("%02x  SSFN_FRAG_BITMAP, pitch %2d, height %2d, data %02x %02x %02x %02x...\n",
                                ptr[1],(ptr[0]&0x1F)+1,ptr[1]+1,ptr[2],ptr[3],ptr[4],ptr[5]);
                        if(dump!=99 && ((ptr[1]+1) > gs || ((ptr[0]&0x1F)+1)*8 > gs)) {
                            printf("Corrupt file, out of bound fragment\n"); return 3;
                        }
                        ptr += 2+((ptr[0]&0x1F)+1)*(ptr[1]+1);
                    break;
                    case SSFN_FRAG_LBITMAP:
                        if(dump<3||dump==99)
                            printf("%02x %02x  SSFN_FRAG_LBITMAP, pitch %2d, height %2d, "
                                "data %02x %02x %02x %02x...\n",ptr[1],ptr[2],((((ptr[0]>>2)&3)<<8)|ptr[1])+1,
                                (((ptr[0]&3)<<8)|ptr[2])+1,ptr[3],ptr[4],ptr[5],ptr[6]);
                        if(dump!=99 && ((((((ptr[0]>>2)&3)<<8)|ptr[1])+1) > gs || ((((ptr[0]&3)<<8)|ptr[2])+1)*8 > gs)) {
                            printf("Corrupt file, out of bound fragment\n"); return 3;
                        }
                        ptr += 3+(((((ptr[0]>>2)&3)<<8)|ptr[1])+1)*((((ptr[0]&3)<<8)|ptr[2])+1);
                    break;
                    case SSFN_FRAG_PIXMAP:
                        if(dump<3||dump==99)
                            printf("%02x %02x %02x %02x  SSFN_FRAG_PIXMAP, width %2d, height %2d, "
                                "size %4d, data %02x %02x %02x %02x...\n",ptr[1],ptr[2],ptr[3],ptr[4],((((ptr[0]>>2)&3)<<8)|
                                ptr[1])+1,(((ptr[0]&3)<<8)|ptr[2])+1,((ptr[4]<<8)|ptr[3])+1,ptr[5],ptr[6],ptr[7],ptr[8]);
                        if(dump!=99 && ((((ptr[0]&3)<<8)|ptr[2]) > gs || ((((ptr[0]>>2)&3)<<8)|ptr[1])+1 > gs)) {
                            printf("Corrupt file, out of bound fragment\n"); return 3;
                        }
                        ptr += 5+((ptr[4]<<8)|ptr[3])+1;
                    break;
                    case SSFN_FRAG_HINTING:
                        if(ptr[0]&0x10) {
                            if(dump<3||dump==99) printf("%02x ", ptr[1]);
                            n = (((ptr[0]&0xF)<<8)|ptr[1])+1;
                            ptr += 2;
                        } else {
                            n = (ptr[0]&0xF)+1;
                            ptr++;
                        }
                        l = font->features & SSFN_FEAT_HBIGCRD ? 2 : 1;
                        if(dump<3||dump==99) {
                            printf(" SSFN_FRAG_HINTING, n=%d\n", n);
                            for(y=0;n--;) {
                                if(l==2) y += (ptr[1]<<8)|ptr[0]; else y += ptr[0];
                                printf("  %4d+  %02x", y, ptr[0]);
                                if(l==2) {
                                    printf(" %02x", ptr[1]);
                                    ptr++;
                                }
                                printf("\n");
                                ptr++;
                                if(dump!=99 && y >= gs) {
                                    printf("Corrupt file, out of bound fragment\n"); return 3;
                                }
                            }
                            printf("\n");
                        } else
                            ptr += (n*l);
                    break;
                    default:
                        printf("Corrupt file, unknown fragment type\n"); return 3;
                    break;
                }
            } else {
                if(ptr[0]&0x40) {
                    n=(((ptr[0]&0x3F)<<8)|ptr[1])+1;
                    if(dump<3||dump==99) printf("%02x  SSFN_FRAG_CONTOUR, n=%d\n",ptr[1],n+1);
                    ptr+=2;
                } else {
                    n=(ptr[0]&0x3F)+1;
                    if(dump<3||dump==99) printf(" SSFN_FRAG_CONTOUR, n=%d\n",n+1);
                    ptr++;
                }
                if(n>127 && dump!=99) {
                    printf("Probably corrupt file, too many points. Use -D if you really want to dump this.\n");
                    return 3;
                }
                if(font->quality < 5) {
                    if(dump<3||dump==99) printf("  SSFN_CONTOUR_MOVE  %02x %02x\n", ptr[0], ptr[1]);
                    if(dump!=99 && ((ptr[0]) >= gs || (ptr[1]) >= gs)) {
                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                    }
                    ptr += 2;
                } else {
                    if(dump<3||dump==99) printf("  SSFN_CONTOUR_MOVE  %02x %02x %02x\n", ptr[0], ptr[1], ptr[2]);
                    if(dump!=99 && ((((ptr[0] & 15) << 8)) >= gs || (((ptr[0] & 0xF0) << 4) | ptr[2]) >= gs)) {
                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                    }
                    ptr += 3;
                }
                for(m=0;n--;m++) {
                    l = font->quality < 4 ? (ptr[0] >> 7) | ((ptr[1] >> 6) & 2) : ptr[0] & 3;
                    if(dump<3||dump==99) printf("  SSFN_CONTOUR_%s %02x %02x", dump_cmd[l], ptr[0], ptr[1]);
                    switch(font->quality) {
                        case 0: case 1: case 2: case 3:
                            switch(l) {
                                case 0:
                                    if(dump!=99 && ((ptr[0]&0xFE) || (((ptr[0]<<8)|(ptr[1]&0x7F)) >= 0xF0))) {
                                        printf("\nCorrupt file, out of bound color index\n"); return 3;
                                    }
                                    if(dump<3||dump==99) printf("        (%02X%02X%02X%02X)",
                                        cmap[((ptr[0]<<8)|(ptr[1]&0x7F))*4+3],cmap[((ptr[0]<<8)|(ptr[1]&0x7F))*4+2],
                                        cmap[((ptr[0]<<8)|(ptr[1]&0x7F))*4+1],cmap[((ptr[0]<<8)|(ptr[1]&0x7F))*4+0]);
                                    ptr += 2;
                                break;
                                case 1:
                                    if(dump!=99 && ((ptr[0]&0x7F) >= gs || (ptr[1]&0x7F) >= gs)) {
                                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                                    }
                                    ptr += 2;
                                break;
                                case 2:
                                    if(dump<3||dump==99) printf(" %02x %02x", ptr[2],ptr[3]);
                                    if(dump!=99 && ((ptr[0]&0x7F) >= gs || (ptr[1]&0x7F) >= gs || ptr[2] >= gs || ptr[3] >= gs)) {
                                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                                    }
                                    ptr += 4;
                                break;
                                case 3:
                                    if(dump<3||dump==99) printf(" %02x %02x %02x %02x", ptr[2],ptr[3],ptr[4],ptr[5]);
                                    if(dump!=99 && ((ptr[0]&0x7F) >= gs || (ptr[1]&0x7F) >= gs || ptr[2] >= gs || ptr[3] >= gs ||
                                        ptr[4] >= gs || ptr[5] >= gs)) {
                                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                                    }
                                    ptr += 6;
                                break;
                            }
                        break;

                        case 4: case 5:
                            if(l && (dump<3||dump==99)) printf(" %02x", ptr[2]);
                            switch(l) {
                                case 0:
                                    if(dump!=99 && ptr[1] >= 0xF0) {
                                        printf("\nCorrupt file, out of bound color index\n"); return 3;
                                    }
                                    if(dump<3||dump==99) printf("        (%02X%02X%02X%02X)",cmap[ptr[1]*4+3],cmap[ptr[1]*4+2],
                                        cmap[ptr[1]*4+1],cmap[ptr[1]*4]);
                                    ptr += 2;
                                break;
                                case 1:
                                    if(dump!=99 && ((((ptr[0]&4)<<6)|ptr[1]) >= gs || (((ptr[0]&8)<<5)|ptr[2]) >= gs)) {
                                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                                    }
                                    ptr += 3;
                                break;
                                case 2:
                                    if(dump<3||dump==99) printf(" %02x %02x", ptr[3],ptr[4]);
                                    if(dump!=99 && ((((ptr[0]&4)<<6)|ptr[1]) >= gs || (((ptr[0]&8)<<5)|ptr[2]) >= gs ||
                                        (((ptr[0]&16)<<4)|ptr[3]) >= gs || (((ptr[0]&32)<<3)|ptr[4]) >= gs)) {
                                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                                    }
                                    ptr += 5;
                                break;
                                case 3:
                                    if(dump<3||dump==99) printf(" %02x %02x %02x %02x", ptr[3],ptr[4],ptr[5],ptr[6]);
                                    if(dump!=99 && ((((ptr[0]&4)<<6)|ptr[1]) >= gs || (((ptr[0]&8)<<5)|ptr[2]) >= gs ||
                                        (((ptr[0]&16)<<4)|ptr[3]) >= gs || (((ptr[0]&32)<<3)|ptr[4]) >= gs ||
                                        (((ptr[0]&64)<<2)|ptr[5]) >= gs || (((ptr[0]&128)<<1)|ptr[6]) >= gs)) {
                                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                                    }
                                    ptr += 7;
                                break;
                            }
                        break;

                        default:
                            if(l && (dump<3||dump==99)) printf(" %02x", ptr[2]);
                            switch(l) {
                                case 0:
                                    if(dump!=99 && ptr[1] >= 0xF0) {
                                        printf("\nCorrupt file, out of bound color index\n"); return 3;
                                    }
                                    if(dump<3||dump==99) printf("        (%02X%02X%02X%02X)",cmap[ptr[1]*4+3],cmap[ptr[1]*4+2],
                                        cmap[ptr[1]*4+1],cmap[ptr[1]*4]);
                                    ptr += 2;
                                break;
                                case 1:
                                    if(dump!=99 && ((((ptr[0]&12)<<6)|ptr[1]) >= gs || (((ptr[0]&48)<<4)|ptr[2]) >= gs)) {
                                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                                    }
                                    ptr += 3;
                                break;
                                case 2:
                                    if(dump<3||dump==99) printf(" %02x %02x %02x", ptr[3], ptr[4],ptr[5]);
                                    if(dump!=99 && ((((ptr[0]&12)<<6)|ptr[1]) >= gs || (((ptr[0]&48)<<4)|ptr[2]) >= gs ||
                                        (((ptr[3]&3)<<8)|ptr[4]) >= gs || (((ptr[3]&12)<<6)|ptr[5]) >= gs)) {
                                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                                    }
                                    ptr += 6;
                                break;
                                case 3:
                                    if(dump<3||dump==99) printf(" %02x %02x %02x %02x %02x", ptr[3],ptr[4],ptr[5],ptr[6],ptr[7]);
                                    if(dump!=99 && ((((ptr[0]&12)<<6)|ptr[1]) >= gs || (((ptr[0]&48)<<4)|ptr[2]) >= gs ||
                                        (((ptr[3]&3)<<8)|ptr[4]) >= gs || (((ptr[3]&12)<<6)|ptr[5]) >= gs ||
                                        (((ptr[3]&48)<<4)|ptr[6]) >= gs || (((ptr[3]&192)<<2)|ptr[7]) >= gs)) {
                                        printf("\nCorrupt file, out of bound fragment\n"); return 3;
                                    }
                                    ptr += 8;
                                break;
                            }
                        break;
                    }
                    if(dump<3||dump==99) printf("\n");
                }
                if(dump<3||dump==99) printf("\n");
            }
        }
        fragnum = i;
        if(font->quality < 5 && font->characters_offs[0] < 65536) l = 4; else
        if(font->characters_offs[0] < 1048576) l = 5; else l = 6;
        if(dump!=99 && !font->characters_offs[0]) {
            printf("Corrupt file, no characters table offset for default variant\n"); return 3;
        }
        if(dump!=99) {
            ptr3 = malloc(0x110000);
            if(!ptr3) { fprintf(stderr,"memory allocation error\n"); return 2; }
        }
        for(v=0;v<SSFN_NUMVARIANTS;v++) {
            if(!font->characters_offs[v]) continue;
            printf("\n---Characters Table (SSFN_VARIANT_LOCAL%d, SSFN_VARIANT_%s)---\n", v, dump_var[v]);
            if(dump!=99 && ((uint64_t)ptr-(uint64_t)font)!=font->characters_offs[v]) {
                printf("Corrupt file, wrong characters table offset (variant %d)\n", v); return 3;
            }
            for(a=1,i=0;i<0x110000;i++) {
                if((!(ptr[0]&0x80) && i) || !a) { if(dump==1||dump==3||dump==99) printf("\n"); }
                if(dump==1||dump==3||dump==99) printf("%02x ", ptr[0]);
                if(ptr[0] & 0x80) {
                    a=1;
                    if(ptr[0] & 0x40) {
                        if(dump==1||dump==3||dump==99)
                            printf("%02x  skip %5d code points\n", ptr[1], (ptr[1] | ((ptr[0] & 0x3f) << 8))+1);
                        i += ptr[1] | ((ptr[0] & 0x3f) << 8); ptr += 2;
                    } else {
                        if(dump==1||dump==3||dump==99)
                            printf("      skip %5d code points\n", (ptr[0] & 0x3f)+1);
                        i += ptr[0] & 0x3f; ptr++;
                    }
                } else {
                    if(dump!=99) {
                        if(!v) ptr3[i]=1;
                        else if(!ptr3[i]) {
                            printf("Corrupt file, glyph variant %d for U+%06X without default glyph\n", v, i); return 3;
                        }
                    }
                    a=0;
                    n = ptr[0];
                    w = ((ptr[1]&0xF)<<8)|ptr[4]; h = ((ptr[1]&0xF0)<<4)|ptr[5];
                    L = ((ptr[3]&0xF)<<8)|ptr[8]; T = ((ptr[3]&0xF0)<<4)|ptr[9];
                    if(dump==1||dump==3||dump==99)
                        printf("%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x  "
                            "U+%06X '%s', width %d, height %d, advance x %d y %d, bearing left %d top %d, n=%d\n",
                            ptr[0],ptr[1],ptr[2],ptr[3],ptr[4],ptr[5],ptr[6],ptr[7],ptr[8],ptr[9], i, utf8(i), w,h,
                            ((ptr[2]&0xF)<<8)|ptr[6],((ptr[2]&0xF0)<<4)|ptr[7], L,T, n);
                    if(dump!=99 && (w > gs || h > gs)) {
                        printf("Corrupt file, out of bound dimension\n"); return 3;
                    }
                    if(dump!=99 && ((((ptr[2]&0xF)<<8)|ptr[6]) > gs || (((ptr[2]&0xF0)<<4)|ptr[7]) > gs)) {
                        printf("Corrupt file, out of bound advance\n"); return 3;
                    }
                    if(dump!=99 && (L > w || T > h)) {
                        printf("Corrupt file, out of bound bearing\n"); return 3;
                    }
                    ptr += 10;
                    for(b=d=0; n--; ptr += l) {
                        if(dump==1||dump==3||dump==99) {
                            printf("  ");
                            for(j=0;j<l;j++) printf("%02x ", ptr[j]);
                        }
                        x = (((ptr[1] >> 4) & 3) << 8) | ptr[4];
                        y = (((ptr[1] >> 6) & 3) << 8) | ptr[5];
                        switch(l) {
                            case 4: k=(ptr[1] << 8) | ptr[0]; x = ptr[2]; y = ptr[3]; break;
                            case 5: k=((ptr[2] & 0xF) << 16) | (ptr[1] << 8) | ptr[0];
                                x = (((ptr[2] >> 4) & 3) << 8) | ptr[3]; y = (((ptr[2] >> 6) & 3) << 8) | ptr[4]; break;
                            default: k=(ptr[2] << 16) | (ptr[1] << 8) | ptr[0];
                                x = ((ptr[3] & 0xF) << 8) | ptr[4]; y = (((ptr[3] >> 4) & 0xF) << 8) | ptr[5]; break;
                        }
                        t = (k < (int)font->fragments_offs || k >= (int)font->characters_offs[0]) ? -1 : ((unsigned char*)font)[k];
                        if(dump==1||dump==3||dump==99) printf(" fragment %06x SSFN_FRAG_%s at x=%d, y=%d\n", k,
                            t == -1 ? "???" : (t & 0x80 ? dump_frg[(t&0x60)>>5] : "CONTOUR"), x, y);
                        /* should add fragment's width and height */
                        if(dump!=99 && (t & 0xE0) != 0xE0 && (L+x > w || T+y > h)) {
                            printf("Corrupt file, out of bound fragment placement\n"); return 3;
                        }
                        if(dump!=99 && (k < (int)font->fragments_offs || k >= (int)font->characters_offs[0])) {
                            printf("Corrupt file, not a valid fragment offset\n");
                            return 3;
                        }
                        if(dump!=99) {
                            for(m=j=0;j<fragnum;j++)
                                if(frgoffs[j]==k) { m=1; break; }
                            if(!m) { printf("Corrupt file, not a valid fragment offset\n"); return 3; }
                        }
                        if(dump!=99 && (t & 0xE0) == 0xE0) {
                            if(b!=0) { printf("Corrupt file, hinting fragment is not the first\n"); return 3; }
                            if((x && y) || (!x && !y)) { printf("Corrupt file, bad hinting fragment coordinates\n"); return 3; }
                        } else b++;
                        if((t & 0x80) && (t & 0xE0) != 0xE0) {
                            if(dump!=99 && y < d) {
                                printf("Corrupt file, bitmap fragments not in scanline order\n");
                                return 3;
                            }
                            d = y;
                        }
                    }
                }
            }
        }
        if(dump!=99) free(ptr3);
        free(frgoffs);

        if(font->kerning_offs) {
            printf("\n---Kerning---\n");
            if(dump!=99 && ((uint64_t)ptr-(uint64_t)font)!=font->kerning_offs) {
                printf("Corrupt file, wrong kerning lookup table offset\n"); return 3;
            }
            if(dump==1||dump==4||dump==99) printf("  -- Lookup Table --\n");
            for(y=font->size,k=i=0;i<0x110000;i++) {
                if(dump==1||dump==4||dump==99) printf("%02x ", ptr[0]);
                if(ptr[0] & 0x80) {
                    if(ptr[0] & 0x40) {
                        if(dump==1||dump==4||dump==99)
                            printf("%02x  skip %5d code points\n", ptr[1], (ptr[1] | ((ptr[0] & 0x3f) << 8))+1);
                        i += ptr[1] | ((ptr[0] & 0x3f) << 8); ptr += 2;
                    } else {
                        if(dump==1||dump==4||dump==99)
                            printf("  skip %5d code points\n", (ptr[0] & 0x3f)+1);
                        i += ptr[0] & 0x3f; ptr++;
                    }
                } else {
                    j = (ptr[0] & 0x7F);
                    n = (ptr[2]<<8)|ptr[1];
                    if(dump==1||dump==4||dump==99) printf("%02x %02x", ptr[1], ptr[2]);
                    if(font->features & SSFN_FEAT_KBIGLKP) {
                        if(dump==1||dump==4||dump==99) printf(" %02x", ptr[3]);
                        n |= ptr[3]<<16;
                        ptr += 4;
                    } else {
                        ptr += 3;
                    }
                    if(dump==1||dump==4||dump==99) {
                        if(j)
                            printf("  U+%06X - U+%06X, group %06x\n",i,i+j,n);
                        else
                            printf("  U+%06X, group %06x\n",i,n);
                    }
                    if(n<y) y=n;
                    if(n>k) k=n;
                    i += j;
                }
            }
            i=(uint64_t)ptr - (uint64_t)font - font->kerning_offs;
            if(dump!=99 &&  i != y) { printf("Corrupt file, wrong kerning groups table offset\n"); return 3; }
            if(dump==1||dump==4||dump==99) printf("  -- Groups Table --\n");
            l = font->features & SSFN_FEAT_KBIGCHR ? 3 : 2;
            m = font->features & SSFN_FEAT_KBIGCRD ? 2 : 1;
            while(i<=k) {
                if(dump==1||dump==4||dump==99) printf("%02x ",ptr[0]);
                if(ptr[0] & 0x80) {
                    if(dump==1||dump==4||dump==99) printf("%02x ",ptr[1]);
                    n = ptr[1] | ((ptr[0] & 0x7f) << 8);
                } else { n = ptr[0] & 0x7F; }
                n++;
                if(dump==1||dump==4||dump==99)
                    printf(" group %06X: %d pair(s)\n",i,n);
                if(ptr[0] & 0x80) { ptr += 2; i += 2; } else { ptr++; i++; }
                while(n--) {
                    if(dump==1||dump==4||dump==99) {
                        printf("  ");
                        for(j=0;j<l+m;j++) printf("%02x ", ptr[j]);
                        printf(" U+%06X %s ", l==2? ((ptr[1]&0x7F)<<8) | ptr[0] : ((ptr[2]&0x7F)<<16) | (ptr[1]<<8) | ptr[0],
                            ptr[l-1]&0x80?"vertical":"horizontal");
                    }
                    ptr += l; i += l;
                    if(dump==1||dump==4||dump==99)
                        printf("%3d\n", m==1? (signed char)ptr[0] : (short)((ptr[1]<<8) | ptr[0]));
                    ptr += m; i += m;
                }
            }
        }
        if(font->features & SSFN_FEAT_HASCMAP) {
            printf("\n---Color Map (offset 0x%lx, size 960)---\n", (uint64_t)ptr-(uint64_t)font);
            if(dump==99) {
                printf("   "); for(i=0;i<16;i++) printf("       %02X",i);
                for(i=j=0;i<240;i++) {
                    if(!(i&15)) printf("\n%02X: ", i);
                    printf("%02x%02x%02x%02x ",ptr[3],ptr[2],ptr[1],ptr[0]);
                    ptr += 4;
                }
                printf("\n");
            } else {
                if(dump==1||dump==5) {
                    for(i=0;i<240 && (ptr[i*4+0] || ptr[i*4+0] || ptr[i*4+1] || ptr[i*4+2]);i++);
                    printf("number of colors: %d\n",i);
                }
                ptr += 960;
            }
        }
        if(((uint64_t)ptr-(uint64_t)font)!=font->size-4 || memcmp(ptr, SSFN_ENDMAGIC, 4))
            { printf("Corrupt file, bad end magic or wrong font size\n"); return 3; }
        else
            printf("Font parsed OK.\n");
        return 0;
    }

    if(range) {
        printf("| Coverage | NumChar | Start  | End    | Description                                |\n"
               "| -------: | ------: | ------ | ------ | ------------------------------------------ |\n");
        ptr = (unsigned char *)font + font->characters_offs[0];
        if(font->quality < 5 && font->characters_offs[0] < 65536) l = 4; else
        if(font->characters_offs[0] < 1048576) l = 5; else l = 6;
        for(j=i=m=0;i<0x110000;i++) {
            if(ptr[0] & 0x80) {
                if(ptr[0] & 0x40) { i += ptr[1] | ((ptr[0] & 0x3f) << 8); ptr += 2; } else { i += ptr[0] & 0x3f; ptr++; }
            } else {
#if HAS_UNICODE
                m++;
                for(k=0;k<(int)(sizeof(ublocks)/sizeof(ublocks[0]));k++) {
                    if(i >= ublocks[k].start && i <= ublocks[k].end) { ublocks[k].cnt++; m--; break; }
                }
#endif
                ptr += ptr[0] * l + 10;
                j++;
            }
        }
#if HAS_UNICODE
        for(a=b=k=0;k<(int)(sizeof(ublocks)/sizeof(ublocks[0]));k++) {
            if(ublocks[k].cnt) {
                n = ublocks[k].end - ublocks[k].start + 1 - ublocks[k].undef;
                if(ublocks[k].cnt > n) { m += ublocks[k].cnt - n; ublocks[k].cnt = n; };
                a += ublocks[k].cnt; b += n;
                d = ublocks[k].cnt * 1000 / n;
                printf("|   %3d.%d%% | %7d | %06X | %06X | %-42s |\n", d/10, d%10,
                    ublocks[k].cnt, ublocks[k].start, ublocks[k].end, ublocks[k].name);
            }
        }
        if(m)
            printf("|        - | %7d | 000000 | 10FFFF | No Block                                   |\n", m);
        d = a * 1000 / b;
        printf("| -------- | ------- | ------------------------------------------------------------ |\n"
        "|   %3d.%d%% | %7d |     = = = = = = = =   Overall Coverage   = = = = = = = =     |\n", d/10, d%10, a);
#else
        d = j * 1000 / 0x110000;
        printf("|   %3d.%d%% | %7d | %06X | %06X | %-42s |\n", d/10, d%10,
            0x110000, 0, 0x10FFFF, "No UNICODE blocks data, showing overall coverage");
#endif
        return 0;
    }
    if(!outfile) { fprintf(stderr,"no output file specified\n"); free(font); return 3; }

    ptr = (unsigned char*)malloc(1024);
    if(ptr) {
        for(i=0;uninamesdb[i];i++) {
            sprintf((char*)ptr, "%s%s/UnicodeData.txt",
                uninamesdb[i][0] != '~'? uninamesdb[i] : getenv("HOME"), uninamesdb[i][0] != '~'? "" : uninamesdb[i]+1);
            f = fopen((char*)ptr,"rb");
            if(!f) continue;
            uninames = (char**)malloc(0x110000 * sizeof(char*));
            if(uninames) {
                memset(uninames, 0, 0x110000 * sizeof(char*));
                while(!feof(f)) {
                    ptr[0] = 0;
                    fgets((char*)ptr, 1024, f);
                    if(!ptr[0]) break;
                    i = gethex((char*)ptr, 6);
                    for(ptr3=ptr+1;*ptr3 && ptr3[-1]!=';';ptr3++);
                    for(ptr2=ptr,j=0;*ptr2 && *ptr2!='\n' && j<10;ptr2++) {
                        if(*ptr2==';') { j++; }
                    }
                    if(*ptr2==';') ptr2 = ptr3;
                    for(ptr3=ptr2;*ptr3 && *ptr3!=';';ptr3++);
                    if(ptr3 > ptr2) {
                        *ptr3=0;
                        uninames[i] = (char*)malloc(ptr3-ptr2+1);
                        if(uninames[i]) memcpy(uninames[i], ptr2, ptr3-ptr2+1);
                    }
                }
            }
            fclose(f);
            break;
        }
        free(ptr);
    }

    f = fopen(outfile,"w");
    if(!f) { fprintf(stderr,"unable to write %s\n", outfile); free(font); return 3; }

    fprintf(f,"## Scalable Screen Font (revision %d)\n\n+@---Header---\n", font->revision);
    fprintf(f,"+#family: %d (%s)\n", font->family, fam[font->family]);
    fprintf(f,"+#style:");
    if(!font->style) fprintf(f," regular");
    else {
        if(font->style & SSFN_STYLE_BOLD) fprintf(f," bold");
        if(font->style & SSFN_STYLE_ITALIC) fprintf(f," italic");
    }
    fprintf(f,"\n+#quality: %d (grid %d x %d)\n", font->quality, 16<<font->quality, 16<<font->quality);
    fprintf(f,"+#baseline: %d\n+#underline: %d\n", font->baseline, font->underline);
    fprintf(f, "+#boundingbox: %d,%d %d,%d (informational)\n",font->bbox_left,font->bbox_top,font->bbox_right,font->bbox_bottom);
    ptr = (unsigned char *)font + sizeof(ssfn_font_t);
    for(i=0;i<6;i++) {
        fprintf(f,"+$%s: %s\n", dump_str[i], ptr); ptr += strlen((char*)ptr)+1;
    }
    fprintf(f,"\n+@---Glyphs---\n");

    ptr = (unsigned char *)font + font->characters_offs[0];
    cmap = (unsigned char *)font + font->size - 964;
    if(font->quality < 5 && font->characters_offs[0] < 65536) l = 4; else
    if(font->characters_offs[0] < 1048576) l = 5; else l = 6;

    for(i=0;i<0x110000;i++) {
        if(ptr[0] & 0x80) {
            if(ptr[0] & 0x40) { i += ptr[1] | ((ptr[0] & 0x3f) << 8); ptr += 2; }
            else { i += ptr[0] & 0x3f; ptr++; }
        } else {
            n = (ptr[0] & 0x7F);
            w = ((ptr[1]&0x0F)<<8)|ptr[4];
            h = ((ptr[1]&0xF0)<<4)|ptr[5];
            x = ((ptr[2]&0x0F)<<8)|ptr[6];
            y = ((ptr[2]&0xF0)<<4)|ptr[7];
            L = ((ptr[3]&0x0F)<<8)|ptr[8];
            T = ((ptr[3]&0xF0)<<4)|ptr[9];
            map = 0;
            if(n) {
                switch(l) {
                    case 4: k=(ptr[11] << 8) | ptr[10]; break;
                    case 5: k=((ptr[12] & 0xF) << 16) | (ptr[11] << 8) | ptr[10]; break;
                    default: k=(ptr[12] << 16) | (ptr[11] << 8) | ptr[10]; break;
                }
                if(k && (((unsigned char*)font + k)[0] & 0x80) && (((unsigned char*)font + k)[0] & 0x60) != 0x60) {
                    map = 1 + ((((unsigned char*)font + k)[0] & 0x40)?1:0);
                }
            }
            if(map==1) {
                if(w == x)
                    fprintf(f,"\n++---%d", i);
                else
                    fprintf(f,"\n+=---%d-%d-%d", i, x, y);
                dy = 0;
            } else if(map==2) {
                fprintf(f,"\n+%%---%d-%d-%d", i, x, y);
                dy = 0;
            } else
                fprintf(f,"\n+!---%d-%d-%d", i, x, y);
            fprintf(f,"---U+%06X-", i);
            if(i>=32) fprintf(f, "'%s'", utf8(i));
            if(!i) fprintf(f,"-'UNDEF'---\n");
            else if(uninames && uninames[i]) fprintf(f,"-'%s'---\n", uninames[i]);
            else fprintf(f,"---\n");
            ptr += 10;
            for(;n--;ptr += l) {
                x = (((ptr[1] >> 4) & 3) << 8) | ptr[4];
                y = (((ptr[1] >> 6) & 3) << 8) | ptr[5];
                switch(l) {
                    case 4: k=(ptr[1] << 8) | ptr[0]; x = ptr[2]; y = ptr[3]; break;
                    case 5: k=((ptr[2] & 0xF) << 16) | (ptr[1] << 8) | ptr[0];
                        x = (((ptr[2] >> 4) & 3) << 8) | ptr[3]; y = (((ptr[2] >> 6) & 3) << 8) | ptr[4]; break;
                    default: k=(ptr[2] << 16) | (ptr[1] << 8) | ptr[0];
                        x = ((ptr[3] & 0xF) << 8) | ptr[4]; y = (((ptr[3] >> 4) & 0xF) << 8) | ptr[5]; break;
                }
                frag(f, (unsigned char*)font + k, w, h, ((((unsigned char*)font + k)[0] & 0xE0) == 0xE0 && !x? 0 : L+x), T+y);
            }
            if(map && dy < font->bbox_bottom+1) {
                if(map==1) {
                    memset(bit, (dot ? '.' : ' '), w);
                    bit[w]=0;
                    for(;dy<font->bbox_bottom+1;dy++)
                        fprintf(f,"%s\n",bit);
                } else {
                    for(;dy<font->bbox_bottom+1;dy++) {
                        for(x=0;x<w;x++)
                            fprintf(f,"%s........",x?" ":"");
                        fprintf(f,"\n");
                    }
                }
            }
            /* this is not effective, but gets the job done */
            for(v=1;v<SSFN_NUMVARIANTS;v++) {
                if(!font->characters_offs[v]) continue;
                ptr2 = (unsigned char *)font + font->characters_offs[v];
                for(j=0;j<0x110000;j++) {
                    if(ptr2[0] & 0x80) {
                        if(ptr2[0] & 0x40) { j += ptr2[1] | ((ptr2[0] & 0x3f) << 8); ptr2 += 2; }
                        else { j += ptr2[0] & 0x3f; ptr2++; }
                    } else {
                        n = (ptr2[0] & 0x7F);
                        if(j==i) {
                            w = ((ptr2[1]&0x0F)<<8)|ptr2[4];
                            h = ((ptr2[1]&0xF0)<<4)|ptr2[5];
                            x = ((ptr2[2]&0x0F)<<8)|ptr2[6];
                            y = ((ptr2[2]&0xF0)<<4)|ptr2[7];
                            L = ((ptr2[3]&0x0F)<<8)|ptr2[8];
                            T = ((ptr2[3]&0xF0)<<4)|ptr2[9];
                            map = 0;
                            if(n) {
                                switch(l) {
                                    case 4: k=(ptr2[11] << 8) | ptr2[10]; break;
                                    case 5: k=((ptr2[12] & 0xF) << 16) | (ptr2[11] << 8) | ptr2[10]; break;
                                    default: k=(ptr2[12] << 16) | (ptr2[11] << 8) | ptr2[10]; break;
                                }
                                if(k && (((unsigned char*)font + k)[0] & 0x80) && (((unsigned char*)font + k)[0] & 0x60) != 0x60) {
                                    map = 1 + ((((unsigned char*)font + k)[0] & 0x40)?1:0);
                                }
                            }
                            if(map==1) {
                                if(w == x)
                                    fprintf(f,"\n++-%c-%d", var[v], i);
                                else
                                    fprintf(f,"\n+=-%c-%d-%d-%d", var[v], i, x, y);
                                dy = 0;
                            } else if(map==2) {
                                fprintf(f,"\n+%%-%c-%d-%d-%d", var[v], i, x, y);
                                dy = 0;
                            } else
                                fprintf(f,"\n+!-%c-%d-%d-%d", var[v], i, x, y);
                            fprintf(f,"---U+%06X-", i);
                            if(i>=32) fprintf(f, "'%s'", utf8(i));
                            if(!i) fprintf(f,"-'UNDEF'---\n");
                            else if(uninames && uninames[i]) fprintf(f,"-'%s'---\n", uninames[i]);
                            else fprintf(f,"---\n");
                            ptr2 += 10;
                            for(;n--;ptr2 += l) {
                                x = (((ptr2[1] >> 4) & 3) << 8) | ptr2[4];
                                y = (((ptr2[1] >> 6) & 3) << 8) | ptr2[5];
                                switch(l) {
                                    case 4: k=(ptr2[1] << 8) | ptr2[0]; x = ptr2[2]; y = ptr2[3]; break;
                                    case 5: k=((ptr2[2] & 0xF) << 16) | (ptr2[1] << 8) | ptr2[0];
                                        x = (((ptr2[2] >> 4) & 3) << 8) | ptr2[3]; y = (((ptr2[2] >> 6) & 3) << 8) | ptr2[4]; break;
                                    default: k=(ptr2[2] << 16) | (ptr2[1] << 8) | ptr2[0];
                                        x = ((ptr2[3] & 0xF) << 8) | ptr2[4]; y = (((ptr2[3] >> 4) & 0xF) << 8) | ptr2[5]; break;
                                }
                                frag(f, (unsigned char*)font + k, w, h, ((((unsigned char*)font + k)[0] & 0xE0) == 0xE0 && !x? 0 : L+x), T+y);
                            }
                            if(map && dy < font->bbox_bottom+1) {
                                if(map==1) {
                                    memset(bit, (dot ? '.' : ' '), w);
                                    bit[w]=0;
                                    for(;dy<font->bbox_bottom+1;dy++)
                                        fprintf(f,"%s\n",bit);
                                } else {
                                    for(;dy<font->bbox_bottom+1;dy++) {
                                        for(x=0;x<w;x++)
                                            fprintf(f,"%s........",x?" ":"");
                                        fprintf(f,"\n");
                                    }
                                }
                            }
                        } else
                            ptr2 += 10 + n * l;
                    }
                }
            } /* for numvariants */
        }
    }

    if(font->kerning_offs) {
        fprintf(f,"\n+@---Kerning Table---\n");
        ptr = (unsigned char *)font + font->kerning_offs;
        a = font->features & SSFN_FEAT_KBIGLKP ? 4 : 3;
        b = font->features & SSFN_FEAT_KBIGCHR;
        c = font->features & SSFN_FEAT_KBIGCRD;
        for(i=0;i<0x110000;i++) {
            if(ptr[0] & 0x80) {
                if(ptr[0] & 0x40) { i += ptr[1] | ((ptr[0] & 0x3f) << 8); ptr += 2; }
                else { i += ptr[0] & 0x3f; ptr++; }
            } else {
                dy = ptr[0] & 0x7F;
                for(y=i;y<=i+dy;y++) {
                    fprintf(f,"%s:",utf8(y));
                    ptr2 = (unsigned char*)font + font->kerning_offs + ((a==4?(ptr[3]<<16):0)|(ptr[2]<<8)|ptr[1]);
                    if(ptr2[0] & 0x80) { k = ptr2[1] | ((ptr2[0] & 0x7f) << 8); ptr2 += 2; }
                    else { k = ptr2[0] & 0x7F; ptr2++; }
                    for(x=0;x<=k;x++) {
                        if(b) { j = ptr2[0] | (ptr2[1] << 8) | ((ptr2[2] & 0x7F) << 16); l = ptr2[2] & 0x80; ptr2 += 3; }
                        else { j = ptr2[0] | ((ptr2[1] & 0x7F) << 8); l = ptr2[1] & 0x80; ptr2 += 2; }
                        if(c) { m = (short)(ptr2[0] | (ptr2[1] << 8)); ptr2 += 2; } else { m = (signed char)ptr2[0]; ptr2++; }
                        if(j>32)
                            fprintf(f,"%s %s %d%c", x?",":"", utf8(j), (short)m, l?'v':'h');
                    }
                    fprintf(f,"\n");
                }
                ptr += a;
                i += dy;
            }
        }
    }

    fprintf(f,"\n+@----End---\n\n");

    fclose(f);
    return 0;
}
