/*
 * sfnconv/sfn2sfn.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Scalable Screen Font collection utility
 *
 * Note: this tools handles memory allocation in a lazy fashion, but
 * that's okay as it's a command line tool which exists immediately
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SSFN_NOIMPLEMENTATION
#include "../ssfn.h"
#if HAS_ZLIB
# include <zlib.h>
#endif

/*** variables ***/
int zip = 0;

/**
 * Load a (compressed) file
 */
ssfn_font_t *load_file(char *infile, int *size)
{
    ssfn_font_t *data = NULL;
    long int origsize = 0;
    FILE *f;
#if HAS_ZLIB
    unsigned char hdr[2];
    gzFile g;
#endif

    f = fopen(infile,"rb");
    if(!f) { fprintf(stderr,"unable to load %s\n", infile); exit(3); }
#if HAS_ZLIB
    fread(&hdr, 2, 1, f);
    if(hdr[0]==0x1f && hdr[1]==0x8b) {
        fseek(f, -4L, SEEK_END);
        fread(&origsize, 4, 1, f);
    } else {
        fseek(f, 0, SEEK_END);
        origsize = ftell(f);
    }
    fclose(f);
    g = gzopen(infile,"r");
#else
    fseek(f, 0, SEEK_END);
    origsize = ftell(f);
    fseek(f, 0, SEEK_SET);
#endif
    data = (ssfn_font_t*)malloc(origsize);
    if(!data) { fprintf(stderr,"memory allocation error\n"); exit(2); }
#if HAS_ZLIB
    gzread(g, data, origsize);
    gzclose(g);
#else
    fread(data, origsize, 1, f);
    fclose(f);
#endif
    *size = origsize;
    return data;
}

/**
 * Save a (compressed) file
 */
void save_file(char *outfile, ssfn_font_t *font)
{
    FILE *f;
#if HAS_ZLIB
    gzFile g;
#endif

#if HAS_ZLIB
    if(zip) {
        g = gzopen(outfile, "wb");
        if(!g) { fprintf(stderr, "unable to write %s\n", outfile); exit(4); }
        gzwrite(g, font, font->size);
        gzclose(g);
    } else
#endif
    {
        f = fopen(outfile, "wb");
        if(!f) { fprintf(stderr, "unable to write %s\n", outfile); exit(4); }
        fwrite(font, font->size, 1, f);
        fclose(f);
    }
}

/**
 * Main procedure
 */
int main(int argc, char **argv)
{
    int i;
    int size = 0, total = 8;
    ssfn_font_t *font, *end;
    unsigned char *out = NULL;

    if(argc < 2) {
        printf("Scalable Screen Font by bzt Copyright (C) 2019 MIT license\n\n"
               "%s "
#if HAS_ZLIB
               "[-z] "
#endif
               "<in sfnc> [<out ssfn 1> [<out ssfn 2> ... ]]\n"
               "%s "
#if HAS_ZLIB
               "[-z] "
#endif
               "<in ssfn 1> [<in ssfn 2> [ ... ]] <out sfnc>\n\n"
#if HAS_ZLIB
               " -z:    compress output with gzip\n"
#endif
               , argv[0], argv[0]);
        return 1;
    }
    /* parse flags */
    for(i=1; i<argc && argv[i][0] == '-'; i++)
        if(argv[i][0] == '-' && argv[i][1] == 'z') zip = 1;

    font = load_file(argv[i], &size);

    if(!memcmp(font->magic, SSFN_COLLECTION, 4)) {
        /* extract */
        end = (ssfn_font_t*)((uint8_t*)font + font->size);
        for(i++, font = (ssfn_font_t*)((uint8_t*)font + 8); font < end; font = (ssfn_font_t*)((uint8_t*)font + font->size)) {
            if(argc < 3)
                printf("%d %c%c %s\n", font->family,
                    font->style & SSFN_STYLE_BOLD ? 'b':'.', font->style & SSFN_STYLE_ITALIC ? 'i':'.',
                    (char*)font + sizeof(ssfn_font_t));
            else
                save_file(argv[i++], font);
        }
    } else {
        /* create */
        for(; i + 1 < argc; i++) {
            if(!size) font = load_file(argv[i], &size);
            out = (unsigned char *)realloc(out, total+size);
            memcpy(out + total, font, font->size);
            total += size;
            size = 0;
            free(font);
        }
        memcpy(out, SSFN_COLLECTION, 4);
        memcpy(out + 4, &total, 4);
        save_file(argv[i], (ssfn_font_t*)out);
    }

    return 0;
}
