/*
 * sfnconv/bit2sfn.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief bitmap and pixmap fonts to Scalable Screen Font converter
 *
 * Note: this tools handles memory allocation in a lazy fashion, but
 * that's okay as it's a command line tool which exists immediately
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SSFN_NOIMPLEMENTATION
#include "../ssfn.h"
/* psfu and bdf files are often gzipped */
#if HAS_ZLIB
# include <zlib.h>
#endif
#if HAS_UNICODE
# define _UNICODE_BLOCKSDATA
# include "../sfnedit/unicode.h"
#endif

#define iswhitespace(x) ((x)==0x20||(x)==0xA0)

typedef struct {
    unsigned int magic;
    unsigned int version;
    unsigned int headersize;
    unsigned int flags;
    unsigned int numglyph;
    unsigned int bytesperglyph;
    unsigned int height;
    unsigned int width;
    unsigned char glyphs;
} __attribute__((packed)) psf_t;

typedef struct {
    int type;
    int px;
    int py;
    int c1x;
    int c1y;
    int c2x;
    int c2y;
} cont_t;

typedef struct {
    int pos;
    int cnt;
    int p;
    int h;
    int t;
    int len;
    unsigned char *data;
    int prod;
} frag_t;

typedef struct {
    int unicode;
    int v;
    int w;
    int h;
    int adv_x;
    int adv_y;
    int bear_l;
    int bear_t;
    int len;
    int *frags;
    int kgrp;
    int klen;
    int *kern;
    int hlen;
    int *hhints;
    int hfrag;
    int vlen;
    int *vhints;
    int vfrag;
} char_t;

typedef struct {
    int pos;
    int klen;
    int *kern;
} kgrp_t;

/*** variables ***/
ssfn_font_t ssfn_hdr;
int replace=0, hinting=0, zip=0, last = -1, numchars = 0, relunderline = 0, haskern = 0, adv = 0;
char *arg_name=NULL, *arg_family=NULL, *arg_subfamily=NULL, *arg_ver=NULL;
char *arg_manufacturer=NULL, *arg_license=NULL;
unsigned char cpal[960], cpalidx[241], cpalrev[241];
int unicode, rs = 0, re = 0x10FFFF, kgrpnum = 0, fragments_num = 0, characters_num;
int frag_len = 0, frag_mx, frag_my, frag_ix, frag_iy, frag_ax, frag_ay, min_y, max_y, min_x, max_x;
int lx, ly, gx, gy, bx, by;
cont_t *contour = NULL;
frag_t *frags = NULL;
char_t *chars = NULL;
kgrp_t *kgrps = NULL;

/****************************** helper functions ******************************/

/**
 * Sort characters by UNICODE code point
 */
int chrsrt(const void *a, const void *b)
{
    return ((char_t*)a)->unicode - ((char_t*)b)->unicode;
}

/**
 * Sort kerning pairs by UNICODE code point
 */
int kgrpsrt(const void *a, const void *b)
{
    return ((int*)a)[0] - ((int*)b)[0];
}

/**
 * Sort fragments by scanline
 */
int frgsrt(const void *a, const void *b)
{
    if(frags[((int*)a)[2]].t == SSFN_FRAG_HINTING || (frags[((int*)a)[2]].t == SSFN_FRAG_CONTOUR &&
        frags[((int*)b)[2]].t != SSFN_FRAG_HINTING && frags[((int*)b)[2]].t != SSFN_FRAG_CONTOUR)) return -1;
    if(frags[((int*)b)[2]].t == SSFN_FRAG_HINTING) return 1;

    if(frags[((int*)a)[2]].prod == frags[((int*)b)[2]].prod) {
        return (((int*)a)[1] == ((int*)b)[1]) ? (((int*)a)[0] - ((int*)b)[0]) : (((int*)a)[1] - ((int*)b)[1]);
    }

    return frags[((int*)a)[2]].prod - frags[((int*)b)[2]].prod;
}

/**
 * Sort color map
 */
int cpalsrt(const void *a, const void *b)
{
    /* zero means undefined color, send to back */
    if(!cpal[*((uint8_t*)a)*4+0] && !cpal[*((uint8_t*)a)*4+1] && !cpal[*((uint8_t*)a)*4+2]) return 1;
    if(!cpal[*((uint8_t*)b)*4+0] && !cpal[*((uint8_t*)b)*4+1] && !cpal[*((uint8_t*)b)*4+2]) return -1;
    /* green is most significant, alpha is the least */
    return
        ((((int)cpal[*((uint8_t*)a)*4+1]<<3) + ((int)cpal[*((uint8_t*)a)*4+0]<<1) +
          ((int)cpal[*((uint8_t*)a)*4+2]<<1) + (int)cpal[*((uint8_t*)a)*4+3])>>2) -
        ((((int)cpal[*((uint8_t*)b)*4+1]<<3) + ((int)cpal[*((uint8_t*)b)*4+0]<<1) +
          ((int)cpal[*((uint8_t*)b)*4+2]<<1) + (int)cpal[*((uint8_t*)b)*4+3])>>2);
}

/**
 * Convert hex string to binary number. Use this lightning fast implementation
 * instead of the unbeliveably crap, slower than a pregnant snail sscanf...
 */
uint32_t gethex(char *ptr, int len)
{
    uint32_t ret = 0;
    for(;len--;ptr++) {
        if(*ptr>='0' && *ptr<='9') {          ret <<= 4; ret += (uint32_t)(*ptr-'0'); }
        else if(*ptr >= 'a' && *ptr <= 'f') { ret <<= 4; ret += (uint32_t)(*ptr-'a'+10); }
        else if(*ptr >= 'A' && *ptr <= 'F') { ret <<= 4; ret += (uint32_t)(*ptr-'A'+10); }
        else break;
    }
    return ret;
}

/**
 * Decode an UTF-8 multibyte, advance string pointer and return UNICODE
 */
uint32_t getutf8(char **s)
{
    uint32_t c = **s;

    if((**s & 128) != 0) {
        if((**s & 32) == 0 ) { c = ((**s & 0x1F)<<6)|(*(*s+1) & 0x3F); *s += 1; } else
        if((**s & 16) == 0 ) { c = ((**s & 0xF)<<12)|((*(*s+1) & 0x3F)<<6)|(*(*s+2) & 0x3F); *s += 2; } else
        if((**s & 8) == 0 ) { c = ((**s & 0x7)<<18)|((*(*s+1) & 0x3F)<<12)|((*(*s+2) & 0x3F)<<6)|(*(*s+3) & 0x3F); *s += 3; }
        else c = 0;
    }
    *s += 1;
    return c;
}

/**
 * Check if a line is empty in a bitmap or pixmap
 */
int iszero(unsigned char *ptr, int chr, int len)
{
    int i;

    for(i=0;i<len;i++)
        if(ptr[i]!=chr) return 0;

    return 1;
}

/**
 * Add a color map entry
 */
unsigned char cpal_add(int r, int g, int b, int a)
{
    int i, dr, dg, db, m, dm, q;

    if(!r && !g && !b && (!a || a==0xFF)) return 240;
    for(q=1; q<=8; q++) {
        m=-1; dm=256;
        for(i=0; i<240 && (cpal[i*4+0] || cpal[i*4+1] || cpal[i*4+2]); i++) {
            if(a==cpal[i*4+3] && b==cpal[i*4+0] && g==cpal[i*4+1] && r==cpal[i*4+2]) return i;
            if(b>>q==cpal[i*4+0]>>q && g>>q==cpal[i*4+1]>>q && r>>q==cpal[i*4+2]>>q) {
                dr = r > cpal[i*4+2] ? r - cpal[i*4+2] : cpal[i*4+2] - r;
                dg = g > cpal[i*4+1] ? g - cpal[i*4+1] : cpal[i*4+1] - g;
                db = b > cpal[i*4+0] ? b - cpal[i*4+0] : cpal[i*4+0] - b;
                if(dg > dr) dr = dg;
                if(db > dr) dr = db;
                if(dr < dm) { dm = dr; m = i; }
                if(!dm) break;
            }
        }
        if(dm>7 && i<240) {
            cpal[i*4+3] = a;
            cpal[i*4+2] = r;
            cpal[i*4+1] = g;
            cpal[i*4+0] = b;
            return i;
        }
        if(m>=0) {
            cpal[m*4+3] = ((cpal[m*4+3] + a) >> 1);
            cpal[m*4+2] = ((cpal[m*4+2] + r) >> 1);
            cpal[m*4+1] = ((cpal[m*4+1] + g) >> 1);
            cpal[m*4+0] = ((cpal[m*4+0] + b) >> 1);
            return m;
        }
    }
    fprintf(stderr,"unable to add color to color map, should never happen\n");
    exit(5);
    return 240;
}

/**
 * Add a line to a glyph's approximation
 */
void line_add(int x, int y)
{
    if(frag_ix < x) frag_ix = x;
    if(frag_iy < y) frag_iy = y;
    if(frag_ax > x) frag_ax = x;
    if(frag_ay > y) frag_ay = y;
    if(x < min_x) min_x = x;
    if(x > max_x) max_x = x;
    if(y < min_y) min_y = y;
    if(y > max_y) max_y = y;
    gx = x; gy = y;
}

/**
 * Add a curve to a glyph's approximation
 */
void curve_add(int x0,int y0, int x1,int y1, int x2,int y2, int x3,int y3, int l)
{
    int m0x, m0y, m1x, m1y, m2x, m2y, m3x, m3y, m4x, m4y,m5x, m5y;
    if(l<8 && (x0!=x3 || y0!=y3) && x0 > 0 && y0 > 0) {
        m0x = ((x1-x0)/2) + x0;     m0y = ((y1-y0)/2) + y0;
        m1x = ((x2-x1)/2) + x1;     m1y = ((y2-y1)/2) + y1;
        m2x = ((x3-x2)/2) + x2;     m2y = ((y3-y2)/2) + y2;
        m3x = ((m1x-m0x)/2) + m0x;  m3y = ((m1y-m0y)/2) + m0y;
        m4x = ((m2x-m1x)/2) + m1x;  m4y = ((m2y-m1y)/2) + m1y;
        m5x = ((m4x-m3x)/2) + m3x;  m5y = ((m4y-m3y)/2) + m3y;
        curve_add(x0,y0, m0x,m0y, m3x,m3y, m5x,m5y, l+1);
        curve_add(m5x,m5y, m4x,m4y, m2x,m2y, x3,y3, l+1);
    }
    line_add(x3, y3);
}

/**
 * Add a contour command to fragment. Originally this tool only read bitmap fonts, but I've
 * added contours too to ASC files so that you can convert any ASC to SFN with this tool.
 */
void cont_add(int type, int px, int py, int c1x, int c1y, int c2x, int c2y)
{
    int cx, cy;
    cont_t *c;

    if(frag_len>32767) {
        fprintf(stderr, "Too many points in contour in U+%06x character's glyph.\n", unicode);
        exit(3);
    }
    if(type!=SSFN_CONTOUR_COLOR) {
        if(px >= (16<<ssfn_hdr.quality)) px = (16<<ssfn_hdr.quality) - 1;   /* due to rounding errors */
        if(py >= (16<<ssfn_hdr.quality)) py = (16<<ssfn_hdr.quality) - 1;
    }
    if(lx>=0 && ly>=0 && px>=0 && py>=0) {
        if(type==SSFN_CONTOUR_CUBIC) {
            cx = ((c1x-lx)<<1)+lx;
            cy = ((c1y-ly)<<1)+ly;
            if(((c2x-cx)<<1)+cx == px && ((c2y-cy)<<1)+cy == py) {
                type = SSFN_CONTOUR_QUAD;
                c1x = cx;
                c1y = cy;
            }
        }
    }
    contour = (cont_t*)realloc(contour, (frag_len+1)*sizeof(cont_t));
    if(!contour) { fprintf(stderr,"memory allocation error\n"); exit(2); }
    c = &contour[frag_len++];
    memset(c, 0, sizeof(cont_t));
    c->type = type;
    if(type == SSFN_CONTOUR_COLOR) {
        ssfn_hdr.features |= SSFN_FEAT_HASCMAP;
        c->px = px;
        c->py = py;
        c->c1x = c1x;
        c->c1y = c1y;
    } else {
        c->px = px; if(px < frag_mx) frag_mx = px;
        c->py = py; if(py < frag_my) frag_my = py;
        if(type > 1) {
            c->c1x = c1x; if(c1x < frag_mx) frag_mx = c1x;
            c->c1y = c1y; if(c1y < frag_my) frag_my = c1y;
            if(type > 2) {
                c->c2x = c2x; if(c2x < frag_mx) frag_mx = c2x;
                c->c2y = c2y; if(c2y < frag_my) frag_my = c2y;
            }
        }
        switch(type) {
            case SSFN_CONTOUR_MOVE:
            case SSFN_CONTOUR_LINE: line_add(px, py); break;
            case SSFN_CONTOUR_QUAD:
                curve_add(gx,gy, ((c1x-gx)>>1)+gx,((c1y-gy)>>1)+gy, ((px-c1x)>>1)+c1x, ((py-c1y)>>1)+c1y, px, py, 0);
            break;
            case SSFN_CONTOUR_CUBIC: curve_add(gx,gy, c1x, c1y, c2x, c2y, px, py, 0); break;
        }
        lx = px; ly = py;
    }
}

/**
 * Add a bitmap fragment and deduplicate
 */
int frag_add(int t, int p, int h, unsigned char *data)
{
    int i, l=p*h;

    /* hack to add contours to ASC files */
    if(t==SSFN_FRAG_CONTOUR) {
        for(i=0;i<frag_len;i++) {
            contour[i].px -= frag_mx;
            contour[i].py -= frag_my;
            if(contour[i].type > 1) {
                contour[i].c1x -= frag_mx;
                contour[i].c1y -= frag_my;
                if(contour[i].type > 2) {
                    contour[i].c2x -= frag_mx;
                    contour[i].c2y -= frag_my;
                }
            }
        }
        l=frag_len*sizeof(cont_t);
    }
    /* normal bitmap fragment code */
    for(i=0;i<fragments_num;i++) {
        if(frags[i].t==t && frags[i].p==p && frags[i].h==h && frags[i].len==l && !memcmp(frags[i].data, data, l)) {
            frags[i].cnt++;
            return i;
        }
    }
    i = fragments_num++;
    frags = (frag_t*)realloc(frags, fragments_num*sizeof(frag_t));
    if(!frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
    frags[i].cnt = 1;
    frags[i].t = t;
    frags[i].p = p;
    frags[i].h = h;
    frags[i].len = l;
    frags[i].data = data;
    frags[i].prod = (frag_ax - frag_ix) * (frag_ay - frag_iy);
    return i;
}

/**
 * Split fragments
 */
int frag_split(int idx, int offs, int siz)
{
    int i, j, k, ni;

    if(siz >= frags[idx].h) return idx;

    if(!offs) {
        ni = frag_add(frags[idx].t, frags[idx].p, frags[idx].h - siz, frags[idx].data + siz*frags[idx].p);
        frags[idx].h = siz;
    } else {
        ni = frag_add(frags[idx].t, frags[idx].p, siz, frags[idx].data + (frags[idx].h - siz)*frags[idx].p);
        frags[idx].h -= siz;
    }
    frags[ni].cnt = frags[idx].cnt;
    frags[idx].len = frags[idx].h*frags[idx].p;
    for(i=0; i < characters_num; i++) {
        for(j=0; j < (int)chars[i].len; j++) {
            if(chars[i].frags[j*3+2] == idx) {
                k=chars[i].len*3; chars[i].len++;
                chars[i].frags = (int*)realloc(chars[i].frags, (k+3)*sizeof(int));
                if(!chars[i].frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                chars[i].frags[k+0] = chars[i].frags[j*3+0];
                chars[i].frags[k+1] = chars[i].frags[j*3+1] + frags[idx].h;
                chars[i].frags[k+2] = ni;
                break;
            }
        }
    }
    return offs ? ni : idx;
}

/**
 * Add a kerning group
 */
int kgrp_add(int klen, int *kern)
{
    int i;
    for(i=0;i<kgrpnum;i++) {
        if(kgrps[i].klen==klen && !memcmp(kgrps[i].kern, kern, klen*3*sizeof(int))) {
            return i;
        }
    }
    i = kgrpnum++;
    kgrps = (kgrp_t*)realloc(kgrps, kgrpnum*sizeof(frag_t));
    if(!kgrps) { fprintf(stderr,"memory allocation error\n"); exit(2); }
    kgrps[i].pos = 0;
    kgrps[i].klen = klen;
    kgrps[i].kern = kern;
    return i;
}

/**
 * Add a character
 */
char_t *char_add(int v, int t, int w, int h, int x, int y, unsigned char *data)
{
    char_t *c;
    int i, j, p = 0, e, o, m, z, s;

    if(unicode<rs || unicode>re || unicode>0x10FFFF) return NULL;
    if(numchars && (int)(re-rs)>numchars) j=characters_num*100/numchars;
    else j = (unicode-rs)*100/(re-rs);
    if(last != j) { printf("\rCompressing glyphs... %3d%%", j); fflush(stdout); last = j; }
    if(w>127 || h>127) {
        fprintf(stderr,"\r%smap %d x %d for U+%06x too big\n", t==SSFN_FRAG_PIXMAP?"pix":"bit", w, h, unicode);
        exit(3);
    }
    switch(t) {
        case SSFN_FRAG_BITMAP: z=0; p = (w+7)/8; break;
        case SSFN_FRAG_PIXMAP: z=0xF0; p = w; break;
    }
    if(p && iszero(data, z, p*h) && !iswhitespace(unicode)) return NULL;

    for(i=0;i<characters_num;i++)
        if(chars[i].v == v && chars[i].unicode == unicode) {
            if(!replace) return NULL;
            c = &chars[i];
            for(j=0;j<chars[i].len;j++)
                frags[c->frags[j*3+2]].cnt--;
            goto repl;
        }

    characters_num++;
    chars = (char_t*)realloc(chars, characters_num*sizeof(char_t));
    if(!chars) { fprintf(stderr,"memory allocation error\n"); exit(2); }
    c = &chars[characters_num-1];
    c->v = v;
    c->unicode = unicode;
repl:
    if(t != SSFN_FRAG_CONTOUR) {
        for(;h>0 && iszero(data + (h-1)*p, z, p);h--);
    }
    c->w = w; if(ssfn_hdr.bbox_right < w-1) ssfn_hdr.bbox_right = w-1;
    c->h = h; if(ssfn_hdr.bbox_bottom < h-1) ssfn_hdr.bbox_bottom = h-1;
    c->adv_x = ((x==0 && y==0) ? w : x) + adv;
    c->adv_y = y;
    c->bear_l = c->bear_t = 0;
    c->kgrp = -1;
    c->len = c->klen = c->hlen = c->vlen = 0;
    c->frags = c->kern = c->hhints = c->vhints = NULL;
    if(t == SSFN_FRAG_CONTOUR) return c;

    x=y=0;
    for(;y<h && iszero(data + y*p, z, p);y++);
    if(c->bear_t < y) c->bear_t = y;
    if(y < ssfn_hdr.bbox_top) ssfn_hdr.bbox_top = y;
    ssfn_hdr.bbox_left = 0;

    /* fixme: only add blocks splitted at empty lines here, then before frg array saved, find
     * the best match to figure out where to split fragment to minimize file size */
    if(t == SSFN_FRAG_BITMAP) {
        ssfn_hdr.features |= SSFN_FEAT_HASBMAP;
        /* special case for bitmap fonts, we only split them at empty lines and on exact fragment match heights */
        while(h) {
            for(;h>0 && iszero(data + (h-1)*p, z, p);h--);
            for(;y<h && iszero(data + y*p, z, p);y++);
            s = m = -1;
            for(i=0;(int)i<fragments_num;i++) {
                if(frags[i].t==t && frags[i].len > 5 && frags[i].h <= (h-y) && frags[i].p == p &&
                    !memcmp(frags[i].data, data + y*p, frags[i].len) && frags[i].h>m) {
                        m=frags[i].h; s=i;
                    }
            }
            if(s!=-1) {
                j=c->len*3; c->len++;
                c->frags = (int*)realloc(c->frags, (j+3)*sizeof(int));
                if(!c->frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                c->frags[j+0] = x;
                c->frags[j+1] = y;
                c->frags[j+2] = s;
                frags[s].cnt++;
                y += m;
            } else {
                s = m = -1;
                for(i=0;(int)i<fragments_num;i++) {
                    if(frags[i].t==t && frags[i].len > 5 && frags[i].h <= (h-y) && frags[i].p == p &&
                        !memcmp(frags[i].data, data + (h-frags[i].h)*p, frags[i].len) && frags[i].h>m) {
                            m=frags[i].h; s=i;
                        }
                }
                if(s!=-1) {
                    j=c->len*3; c->len++;
                    c->frags = (int*)realloc(c->frags, (j+3)*sizeof(int));
                    if(!c->frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                    c->frags[j+0] = x;
                    c->frags[j+1] = h-m;
                    c->frags[j+2] = s;
                    frags[s].cnt++;
                    h -= m;
                } else {
                    for(m=y;m<h && !iszero(data+m*p, z, p);m++);
                    if(m==y) break;
                    s=frag_add(t, p, m-y, data+y*p);
                    j=c->len*3; c->len++;
                    c->frags = (int*)realloc(c->frags, (j+3)*sizeof(int));
                    if(!c->frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                    c->frags[j+0] = x;
                    c->frags[j+1] = y;
                    c->frags[j+2] = s;
                    y = m;
                }
            }
        }
    } else {
        ssfn_hdr.features |= SSFN_FEAT_HASCMAP;

        while(y<h) {
            for(;h>0 && iszero(data + (h-1)*p, z, p);h--);
            for(;y<h && iszero(data + y*p, z, p);y++);
            if(y >= h) break;
            s = m = -1; o = e = 0;
            for(i=0;y+2<h && (int)i<fragments_num;i++) {
                if(frags[i].t==t && frags[i].len > 5 && frags[i].h <= (h-y) && frags[i].p == p && frags[i].h >= m) {
                    for(j = m > 2 ? m : 2; j < (h-y) && j <= frags[i].h; j++) {
                        if(!memcmp(frags[i].data, data + y*p, j*p)) { s=i; e=o=0; m=j; } else
                        if(!memcmp(frags[i].data, data + (h-j)*p, j*p)) { s=i; o=0; e=m=j; } else
                        if(!memcmp(frags[i].data + (frags[i].h-j)*p, data + y*p, j*p)) { s=i; e=0; o=m=j; } else
                        if(!memcmp(frags[i].data + (frags[i].h-j)*p, data + (h-j)*p, j*p)) { s=i; e=o=m=j; } else
                        break;
                    }
                }
            }
            if(s != -1 && m > 5 && h-y-m > 5) {
                s = frag_split(s, o, m);
            } else {
                for(m=y;m<h && !iszero(data+m*p, z, p);m++);
                if(m==y) continue;
                s = frag_add(t, p, m-y, data+y*p);
                m -= y;
                e = 0;
            }
            j=c->len*3; c->len++;
            c->frags = (int*)realloc(c->frags, (j+3)*sizeof(int));
            if(!c->frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            c->frags[j+0] = 0;
            c->frags[j+1] = e ? h - m : y;
            c->frags[j+2] = s;
            if(e) h -= m; else y += m;
            frags[s].cnt++;
        }
    }
    return c;
}

/**
 * Turn a decimal or hex string into binary number
 */
uint32_t getnum(char *s)
{
    if(!s || !*s) return 0;
    if(*s=='\'') { s++; return getutf8(&s); }
    if((*s=='0' && s[1]=='x') || (*s=='U' && s[1]=='+')) return gethex(s+2,8);
    return atoi(s);
}

/**
 * Block name comparition according to UNICODE Inc.
 */
#define tolower(X)  (((X) >= 'A') && ((X) <= 'Z') ? ('a'+((X)-'A')) : (X))
int unicmp(char *a, char *b)
{
    for(;*a && *b;a++,b++) {
        while(*a==' ' || *a=='-' || *a=='_') a++;
        while(*b==' ' || *b=='-' || *b=='_') b++;
        if(tolower(*a) != tolower(*b)) return 1;
    }
    return *a || *b;
}


/****************************** file format parsers ******************************/

/**
 * Parse PSF2 font (binary)
 */
void psf(unsigned char *ptr, int size)
{
    psf_t *psf = (psf_t*)ptr;
    uint16_t *utbl = NULL;
    uint32_t c, g=0;
    unsigned char *s, *e, *glyph, *bitmap;
    int i, j, k;

    numchars = psf->numglyph;
    if(psf->flags & 1) {
        numchars = 0;
        utbl = (uint16_t*)malloc(65536*sizeof(uint16_t));
        if(!utbl) { fprintf(stderr,"memory allocation error\n"); exit(2); }
        memset(utbl, 0, 65536*sizeof(uint16_t));
        s=(unsigned char*)(ptr + psf->headersize + psf->numglyph*psf->bytesperglyph);
        e=ptr + size;
        while(s<e && g<65536) {
            c = (uint16_t)((uint8_t)s[0]);
            if(c == 0xFF) { g++; } else {
                if((c & 128) != 0) {
                    if((c & 32) == 0 ) { c=((s[0] & 0x1F)<<6)+(s[1] & 0x3F); s++; } else
                    if((c & 16) == 0 ) { c=((((s[0] & 0xF)<<6)+(s[1] & 0x3F))<<6)+(s[2] & 0x3F); s+=2; } else
                    if((c & 8) == 0 ) { c=((((((s[0] & 0x7)<<6)+(s[1] & 0x3F))<<6)+(s[2] & 0x3F))<<6)+(s[3] & 0x3F); s+=3;}
                    else c=0;
                }
                if(c<65536) {
                    utbl[c] = g;
                    numchars++;
                }
            }
            s++;
        }
    }
    if((psf->flags >> 24) && !ssfn_hdr.baseline) ssfn_hdr.baseline = (psf->flags >> 24);
    for(unicode=rs;unicode<=(re<65535?re:65535);unicode++) {
        g = utbl? utbl[unicode] : unicode;
        if((!g && unicode) || g >= psf->numglyph) continue;
        glyph = ptr + psf->headersize + g*psf->bytesperglyph;
        bitmap = (unsigned char*)malloc(psf->bytesperglyph);
        if(!bitmap) { fprintf(stderr,"memory allocation error\n"); exit(2); }
        memset(bitmap, 0, psf->bytesperglyph);
        for(i=0;i<(int)psf->bytesperglyph;i++) {
            for(k=1,j=0x80;j;j>>=1,k<<=1) if(glyph[i] & j) bitmap[i] |= k;
        }
        char_add(0, SSFN_FRAG_BITMAP, psf->width, psf->height, psf->width, 0, bitmap);
    }
    free(utbl);
}

/**
 * Parse GNU unifont hex format (text)
 */
void hex(char *ptr, int size)
{
    uint32_t i, j, k, c;
    char *end = ptr + size;
    unsigned char *bitmap;

    numchars = 0;
    for(numchars=0;ptr < end && *ptr;ptr++) if(*ptr=='\n') numchars++;
    ptr = end - size;
    while(ptr < end && *ptr) {
        unicode = gethex(ptr, 6);
        while(*ptr && *ptr!=':') ptr++;
        if(!*ptr) return;
        ptr++;
        while(*ptr && (*ptr==' '||*ptr=='\t')) ptr++;
        if(!*ptr) return;
        bitmap = (unsigned char*)malloc(33);
        if(!bitmap) { fprintf(stderr,"memory allocation error\n"); exit(2); }
        memset(bitmap, 0, 33);
        for(i = 0;i<32 && *ptr && *ptr!='\n' && *ptr!='\r';ptr += 2,i++) {
            c = gethex(ptr, 2);
            for(k=1,j=0x80;j;j>>=1,k<<=1) if(c & j) bitmap[i] |= k;
        }
        while(*ptr && *ptr!='\n' && *ptr!='\r') ptr++;
        while(*ptr && (*ptr=='\n' || *ptr=='\r')) ptr++;
        char_add(0, SSFN_FRAG_BITMAP, i>16? 16 : 8, 16, i>16? 16 : 8, 0, bitmap);
    }
}

/**
 * Parse X11 BDF font format (text)
 */
void bdf(char *ptr, int size)
{
    uint32_t c;
    int w, h, i, j, k, l;
    char *end = ptr + size, *e, *name = NULL, *style = NULL, *manu = NULL;
    unsigned char *bitmap;

    while(ptr < end && *ptr) {
        if(!memcmp(ptr, "FACE_NAME ", 10) && !arg_name && !name) {
            ptr += 10; if(*ptr=='\"') ptr++; for(e=ptr;*e && *e!='\"' && *e!='\r' && *e!='\n';e++);
            while(e > ptr +1 && (e[-1]==' ' || e[-1] == '\t')) e--;
            name = malloc(e-ptr+1);
            if(!name) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memcpy(name, ptr, e-ptr);
            name[e-ptr]=0;
        }
        if(!memcmp(ptr, "FONT_VERSION ", 13) && !arg_ver) {
            ptr += 13; if(*ptr=='\"') ptr++; for(e=ptr;*e && *e!='\"' && *e!='\r' && *e!='\n';e++);
            while(e > ptr +1 && (e[-1]==' ' || e[-1] == '\t')) e--;
            bitmap = malloc(e-ptr+1);
            if(!bitmap) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memcpy(bitmap, ptr, e-ptr);
            bitmap[e-ptr]=0;
            arg_ver = (char*)bitmap;
        }
        if(!memcmp(ptr, "ADD_STYLE_NAME ", 15) && !arg_name && !style) {
            ptr += 15; if(*ptr=='\"') ptr++; for(e=ptr;*e && *e!='\"' && *e!='\r' && *e!='\n';e++);
            while(e > ptr +1 && (e[-1]==' ' || e[-1] == '\t')) e--;
            style = malloc(e-ptr+1);
            if(!style) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memcpy(style, ptr, e-ptr);
            style[e-ptr]=0;
        }
        if(!memcmp(ptr, "FOUNDRY ", 8) && !arg_manufacturer) {
            ptr += 8; if(*ptr=='\"') ptr++; for(e=ptr;*e && *e!='\"' && *e!='\r' && *e!='\n';e++);
            while(e > ptr +1 && (e[-1]==' ' || e[-1] == '\t')) e--;
            manu = realloc(manu, e-ptr+1);
            if(!manu) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memcpy(manu, ptr, e-ptr);
            manu[e-ptr]=0;
        }
        if(!memcmp(ptr, "HOMEPAGE ", 9) && !arg_manufacturer) {
            ptr += 9; if(*ptr=='\"') ptr++; for(e=ptr;*e && *e!='\"' && *e!='\r' && *e!='\n';e++);
            while(e > ptr +1 && (e[-1]==' ' || e[-1] == '\t')) e--;
            manu = realloc(manu, e-ptr+1);
            if(!manu) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memcpy(manu, ptr, e-ptr);
            manu[e-ptr]=0;
        }
        if(!memcmp(ptr, "FAMILY_NAME ", 12) && !arg_family) {
            ptr += 12; if(*ptr=='\"') ptr++; for(e=ptr;*e && *e!='\"' && *e!='\r' && *e!='\n';e++);
            while(e > ptr +1 && (e[-1]==' ' || e[-1] == '\t')) e--;
            bitmap = malloc(e-ptr+1);
            if(!bitmap) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memcpy(bitmap, ptr, e-ptr);
            bitmap[e-ptr]=0;
            arg_family = (char*)bitmap;
        }
        if(!memcmp(ptr, "WEIGHT_NAME ", 12) && !arg_subfamily) {
            ptr += 12; if(*ptr=='\"') ptr++; for(e=ptr;*e && *e!='\"' && *e!='\r' && *e!='\n';e++);
            bitmap = malloc(e-ptr+1);
            if(!bitmap) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memcpy(bitmap, ptr, e-ptr);
            bitmap[e-ptr]=0;
            arg_subfamily = (char*)bitmap;
        }
        if(!memcmp(ptr, "COPYRIGHT ", 10) && !arg_license) {
            ptr += 10; if(*ptr=='\"') ptr++; for(e=ptr;*e && *e!='\"' && *e!='\r' && *e!='\n';e++);
            bitmap = malloc(e-ptr+1);
            if(!bitmap) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memcpy(bitmap, ptr, e-ptr);
            bitmap[e-ptr]=0;
            arg_license = (char*)bitmap;
        }
        if(!memcmp(ptr, "FONT_ASCENT ", 12)) { ptr += 12; if(!ssfn_hdr.baseline) ssfn_hdr.baseline = atoi(ptr); }
        if(!memcmp(ptr, "UNDERLINE_POSITION ", 19)) { ptr += 19; relunderline = atoi(ptr); }
        if(!memcmp(ptr, "CHARS ", 6)) { ptr+=6; numchars = atoi(ptr); break; }
        while(*ptr && *ptr!='\n') ptr++;
        while(*ptr=='\n') ptr++;
    }
    if(!arg_name) {
        if(!name) name = arg_family;
        if(name && style && style[0]) {
            i = strlen(name);
            name = realloc(name, strlen(name)+strlen(style)+2);
            if(!name) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            name[i] = ' ';
            memcpy(name + i + 1, style, strlen(style));
            name[i+strlen(style)+1] = 0;
        }
        arg_name = name ? name : arg_family;
    }
    if(!arg_manufacturer && manu)
        arg_manufacturer = manu;

    while(ptr < end && *ptr) {
        if(!memcmp(ptr, "ENCODING ", 9)) { ptr += 9; unicode = atoi(ptr); }
        if(!memcmp(ptr, "BBX ", 4)) {
            ptr += 4; w = atoi(ptr);
            while(*ptr && *ptr!=' ') ptr++;
            ptr++; h = atoi(ptr);
            l = (w+7)/8 * h;
        }
        if(!memcmp(ptr, "BITMAP", 6)) {
            ptr += 6; while(*ptr && *ptr!='\n') ptr++;
            ptr++;
            bitmap = malloc(l);
            if(!bitmap) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memset(bitmap, 0, l);
            for(i = 0;i<l && *ptr;ptr += 2,i++) {
                while(*ptr=='\n' || *ptr=='\r') ptr++;
                c = gethex(ptr, 2);
                for(k=1,j=0x80;j;j>>=1,k<<=1) if(c & j) bitmap[i] |= k;
            }
            if(w != 16 || h != 16 || !(!bitmap[0] && !bitmap[1] && bitmap[2]==0xFE && bitmap[3]==0x7F &&
                bitmap[(w+7)/8 * h/4]!=0xFE && bitmap[(w+7)/8 * h/4+1]!=0x7F &&
                bitmap[(w+7)/8 * h/2]==0xFE && bitmap[(w+7)/8 * h/2+1]==0x7F &&
                !bitmap[l-1] && !bitmap[l-2] && bitmap[l-3]==0x7F && bitmap[l-4]==0xFE))
                    char_add(0, SSFN_FRAG_BITMAP, w, h, w, 0, bitmap);
        }
        while(*ptr && *ptr!='\n') ptr++;
        while(*ptr=='\n') ptr++;
    }
}

/**
 * Parse TGA format for pixel fonts (binary)
 */
void tga(unsigned char *ptr, int size)
{
    unsigned char *data, *data2;
    int i, j, k, x, y, w, h, o, m;

    if(rs==0 && re==0x10FFFF) {
        fprintf(stderr, "You must specify a range for TGA files. Image height will\n"
            "be divided by the range size to get one glyph's dimensions.\n");
        exit(2);
    }

    o = (ptr[11] << 8) + ptr[10];
    w = (ptr[13] << 8) + ptr[12];
    h = (ptr[15] << 8) + ptr[14];
    if(w<1 || h<1) { fprintf(stderr,"unsupported TGA file format\n"); exit(2); }
    m = ((ptr[1]? (ptr[7]>>3)*ptr[5] : 0) + 18);
    data = (unsigned char*)malloc(w*h);
    if(!data) { fprintf(stderr,"memory allocation error\n"); exit(2); }
    switch(ptr[2]) {
        case 1:
            if(ptr[6]!=0 || ptr[4]!=0 || ptr[3]!=0 || (ptr[7]!=24 && ptr[7]!=32)) {
                fprintf(stderr,"unsupported TGA file format\n");
                exit(2);
            }
            for(y=i=0; y<h; y++) {
                k = ((!o?h-y-1:y)*w);
                for(x=0; x<w; x++) {
                    j = ptr[m + k++]*(ptr[7]>>3) + 18;
                    data[i++] = cpal_add(ptr[j+2], ptr[j+1], ptr[j], ptr[7]==32?ptr[j+3]:0xFF);
                }
            }
        break;

        case 2:
            if(ptr[5]!=0 || ptr[6]!=0 || ptr[1]!=0 || (ptr[16]!=24 && ptr[16]!=32)) {
                fprintf(stderr,"unsupported TGA file format\n");
                exit(2);
            }
            for(y=i=0; y<h; y++) {
                j = ((!o?h-y-1:y)*w*(ptr[16]>>3));
                for(x=0; x<w; x++) {
                    data[i++] = cpal_add(ptr[j+2], ptr[j+1], ptr[j], ptr[16]==32?ptr[j+3]:0xFF);
                    j += ptr[16]>>3;
                }
            }
        break;

        case 9:
            if(ptr[6]!=0 || ptr[4]!=0 || ptr[3]!=0 || (ptr[7]!=24 && ptr[7]!=32)) {
                fprintf(stderr,"unsupported TGA file format\n");
                exit(2);
            }
            y = i = 0;
            for(x=0; x<w*h && m<size;) {
                k = ptr[m++];
                if(k > 127) {
                    k -= 127; x += k;
                    j = ptr[m++]*(ptr[7]>>3) + 18;
                    while(k--) {
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = cpal_add(ptr[j+2], ptr[j+1], ptr[j], ptr[7]==32?ptr[j+3]:0xFF);
                    }
                } else {
                    k++; x += k;
                    while(k--) {
                        j = ptr[m++]*(ptr[7]>>3) + 18;
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = cpal_add(ptr[j+2], ptr[j+1], ptr[j], ptr[7]==32?ptr[j+3]:0xFF);
                    }
                }
            }
        break;

        case 10:
            if(ptr[5]!=0 || ptr[6]!=0 || ptr[1]!=0 || (ptr[16]!=24 && ptr[16]!=32)) {
                fprintf(stderr,"unsupported TGA file format\n");
                exit(2);
            }
            y = i = 0;
            for(x=0; x<w*h && m<size;) {
                k = ptr[m++];
                if(k > 127) {
                    k -= 127; x += k;
                    while(k--) {
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = cpal_add(ptr[m+2], ptr[m+1], ptr[m], ptr[16]==32?ptr[m+3]:0xFF);
                    }
                    m += ptr[16]>>3;
                } else {
                    k++; x += k;
                    while(k--) {
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = cpal_add(ptr[m+2], ptr[m+1], ptr[m], ptr[16]==32?ptr[m+3]:0xFF);
                        m += ptr[16]>>3;
                    }
                }
            }
        break;
    }

    if(h > w ) {
        m = h / (re - rs + 1);
        if(!ssfn_hdr.baseline) ssfn_hdr.baseline = m-1;
        for(unicode=rs, i=0; unicode<=re; unicode++, i += w*m) {
            for(y=k=0;y<m;y++)
                for(j=w-1;j>k;j--)
                    if(data[i+y*w+j] < 0xF0) k=j;
            char_add(0, SSFN_FRAG_PIXMAP, w, m, k+1, 0, data + i);
        }
    } else {
        m = w / (re - rs + 1);
        if(!ssfn_hdr.baseline) ssfn_hdr.baseline = h-1;
        for(unicode=rs; unicode<=re; unicode++) {
            data2 = (unsigned char*)malloc(m*h);
            if(!data2) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            for(y=o=k=0;y<h;y++) {
                i = y*w + (unicode-rs)*m;
                for(x=0;x<m;x++) {
                    if(data[i] < 0xF0 && k < x) k = x;
                    data2[o++] = data[i++];
                }
            }
            char_add(0, SSFN_FRAG_PIXMAP, m, h, k+1, 0, data2);
        }
        free(data);
    }
    ssfn_hdr.family = SSFN_FAMILY_DECOR;
}

/**
 * Parse SSFN ASCII font format (text)
 * Originally only bitmap info was intended, but I've hacked in contour paths too.
 */
void asc(char *ptr, int size)
{
    int x, y, l, w, h, i, j, k, v, par[6], p, right, *hi;
    char *end = ptr + size-5, *e, t, cmd;
    unsigned char *bitmap;
    char_t *c;

    for(numchars = 0,e=ptr;e < end && *e;e++)
        if(e[0]=='+' && (e[1]=='+' || e[1]=='=' || e[1]=='%' || e[1]=='!')) numchars++;
    ssfn_hdr.bbox_top = ssfn_hdr.bbox_left = 65535;

    while(ptr < end && *ptr) {
        if(*ptr=='+') {
            if(ptr[1]=='#') {
                if(ptr[2]=='f' && ptr[3]=='a') { ptr += 10; ssfn_hdr.family = atoi(ptr); }
                if(ptr[2]=='q') { ptr += 11; ssfn_hdr.quality = atoi(ptr); }
                if(ptr[2]=='b' && ptr[3]=='a') { ptr += 12; if(!ssfn_hdr.baseline) ssfn_hdr.baseline = atoi(ptr); }
                if(ptr[2]=='u') { ptr += 13; ssfn_hdr.underline = atoi(ptr); }
                if(ptr[2]=='s') {
                    for(ptr += 9;*ptr!='\n';ptr++) {
                        if(*ptr=='b') ssfn_hdr.style |= SSFN_STYLE_BOLD;
                        if(*ptr=='i') ssfn_hdr.style |= SSFN_STYLE_ITALIC;
                    }
                }
            } else
            if(ptr[1]=='$') {
                k = ptr[2];
                while(*ptr && *ptr!=' ') ptr++;
                while(*ptr==' ') ptr++;
                for(e=ptr;*e && *e!='\r' && *e!='\n';e++);
                bitmap = malloc(e-ptr+1);
                if(!bitmap) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                memcpy(bitmap, ptr, e-ptr);
                bitmap[e-ptr]=0;
                switch(k) {
                    case 'n': if(!arg_name) arg_name = (char*)bitmap; break;
                    case 'f': if(!arg_family) arg_family = (char*)bitmap; break;
                    case 's': if(!arg_subfamily) arg_subfamily = (char*)bitmap; break;
                    case 'r': if(!arg_ver) arg_ver = (char*)bitmap; break;
                    case 'm': if(!arg_manufacturer) arg_manufacturer = (char*)bitmap; break;
                    case 'l': if(!arg_license) arg_license = (char*)bitmap; break;
                }
            } else
            if(ptr[1]!='@') break;
        }
        ptr++;
    }

    while(ptr < end && *ptr) {
        while(ptr < end && *ptr && !(ptr[0]=='+' && (ptr[1]=='+' || ptr[1]=='=' || ptr[1]=='%' || ptr[1]=='!' || ptr[1]=='@') &&
            ptr[2]=='-' && ptr[4]=='-')) ptr++;
        if(ptr[0]!='+') return;
        if(ptr[1]=='@') { if(ptr[5]=='K' || ptr[5]=='E') break; else { ptr++; continue; } }
        x = y = -1;
        t = ptr[1];
        switch(ptr[3]) {
            case '1': case 'i': v = 1; break;
            case '2': case 'm': v = 2; break;
            case '3': case 'f': v = 3; break;
            case '4': v = 4; break;
            case '5': v = 5; break;
            case '6': v = 6; break;
            default: v = 0; break;
        }
        ptr += 5;
        unicode = atoi(ptr);
        while(*ptr && *ptr!='-') ptr++;
        while(*ptr=='-') ptr++;
        if(t!='+') {
            x = atoi(ptr);
            while(*ptr && *ptr!='-') ptr++;
            while(*ptr=='-') ptr++;
            y = atoi(ptr);
        }
        while(*ptr && *ptr!='\n') ptr++;
        while(*ptr=='\n') ptr++;
        if(t!='!') {
            bitmap = (unsigned char*)malloc(129*128);
            if(!bitmap) { fprintf(stderr,"memory allocation error\n"); exit(2); }
            memset(bitmap, 0, 129*128);
            i = h = 0;
            while(*ptr && *ptr!='+') {
                for(l = w = 0;*ptr && *ptr!='\r' && *ptr!='\n';l++,i++) {
                    if(t=='%') {
                        if(ptr[0]=='.') { bitmap[i] = 0xF0; while(*ptr && *ptr=='.') ptr++; }
                        else {
                            bitmap[i] = cpal_add(gethex(ptr+2, 2), gethex(ptr+4, 2), gethex(ptr+6,2), gethex(ptr,2));
                            ptr += 8;
                        }
                        if(*ptr==' ') ptr++;
                    } else {
                        bitmap[i>>3] >>= 1;
                        if(*ptr!=' ' && *ptr!='.' && *ptr!='+') bitmap[i>>3] |= 0x80;
                        ptr++;
                    }
                }
                while(*ptr=='\r' || *ptr=='\n') ptr++;
                if(l > w) w = l;
                h++;
                if(l > 128 || h > 128) {
                    fprintf(stderr,"\r%smap %d x %d for U+%06x too big\n", t=='%'?"pix":"bit", l, h, unicode);
                    exit(3);
                }
            }
            if(t=='%') {
                bitmap = realloc(bitmap, w*h);
                char_add(v, SSFN_FRAG_PIXMAP, w, h, x==-1? w : x, y==-1? 0 : y, bitmap);
            } else {
                bitmap = realloc(bitmap, (w+7)/8*h);
                char_add(v, SSFN_FRAG_BITMAP, w, h, x==-1? w : x, y==-1? 0 : y, bitmap);
            }
        } else {
            c = char_add(v, SSFN_FRAG_CONTOUR, 1, 1, x==-1? w : x, y==-1? 0 : y, NULL);
            if(c) {
                c->bear_l = c->bear_t = 65536;
                frag_len = max_x = max_y = 0; min_x = min_y = 65535; lx = ly = -1;
                contour=NULL;
                while(*ptr && *ptr!='+') {
                    cmd = *ptr; par[0]=par[1]=par[2]=par[3]=par[4]=par[5]=0;
                    if(cmd == 'H') {
                        for(p=1,k=0;ptr[p]!='\n' && ptr[p]!='+';p++) if(ptr[p]==' ') k++;
                        c->hlen = k;
                        c->hhints = (int*)realloc(c->hhints, c->hlen*sizeof(int));
                        if(!c->hhints) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                        for(ptr+=2,p=0;*ptr && *ptr!='\n' && *ptr!='+' && p<k;ptr++) {
                            c->hhints[p++] = atoi(ptr);
                            while(*ptr!=' ' && ptr[1] && ptr[1]!='\n' && ptr[1]!='+') ptr++;
                        }
                        while(*ptr=='\n') ptr++;
                        continue;
                    } else
                    if(cmd == 'V') {
                        for(p=1,k=0;ptr[p]!='\n' && ptr[p]!='+';p++) if(ptr[p]==' ') k++;
                        c->vlen = k;
                        c->vhints = (int*)realloc(c->vhints, c->vlen*sizeof(int));
                        if(!c->vhints) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                        for(ptr+=2,p=0;*ptr && *ptr!='\n' && *ptr!='+' && p<k;ptr++) {
                            c->vhints[p++] = atoi(ptr);
                            while(*ptr!=' ' && ptr[1] && ptr[1]!='\n' && ptr[1]!='+') ptr++;
                        }
                        while(*ptr=='\n') ptr++;
                        continue;
                    } else {
                        for(ptr+=2,p=0;*ptr && *ptr!='\n' && *ptr!='+' && p<6;ptr++) {
                            if(cmd=='c' || cmd=='g') {
                                par[p++] = *ptr=='.' ? 240 : cpal_add(
                                    gethex((char*)ptr+2, 2), gethex((char*)ptr+4, 2),
                                    gethex((char*)ptr+6, 2), gethex((char*)ptr, 2));
                            } else
                                par[p++] = atoi(ptr);
                            while(*ptr!=' ' && *ptr!=',' && ptr[1] && ptr[1]!='\n' && ptr[1]!='+') ptr++;
                        }
                    }
                    while(*ptr=='\n') ptr++;
                    switch(cmd) {
                        case 'm':
                            if(frag_len) {
                                k=c->len*3; c->len++;
                                c->frags = (int*)realloc(c->frags, (k+3)*sizeof(int));
                                if(!c->frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                                c->frags[k+0] = frag_mx;
                                c->frags[k+1] = frag_my;
                                c->frags[k+2] = frag_add(SSFN_FRAG_CONTOUR, 1, 1, (unsigned char*)contour);
                                if(frag_mx < c->bear_l) c->bear_l = frag_mx;
                                if(frag_my < c->bear_t) c->bear_t = frag_my;
                            }
                            frag_len = 0; frag_mx = frag_my = frag_ix = frag_iy = 65535; frag_ax = frag_ay = 0;
                            gx = gy = -1;
                            contour=NULL;
                            if(p<2) { fprintf(stderr,"Too few move arguments for U+%06X\n",unicode); exit(3); }
                            cont_add(SSFN_CONTOUR_MOVE, par[0], par[1], par[2], par[3], par[4], par[5]);
                        break;

                        case 'l':
                            if(p<2) { fprintf(stderr,"Too few line arguments for U+%06X\n",unicode); exit(3); }
                            cont_add(SSFN_CONTOUR_LINE, par[0], par[1], par[2], par[3], par[4], par[5]);
                        break;

                        case 'q':
                            if(p<4) { fprintf(stderr,"Too few quadratic curve arguments for U+%06X\n",unicode); exit(3); }
                            cont_add(SSFN_CONTOUR_QUAD, par[0], par[1], par[2], par[3], par[4], par[5]);
                        break;

                        case 'b':
                            if(p<6) { fprintf(stderr,"Too few bezier curve arguments for U+%06X\n",unicode); exit(3); }
                            cont_add(SSFN_CONTOUR_CUBIC, par[0], par[1], par[2], par[3], par[4], par[5]);
                        break;

                        case 'c':
                            if(p<1) { fprintf(stderr,"Too few color arguments for U+%06X\n",unicode); exit(3); }
                            cont_add(SSFN_CONTOUR_COLOR, par[0], par[0], par[0], par[0], 0, 0);
                        break;

                        case 'g':
                            if(p<4) { fprintf(stderr,"Too few gradient arguments for U+%06X\n",unicode); exit(3); }
                            cont_add(SSFN_CONTOUR_COLOR, par[0], par[1], par[2], par[3], 0, 0);
                        break;
                    }
                }
                if(frag_len) {
                    k=c->len*3; c->len++;
                    c->frags = (int*)realloc(c->frags, (k+3)*sizeof(int));
                    if(!c->frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                    c->frags[k+0] = frag_mx;
                    c->frags[k+1] = frag_my;
                    c->frags[k+2] = frag_add(SSFN_FRAG_CONTOUR, 1, 1, (unsigned char*)contour);
                    if(frag_mx < c->bear_l) c->bear_l = frag_mx;
                    if(frag_my < c->bear_t) c->bear_t = frag_my;
                }
                if(ssfn_hdr.bbox_left > min_x) ssfn_hdr.bbox_left = min_x;
                if(ssfn_hdr.bbox_top > min_y) ssfn_hdr.bbox_top = min_y;
                if(x > max_x) max_x = x;
                if(y > max_y) max_y = y;
                c->w = max_x; if(ssfn_hdr.bbox_right < max_x) ssfn_hdr.bbox_right = max_x;
                c->h = max_y; if(ssfn_hdr.bbox_bottom < max_y) ssfn_hdr.bbox_bottom = max_y;
                if(hinting) {
                    if(c->vlen) {
                        ssfn_hdr.features |= SSFN_FEAT_HASHINT;
                        hi = (int*)malloc(c->vlen*sizeof(int));
                        if(!hi) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                        qsort(c->vhints, c->vlen, sizeof(int), kgrpsrt);
                        for(k=1;k<c->vlen;k++) {
                            hi[k-1]=c->vhints[k]-c->vhints[k-1];
                            if(hi[k-1] > 255) ssfn_hdr.features |= SSFN_FEAT_HBIGCRD;
                        }
                        j=c->len*3; c->len++;
                        c->frags = (int*)realloc(c->frags, (j+3)*sizeof(int));
                        if(!c->frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                        for(;j; j--) c->frags[j+2] = c->frags[j-1];
                        c->frags[0] = c->bear_l;
                        c->frags[1] = c->vhints[0] + 1;
                        c->frags[2] = frag_add(SSFN_FRAG_HINTING, k-1, sizeof(int), (unsigned char*)hi);
                        c->vlen = 0;
                    }
                    if(c->hlen) {
                        ssfn_hdr.features |= SSFN_FEAT_HASHINT;
                        hi = (int*)malloc(c->hlen*sizeof(int));
                        if(!hi) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                        qsort(c->hhints, c->hlen, sizeof(int), kgrpsrt);
                        for(k=1;k<c->hlen;k++) {
                            hi[k-1]=c->hhints[k]-c->hhints[k-1];
                            if(hi[k-1] > 255) ssfn_hdr.features |= SSFN_FEAT_HBIGCRD;
                        }
                        j=c->len*3; c->len++;
                        c->frags = (int*)realloc(c->frags, (j+3)*sizeof(int));
                        if(!c->frags) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                        for(;j; j--) c->frags[j+2] = c->frags[j-1];
                        c->frags[0] = c->hhints[0] + 1;
                        c->frags[1] = c->bear_t;
                        c->frags[2] = frag_add(SSFN_FRAG_HINTING, k-1, sizeof(int), (unsigned char*)hi);
                        c->hlen = 0;
                    }
                }
            } else {
                while(*ptr && *ptr!='+') ptr++;
            }
        }
    }
    while(ptr < end && *ptr) {
        while(ptr < end && *ptr && !(ptr[0]=='+' && ptr[1]=='@' && ptr[2]=='-' && ptr[3]=='-')) ptr++;
        t=ptr[5];
        while(*ptr && *ptr!='\n') ptr++;
        while(*ptr=='\n') ptr++;
        if(t=='K') {
            while(ptr < end && *ptr && *ptr!='\n' && ptr[1]==':') {
                unicode = getutf8(&ptr);
                if(unicode<=32 || ptr[0]!=':') break;
                for(i=-1,j=0;j<characters_num;j++)
                    if(!chars[j].v && chars[j].unicode==unicode) { i=j; break; }
                if(i==-1) break;
                ptr += 2;
                while(ptr < end && *ptr && *ptr!='\n') {
                    while(*ptr==' ') ptr++;
                    right = getutf8(&ptr);
                    ptr++;
                    l = atoi(ptr);
                    while(*ptr && *ptr!='v' && *ptr!='h') ptr++;
                    if(chars[i].klen>32767)
                        fprintf(stderr,"Too many kerning pairs for U+%06x, truncated to 32768\n", chars[i].unicode);
                    else if(l) {
                        for(k=j=0;k<chars[i].klen;k+=3) {
                            if(chars[i].kern[k]==right) { j=1; break; }
                        }
                        if(!j) {
                            k = chars[i].klen*3;
                            chars[i].klen++;
                            chars[i].kern = (int*)realloc(chars[i].kern, (k+3)*sizeof(int));
                            if(!chars[i].kern) { fprintf(stderr,"memory allocation error\n"); exit(2); }
                            chars[i].kern[k+0] = right;
                            chars[i].kern[k+1] = chars[i].kern[k+2] = 0;
                        }
                        chars[i].kern[k+(*ptr!='v'?1:2)] = l;
                        haskern++;
                    }
                    ptr++;
                    if(*ptr==',') ptr++;
                }
                if(*ptr=='\n') ptr++;
            }
        }
        if(t=='E') break;
        ptr++;
    }
}

/**
 * Usage instructions
 */
void usage()
{
    printf("Scalable Screen Font by bzt Copyright (C) 2019 MIT license\n\n"
           "./bit2sfn [-0|-1|-2|-3|-4|-5|-6] [-R] [-g] "
#if HAS_ZLIB
            "[-z] "
#endif
           "[-b <p>] [-u <+p>] [-a <p>]\n");
    printf("   [-n <name>] [-f <family>] [-s <subfamily>] [-v <ver>] [-m <manufacturer>]\n"
           "   [-l <license>] [-r <from> <to>] <in> [ [-r <from> <to>] <in> ...] <out>\n\n"
           " -0..6: grid from 0 (lowest, 16x16) to 6 (higher, 1024x1024)\n"
           " -R:    replace characters from new files\n"
           " -g:    save grid hinting information\n"
#if HAS_ZLIB
           " -z:    compress output with gzip\n"
#endif
           " -b:    horizontal baseline in pixels\n"
           " -u:    underline position in pixels (relative to baseline)\n"
           " -a:    add a constant to advance (some fonts need it, others don't)\n");
    printf(" -n:    set font unique name\n"
           " -f:    set family name\n"
           " -s:    set subfamily name\n"
           " -v:    set font revision / version\n"
           " -m:    set manufacturer (designer, foundry)\n"
           " -l:    set license\n"
           " -r:    code point range, this flag can be repeated before each input\n");
    printf(" in:    input PSF2, BDF, hex, TGA or ASC font filename"
#if HAS_ZLIB
           "*"
#endif
           "\n out:   output SSFN filename\n"
#if HAS_ZLIB
           "\n* - input files can be gzip compressed, like .psfu.gz, .bdf.gz or .hex.gz\n"
#endif
        );
    exit(1);
}

/**
 * Main procedure
 */
int main(int argc, char **argv)
{
    uint32_t unicode = 0;
    int in = 0, i, j, k, l, m, n, v, s = 0, fs = 0, cs = 0, ks = 0, kg = 0, *hints;
    char *str, *outfile = NULL, *infile = NULL;
    unsigned char *frg = NULL, *chr = NULL, *kern = NULL, *kgrp = NULL, *data;
    long int origsize = 0, size;
    FILE *f;
#if HAS_ZLIB
    unsigned char hdr[2];
    gzFile g;
#endif

    /* clear and set up SSFN header */
    memset(&ssfn_hdr, 0, sizeof(ssfn_font_t));
    memcpy(&ssfn_hdr.magic, SSFN_MAGIC, 4);
    ssfn_hdr.size = sizeof(ssfn_font_t);
    ssfn_hdr.family = SSFN_FAMILY_MONOSPACE;
    ssfn_hdr.bbox_top = ssfn_hdr.bbox_left = 65535;
    memset(cpal, 0, 960);

    /* parse flags and arguments */
    if(argc<3) usage();
    for(i=1;argv[i];i++){
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 'n': if(++i>=argc) usage(); arg_name = argv[i]; continue;
                case 'f': if(++i>=argc) usage(); arg_family = argv[i]; continue;
                case 's': if(++i>=argc) usage(); arg_subfamily = argv[i]; continue;
                case 'v': if(++i>=argc) usage(); arg_ver = argv[i]; continue;
                case 'm': if(++i>=argc) usage(); arg_manufacturer = argv[i]; continue;
                case 'l': if(++i>=argc) usage(); arg_license = argv[i]; continue;
                case 'b': if(++i>=argc) usage(); ssfn_hdr.baseline = atoi(argv[i]); continue;
                case 'a': if(++i>=argc) usage(); adv = atoi(argv[i]); continue;
                case 'u': if(++i>=argc) usage(); relunderline = atoi(argv[i]); continue;
                case 'r':
                    if(++i>=argc) usage();
                    if((argv[i][0] >= '0' && argv[i][0] <= '9') || (argv[i][0]=='U' && argv[i][1]=='+') || argv[i][0]=='\'') {
                        if(i+1>=argc) usage();
                        rs = getnum(argv[i++]); re = getnum(argv[i]);
#if HAS_UNICODE
                    } else {
                        for(rs=re=j=0;j<(int)(sizeof(ublocks)/sizeof(ublocks[0]));j++)
                            if(!unicmp(argv[i], ublocks[j].name)) {
                                rs = ublocks[j].start; re = ublocks[j].end;
                                break;
                            }
                        if(!re) {
                            fprintf(stderr, "unable to get range '%s', did you mean:\n", argv[i]);
                            for(j=0;j<(int)(sizeof(ublocks)/sizeof(ublocks[0]));j++)
                                if(tolower(argv[i][0]) == tolower(ublocks[j].name[0]) &&
                                    tolower(argv[i][1]) == tolower(ublocks[j].name[1])) {
                                    fprintf(stderr, " %s\n", ublocks[j].name);
                                    re++;
                                }
                            if(!re)
                                for(j=0;j<(int)(sizeof(ublocks)/sizeof(ublocks[0]));j++)
                                    if(tolower(argv[i][0]) == tolower(ublocks[j].name[0])) {
                                        fprintf(stderr, " %s\n", ublocks[j].name);
                                        re++;
                                    }
                            if(!re)
                                printf(" no matching blocks found\n");
                            return 1;
                        }
#endif
                    }
                    if(rs > 0x10FFFF || re > 0x10FFFF || rs > re) {
                        fprintf(stderr, "unable to get range '%s' '%s'\n", argv[i], argv[i]+1);
                        return 1;
                    }
                    continue;
                default:
                    for(j=1;argv[i][j];j++) {
                        switch(argv[i][j]) {
                            case 'g': hinting = 1; break;
                            case 'z': zip = 1; break;
                            case 'R': replace = 1; break;
                            case '6':
                            case '5':
                            case '4':
                            case '3':
                            case '2':
                            case '1':
                            case '0': ssfn_hdr.quality = argv[i][j] - '0'; break;
                            default: fprintf(stderr, "unknown flag '%c'\n", argv[i][j]); return 1;
                        }
                    }
                break;
            }
        } else {
            if(argv[i+1]) {
                if(!argv[i]) usage();
                if(!infile) infile=argv[i];
                f = fopen(argv[i],"rb");
                if(!f) { fprintf(stderr,"unable to load %s\n", argv[i]); return 3; }
                size = 0;
#if HAS_ZLIB
                fread(&hdr, 2, 1, f);
                if(hdr[0]==0x1f && hdr[1]==0x8b) {
                    fseek(f, -4L, SEEK_END);
                    fread(&size, 4, 1, f);
                } else {
                    fseek(f, 0, SEEK_END);
                    size = ftell(f);
                }
                fclose(f);
                g = gzopen(argv[i],"r");
#else
                fseek(f, 0, SEEK_END);
                size = ftell(f);
                fseek(f, 0, SEEK_SET);
#endif
                if(size) {
                    origsize += size;
                    data = (unsigned char*)malloc(size+1);
                    if(!data) { fprintf(stderr,"memory allocation error\n"); return 2; }
#if HAS_ZLIB
                    gzread(g, data, size);
#else
                    fread(data, size, 1, f);
#endif
                    data[size]=0; last = -1;
                    if(data[0]==0x72 && data[1]==0xB5 && data[2]==0x4A && data[3]==0x86) {
                        printf("Loaded '%s' (PSF2, %X - %X)\n", argv[i], rs, re);
                        psf(data, size);
                    } else if((data[0]>='0' && data[0]<='9') || (data[0]>='A' && data[0]<='F')) {
                        printf("Loaded '%s' (GNU unifont hex, %X - %X)\n", argv[i], rs, re);
                        hex((char*)data, size);
                    } else if(data[0]=='S' && data[1]=='T' && data[2]=='A' && data[3]=='R') {
                        printf("Loaded '%s' (X11 BDF, %X - %X)\n", argv[i], rs, re);
                        bdf((char*)data, size);
                    } else if(data[0]=='#' && data[1]=='#' && data[2]==' ' && data[3]=='S') {
                        printf("Loaded '%s' (SSFN ASCII, %X - %X)\n", argv[i], rs, re);
                        asc((char*)data, size);
                    } else if(data[0]==0 && (data[1]==0 || data[1]==1) &&
                        (data[2]==1 || data[2]==2 || data[2]==9 || data[2]==10) &&
                        (data[16]==8 || data[16]==24 || data[16]==32)) {
                            printf("Loaded '%s' (TARGA, %X - %X)\n", argv[i], rs, re);
                            tga(data, size);
                    } else {
                        fprintf(stderr,"bad file format %s\n", argv[i]); return 3;
                    }
                    printf("\r");
                    free(data);
                }
#if HAS_ZLIB
                gzclose(g);
#else
                fclose(f);
#endif
                rs = 0; re = 0x10FFFF; numchars = 0; in++;
            } else outfile = argv[i];
        }
    }
    if(!in || !outfile) usage();

    if(ssfn_hdr.bbox_bottom > 15 && ssfn_hdr.quality < 1) ssfn_hdr.quality = 1;
    if(ssfn_hdr.bbox_bottom > 31 && ssfn_hdr.quality < 2) ssfn_hdr.quality = 2;
    if(ssfn_hdr.bbox_bottom > 63 && ssfn_hdr.quality < 3) ssfn_hdr.quality = 3;
    if(ssfn_hdr.bbox_bottom > 127 && ssfn_hdr.quality < 4) ssfn_hdr.quality = 4;
    if(ssfn_hdr.bbox_bottom > 255 && ssfn_hdr.quality < 5) ssfn_hdr.quality = 5;
    if(ssfn_hdr.bbox_bottom > 511 && ssfn_hdr.quality < 6) ssfn_hdr.quality = 6;
    if(ssfn_hdr.bbox_bottom > 1023 && ssfn_hdr.quality < 7) ssfn_hdr.quality = 7;
    if(!ssfn_hdr.baseline || ssfn_hdr.baseline > (16<<ssfn_hdr.quality)) {
        ssfn_hdr.baseline = ssfn_hdr.bbox_bottom * 12 / 16;
        if(ssfn_hdr.baseline + 2 >= ssfn_hdr.bbox_bottom) ssfn_hdr.baseline = ssfn_hdr.bbox_bottom - 2;
    }
    if(!ssfn_hdr.underline || ssfn_hdr.underline > (16<<ssfn_hdr.quality)) {
        if(relunderline < 0) relunderline = -relunderline;
        if(!relunderline) relunderline = 2;
        ssfn_hdr.underline = (ssfn_hdr.baseline + relunderline + 1 < ssfn_hdr.bbox_bottom) ?
            ssfn_hdr.baseline + relunderline : (ssfn_hdr.baseline + 1 < ssfn_hdr.bbox_bottom ?
            ssfn_hdr.baseline + 1 : ssfn_hdr.bbox_bottom);
    }

    /* construct SSFN string table */
    str = malloc(BUFSIZ);
    if(!str) { fprintf(stderr,"memory allocation error\n"); return 2; }
    memset(str, 0, BUFSIZ);
    if(!arg_name) arg_name=infile;
    i = strlen(arg_name)+1; memcpy(str+s, arg_name, i); s += i;
    if(!arg_family) arg_family="No family";
    i = strlen(arg_family)+1; memcpy(str+s, arg_family, i); s += i;
    if(!arg_subfamily) arg_subfamily="";
    i = strlen(arg_subfamily)+1; memcpy(str+s, arg_subfamily, i); s += i;
    if(!arg_ver) arg_ver="1.0";
    i = strlen(arg_ver)+1; memcpy(str+s, arg_ver, i); s += i;
    if(!arg_manufacturer) arg_manufacturer=getenv("LOGNAME");
    i = strlen(arg_manufacturer)+1; memcpy(str+s, arg_manufacturer, i); s += i;
    if(!arg_license) arg_license="MIT";
    i = strlen(arg_license)+1; memcpy(str+s, arg_license, i); s += i;

    ssfn_hdr.size = ssfn_hdr.fragments_offs = sizeof(ssfn_font_t) + s;
    ssfn_hdr.size += 4;

    /* serialize fragments */
    do {
        l=0;
        /* merge way too splitted bitmaps. we should split them here when we have all the fragments */
        for(i=0;i<characters_num;i++) {
            if(chars[i].len>1) {
                qsort(chars[i].frags, chars[i].len, 3*sizeof(int), frgsrt);
                for(j=1;j<chars[i].len;j++) {
                    if(frags[chars[i].frags[j*3-1]].t==SSFN_FRAG_BITMAP && frags[chars[i].frags[j*3+2]].t==SSFN_FRAG_BITMAP &&
                        frags[chars[i].frags[j*3-1]].cnt==1 && frags[chars[i].frags[j*3+2]].cnt==1 &&
                        frags[chars[i].frags[j*3-1]].p==frags[chars[i].frags[j*3+2]].p) {
                            k=(chars[i].frags[j*3+1]+frags[chars[i].frags[j*3+2]].h-chars[i].frags[j*3-2]);
                            data = malloc(k*frags[chars[i].frags[j*3+2]].p);
                            if(!data) { fprintf(stderr,"memory allocation error\n"); return 2; }
                            memset(data, 0, k*frags[chars[i].frags[j*3+2]].p);
                            memcpy(data, frags[chars[i].frags[j*3-1]].data, frags[chars[i].frags[j*3-1]].len);
                            memcpy(data + (chars[i].frags[j*3+1]-chars[i].frags[j*3-2])*frags[chars[i].frags[j*3+2]].p,
                                frags[chars[i].frags[j*3+2]].data, frags[chars[i].frags[j*3+2]].len);
                            chars[i].frags[j*3+1] = chars[i].frags[j*3-2];
                            chars[i].frags[j*3-2] = 65536;
                            frags[chars[i].frags[j*3+2]].h = k;
                            frags[chars[i].frags[j*3+2]].len = k*frags[chars[i].frags[j*3+2]].p;
                            frags[chars[i].frags[j*3+2]].data = data;
                            frags[chars[i].frags[j*3-1]].cnt--;
                            l=1;
                    }
                }
            }
        }
    } while(l);

    if(ssfn_hdr.features & SSFN_FEAT_HASCMAP) {
        for(i=0;i<241;i++) cpalidx[i] = i;
        qsort(cpalidx, 240, 1, cpalsrt);
        for(i=0;i<241;i++) cpalrev[cpalidx[i]]=i;
    }

    for(fs=0,i=k=0;i<fragments_num;i++) {
        if(frags[i].cnt<1 || frags[i].h<1) continue;
        frags[i].pos = ssfn_hdr.fragments_offs + fs;
        if(frags[i].t==SSFN_FRAG_HINTING) {
            /* hinting fragment */
            frg = (unsigned char*)realloc(frg, fs+2+2*frags[i].len);
            if(!frg) { fprintf(stderr,"memory allocation error\n"); return 2; }
            if(frags[i].p > 16) {
                frg[fs++] =  (((frags[i].p-1)>>8) & 0xF) | 0xF0;
                frg[fs++] =  (frags[i].p-1) & 0xFF;
            } else
                frg[fs++] =  ((frags[i].p-1) & 0xF) | 0xE0;
            hints = (int*)frags[i].data;
            for(j=0;j<frags[i].p;j++) {
                frg[fs++] = hints[j] & 0xFF;
                if(ssfn_hdr.features & SSFN_FEAT_HBIGCRD)
                    frg[fs++] = (hints[j]>>8) & 0xFF;
            }
        } else
        if(frags[i].t!=SSFN_FRAG_CONTOUR) {
            /* bitmap fragment, this is easy */
            frg = (unsigned char*)realloc(frg, fs+6+2*frags[i].len);
            if(!frg) { fprintf(stderr,"memory allocation error\n"); return 2; }
            if(frags[i].t==SSFN_FRAG_BITMAP) {
                frg[fs++] =  ((frags[i].p-1) & 0x1F) | 0x80;
                frg[fs++] =  (frags[i].h-1) & 0x7F;
                for(j=0;j<frags[i].len;j++) frg[fs++] = frags[i].data[j];
            } else {
                /* pixmap fragment */
                frg[fs++] =  ((((frags[i].p-1)>>8) & 0x3)<<2) | (((frags[i].h-1)>>8) & 0x3) | 0xC0;
                frg[fs++] =  (frags[i].p-1) & 0xFF;
                frg[fs++] =  (frags[i].h-1) & 0xFF;
                l = fs;
                frg[fs++] = 0;
                frg[fs++] = 0;
                n = fs;
                frg[fs++] = 0;

                for(j = 0; j < frags[i].len; j++) {
                    for(m = 1; m < 129 && j + m < frags[i].len && frags[i].data[j] == frags[i].data[j + m]; m++);
                    if(m > 1) {
                        m--;
                        if(frg[n]) { frg[n]--; frg[fs++] = 0x80 | m; }
                        else frg[n] = 0x80 | m;
                        frg[fs++] = cpalrev[frags[i].data[j]];
                        n = fs; frg[fs++] = 0;
                        j += m;
                        continue;
                    }
                    frg[n]++;
                    frg[fs++] = cpalrev[frags[i].data[j]];
                    if(frg[n] > 127) { frg[n]--; n = fs; frg[fs++] = 0; }
                }
                if(!(frg[n] & 0x80)) { if(frg[n]) frg[n]--; else fs--; }
                j = fs - l - 3;
                if(j > 65535) {
                    fprintf(stderr,"compressed pixmap fragment too big, referenced from characters:\n");
                    for(j=0;j<characters_num;j++) {
                        for(l=0;l<chars[j].len;l++)
                            if(chars[j].frags[l*3+2]==i) { fprintf(stderr," U+%06X",chars[j].unicode); break; }
                    }
                    fprintf(stderr,"\n");
                    return 2;
                }
                frg[l] = j & 0xFF;
                frg[l+1] = j >> 8;
            }
        } else {
            /* hack, countour path */
            l=frags[i].len/sizeof(cont_t);
            if(l < 2) continue;
            frg = (unsigned char*)realloc(frg, fs+16);
            if(!frg) { fprintf(stderr,"memory allocation error\n"); return 2; }
            if(l>65)
                frg[fs++] = (((l - 2) >> 8) & 0x3F) | 0x40;
            frg[fs++] = (l - 2) & 0xFF;
            contour=(cont_t*)frags[i].data;
            if(contour->type != SSFN_CONTOUR_MOVE) {
                fprintf(stderr,"contour fragment %d does not start with move to command\n", i);
                return 2;
            }
            /* SSFN_CONTOUR_MOVE */
            if(ssfn_hdr.quality < 5) {
                frg[fs++] = contour->px & 0xFF;
                frg[fs++] = contour->py & 0xFF;
            } else {
                frg[fs++] = ((contour->px >> 8) & 15) | (((contour->py >> 8) & 15) << 4);
                frg[fs++] = contour->px & 0xFF;
                frg[fs++] = contour->py & 0xFF;
            }
            contour++;
            for(j=1;j<l;j++,contour++) {
                frg = (unsigned char*)realloc(frg, fs+16);
                if(!frg) { fprintf(stderr,"memory allocation error\n"); return 2; }
                if(contour->type == SSFN_CONTOUR_COLOR) {
                    n = (contour->px == contour->py && contour->px == contour->c1x && contour->px == contour->c1y) ? 0 : 0x4;
                }
                switch(ssfn_hdr.quality) {
                    /* low quality */
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        switch(contour->type) {
                            case SSFN_CONTOUR_LINE:
                                frg[fs++] = (contour->px & 0x7F) | 0x80;
                                frg[fs++] = contour->py & 0x7F;
                            break;
                            case SSFN_CONTOUR_QUAD:
                                frg[fs++] = contour->px & 0x7F;
                                frg[fs++] = (contour->py & 0x7F) | 0x80;
                                frg[fs++] = contour->c1x & 0x7F;
                                frg[fs++] = contour->c1y & 0x7F;
                            break;
                            case SSFN_CONTOUR_CUBIC:
                                frg[fs++] = (contour->px & 0x7F) | 0x80;
                                frg[fs++] = (contour->py & 0x7F) | 0x80;
                                frg[fs++] = contour->c1x & 0x7F;
                                frg[fs++] = contour->c1y & 0x7F;
                                frg[fs++] = contour->c2x & 0x7F;
                                frg[fs++] = contour->c2y & 0x7F;
                            break;
                            case SSFN_CONTOUR_COLOR:
                                frg[fs++] = n | (contour->px >> 7);
                                frg[fs++] = contour->px & 0x7F;
                                if(n) {
                                    frg[fs++] = contour->py;
                                    frg[fs++] = contour->c1x;
                                    frg[fs++] = contour->c1y;
                                }
                            break;
                        }
                    break;

                    /* reasonable quality */
                    case 4:
                        switch(contour->type) {
                            case SSFN_CONTOUR_LINE:
                                frg[fs++] = SSFN_CONTOUR_LINE;
                                frg[fs++] = contour->px & 0xFF;
                                frg[fs++] = contour->py & 0xFF;
                            break;
                            case SSFN_CONTOUR_QUAD:
                                frg[fs++] = SSFN_CONTOUR_QUAD;
                                frg[fs++] = contour->px & 0xFF;
                                frg[fs++] = contour->py & 0xFF;
                                frg[fs++] = contour->c1x & 0xFF;
                                frg[fs++] = contour->c1y & 0xFF;
                            break;
                            case SSFN_CONTOUR_CUBIC:
                                frg[fs++] = SSFN_CONTOUR_CUBIC;
                                frg[fs++] = contour->px & 0xFF;
                                frg[fs++] = contour->py & 0xFF;
                                frg[fs++] = contour->c1x & 0xFF;
                                frg[fs++] = contour->c1y & 0xFF;
                                frg[fs++] = contour->c2x & 0xFF;
                                frg[fs++] = contour->c2y & 0xFF;
                            break;
                            case SSFN_CONTOUR_COLOR:
                                frg[fs++] = n;
                                frg[fs++] = contour->px & 0xFF;
                                if(n) {
                                    frg[fs++] = contour->py;
                                    frg[fs++] = contour->c1x;
                                    frg[fs++] = contour->c1y;
                                }
                            break;
                        }
                    break;

                    /* medium quality */
                    case 5:
                        switch(contour->type) {
                            case SSFN_CONTOUR_LINE:
                                frg[fs++] = SSFN_CONTOUR_LINE
                                    | (((contour->px >> 8) & 1) << 2)
                                    | (((contour->py >> 8) & 1) << 3);
                                frg[fs++] = contour->px & 0xFF;
                                frg[fs++] = contour->py & 0xFF;
                            break;
                            case SSFN_CONTOUR_QUAD:
                                frg[fs++] = SSFN_CONTOUR_QUAD
                                    | (((contour->px >> 8) & 1) << 2)
                                    | (((contour->py >> 8) & 1) << 3)
                                    | (((contour->c1x >> 8) & 1) << 4)
                                    | (((contour->c1y >> 8) & 1) << 5);
                                frg[fs++] = contour->px & 0xFF;
                                frg[fs++] = contour->py & 0xFF;
                                frg[fs++] = contour->c1x & 0xFF;
                                frg[fs++] = contour->c1y & 0xFF;
                            break;
                            case SSFN_CONTOUR_CUBIC:
                                frg[fs++] = SSFN_CONTOUR_CUBIC
                                    | (((contour->px >> 8) & 1) << 2)
                                    | (((contour->py >> 8) & 1) << 3)
                                    | (((contour->c1x >> 8) & 1) << 4)
                                    | (((contour->c1y >> 8) & 1) << 5)
                                    | (((contour->c2x >> 8) & 1) << 6)
                                    | (((contour->c2y >> 8) & 1) << 7);
                                frg[fs++] = contour->px & 0xFF;
                                frg[fs++] = contour->py & 0xFF;
                                frg[fs++] = contour->c1x & 0xFF;
                                frg[fs++] = contour->c1y & 0xFF;
                                frg[fs++] = contour->c2x & 0xFF;
                                frg[fs++] = contour->c2y & 0xFF;
                            break;
                            case SSFN_CONTOUR_COLOR:
                                frg[fs++] = n;
                                frg[fs++] = contour->px & 0xFF;
                                if(n) {
                                    frg[fs++] = contour->py;
                                    frg[fs++] = contour->c1x;
                                    frg[fs++] = contour->c1y;
                                }
                            break;
                        }
                    break;

                    /* high quality */
                    default:
                        switch(contour->type) {
                            case SSFN_CONTOUR_LINE:
                                frg[fs++] = SSFN_CONTOUR_LINE
                                    | (((contour->px >> 8) & 3) << 2)
                                    | (((contour->py >> 8) & 3) << 4);
                                frg[fs++] = contour->px & 0xFF;
                                frg[fs++] = contour->py & 0xFF;
                            break;
                            case SSFN_CONTOUR_QUAD:
                                frg[fs++] = SSFN_CONTOUR_QUAD
                                    | (((contour->px >> 8) & 3) << 2)
                                    | (((contour->py >> 8) & 3) << 4);
                                frg[fs++] = contour->px & 0xFF;
                                frg[fs++] = contour->py & 0xFF;
                                frg[fs++] =
                                      (((contour->c1x >> 8) & 3) << 0)
                                    | (((contour->c1y >> 8) & 3) << 2);
                                frg[fs++] = contour->c1x & 0xFF;
                                frg[fs++] = contour->c1y & 0xFF;
                            break;
                            case SSFN_CONTOUR_CUBIC:
                                frg[fs++] = SSFN_CONTOUR_CUBIC
                                    | (((contour->px >> 8) & 3) << 2)
                                    | (((contour->py >> 8) & 3) << 4);
                                frg[fs++] = contour->px & 0xFF;
                                frg[fs++] = contour->py & 0xFF;
                                frg[fs++] =
                                      (((contour->c1x >> 8) & 3) << 0)
                                    | (((contour->c1y >> 8) & 3) << 2)
                                    | (((contour->c2x >> 8) & 3) << 4)
                                    | (((contour->c2y >> 8) & 3) << 6);
                                frg[fs++] = contour->c1x & 0xFF;
                                frg[fs++] = contour->c1y & 0xFF;
                                frg[fs++] = contour->c2x & 0xFF;
                                frg[fs++] = contour->c2y & 0xFF;
                            break;
                            case SSFN_CONTOUR_COLOR:
                                frg[fs++] = n;
                                frg[fs++] = contour->px & 0xFF;
                                if(n) {
                                    frg[fs++] = contour->py;
                                    frg[fs++] = contour->c1x;
                                    frg[fs++] = contour->c1y;
                                }
                            break;
                        }
                    break;
                }
            }
        }
        k++;
    }
    fragments_num = k;
    printf("Fragments: %d, Characters: %d, bbox: (%d,%d) (%d,%d), baseline: %d, underline: %d\n",
        fragments_num, characters_num, ssfn_hdr.bbox_left,ssfn_hdr.bbox_top,
        ssfn_hdr.bbox_right,ssfn_hdr.bbox_bottom, ssfn_hdr.baseline, ssfn_hdr.underline);

    ssfn_hdr.characters_offs[0] = ssfn_hdr.fragments_offs + fs;
    ssfn_hdr.size += fs;

    /* serialize character maps */
    if(ssfn_hdr.quality < 5 && ssfn_hdr.characters_offs[0] < 65536) l = 0; else
    if(ssfn_hdr.characters_offs[0] < 1048576) l = 1; else l = 2;

    qsort(chars, characters_num, sizeof(char_t), chrsrt);
    cs = 0;
    for(v = 0; v < SSFN_NUMVARIANTS; v++) {
        ssfn_hdr.characters_offs[v] = ssfn_hdr.fragments_offs + fs + cs;
        unicode = -1U;
        for(i=0;i<characters_num;i++) {
            if(chars[i].v != v) continue;
            for(j=k=0;j<chars[i].len;j++) {
                if(chars[i].frags[j*3+1]<65536 && frags[chars[i].frags[j*3+2]].cnt>0) k++;
            }
            if(!k && !iswhitespace(chars[i].unicode)) continue;
            j = chars[i].unicode - unicode - 1;
            chr = (unsigned char*)realloc(chr, cs+256+chars[i].len*10);
            if(!chr) { fprintf(stderr,"memory allocation error\n"); return 2; }
            while(j > 0) {
                if(j <= 64) {
                    chr[cs++] = ((j-1) & 0x3F) | 0x80;
                    break;
                } else {
                    while(j>16384) {
                        chr[cs++] = 0xFF;
                        chr[cs++] = 0xFF;
                        j -= 16384;
                    }
                    if(j > 64) {
                        chr[cs++] = (((j-1) >> 8) & 0x3F) | 0xC0;
                        chr[cs++] = (j-1) & 0xFF;
                        break;
                    }
                }
            }
            chr[cs++] = k & 0x7F;
            chr[cs++] = (((chars[i].h >> 8) & 15) << 4) | ((chars[i].w >> 8) & 15);
            chr[cs++] = (((chars[i].adv_y >> 8) & 15) << 4) | ((chars[i].adv_x >> 8) & 15);
            chr[cs++] = (((chars[i].bear_t >> 8) & 15) << 4) | ((chars[i].bear_l >> 8) & 15);
            chr[cs++] = chars[i].w & 0xFF;
            chr[cs++] = chars[i].h & 0xFF;
            chr[cs++] = chars[i].adv_x & 0xFF;
            chr[cs++] = chars[i].adv_y & 0xFF;
            chr[cs++] = chars[i].bear_l & 0xFF;
            chr[cs++] = chars[i].bear_t & 0xFF;
            if(chars[i].len) qsort(chars[i].frags, chars[i].len, 3*sizeof(int), frgsrt);
            for(j=0;j<chars[i].len && chars[i].frags[j*3+1]<65536 && frags[chars[i].frags[j*3+2]].cnt;j++) {
                chars[i].frags[j*3+0] -= chars[i].bear_l;
                chars[i].frags[j*3+1] -= chars[i].bear_t;
                switch(l) {
                    case 0:
                        chr[cs++] = frags[chars[i].frags[j*3+2]].pos & 0xFF;
                        chr[cs++] = (frags[chars[i].frags[j*3+2]].pos >> 8) & 0xFF;
                        chr[cs++] = chars[i].frags[j*3+0] & 0xFF;
                        chr[cs++] = chars[i].frags[j*3+1] & 0xFF;
                    break;

                    case 1:
                        chr[cs++] = frags[chars[i].frags[j*3+2]].pos & 0xFF;
                        chr[cs++] = (frags[chars[i].frags[j*3+2]].pos >> 8) & 0xFF;
                        chr[cs++] = ((frags[chars[i].frags[j*3+2]].pos >> 16) & 0xF) |
                            (((chars[i].frags[j*3+0] >> 8) & 3) << 4) |
                            (((chars[i].frags[j*3+1] >> 8) & 3) << 6);
                        chr[cs++] = chars[i].frags[j*3+0] & 0xFF;
                        chr[cs++] = chars[i].frags[j*3+1] & 0xFF;
                    break;

                    default:
                        chr[cs++] = frags[chars[i].frags[j*3+2]].pos & 0xFF;
                        chr[cs++] = (frags[chars[i].frags[j*3+2]].pos >> 8) & 0xFF;
                        chr[cs++] = (frags[chars[i].frags[j*3+2]].pos >> 16) & 0xFF;
                        chr[cs++] = ((chars[i].frags[j*3+0] >> 8) & 15) | (((chars[i].frags[j*3+1] >> 8) & 0xF) << 4);
                        chr[cs++] = chars[i].frags[j*3+0] & 0xFF;
                        chr[cs++] = chars[i].frags[j*3+1] & 0xFF;
                    break;
                }
            }
            if(chars[i].frags) free(chars[i].frags);
            unicode = chars[i].unicode;
        }
        if(v && unicode == -1U) {
            ssfn_hdr.characters_offs[v] = 0;
        } else {
            j = 0x110000 - unicode;
            chr = (unsigned char*)realloc(chr, cs+256);
            if(!chr) { fprintf(stderr,"memory allocation error\n"); return 2; }
            while(j > 0) {
                if(j <= 64) {
                    chr[cs++] = ((j-1) & 0x3F) | 0x80;
                    break;
                } else {
                    while(j>16384) {
                        chr[cs++] = 0xFF;
                        chr[cs++] = 0xFF;
                        j -= 16384;
                    }
                    if(j > 64) {
                        chr[cs++] = (((j-1) >> 8) & 0x3F) | 0xC0;
                        chr[cs++] = (j-1) & 0xFF;
                        break;
                    }
                }
            }
        }
    }
    ssfn_hdr.size += cs;

    /* serialize kerning information */
    if(haskern) {
        ssfn_hdr.kerning_offs = ssfn_hdr.characters_offs[0] + cs;
        for(kgrpnum=i=0;i<characters_num;i++) {
            if(chars[i].klen) {
                qsort(chars[i].kern, chars[i].klen, 3*sizeof(int), kgrpsrt);
                chars[i].kgrp = kgrp_add(chars[i].klen, chars[i].kern);
            }
        }
        printf("Kerning: %d pairs, %d groups\n", haskern, kgrpnum);
        min_x = max_x = haskern = 0;
        for(i=0;i<kgrpnum;i++) {
            for(j=0;j<kgrps[i].klen;j++) {
                if(kgrps[i].kern[j*3+0] > haskern) haskern = kgrps[i].kern[j*3+0];
                if(kgrps[i].kern[j*3+1] > max_x) max_x = kgrps[i].kern[j*3+1];
                if(kgrps[i].kern[j*3+2] > max_x) max_x = kgrps[i].kern[j*3+2];
                if(kgrps[i].kern[j*3+1] < min_x) min_x = kgrps[i].kern[j*3+1];
                if(kgrps[i].kern[j*3+2] < min_x) min_x = kgrps[i].kern[j*3+2];
            }
        }
        if(min_x < -128 || max_x > 127) ssfn_hdr.features |= SSFN_FEAT_KBIGCRD;
        if(haskern > 32767) ssfn_hdr.features |= SSFN_FEAT_KBIGCHR;
        for(i=0;i<kgrpnum;i++) {
            for(k=j=0;j<kgrps[i].klen;j++) {
                if(kgrps[i].kern[j*3+1]) k++;
                if(kgrps[i].kern[j*3+2]) k++;
            }
            kgrp = (unsigned char*)realloc(kgrp, kg+k*5);
            if(!kgrp) { fprintf(stderr,"memory allocation error\n"); return 2; }
            kgrps[i].pos = kg;
            if(k<=128) {
                kgrp[kg++] = ((k - 1) & 0x7F);
            } else {
                kgrp[kg++] = (((k - 1) >> 8) & 0x7F) | 0x80;
                kgrp[kg++] = (k - 1) & 0xFF;
            }
            for(j=0;j<kgrps[i].klen;j++) {
                if(kgrps[i].kern[j*3+1]) {
                    kgrp[kg++] = kgrps[i].kern[j*3] & 0xFF;
                    if(ssfn_hdr.features & SSFN_FEAT_KBIGCHR) {
                        kgrp[kg++] = (kgrps[i].kern[j*3] >> 8) & 0xFF;
                        kgrp[kg++] = (kgrps[i].kern[j*3] >> 16) & 0x7F;
                    } else
                        kgrp[kg++] = (kgrps[i].kern[j*3] >> 8) & 0x7F;
                    kgrp[kg++] = kgrps[i].kern[j*3+1] & 0xFF;
                    if(ssfn_hdr.features & SSFN_FEAT_KBIGCRD)
                        kgrp[kg++] = (kgrps[i].kern[j*3+1] >> 8) & 0xFF;
                }
                if(kgrps[i].kern[j*3+2]) {
                    kgrp[kg++] = kgrps[i].kern[j*3] & 0xFF;
                    if(ssfn_hdr.features & SSFN_FEAT_KBIGCHR) {
                        kgrp[kg++] = (kgrps[i].kern[j*3] >> 8) & 0xFF;
                        kgrp[kg++] = ((kgrps[i].kern[j*3] >> 16) & 0x7F) | 0x80;
                    } else
                        kgrp[kg++] = ((kgrps[i].kern[j*3] >> 8) & 0x7F) | 0x80;
                    kgrp[kg++] = kgrps[i].kern[j*3+2] & 0xFF;
                    if(ssfn_hdr.features & SSFN_FEAT_KBIGCRD)
                        kgrp[kg++] = (kgrps[i].kern[j*3+2] >> 8) & 0xFF;
                }
            }
        }
        unicode = -1;
        for(ks=k=i=0;i<characters_num;i++) {
            if(chars[i].kgrp == -1) continue;
            j = chars[i].unicode - unicode - 1;
            while(j > 0) {
                if(j <= 64) { ks++; break;
                } else {
                    while(j>16384) { ks += 2; j -= 16384; }
                    if(j > 64) { ks += 2; break; }
                }
            }
            for(j=0;j<characters_num-i && j<128 &&
                chars[i].unicode+j == chars[i+j].unicode && chars[i].kgrp == chars[i+j].kgrp;j++);
            j--;
            unicode = chars[i].unicode + j;
            i += j;
            k++;
        }
        j = 0x110000 - unicode;
        while(j > 0) {
            if(j <= 64) { ks++; break;
            } else {
                while(j>16384) { ks += 2; j -= 16384; }
                if(j > 64) { ks += 2; break; }
            }
        }
        if(ks+k*3+kg > 65535) {
            ssfn_hdr.features |= SSFN_FEAT_KBIGLKP;
            k *= 4; k += ks;
        } else { k *= 3; k += ks; }

        unicode = -1;
        for(ks=i=0;i<characters_num;i++) {
            if(chars[i].kgrp == -1) continue;
            j = chars[i].unicode - unicode - 1;
            kern = (unsigned char*)realloc(kern, ks+256+3);
            if(!kern) { fprintf(stderr,"memory allocation error\n"); return 2; }
            while(j > 0) {
                if(j <= 64) {
                    kern[ks++] = ((j-1) & 0x3F) | 0x80;
                    break;
                } else {
                    while(j>16384) {
                        kern[ks++] = 0xFF;
                        kern[ks++] = 0xFF;
                        j -= 16384;
                    }
                    if(j > 64) {
                        kern[ks++] = (((j-1) >> 8) & 0x3F) | 0xC0;
                        kern[ks++] = (j-1) & 0xFF;
                        break;
                    }
                }
            }
            for(j=0;j<characters_num-i && j<128 &&
                chars[i].unicode+j == chars[i+j].unicode && chars[i].kgrp == chars[i+j].kgrp;j++);
            j--;
            kern[ks++] = j & 0x7F;
            l = kgrps[chars[i].kgrp].pos + k;
            kern[ks++] = l & 0xFF;
            kern[ks++] = (l >> 8) & 0xFF;
            if(ssfn_hdr.features & SSFN_FEAT_KBIGLKP) {
                kern[ks++] = (l >> 16) & 0xFF;
            }
            unicode = chars[i].unicode + j;
            i += j;
        }
        j = 0x110000 - unicode;
        kern = (unsigned char*)realloc(kern, ks+256);
        if(!kern) { fprintf(stderr,"memory allocation error\n"); return 2; }
        while(j > 0) {
            if(j <= 64) {
                kern[ks++] = ((j-1) & 0x3F) | 0x80;
                break;
            } else {
                while(j>16384) {
                    kern[ks++] = 0xFF;
                    kern[ks++] = 0xFF;
                    j -= 16384;
                }
                if(j > 64) {
                    kern[ks++] = (((j-1) >> 8) & 0x3F) | 0xC0;
                    kern[ks++] = (j-1) & 0xFF;
                    break;
                }
            }
        }
    } else {
        printf("No kerning information\n");
        ssfn_hdr.kerning_offs = 0;
    }
    ssfn_hdr.size += ks + kg;

    if(ssfn_hdr.features & SSFN_FEAT_HASCMAP) ssfn_hdr.size += 960;

    /* write output file */
#if HAS_ZLIB
    if(zip) {
        g = gzopen(outfile, "wb");
        if(!g) { fprintf(stderr, "unable to write %s\n", outfile); return 4; }
        gzwrite(g, &ssfn_hdr, sizeof(ssfn_font_t));
        gzwrite(g, str, s);
        gzwrite(g, frg, fs);
        gzwrite(g, chr, cs);
        gzwrite(g, kern, ks);
        gzwrite(g, kgrp, kg);
        if(ssfn_hdr.features & SSFN_FEAT_HASCMAP)
            for(i=0;i<240;i++)
                gzwrite(g, cpal + cpalidx[i]*4, 4);
        gzwrite(g, SSFN_ENDMAGIC, 4);
        gzclose(g);
        f = fopen(outfile, "rb");
        if(f) {
            fseek(f, 0, SEEK_END);
            s = (int)ftell(f);
            fclose(f);
        } else
            s = 0;
    } else
#endif
    {
        f = fopen(outfile, "wb");
        if(!f) { fprintf(stderr, "unable to write %s\n", outfile); return 4; }
        fwrite(&ssfn_hdr, sizeof(ssfn_font_t), 1, f);
        fwrite(str, s, 1, f);
        fwrite(frg, fs, 1, f);
        fwrite(chr, cs, 1, f);
        fwrite(kern, ks, 1, f);
        fwrite(kgrp, kg, 1, f);
        if(ssfn_hdr.features & SSFN_FEAT_HASCMAP)
            for(i=0;i<240;i++)
                fwrite(cpal + cpalidx[i]*4, 4, 1, f);
        fwrite(SSFN_ENDMAGIC, 4, 1, f);
        fclose(f);
        s = ssfn_hdr.size;
    }

    i = 1000 - (s*1000/origsize);
    if(i<0) { str="+"; i *= -1; } else { str="-"; }
    printf("Original %ld bytes, compressed %d bytes, %s%d.%d%%\n", origsize, s, str, i/10, i%10);

    return 0;
}
