Scalable Screen Font Converters
===============================

Various utilities to generate and read [SSFN](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/sfn_format.md) files.

 * ttf2sfn - vector font converter for PS Type 1, OpenType and TrueType formats (requires [freetype2](http://www.freetype.org))

 * bit2sfn - bitmap and pixmap font converter for PSF2 (or PSFU, Linux Console), BDF (Bitmap Disrtibution Format, X11), HEX (GNU unifont), TGA (Truevision TARGA) and [ASC](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/asc_format.md) (a plain text format)

 * sfn2asc - converts an SSFN into plain ASCII text format and generates code point coverage reports
 
 * sfn2sfn - tool to create and extract SSFN font collections

Read more details about these tools and the font formats they support in the [documentation](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/ecosystem.md).
