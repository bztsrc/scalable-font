/*
 * sfntest2.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief testing Scalable Screen Font normal renderer with bitmap fonts
 *
 */

#include <stdio.h>
#include "../ssfn.h"
#if HAS_ZLIB
#include <zlib.h>
#endif

#include <SDL.h>

/**
 * Load a font
 */
ssfn_font_t *load_font(char *filename)
{
    char *fontdata = NULL;
    long int size;
    FILE *f;
#if HAS_ZLIB
    unsigned char hdr[2];
    gzFile g;
#endif

    f = fopen(filename, "rb");
    if(!f) { fprintf(stderr,"unable to load %s\n", filename); exit(3); }
    size = 0;
#if HAS_ZLIB
    fread(&hdr, 2, 1, f);
    if(hdr[0]==0x1f && hdr[1]==0x8b) {
        fseek(f, -4L, SEEK_END);
        fread(&size, 4, 1, f);
    } else {
        fseek(f, 0, SEEK_END);
        size = ftell(f);
    }
    fclose(f);
    g = gzopen(filename,"r");
#else
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
#endif
    if(!size) { fprintf(stderr,"unable to load %s\n", filename); exit(3); }
    fontdata = malloc(size);
    if(!fontdata) { fprintf(stderr,"memory allocation error\n"); exit(2); }
#if HAS_ZLIB
    gzread(g, fontdata, size);
    gzclose(g);
#else
    fread(fontdata, size, 1, f);
    fclose(f);
#endif

    return (ssfn_font_t*)fontdata;
}

/**
 * Draw a line on screen
 */
void line(SDL_Surface *screen, int x0, int y0, int x1, int y1)
{
    int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
    int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
    int err = dx+dy, e2;

    for (;;) {
        ((uint32_t*)(screen->pixels))[y0*screen->pitch/4+x0] = 0;
        e2 = 2*err;
        if (e2 >= dy) {
            if (x0 == x1) break;
            err += dy; x0 += sx;
        }
        if (e2 <= dx) {
            if (y0 == y1) break;
            err += dx; y0 += sy;
        }
    }
}

/**
 * Blit a glyph to screen
 */
void blit(SDL_Surface *screen, ssfn_glyph_t *glyph, int px, int py)
{
    int x,y,i,m;

    switch(glyph->mode) {
    case SSFN_MODE_BITMAP:
        for(y=0;y<glyph->h;y++) {
            for(x=0,i=0,m=1; x<glyph->w; x++,m<<=1) {
                if(m>0x80) { m=1; i++; }
                if(glyph->data[y*glyph->pitch+i] & m)
                    ((uint32_t*)(screen->pixels))[(py+y-glyph->baseline)*screen->pitch/4+(px+x)] = 0;
            }
        }
    break;

    case SSFN_MODE_ALPHA:
        for(y=0;y<glyph->h;y++) {
            for(x=0;x<glyph->w;x++) {
                m = 256-glyph->data[y*glyph->pitch+x];
                if(m!=256)
                    ((uint32_t*)(screen->pixels))[(py+y-glyph->baseline)*screen->pitch/4+(px+x)] = (uint32_t)((m<<16)|(m<<8)|m);
            }
        }
    break;

    case SSFN_MODE_CMAP:
        for(y=0;y<glyph->h;y++) {
            for(x=0;x<glyph->w;x++) {
                m = 256 - (SSFN_CMAP_TO_ARGB(glyph->data[y*glyph->pitch+x], glyph->cmap, 0) >> 24);
                if(m!=256)
                    ((uint32_t*)(screen->pixels))[(py+y-glyph->baseline)*screen->pitch/4+(px+x)] = (uint32_t)((m<<16)|(m<<8)|m);
            }
        }
    break;
    }
}

/**
 * testing the SSFN library (normal renderer)
 */
void do_test(SDL_Surface *screen, char *fontfn)
{
    char *s, *str[] = {
        "Normal renderer with UNICODE VGA!", "Üdvözlet!", "¡Bienvenido!", "Здравствуйте!", "Καλως ηρθες!", "متعدد اللغات",
        NULL };
    char *str0 = "Bitmap fonts rendering to Bitmap";
    char *str1 = "Bitmap to Alpha channel rendering";
    char *str2 = "Bitmap Italic (not that bad)";
    char *str3 = "Bitmap Bold style";
    char *str4 = "Underline text pqg";
    char *str5 = "Strike-through test";
    char *str6 = "Color map mode test";
    int i, err, x, y, px, py, z=32;
    ssfn_t ctx;
    ssfn_glyph_t *glyph;
    ssfn_font_t *font;

    /* initialize the normal renderer */
    memset(&ctx, 0, sizeof(ssfn_t));

    /* load and select a font */
    font = load_font(fontfn ? fontfn : "../fonts/u_vga16.sfn");
    err = ssfn_load(&ctx, font);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn load error: %s\n", ssfn_error(err)); exit(2); }

    printf("Testing normal renderer with bitmap font\n");

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_ALPHA);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    for(i=0; str[i]; i++) {
        s = str[i];
        px = 20;
        py = 16*(i+1)+1;
        for(s = str[i]; *s;) {
            glyph = ssfn_render(&ctx, ssfn_utf8(&s));
            if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
            /* display the returned buffer */
            blit(screen, glyph, px, py);
            /* advances */
            px += glyph->adv_x; py += glyph->adv_y;
            free(glyph);
        }
    }

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR, 27, SSFN_MODE_ALPHA);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    for(i=1; str[i]; i++) {
        s = str[i];
        px = 320;
        py = 24*i+1;
        for(s = str[i]; *s;) {
            glyph = ssfn_render(&ctx, ssfn_utf8(&s));
            if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
            /* display the returned buffer */
            blit(screen, glyph, px, py);
            /* advances */
            px += glyph->adv_x; py += glyph->adv_y;
            free(glyph);
        }
    }

    printf("Testing normal renderer with bitmap font in outline mode\n");

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR, z, SSFN_MODE_OUTLINE);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    /* display strings */
    px=5; py=128;
    for(s = str0; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned outline */
        if(glyph->pitch>1) {
            x = glyph->data[0]; y = glyph->data[1];
            for(i = 0; i < glyph->pitch; i += 2) {
                /* end of a contour? */
                if(glyph->data[i] == 255 && glyph->data[i+1] == 255) i += 2;
                /* no, connect this point to the previous one */
                else if(py+y>0 && px+x>0) line(screen, px+x, py+y, px+glyph->data[i], py+glyph->data[i+1]);
                x = glyph->data[i]; y = glyph->data[i+1];
            }
        }
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    printf("Testing normal renderer with bitmap font in bitmap mode\n");

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR, z, SSFN_MODE_BITMAP);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=5; py=192;
    for(s = str0; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_ITALIC, z, SSFN_MODE_BITMAP);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=5; py=224;
    for(s = str2; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_BOLD, z, SSFN_MODE_BITMAP);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=5; py=256;
    for(s = str3; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_UNDERLINE, z, SSFN_MODE_BITMAP);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=5; py=288;
    for(s = str4; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_STHROUGH, z, SSFN_MODE_BITMAP);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=400; py=288;
    for(s = str5; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    printf("Testing normal renderer with bitmap font in alpha channel mode (anti-aliasing)\n");

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR, z, SSFN_MODE_ALPHA);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=5; py=330;
    for(s = str1; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_ITALIC, z, SSFN_MODE_ALPHA);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=5; py=362;
    for(s = str2; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_BOLD, z, SSFN_MODE_ALPHA);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=5; py=394;
    for(s = str3; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_UNDERLINE, z, SSFN_MODE_ALPHA);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=5; py=426;
    for(s = str4; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_STHROUGH, z, SSFN_MODE_ALPHA);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=400; py=426;
    for(s = str5; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    printf("Testing normal renderer with bitmap font in color map mode\n");

    err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR, z, SSFN_MODE_CMAP);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn select error: %s\n", ssfn_error(err)); exit(2); }

    px=5; py=470;
    for(s = str6; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    printf("Testing normal renderer bitmap font scaling\n");

    px=5; py=560;
    for(i=1, z=8; z<255 && px < screen->w - z/2; z += (z < 32 ? 1 : i++)) {
        err = ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR, z, SSFN_MODE_ALPHA);
        glyph = ssfn_render(&ctx, 'S');
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    printf("Memory allocated: %d\n", ssfn_mem(&ctx));
    ssfn_free(&ctx);
    free(font);
}

/**
 * Main procedure
 */
int main(int argc __attribute__((unused)), char **argv)
{
    SDL_Window *window;
    SDL_Surface *screen;
    SDL_Event event;

    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS)) {
        fprintf(stderr,"SDL error %s\n", SDL_GetError());
        return 2;
    }

    window = SDL_CreateWindow("SSFN normal renderer bitmap font test",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,800,600,0);
    screen = SDL_GetWindowSurface(window);
    memset(screen->pixels, 0xF8, screen->pitch*screen->h);

    do_test(screen, argv[1]);

    do{ SDL_UpdateWindowSurface(window); SDL_Delay(10); } while(SDL_WaitEvent(&event) && event.type != SDL_QUIT &&
        event.type != SDL_MOUSEBUTTONDOWN && event.type != SDL_KEYDOWN);

    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
