Scalable Screen Font Tests
==========================

sfntest1: tests the simple renderer

sfntest2: tests the normal renderer with bitmap fonts. You can pass a font file name on the command line.

sfntest3: tests the styling capabilities of the normal renderer with vector fonts. You can pass a font file name on the command line.

sfntest4: tests the normal renderer with pixel fonts. You can pass a font file name on the command line.

sfntest5: tests antialising code with magnified alpha glyphs. You can pass a font file name on the command line.

sfntest6: tests hinting grid code with magnified alpha glyphs comparing two fonts. You can pass two font file names on the command line.

sfntest7: tests hinting grid and scaling. You can pass a font file name as first argument. If you specify a second argument (anything) hinting will be turned off

sfntest8: tests hinting and kerning. You can pass a font file name on the command line.

sfntest9: various features demo

tgaview: helper tool to view TGA files which have small width but huge height (like 16 x 4096) or vice versa (eg. 4096 x 16)

