/*
 * tgaview.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief helper utility to display pixmap glyphs in TGA images
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <SDL.h>
#undef HAS_ZLIB

long int size;

/**
 * Load a font
 */
unsigned char *load_font(char *filename)
{
    char *fontdata = NULL;
    FILE *f;
#if HAS_ZLIB
    unsigned char hdr[2];
    gzFile g;
#endif

    f = fopen(filename, "rb");
    if(!f) { fprintf(stderr,"unable to load %s\n", filename); exit(3); }
    size = 0;
#if HAS_ZLIB
    fread(&hdr, 2, 1, f);
    if(hdr[0]==0x1f && hdr[1]==0x8b) {
        fseek(f, -4L, SEEK_END);
        fread(&size, 4, 1, f);
    } else {
        fseek(f, 0, SEEK_END);
        size = ftell(f);
    }
    fclose(f);
    g = gzopen(filename,"r");
#else
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
#endif
    if(!size) { fprintf(stderr,"unable to load %s\n", filename); exit(3); }
    fontdata = malloc(size);
    if(!fontdata) { fprintf(stderr,"memory allocation error\n"); exit(2); }
#if HAS_ZLIB
    gzread(g, fontdata, size);
    gzclose(g);
#else
    fread(fontdata, size, 1, f);
    fclose(f);
#endif

    return (unsigned char*)fontdata;
}

/**
 * testing the SSFN library (simple bitmap renderer)
 */
#define cpal_add(r,g,b) (((uint32_t)r<<16) | ((uint32_t)g<<8) | b)
void do_test(SDL_Surface *screen,char *fn, int s)
{
    unsigned char *ptr;
    uint32_t *data;
    int i, j, k, x, y, w, h, o, m;

    /* initialize the simple renderer */
    ptr = load_font(fn);

    o = (ptr[11] << 8) + ptr[10];
    w = (ptr[13] << 8) + ptr[12];
    h = (ptr[15] << 8) + ptr[14];
    m = ((ptr[1]? (ptr[7]>>3)*ptr[5] : 0) + 18);
    data = (uint32_t*)malloc(w*h*4);
    if(!data) { fprintf(stderr,"memory allocation error\n"); exit(2); }
    switch(ptr[2]) {
        case 1:
            if(ptr[6]!=0 || ptr[4]!=0 || ptr[3]!=0 || (ptr[7]!=24 && ptr[7]!=32)) {
                fprintf(stderr,"unsupported TGA file format\n");
                exit(2);
            }
            for(y=i=0; y<h; y++) {
                k = ((!o?h-y-1:y)*w);
                for(x=0; x<w; x++) {
                    j = ptr[m + k++]*(ptr[7]>>3) + 18;
                    data[i++] = cpal_add(ptr[j+2], ptr[j+1], ptr[j]);
                }
            }
        break;

        case 2:
            if(ptr[5]!=0 || ptr[6]!=0 || ptr[1]!=0 || (ptr[16]!=24 && ptr[16]!=32)) {
                fprintf(stderr,"unsupported TGA file format\n");
                exit(2);
            }
            for(y=i=0; y<h; y++) {
                j = ((!o?h-y-1:y)*w*(ptr[16]>>3));
                for(x=0; x<w; x++) {
                    data[i++] = cpal_add(ptr[j+2], ptr[j+1], ptr[j]);
                    j += ptr[16]>>3;
                }
            }
        break;

        case 9:
            if(ptr[6]!=0 || ptr[4]!=0 || ptr[3]!=0 || (ptr[7]!=24 && ptr[7]!=32)) {
                fprintf(stderr,"unsupported TGA file format\n");
                exit(2);
            }
            y = i = 0;
            for(x=0; x<w*h && m<size;) {
                k = ptr[m++];
                if(k > 127) {
                    k -= 127; x += k;
                    j = ptr[m++]*(ptr[7]>>3) + 18;
                    while(k--) {
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = cpal_add(ptr[j+2], ptr[j+1], ptr[j]);
                    }
                } else {
                    k++; x += k;
                    while(k--) {
                        j = ptr[m++]*(ptr[7]>>3) + 18;
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = cpal_add(ptr[j+2], ptr[j+1], ptr[j]);
                    }
                }
            }
        break;

        case 10:
            if(ptr[5]!=0 || ptr[6]!=0 || ptr[1]!=0 || (ptr[16]!=24 && ptr[16]!=32)) {
                fprintf(stderr,"unsupported TGA file format\n");
                exit(2);
            }
            y = i = 0;
            for(x=0; x<w*h && m<size;) {
                k = ptr[m++];
                if(k > 127) {
                    k -= 127; x += k;
                    while(k--) {
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = cpal_add(ptr[m+2], ptr[m+1], ptr[m]);
                    }
                    m += ptr[16]>>3;
                } else {
                    k++; x += k;
                    while(k--) {
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = cpal_add(ptr[m+2], ptr[m+1], ptr[m]);
                        m += ptr[16]>>3;
                    }
                }
            }
        break;
    }

    if(s<1) s=h;
    printf("w %d h %d h/w %d s %d\n",w,h,h/w,s);
    if(h > w)
        for(y=i=0;y<h && ((y/screen->h+1)*w) < screen->w;y++)
            for(x=0;x<w;x++)
                ((uint32_t*)(screen->pixels))[(y%screen->h)*screen->pitch/4+(x+(y/screen->h*w))] = data[i++];
    else {
        k = screen->h/h;
        for(o=0;o<w/s;o++) {
            for(y=0;y<h;y++) {
                i = y*w + o*s;
                for(x=0;x<s;x++)
                    ((uint32_t*)(screen->pixels))[((o%k)*h+y)*screen->pitch/4+(x+(o/k*s))] = data[i++];
            }
        }
    }
}

/**
 * Main procedure
 */
int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
    SDL_Window *window;
    SDL_Surface *screen;
    SDL_Event event;

    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS)) {
        fprintf(stderr,"SDL error %s\n", SDL_GetError());
        return 2;
    }

    window = SDL_CreateWindow("TGA viewer for high images", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280, 720, 0);
    screen = SDL_GetWindowSurface(window);

    if(!argv[1]) {
        printf("%s <tga file> [dimension]\n", argv[0]);
        exit(0);
    }
    do_test(screen,argv[1],argc > 2? atoi(argv[2]) : 0);

    do{ SDL_UpdateWindowSurface(window); SDL_Delay(10); } while(SDL_WaitEvent(&event) && event.type != SDL_QUIT &&
        event.type != SDL_MOUSEBUTTONDOWN && event.type != SDL_KEYDOWN);

    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
