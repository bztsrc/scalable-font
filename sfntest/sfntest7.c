/*
 * sfntest7.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief testing Scalable Screen Font renderer hinting and scaling
 *
 */

#include <stdio.h>
#include "../ssfn.h"
#if HAS_ZLIB
#include <zlib.h>
#endif

#include <SDL.h>

/**
 * Load a font
 */
ssfn_font_t *load_font(char *filename)
{
    char *fontdata = NULL;
    long int size;
    FILE *f;
#if HAS_ZLIB
    unsigned char hdr[2];
    gzFile g;
#endif

    f = fopen(filename, "rb");
    if(!f) { fprintf(stderr,"unable to load %s\n", filename); exit(3); }
    size = 0;
#if HAS_ZLIB
    fread(&hdr, 2, 1, f);
    if(hdr[0]==0x1f && hdr[1]==0x8b) {
        fseek(f, -4L, SEEK_END);
        fread(&size, 4, 1, f);
    } else {
        fseek(f, 0, SEEK_END);
        size = ftell(f);
    }
    fclose(f);
    g = gzopen(filename,"r");
#else
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
#endif
    if(!size) { fprintf(stderr,"unable to load %s\n", filename); exit(3); }
    fontdata = malloc(size);
    if(!fontdata) { fprintf(stderr,"memory allocation error\n"); exit(2); }
#if HAS_ZLIB
    gzread(g, fontdata, size);
    gzclose(g);
#else
    fread(fontdata, size, 1, f);
    fclose(f);
#endif

    return (ssfn_font_t*)fontdata;
}

/**
 * Draw a line on screen
 */
void line(SDL_Surface *screen, int x0, int y0, int x1, int y1)
{
    int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
    int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
    int err = dx+dy, e2;

    for (;;) {
        ((uint32_t*)(screen->pixels))[y0*screen->pitch/4+x0] = 0;
        e2 = 2*err;
        if (e2 >= dy) {
            if (x0 == x1) break;
            err += dy; x0 += sx;
        }
        if (e2 <= dx) {
            if (y0 == y1) break;
            err += dx; y0 += sy;
        }
    }
}

/**
 * Blit a glyph to screen
 */
void blit(SDL_Surface *screen, ssfn_glyph_t *glyph, int px, int py)
{
    int x,y,i,m;

    if(px < 0 || px >= screen->w || py < 0 || py >= screen->h) return;

    switch(glyph->mode) {
    case SSFN_MODE_BITMAP:
        for(y=0;y<glyph->h;y++) {
            for(x=0,i=0,m=1; x<glyph->w; x++,m<<=1) {
                if(m>0x80) { m=1; i++; }
                if(glyph->data[y*glyph->pitch+i] & m)
                    ((uint32_t*)(screen->pixels))[(py+y-glyph->baseline)*screen->pitch/4+(px+x)] = 0;
            }
        }
    break;

    case SSFN_MODE_ALPHA:
        for(y=0;y<glyph->h;y++) {
            for(x=0;x<glyph->w;x++) {
                m = 256-glyph->data[y*glyph->pitch+x];
                if(m!=256)
                    ((uint32_t*)(screen->pixels))[(py+y-glyph->baseline)*screen->pitch/4+(px+x)] = (uint32_t)((m<<16)|(m<<8)|m);
            }
        }
    break;
    }
}

/**
 * testing the SSFN library (normal renderer)
 */
void do_test(SDL_Surface *screen, char *fontfn, int argc)
{
    char *s, str0[64];
    int i, err, px, py;
    ssfn_t ctx;
    ssfn_glyph_t *glyph;
    ssfn_font_t *font;

    /* initialize the normal renderer */
    memset(&ctx, 0, sizeof(ssfn_t));

    /* load and select a font */
    font = load_font(fontfn ? fontfn : "../fonts/FreeSerif.sfn.gz");
    err = ssfn_load(&ctx, font);
    if(err != SSFN_OK) { fprintf(stderr, "ssfn load error: %s\n", ssfn_error(err)); exit(2); }

    py=10;
    for(i=8; py < screen->h - 60; i++) {
        sprintf(str0,"%d How much can you read this?",i);
        err = ssfn_select(&ctx, SSFN_FAMILY_ANY, NULL, SSFN_STYLE_REGULAR | (argc>2? SSFN_STYLE_NOHINTING:0), i, SSFN_MODE_ALPHA);
        px = 5;
        for(s=str0;*s;s++) {
            glyph = ssfn_render(&ctx, *s);
            if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
            /* display the returned buffer */
            blit(screen, glyph, px, py);
            /* advances */
            px += glyph->adv_x+1;
            free(glyph);
        }
        py += i;
    }

    /* meaning of... */
    i = 42;
    sprintf(str0,"%d How much can you read this?",i);
    err = ssfn_select(&ctx, SSFN_FAMILY_ANY, NULL, SSFN_STYLE_REGULAR | (argc>2? SSFN_STYLE_NOHINTING:0), i, SSFN_MODE_ALPHA);
    px = 5; py += 10;
    for(s=str0;*s;s++) {
        glyph = ssfn_render(&ctx, *s);
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x+1;
        free(glyph);
    }

    printf("Memory allocated: %d\n", ssfn_mem(&ctx));
    ssfn_free(&ctx);
    free(font);
}

/**
 * Main procedure
 */
int main(int argc __attribute__((unused)), char **argv)
{
    SDL_Window *window;
    SDL_Surface *screen;
    SDL_Event event;

    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS)) {
        fprintf(stderr,"SDL error %s\n", SDL_GetError());
        return 2;
    }

    window = SDL_CreateWindow("SSFN scaling test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, 0);
    screen = SDL_GetWindowSurface(window);
    memset(screen->pixels, 0xF8, screen->pitch*screen->h);

    do_test(screen, argv[1], argc);

    do{ SDL_UpdateWindowSurface(window); SDL_Delay(10); } while(SDL_WaitEvent(&event) && event.type != SDL_QUIT &&
        event.type != SDL_MOUSEBUTTONDOWN && event.type != SDL_KEYDOWN);

    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
