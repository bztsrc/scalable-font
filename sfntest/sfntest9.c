/*
 * sfntest9.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief testing Scalable Screen Font renderer features demo
 *
 */

#include <math.h>
#include <stdio.h>
#define SSFN_CONSOLEBITMAP_TRUECOLOR
#include "../ssfn.h"
#if HAS_ZLIB
#include <zlib.h>
#endif

#include <SDL.h>

/**
 * Load a font
 */
ssfn_font_t *load_font(char *filename)
{
    char *fontdata = NULL;
    long int size;
    FILE *f;
#if HAS_ZLIB
    unsigned char hdr[2];
    gzFile g;
#endif

    f = fopen(filename, "rb");
    if(!f) { fprintf(stderr,"unable to load %s\n", filename); exit(3); }
    size = 0;
#if HAS_ZLIB
    fread(&hdr, 2, 1, f);
    if(hdr[0]==0x1f && hdr[1]==0x8b) {
        fseek(f, -4L, SEEK_END);
        fread(&size, 4, 1, f);
    } else {
        fseek(f, 0, SEEK_END);
        size = ftell(f);
    }
    fclose(f);
    g = gzopen(filename,"r");
#else
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
#endif
    if(!size) { fprintf(stderr,"unable to load %s\n", filename); exit(3); }
    fontdata = malloc(size);
    if(!fontdata) { fprintf(stderr,"memory allocation error\n"); exit(2); }
#if HAS_ZLIB
    gzread(g, fontdata, size);
    gzclose(g);
#else
    fread(fontdata, size, 1, f);
    fclose(f);
#endif

    return (ssfn_font_t*)fontdata;
}

/**
 * Draw a line on screen
 */
void line(SDL_Surface *screen, int x0, int y0, int x1, int y1)
{
    int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
    int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
    int err = dx+dy, e2;

    for (;;) {
        ((uint32_t*)(screen->pixels))[y0*screen->pitch/4+x0] = 0;
        e2 = 2*err;
        if (e2 >= dy) {
            if (x0 == x1) break;
            err += dy; x0 += sx;
        }
        if (e2 <= dx) {
            if (y0 == y1) break;
            err += dx; y0 += sy;
        }
    }
}

/**
 * Blit a glyph to screen
 */
void blit(SDL_Surface *screen, ssfn_glyph_t *glyph, int px, int py)
{
    int x,y,i,m;

    if(px < 0 || px >= screen->w || py < 0 || py >= screen->h) return;

    switch(glyph->mode) {
    case SSFN_MODE_BITMAP:
        for(y=0;y<glyph->h;y++) {
            for(x=0,i=0,m=1; x<glyph->w; x++,m<<=1) {
                if(m>0x80) { m=1; i++; }
                if(glyph->data[y*glyph->pitch+i] & m)
                    ((uint32_t*)(screen->pixels))[(py+y-glyph->baseline)*screen->pitch/4+(px+x)] = 0;
            }
        }
    break;

    case SSFN_MODE_ALPHA:
        for(y=0;y<glyph->h;y++) {
            for(x=0;x<glyph->w;x++) {
                m = 256-glyph->data[y*glyph->pitch+x];
                if(m!=256 && (py+y-glyph->baseline) > 0 && (py+y-glyph->baseline) < screen->h && (px+x) < screen->w)
                    ((uint32_t*)(screen->pixels))[(py+y-glyph->baseline)*screen->pitch/4+(px+x)] = (uint32_t)((m<<16)|(m<<8)|m);
            }
        }
    break;

    case SSFN_MODE_CMAP:
        for(y=0;y<glyph->h;y++) {
            for(x=0;x<glyph->w;x++) {
                if(glyph->data[y*glyph->pitch+x] >= 0xF0) {
                    /* fake alpha channel to color */
                    m = (~(SSFN_CMAP_TO_ARGB(glyph->data[y*glyph->pitch+x], glyph->cmap, 0) >> 24)) & 0xFF;
                    m = (m<<16)|(m<<8)|m;
                } else
                    m = SSFN_CMAP_TO_ARGB(glyph->data[y*glyph->pitch+x], glyph->cmap, 0);
                if(m != 0xffffff && ((py+y-glyph->baseline) < screen->h) && (px+x) < screen->w)
                    ((uint32_t*)(screen->pixels))[(py+y-glyph->baseline)*screen->pitch/4+(px+x)] = (uint32_t)(m);
            }
        }
    break;
    }
}

/**
 * testing the SSFN library (normal renderer)
 */
void do_test(SDL_Surface *screen)
{
    char *s;
    char *title="Scalable Screen Font (SSFN) Feature Demo";
    char *str0="Simple renderer आइऊईऋऌ 丕世丽乇 Многоязычный with GNU unifont (unscaled bitmap fonts only)";
    char *str1="Normal renderer आइऊईऋऌ 丕世丽乇 Многоязычный with GNU unifont (scaled to 1:1)";
    char *str4="Using UNICODE VGA but switching to GNU unifont आइऊईऋऌ for uncovered glyphs automatically";
    char *str2="Scaling bitmap fonts in bitmap mode";
    char *str3="Scaling bitmap fonts with antialiasing";
    char *str5="Vector based fonts are also supported of course";
    char *str6="%Applying #different @styles to !vector bitmap fonts";
    char *str7="%Applying #different @styles to !bitmap vector fonts";
    char *str8="FreeSerif #Italic and %Bold";
    char *str81="FreeSans #Italic and %Bold";
    char *str9="BitStream Vera #Italic and %Bold (with emulated styles)";
    char *strA="BitStream Vera #Italic and %Bold (styled fonts provided)";
    char *strB="How does it look like hmmm? Without hinting";
    char *strC="How does it look like hmmm? With autohinting";
    char *strD="Oh, almost forgot: the compressed 😀 SSFN format supports bitmap based glyphs, vector based glyphs, and...";
    char *strE="->@-PIXEL-BASED-GLYPHS-=-ALSO>AVAILABLE-BY:->\n->THE<SSFN>FORMAT-AND>RENDERER-;->=@->";
    char *strF="Vector font without antialiasing";
    char *str10="Vector font with antialiasing";
    char *str11="All three glyph types can be rendered to all four modes (outline, bitmap, alpha channel, color map):";
    char *str12="种आ😎O";
    char *str13="种आ😎 B";
    char *str14="种आ😎 A";
    char *str15="种आ😎 C";
    char str16[128];
    int sintbl[] = { 0, 1, 5, 9, 14, 21, 28, 36, 56, 67, 78, 67, 56, 46, 36, 28, 21, 14, 9, 5, 1 };
    int i, px, py, x, y;
    ssfn_t ctx;
    ssfn_glyph_t *glyph;

    /* simple renderer */
    ssfn_font = load_font("../fonts/unifont.sfn.gz");
    ssfn_dst_ptr = (uint8_t*)screen->pixels;
    ssfn_dst_pitch = screen->pitch;
    ssfn_fg = 0;
    s = str0;
    ssfn_x = 8;
    ssfn_y = 48;
    while(*s)
        ssfn_putc(ssfn_utf8(&s));

    /* initialize the normal renderer */
    memset(&ctx, 0, sizeof(ssfn_t));

    /* load and select a font */
    ssfn_load(&ctx, ssfn_font);
    ssfn_select(&ctx, SSFN_FAMILY_ANY, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_ALPHA);

    px=8; py=80;
    for(s = str1; *s;) {
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    ssfn_free(&ctx);

    ssfn_load(&ctx, load_font("../fonts/u_vga16.sfn"));
    ssfn_load(&ctx, ssfn_font);
    ssfn_load(&ctx, load_font("../fonts/FreeSerif4.sfn.gz"));
    ssfn_load(&ctx, load_font("../fonts/VeraR.sfn"));
    ssfn_load(&ctx, load_font("../fonts/emoji.sfn"));
    ssfn_load(&ctx, load_font("../fonts/stoneage.sfn"));
    ssfn_load(&ctx, load_font("../fonts/chrome.sfn"));

    px=8; py=140;
    for(i=0, s = str2; *s; i++) {
        ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR,
            16 + sintbl[i%(sizeof(sintbl)/sizeof(sintbl[0]))]/2, SSFN_MODE_BITMAP);
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=180;
    for(i=0, s = str3; *s; i++) {
        ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR,
            16 + sintbl[i%(sizeof(sintbl)/sizeof(sintbl[0]))]/2, SSFN_MODE_ALPHA);
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 32, SSFN_MODE_ALPHA);

    px=100; py=32;
    for(s = title; *s;) {
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_ALPHA);

    px=8; py=100;
    for(s = str4; *s;) {
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=244;
    for(i=0, s = str5; *s; s++, i++) {
        ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR,
            10 + sintbl[i%(sizeof(sintbl)/sizeof(sintbl[0]))], SSFN_MODE_ALPHA);
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=280;
    for(s = str6; *s; s++) {
        if(*s=='@') { ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_UNDERLINE, 16, SSFN_MODE_ALPHA); continue; }
        if(*s=='#') { ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_ITALIC, 16, SSFN_MODE_ALPHA); continue; }
        if(*s=='%') { ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_BOLD, 16, SSFN_MODE_ALPHA); continue; }
        if(*s=='!') { ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_STHROUGH, 16, SSFN_MODE_ALPHA); continue; }
        if(*s==' ') { ssfn_select(&ctx, SSFN_FAMILY_MONOSPACE, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_ALPHA); }
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=420; py=279;
    for(s = str7; *s; s++) {
        if(*s=='@') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_UNDERLINE, 16, SSFN_MODE_ALPHA); continue; }
        if(*s=='#') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_ITALIC, 16, SSFN_MODE_ALPHA); continue; }
        if(*s=='%') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_BOLD, 16, SSFN_MODE_ALPHA); continue; }
        if(*s=='!') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_STHROUGH, 16, SSFN_MODE_ALPHA); continue; }
        if(*s==' ') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_ALPHA); }
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=350;
    ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 27, SSFN_MODE_ALPHA);
    for(s = str8; *s; s++) {
        if(*s=='#') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_ITALIC, 27, SSFN_MODE_ALPHA); continue; }
        if(*s=='%') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_BOLD, 27, SSFN_MODE_ALPHA); continue; }
        if(*s==' ') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 27, SSFN_MODE_ALPHA); }
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=375;
    ssfn_select(&ctx, SSFN_FAMILY_SANS, NULL, SSFN_STYLE_REGULAR, 27, SSFN_MODE_ALPHA);
    for(s = str9; *s; s++) {
        if(*s=='#') { ssfn_select(&ctx, SSFN_FAMILY_SANS, NULL, SSFN_STYLE_ITALIC, 27, SSFN_MODE_ALPHA); continue; }
        if(*s=='%') { ssfn_select(&ctx, SSFN_FAMILY_SANS, NULL, SSFN_STYLE_BOLD, 27, SSFN_MODE_ALPHA); continue; }
        if(*s==' ') { ssfn_select(&ctx, SSFN_FAMILY_SANS, NULL, SSFN_STYLE_REGULAR, 27, SSFN_MODE_ALPHA); }
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    ssfn_load(&ctx, load_font("../fonts/VeraIt.sfn"));
    ssfn_load(&ctx, load_font("../fonts/VeraBd.sfn"));
    ssfn_load(&ctx, load_font("../fonts/FreeSans.sfn.gz"));

    px=420; py=350;
    ssfn_select(&ctx, SSFN_FAMILY_BYNAME, "GNU: FreeSans Normal: 2012", SSFN_STYLE_REGULAR, 27, SSFN_MODE_ALPHA);
    for(s = str81; *s; s++) {
        if(*s=='#') { ssfn_select(&ctx, SSFN_FAMILY_BYNAME, "GNU: FreeSans Normal: 2012",
            SSFN_STYLE_ITALIC, 27, SSFN_MODE_ALPHA); continue; }
        if(*s=='%') { ssfn_select(&ctx, SSFN_FAMILY_BYNAME, "GNU: FreeSans Normal: 2012",
            SSFN_STYLE_BOLD, 27, SSFN_MODE_ALPHA); continue; }
        if(*s==' ') { ssfn_select(&ctx, SSFN_FAMILY_BYNAME, "GNU: FreeSans Normal: 2012",
            SSFN_STYLE_REGULAR, 27, SSFN_MODE_ALPHA); }
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=400;
    ssfn_select(&ctx, SSFN_FAMILY_SANS, NULL, SSFN_STYLE_REGULAR, 27, SSFN_MODE_ALPHA);
    for(s = strA; *s; s++) {
        if(*s=='#') { ssfn_select(&ctx, SSFN_FAMILY_SANS, NULL, SSFN_STYLE_ITALIC, 27, SSFN_MODE_ALPHA); continue; }
        if(*s=='%') { ssfn_select(&ctx, SSFN_FAMILY_SANS, NULL, SSFN_STYLE_BOLD, 27, SSFN_MODE_ALPHA); continue; }
        if(*s==' ') { ssfn_select(&ctx, SSFN_FAMILY_SANS, NULL, SSFN_STYLE_REGULAR, 27, SSFN_MODE_ALPHA); }
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=420; py=300;
    ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR|SSFN_STYLE_NOHINTING, 16, SSFN_MODE_ALPHA);
    for(s = strB; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }
    px=420; py=320;
    ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_ALPHA);
    for(s = strC; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=300;
    ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_BITMAP);
    for(s = strF; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }
    px=8; py=320;
    ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR|SSFN_STYLE_NOHINTING, 16, SSFN_MODE_ALPHA);
    for(s = str10; *s; s++) {
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=425;
    ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_CMAP);
    for(s = strD; *s;) {
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=32; py=450;
    ssfn_select(&ctx, SSFN_FAMILY_DECOR, NULL, SSFN_STYLE_REGULAR|SSFN_STYLE_ABS_SIZE, 16, SSFN_MODE_CMAP);
    for(s = strE; *s; s++) {
        if(*s=='>') { ssfn_select(&ctx, SSFN_FAMILY_DECOR, NULL, SSFN_STYLE_REGULAR|SSFN_STYLE_ABS_SIZE, 16, SSFN_MODE_CMAP); }
        if(*s=='\n') { px=64; py+=32; continue; }
        glyph = ssfn_render(&ctx, *s);
        if(*s=='<') { ssfn_select(&ctx, SSFN_FAMILY_BYNAME, "Retro Chrome", SSFN_STYLE_REGULAR|SSFN_STYLE_ABS_SIZE,
            42, SSFN_MODE_CMAP); }
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=510;
    ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_ALPHA);
    for(s = str11; *s;) {
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=8; py=525;
    s = str12;
        ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 31, SSFN_MODE_OUTLINE);
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned outline */
        if(glyph->pitch>1) {
            x = glyph->data[0]; y = glyph->data[1];
            for(i = 0; i < glyph->pitch; i += 2) {
                /* end of a contour? */
                if(glyph->data[i] == 255 && glyph->data[i+1] == 255) i += 2;
                /* no, connect this point to the previous one */
                else if(py+y>0 && px+x>0) line(screen, px+x, py+y, px+glyph->data[i], py+glyph->data[i+1]);
                x = glyph->data[i]; y = glyph->data[i+1];
            }
        }
        free(glyph);

    px=10; py=495;
        ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 100, SSFN_MODE_OUTLINE);
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned outline */
        if(glyph->pitch>1) {
            x = glyph->data[0]; y = glyph->data[1];
            for(i = 0; i < glyph->pitch; i += 2) {
                /* end of a contour? */
                if(glyph->data[i] == 255 && glyph->data[i+1] == 255) i += 2;
                /* no, connect this point to the previous one */
                else if(py+y>0 && px+x>0) line(screen, px+x, py+y, px+glyph->data[i], py+glyph->data[i+1]);
                x = glyph->data[i]; y = glyph->data[i+1];
            }
        }
        free(glyph);

    px=90; py=520;
        ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 63, SSFN_MODE_OUTLINE);
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned outline */
        if(glyph->pitch>1) {
            x = glyph->data[0]; y = glyph->data[1];
            for(i = 0; i < glyph->pitch; i += 2) {
                /* end of a contour? */
                if(glyph->data[i] == 255 && glyph->data[i+1] == 255) i += 2;
                /* no, connect this point to the previous one */
                else if(py+y>0 && px+x>0) line(screen, px+x, py+y, px+glyph->data[i], py+glyph->data[i+1]);
                x = glyph->data[i]; y = glyph->data[i+1];
            }
        }
        free(glyph);

    px=95; py=495;
        ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 100, SSFN_MODE_OUTLINE);
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned outline */
        if(glyph->pitch>1) {
            x = glyph->data[0]; y = glyph->data[1];
            for(i = 0; i < glyph->pitch; i += 2) {
                /* end of a contour? */
                if(glyph->data[i] == 255 && glyph->data[i+1] == 255) i += 2;
                /* no, connect this point to the previous one */
                else if(py+y>0 && px+x>0) line(screen, px+x, py+y, px+glyph->data[i], py+glyph->data[i+1]);
                x = glyph->data[i]; y = glyph->data[i+1];
            }
        }
        free(glyph);

    px=220; py=550;
    for(s = str13; *s;) {
        ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 31, SSFN_MODE_BITMAP);
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=430; py=550;
    for(s = str14; *s;) {
        ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 31, SSFN_MODE_ALPHA);
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=640; py=550;
    for(s = str15; *s;) {
        ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 31, SSFN_MODE_CMAP);
        glyph = ssfn_render(&ctx, ssfn_utf8(&s));
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    px=16; py=585;
    sprintf(str16,
        "Memory allocated: @%d bytes. Not bad, all of this from a 22k of code in a single ANSI C header, right?",
        ssfn_mem(&ctx));
    ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_ALPHA);
    for(s = str16; *s; s++) {
        if(*s=='@') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_BOLD, 16, SSFN_MODE_ALPHA); continue; }
        if(*s=='.') { ssfn_select(&ctx, SSFN_FAMILY_SERIF, NULL, SSFN_STYLE_REGULAR, 16, SSFN_MODE_ALPHA); }
        glyph = ssfn_render(&ctx, *s);
        if(ssfn_lasterr(&ctx) == SSFN_ERR_NOGLYPH) continue;
        if(!glyph) { fprintf(stderr, "ssfn render error: %s\n", ssfn_error(ctx.err)); exit(2); }
        /* display the returned buffer */
        blit(screen, glyph, px, py);
        /* advances */
        px += glyph->adv_x; py += glyph->adv_y;
        free(glyph);
    }

    printf("Memory allocated: %d\n", ssfn_mem(&ctx));
}

/**
 * Main procedure
 */
int main()
{
    SDL_Window *window;
    SDL_Surface *screen;
    SDL_Event event;

    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS)) {
        fprintf(stderr,"SDL error %s\n", SDL_GetError());
        return 2;
    }

    window = SDL_CreateWindow("SSFN features demo", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, 0);
    screen = SDL_GetWindowSurface(window);
    memset(screen->pixels, 0xF8, screen->pitch*screen->h);

    do_test(screen);

    do{ SDL_UpdateWindowSurface(window); SDL_Delay(10); } while(SDL_WaitEvent(&event) && event.type != SDL_QUIT &&
        event.type != SDL_MOUSEBUTTONDOWN && event.type != SDL_KEYDOWN);

    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
