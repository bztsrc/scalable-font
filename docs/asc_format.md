Scalable Screen Font File ASCII Format
======================================

- File extension: `.asc`
- Mime type: `text/x-ssfont`

(For the binary format that the renderer uses, see [sfn_format.md](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/sfn_format.md)).

This is merely a convinience format to help font designers. It is a plain ASCII file, with some UTF-8
strings in it, each line is terminated by a '\n' (UNICODE 10) character. It is grep friendly, and it
was designed in a way that you can safely copy'n'paste glyph definitions from one file to another as
long as they have the same quality (grid size) setting. It can be considered as an easily editable
"source file" format of SSFN fonts.

The `sfn2asc` utility outputs in this format, and `bit2sfn` tool can parse this to convert the font
back to binary (don't let the name fool you, `bit2sfn` can read not only bitmap glyphs, but as long as
ASC format concerned, pixmap and vector fonts too). Read more about [converters](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/ecosystem.md)).

Header
------

File starts with the magic line:

```
## Scalable Screen Font
```

which may be ended in a revision information (not parsed).

Other Lines
-----------

### Properties

Lines starting with `+#` encode header field values, and are in the form: `+#(key): (value)` optionally followed
by comments. The style value is only parsed for the characters `b` and `i`. Not all font prorepties are stored in
an ASCII file, only those that can't be calculated. Bounding box is only there for the user's information, it is
not parsed rather recalculated from glyph contours.

Lines starting with `+$` are string table entries. They look like `+$(key): (value)`. Values are UTF-8 sequences
terminated by a newline character.

Lines starting with `+@` are group separators, only for easy reading, not parsed, except for the first character
to detect optional blocks (see below).

### Glyphs

#### Vectors

Lines starting with `+!` are scalable character definitions, and look like `+!---(unicode)-(adv x)-(adv y)---` which
may be followed by comments (usually hex and UTF-8 representation of the code point), ended in a newline. After that
optionally comes two lines, one prefixed by `H`, the other `V` if there's a horizontal or vertical
[hinting information](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/hinting.md) associated with the glyph,
which identifies important grid coordinates.
```
H (x0) (x1) (xN) ...
V (y0) (y1) (yN) ...
```
Then the contour commands (move to, line to, quadratic curve to, bezier curve to, set color, set gradient) follows, one of
```
m (x),(y)
l (x),(y)
q (x),(y) (a),(b)
b (x),(y) (a),(b) (c),(d)
c (aarrggbb)
g (aarrggbb) (aarrggbb) (aarrggbb) (aarrggbb)
```
The contour path must start with a move to (m) command and goes on until the first line which does not start with a
valid command.

Gradients contain 4 color codes for upper left, upper right, bottom left, bottom right corners of the glyph in order.
If one color is not specified, then it defaults to the foreground color and specified as a dot '.'. There can be only
one color (c) or gradient (g) command in a path, and if there is, it must come right after the move to command.

Line to (l), quadratic Bezier curve (q) to and cubic Bezier curve to (b) commands can be repeated as many times as
needed to describe a path. If the last command does not return to the move to command's coordinate, then an implicit
line to command will be added to close the path.

#### Bitmaps

Lines starting with `++` encode bitmap character definitions, and look like `++---(unicode)---` which may be followed
by comments (if advance is the same as bitmap width). Lines starting with `+=` also encode bitmap characters, but
similarily to contours, with advance values: `+=---(unicode)-(adv x)-(adv y)---`. After these lines comes a block which
has glyph height lines, and each line has the same length. Those lines can contain ' ' (space), '.' (dot) to encode
transparency, and 'X' (or anything else except '+') to encode foreground. The block goes on until the first line which
does not start with valid bitmap character.

#### Pixmaps

Lines starting with `+%` encode pixmap character definitions, and look like `+%---(unicode)-(adv x)-(adv y)---`.
After this line come glyph height lines with glyph width times hex colors `(aarrggbb)` in each, separated by a space.
Each line encode one row of the pixmap. The values `00000000` and `FF000000` mean transparency, which can be written
as eight dots `........` for increased visibility. To encode black foreground color, use `FE000000` or `FF000100`.

#### Glyph Variants

Alternative glyph versions must be stored right after the default one. They are encoded exactly the same way, except
their header line has a variant number at the 4th character, `+!-(variant)-(unicode)-(adv x)-(adv y)---`,
`++-(variant)-(unicode)-`, `+=-(variant)-(unicode)-(adv x)-(adv y)---`, `+%-(variant)-(unicode)-(adv x)-(adv y)---`.
Variant numbers are from '0' to '6', and the first 4 variants are also recognized by the characters '-' (0, the default,
also the isolated variant), 'i' (1, initial), 'm' (2, medial), 'f' (3, final).

Variations are usually not needed as contextual letters have their own UNICODE code points, and they are stored as
the default variant.

### Kerning Table (optional)

The line starting with `+@---K` encode kerning information. The line is not parsed any further, instead the following
lines have information. Each line describe one kerning group, where UNICODE code points are encoded in UTF-8, and
coordinates in decimal. Coordinates are suffixed by either `v` or `h` depending on if it's a vertical or horizontal
offset. Coordinates can be negative values as well. If there's more, a comma comes. So one line looks like:

```
(left char): (right char) (coord)[vh], (right char) (coord)[vh],...
```

The list is ended by an empty line or a `+@` character pair. This means that space and newline characters can't have
kerning information, but that's perfectly fine. If there's both horizontal and vertical value, now you must repeat
the UTF-8 sequence. In the future, `(right char) (coord)h (coord)v, (right char) ...` format should be recognised.

### End Tag

To be sure that the file wasn't truncated, the font is closed with a `+@---End---` line.

Examples
--------

These ASC examples were generated from the SSFN files included in the [fonts](https://gitlab.com/bztsrc/scalable-font/tree/master/fonts/) folder.

### SSFN ASCII with vector glyphs and kerning info

This is not really readable for humans, but the point is to make glyphs editable and copy'n'pastable.

```
## Scalable Screen Font (revision 0)

+@---Header---
+#family: 0 (Serif)
+#style: regular
+#quality: 6 (grid 1024 x 1024)
+#baseline: 605
+#underline: 698
+#boundingbox: 0,232 1023,791 (informational)
+$name: GNU: FreeSerif Normal: 2012
+$family: FreeSerif
+$subfamily: Regular
+$revision: Version 0412.2263
+$manufacturer: https://savannah.gnu.org/projects/freefont/
+$license: Copyright 2002, 2003, 2005, 2008, 2009, 2010, 2012 GNU Freefont contributors.

+@---Glyphs---

+!---0-245-0---U+0000--'UNDEF'---
m 584,301
l 369,301
l 369,634
l 584,634
l 584,301
m 565,318
l 565,621
l 389,621
l 389,318
l 565,318
m 472,331
b 407,390 435,331 407,356
b 426,415 407,406 414,415
b 443,398 436,415 443,408
b 427,369 443,386 427,377
b 466,343 427,356 446,343
b 509,392 490,343 509,365
b 494,455 509,410 504,433
b 471,538 481,485 472,502
l 478,538
b 504,476 482,511 489,495
b 546,394 527,448 546,425
b 472,331 546,355 511,331
m 475,564
b 454,587 463,564 454,574
b 475,608 454,599 462,608
b 497,587 488,608 497,599
b 475,564 497,575 487,564

+!---32-102-0---U+0020-' '-'SPACE'---

+!---33-136-0---U+0021-'!'-'EXCLAMATION MARK'---
m 432,534
b 451,363 442,418 451,392
b 430,331 451,343 443,331
b 408,362 416,331 408,343
b 426,534 408,392 417,418
l 432,534
m 430,565
b 408,587 417,565 408,575
b 429,608 408,599 417,608
b 451,587 442,608 451,599
b 430,565 451,575 441,565

+!---34-164-0---U+0022-'"'-'QUOTATION MARK'---
m 406,431
b 419,348 413,395 419,355
b 402,331 419,339 411,331
b 385,348 392,331 385,339
b 398,431 385,354 391,394
l 406,431
m 475,431
b 488,348 482,395 488,355
b 471,331 488,339 480,331
b 453,348 461,331 453,339
b 466,431 453,354 459,394
l 475,431
    . . . (>8) . . .
m 613,622
b 591,644 601,622 591,632
b 612,666 591,656 601,666
b 636,644 625,666 636,656
b 613,622 636,632 625,622

+@---Kerning Table---
": 𐅄 -11h
#: 5 -11h, 6 -15h, 7 -15h, 8 -27h, 9 -27h, ; -27h, C -3h, D -11h, E -11h, F -11h, G -11h, I -7h, L -27h, Q -11h, S -11h, V -15h
$: L -27h, V -3h, ö -27h, Ĥ -3h, Ħ -3h, Ů -3h, ǜ -3h
%: L -27h, V -3h, ö -27h, Ĥ -3h, Ħ -3h, Ů -3h, ǜ -3h
&: L -27h, V -3h, ö -27h, Ĥ -3h, Ħ -3h, Ů -3h, ǜ -3h
': L -27h, V -3h, ö -27h, Ĥ -3h, Ħ -3h, Ů -3h, ǜ -3h
(: # -15h, C -7h, E -7h, F -7h, G -7h, L -27h, Q -7h, S -7h, V -3h, ¡ -7h, ¢ -7h, £ -7h, ¤ -7h, ¥ -7h, ¦ -7h, § -7h, ¨ -7h, ³ -7h
): L -27h, V -3h, ö -27h, Ĥ -3h, Ħ -3h, Ů -3h, ǜ -3h
*: L -27h, V -7h, ö -27h, Ĥ -7h, Ħ -7h, Ů -7h, ǜ -7h
:: % -15h, ) -15h, 1 -15h, 3 -15h, 5 -3h, E -7h, F -7h, G -7h, I -3h, L -27h, Q -7h, S -7h, V -15h, X -15h, Y -15h, [ -11h, ¨ -7h
    . . . (>8) . . .
ඖ: 𐅄 -7h
ජ: 𐅄 -3h

+@----End---
```

### SSFN ASCII with bitmap glyphs

Of the three, this one is the best for humans to read. Glyphs are perfectly visible
in a simple text editor. Note that space could have been encoded as `+=---32-8-0-`
or `+%---32-8-0-` as well, because if there's no glyph just an advance, glyph type
doesn't matter.

```
## Scalable Screen Font (revision 0)

+@---Header---
+#family: 3 (Monospace)
+#style: regular
+#quality: 0 (grid 16 x 16)
+#baseline: 12
+#underline: 13
+#boundingbox: 0,0 7,15 (informational)
+$name: Vga Unicode
+$family: VGA
+$subfamily: Medium
+$revision: 1.0
+$manufacturer: http://www.inp.nsk.su/~bolkhov/files/fonts/univga/
+$license: Copyright (c) 2000 Dmitry Bolkhovityanov, bolkhov@inp.nsk.su

+@---Glyphs---

++---0---U+0000--'UNDEF'---
........
........
XX.XX.X.
......X.
X.......
X.....X.
......X.
X.......
X.....X.
......X.
X.......
X.XX.XX.
........
........
........
........

+!---32-8-0---U+0020-' '---

++---33---U+0021-'!'---
........
........
...XX...
..XXXX..
..XXXX..
..XXXX..
...XX...
...XX...
...XX...
........
...XX...
...XX...
........
........
........
........

++---34---U+0022-'"'---
........
.XX..XX.
.XX..XX.
.XX..XX.
..X..X..
........
........
........
........
........
........
........
........
........
........
........
    . . . (>8) . . .
........

++---65533---U+FFFD-'�'---
........
..XXX...
.XXXXX..
.XXXXX..
XX...XX.
X..X..X.
XXXX..X.
XXX..XX.
XXXXXXX.
XXX..XX.
.XXXXX..
.XXXXX..
..XXX...
........
........
........

+@----End---
```

### SSFN ASCII with pixmap glyphs

Finally pixmaps are just like bitmaps, they would been more readable if it weren't
for the long color codes. But you can edit the pixmaps with a simple text editor and
you can copy'n'paste glyphs from one font to another, which is the point.

Note I had to cut the end of the lines in the pixmaps because lines were to long for
a browser. Shouldn't be a problem for a text editor.

```
## Scalable Screen Font (revision 0)

+@---Header---
+#family: 2 (Decorative)
+#style: regular
+#quality: 1 (grid 32 x 32)
+#baseline: 30
+#underline: 31
+#boundingbox: 0,0 31,31 (informational)
+$name: Stone Age
+$family: Dingbats
+$subfamily:
+$revision: 1.0
+$manufacturer: Eclipse
+$license: From the Stone Age game

+@---Glyphs---

+%---44-32-0---U+002C-','-'COMMA'---
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........    (>8)
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ffc08060 ffc08060 ffc08060 ffc08060 ff804020
........ ........ ........ ........ ........ ........ ........ ........ ffc08060 ffc08060 ffc08060 ffc08060 ff804020
........ ........ ........ ........ ........ ........ ........ ........ ffc08060 ffc08060 ffc08060 ffc08060 ffe0a080
........ ........ ........ ........ ........ ........ ........ ........ ffc08060 ffc08060 ffc08060 ffc08060 ffe0a080     .  .  .
........ ........ ........ ........ ........ ........ ff602000 ff602000 ffc08060 ffc08060 ffc08060 ffc08060 ffe0a080
........ ........ ........ ........ ........ ........ ff602000 ff602000 ffc08060 ffc08060 ffc08060 ffc08060 ffe0a080
........ ........ ........ ........ ff602000 ff602000 ff804020 ff804020 ffa06040 ffa06040 ffa06040 ffa06040 ff804020
........ ........ ........ ........ ff602000 ff602000 ff804020 ff804020 ffa06040 ffa06040 ffa06040 ffa06040 ff804020
ff602000 ff602000 ff804020 ff804020 ffa06040 ffa06040 ff804020 ff804020 ffa06040 ffa06040 ffa06040 ffa06040 ........     .  .  .
ff602000 ff602000 ff804020 ff804020 ffa06040 ffa06040 ff804020 ff804020 ffa06040 ffa06040 ffa06040 ffa06040 ........
ff804020 ff804020 ff804020 ff804020 ff804020 ff804020 ff804020 ff804020 ff602000 ff602000 ........ ........ ........
ff804020 ff804020 ff804020 ff804020 ff804020 ff804020 ff804020 ff804020 ff602000 ff602000 ........ ........ ........
ffa06040 ffa06040 ffa06040 ffa06040 ff804020 ff804020 ff804020 ff804020 ff406040 ff406040 ff406040 ff406040 ff406040
ffa06040 ffa06040 ffa06040 ffa06040 ff804020 ff804020 ff804020 ff804020 ff406040 ff406040 ff406040 ff406040 ff406040

+%---45-32-0---U+002D-'-'-'HYPHEN-MINUS'---
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........
........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........ ........     .  .  .
ff385838 ff385838 ........ ........ ff1c381c ff1c381c ff1c381c ff1c381c ff385838 ff385838 ........ ........ ........
ff385838 ff385838 ........ ........ ff1c381c ff1c381c ff1c381c ff1c381c ff385838 ff385838 ........ ........ ........
ff1c381c ff1c381c ........ ........ ff385838 ff385838 ff587458 ff587458 ff1c381c ff1c381c ........ ........ ff385838
ff1c381c ff1c381c ........ ........ ff385838 ff385838 ff587458 ff587458 ff1c381c ff1c381c ........ ........ ff385838
ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff587458     .  .  .
ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff385838 ff587458
    . . . (>8) . . .
ff985c3c ff985c3c ff985c3c ff985c3c ff985c3c ff985c3c ff985c3c ff985c3c ff985c3c ff985c3c ffb87c5c ffb87c5c ffb87c5c     .  .  .

+@----End---
```

