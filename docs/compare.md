Scalable Screen Font Comparitions
=================================

Feature Comparition to Other Font Formats
-----------------------------------------

| Feature                | SSFN | ASC  | TTF  | OTF  | PST1 | PSF2 | BDF  | TGA  |
| ---------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| Binary file            | ✔Yes | ✗No  | ✔Yes | ✔Yes | ✗No  | ✔Yes | ✗No  | ✔Yes |
| Varying width glyphs   | ✔Yes | ✔Yes | ✔Yes | ✔Yes | ✔Yes | ✗No  | ✔Yes | ✗No  |
| Bitmap fonts           | ✔Yes | ✔Yes | ✗No  | ✗No  | ✗No  | ✔Yes | ✔Yes | ✗No  |
| Pixmap fonts           | ✔Yes | ✔Yes | ✗No  | ✗No  | ✗No  | ✗No  | ✗No  | ✔Yes |
| Scalable fonts         | ✔Yes | ✔Yes | ✔Yes | ✔Yes | ✔Yes | ✗No  | ✗No  | ✗No  |
| Quadratic curves       | ✔Yes | ✔Yes | ✔Yes | ✗No  | ✗No  | ✗No  | ✗No  | ✗No  |
| Cubic Bezier curves    | ✔Yes | ✔Yes | ✗No  | ✔Yes | ✔Yes | ✗No  | ✗No  | ✗No  |
| Kerning information    | ✔Yes | ✔Yes | ✔Yes | ✔Yes | ✔Yes | ✗No  | ✗No  | ✗No  |
| Kerning groups         | ✔Yes | ✗No  | ✗No  | ✔Yes | ✗No  | ✗No  | ✗No  | ✗No  |
| Glyph Compression      | ✔Yes | ✗No  | ✗No  | ✗No  | ✗No  | ✗No  | ✗No  | ✔Yes |
| CLI converters         | ✔Yes | ✔Yes | ✗No  | ✗No  | ✔Yes | ✔Yes | ✔Yes | ✔Yes |
| Requires SO library(1) | ✗No  | ✗No  | ✔Yes | ✔Yes | ✔Yes | ✗No  | ✗No  | ✗No  |
| Autohinting grid       | ✔Yes | ✗No  | ✗No  | ✗No  | ✗No  | ✗No  | ✗No  | ✗No  |
| Hinting interpreter(2) | ✗No  | ✗No  | ✔Yes | ✔Yes | ✗No  | ✗No  | ✗No  | ✗No  |
| Codepage nightmare(3)  | ✗No  | ✗No  | ✔Yes | ✔Yes | ✔Yes | ✔Yes | ✔Yes | ✗No  |

Note(1): SSFN has a very small, simple to use renderer in an ANSI C header file. Others need the
freetype2 library, but PSF2 and BDF are simple enough to parse them manually.

Note(2): Not needing a bytecode interpreter in fonts for [hinting](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/hinting.md) is a *very good* thing.

Note(3): SSFN excusively uses UNICODE ISO-10646 standard as it's only character mapping (which is
usually encoded with UTF-8).

Legend:
 - SSFN: [Scalable Screen Font Binary Format](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/sfn_format.md)
 - ASC: [Scalable Screen Font ASCII Format](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/sfn_format.md)
 - TTF: TrueType Font
 - OTF: OpenType Font
 - PST1: PostScript Type 1
 - PSF2: PC Screen Format with UNICODE table (Linux Console)
 - BDF: X11 Bitmap Distribution Format
 - TGA: Truevision TARGA, used by SSFN to import pixel fonts

Compression Ratio
-----------------

### Vector Glyphs

Because vector font formats are just crazy, you can easily gain 40% - 60% extra space by
[converting them into SSFN](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/ecosystem.md). The
compression is based on the intimate knowledge of the glyphs and geometrical transformations, not on
byte occurances, therefore SSFN fonts can be further compressed with Huffmann and LZW efficiently. You
can set the quality of the compression too, smaller values mean more information loss and smaller files,
higher values more precise grid and bigger file size. The quality 6 defines the same grid precision as
a usual OTF, meaning there will be no information loss.

The FreeSerif font with a decent Basic Multilingual Plane coverage from the GNU freefonts for example:

| Format | Quality | Size   | Gzipped | Grid size   | Compression |
| ------ | ------: | -----: | ------: | ----------: | ----------: |
| .otf   |       - |  2.0 M |   1.3 M | 1000 x 1000 |          0% |
| .sfn   |       6 |  1.1 M |   762 K | 1024 x 1024 |      -45.8% |
| .sfn   |       3 |  831 K |   492 K |  128 x  128 |      -59.4% |

For most vector fonts, quality 4 (256 x 256 grid) is perfectly sufficient. Some simpler fonts can be
converted at quality 3 without noticable difference, but most fonts loose some of their typeface detail
at quality 3. Lower qualities require manually grid fitted glyphs to keep font readability (but possible
if extremely small font sizes are required).

<img src="https://gitlab.com/bztsrc/scalable-font/raw/master/docs/quality.png">

### Bitmap Glyphs

For bitmap fonts, the quality only specifies the grid size for the largest glyph in the font and not the
precision (as bitmaps have no such thing). So compression depends mostly on the glyph bitmaps, but there's
a good chance you can compress those too. In some cases SSFN bitmap fonts can be bigger than the PSF2
counterparts by ca. +15% (specially for small fonts), but let's note that PSF2 can't contain half or double
sized glyphs within the same file, not to mention the lack of composite glyph support.

GNU unifont plane 0 (Basic Multilingual Plane) in different formats for example:

| Format  | Size   | Gzipped | Comment |
| ------- | -----: | ------: | ------- |
| .bdf    |  9.3 M |   1.3 M | Plain text data with glyph meta info |
| .pcf    |  5.3 M |   1.3 M | X11 binary font format, with proper glyph meta info |
| .hex    |  2.3 M |   615 K | Plain text data, implicit meta info, you have to measure the length of lines |
| .bmp    |  2.1 M |   862 K | Image file, not a font, no per glyph info at all |
| .psf(1) |  1.2 M |   536 K | Console font, small, but no per glyph meta info |
| .sfn    |  1.2 M |   536 K | The overall winner, the smallest size with all the meta info |

Note(1): .psfu files can't store glyphs with different widths, so I've converted unifont into two .psfu files
(one for the 8x16 glyphs and one for the 16x16 glyphs) then concatenated them together for this measurement.

### Pixmap Glyphs

Same as bitmaps, all redundant blocks removed, plus fragments are RLE compressed.

