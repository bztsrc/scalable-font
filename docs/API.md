Scalable Screen Font Renderer API
=================================

Font Metrics
------------

Before you can use any font renderer, you must be familiar with its font metrics:

```
   (0,0)................._______
        .................      ^
     ___.................      | bearing top
     ^  ,,,,,,,,,,,,,,,,,______v
     |  ....:.XXX.XX:....  ^ ^ ^
size |  ....:XX..XX.:....  | | |
     |  ....:XX..XX.:....  | | | horizontal baseline
     |  ....:XX..XX.:....  | | |
     |  ....:XX..XX.:....  | | |
     |  ....:XX..XX.:....  | | |
     v__,,,,;.XXXXX,;,,,,__|_|_v
        ....:....XX.:....  | |
        ....|XX..XX.:....  | | glyph height
        ....:.XXXX..:....__|_v
        ....:...:...:....  |
        ....:...:...:....  | advance y
        ....:...:...:....__v
        |   |   |   | |  (font width, font height)
        |   |   |   | |
        |   |<->|   | |  vertical baseline (center of the glyph, always width / 2)
        |   |<----->| |  glyph width
        |   |<------->|  advance x
        |   |<---->|     advance x with kerning x
        |<->|            bearing left
```

Glyphs must be aligned on `baseline`. When displaying, horizontal baseline must be
substracted from current pen y position, or when advance y is not zero, then vertical
baseline must be substracted from pen x (see Text Directions below).

The `advance` tells how much you should move your pen (cursor pointer) after the glyph is
drawn on screen (usually not the same as font width). It typically equals to width plus a
little, or in case of vertical letters (when advance y is not zero), height plus a little.

`Kerning` is very similar to advance, but it tells how much you should move your cursor
if the next character is a particular one. For example the advance is different for 'f'
if it's followed by 'M' or an 'i'. The advance is bigger for 'fM' and smaller for 'fi'.

`Size` is how SSFN specifies a glyph's size. It's from the bearing top minimum of all
glyphs to the horizontal baseline. This in unortodox, but guarantees that you get
approximately similar sized glyphs regardless to font metrics and quality (I hate when
I get totally different pixel sizes for different TTF fonts when I ask for the same size).
Passing `SSFN_STYLE_ABS_SIZE` will avoid this scheme, and will scale the glyph's height
to size as usual in other renderers.

Text Directions (Vertical and Right-to-Left Texts)
--------------------------------------------------

Depending on the text direction, you'll have to apply font metrics differently. Vertical
direction is stored within the font, but for right-to-left you'll have to implement
[bidirectional text algorithm](http://www.unicode.org/reports/tr9) as specified by UNICODE.
The BiDi state machine is not part of the SSFN renderer, because this is a low level font
rasterizer.

The logic is as follows:
1. you have to take `baseline` into consideration by substracting from Y or X
2. for right-to-left, you have to substract `glyph width` from X
3. draw the glyph at X, Y.
4. for right-to-left, subtract `advance x` from X, otherwise add to it.
5. add `advance y` to Y.

```c
if(glyph->adv_y) {
    x = pen_x - glyph->baseline;
    y = pen_y;
} else {
    x = pen_x - (bidi_state_machine.rtl ? glyph->w : 0);
    y = pen_y - glyph->baseline;
}

my_draw_glyph(x, y, ...);

if(bidi_state_machine.rtl)
    pen_x -= glyph->adv_x;
else
    pen_x += glyph->adv_x;
pen_y += glyph->adv_y;
```

Usage
-----

Because the renderer is a single ANSI C header file, no shared library needed. It depends
only on libc memory allocation, nothing else. You can configure the renderers by defining
special defines.

```c
/* configure the renderer here with defines */
#include <ssfn.h>
```

Once you have the include, you can start using the library, no need for initialization, but you must
make sure that the initial context is zerod out (by putting it in the bss segment it will be, or call
memset zero manually).

As my favourite principle is K.I.S.S., there's only a few, clearly named functions in the API:
 - load one or more fonts into the context using `ssfn_load()` on program start.
 - set up rendering configuration by specifing font family, style, size and rendering mode with `ssfn_select()`.
 - if you want to change the size for example, you can call `ssfn_select()` again, no need to load the fonts again.
 - if you need it, you can get the rendered text's dimensions in advance with `ssfn_bbox()`.
 - use `ssfn_utf8()` to decode an UTF-8 multibyte character into UNICODE code point.
 - optionally select a glyph variant with `ssfn_variant()`.
 - call `ssfn_render()` to rasterize a glyph in the given style and size for that UNICODE code point.
 - draw the returned bitmap on your screen at cursor position.
 - optionally alter the returned advance offsets with `ssfn_kern()`.
 - move the cursor by the returned advance offsets.
 - repeat the last 6 steps until you reach the end of the UTF-8 string.
 - when not needed any more, free the rasterized glyphs
 - when done with rendering, call `ssfn_free()`.

Configuration
-------------

```c
#define SSFN_NOIMPLEMENTATION
```

Do not include the normal renderer implementation, only the file format defines and function prototypes.

```c
#define SSFN_CONSOLEBITMAP_PALETTE
#define SSFN_CONSOLEBITMAP_HICOLOR
#define SSFN_CONSOLEBITMAP_TRUECOLOR
```

Only one of these can be specified at once. These include the simple renderer which is totally independent
from the rest of the API. It is a very specialized renderer, limited and optimized for OS kernel consoles.
It renderers directly to the framebuffer, and you specify the framebuffer's pixel format with one of these.

```c
#define SSFN_CONSOLEBITMAP_CLEARBG
```

Normally the simple renderer displays only font face, leaving the background untouched. With this define
you can tell it to cover the back of the glyph with a given color, hence overlapping console messages
won't mess up the screen.

Variable Types
--------------

| typedef        | Description |
| -------------- | ----------- |
| `ssfn_t`       | the renderer context, it's internals are irrelevant |
| `ssfn_font_t`  | the font stucture, same as the [SSFN file header](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/sfn_format.md) |
| `ssfn_glyph_t` | the returned rasterized glyph (see `ssfn_render()`) |
| `int`          | the returned error code (if any) |

Error Codes
-----------

Most functions return an integer error code. If not, then you can query the last error code with `ssfn_lasterr(ctx)`.
To get the human readable string representation of the error code in English, use `ssfn_error(errcode)`.

| Error               | Numeric | Description |
| ------------------- | ------: | ----------- |
| `SSFN_OK`           |       0 | success |
| `SSFN_ERR_ALLOC`    |       1 | memory allocation error |
| `SSFN_ERR_NOFACE`   |       2 | no font face selected, call `ssfn_select()` first |
| `SSFN_ERR_INVINP`   |       3 | invalid input parameter (usually a NULL pointer) |
| `SSFN_ERR_BADFILE`  |       4 | bad SSFN file format |
| `SSFN_ERR_BADSTYLE` |       5 | unsupported style |
| `SSFN_ERR_BADSIZE`  |       6 | unsupported size |
| `SSFN_ERR_BADMODE`  |       7 | unsupported mode |
| `SSFN_ERR_NOGLYPH`  |       8 | glyph (or it's kerning info) not found |

Normal Renderer Functions
-------------------------

The following functions are only available if the header is included without `SSFN_NOIMPLEMENTATION` define.
They allocate memory and therefore depend on libc (or crafted compilers like gcc have all four functions
as built-ins), hence are expected to be used by normal user space applications. If you really want to use
this renderer from a kernel, you can replace all dependent functions with your own implemenations using the
following defines:

```c
#define SSFN_memcmp memcmp
#define SSFN_memset memset
#define SSFN_realloc realloc
#define SSFN_free free
```

If you don't specify these, then the SSFN header will try to figure it out if they are supported as built-ins
or provided by libc.

That's all, no more dependencies. :-)

## Load Fonts

```c
int ssfn_load(ssfn_t *ctx, ssfn_font_t *font);
```

Loads a font or font collection into the renderer context.

### Parameters

| Parameter | Description |
| --------- | ----------- |
| ctx       | pointer to the renderer's context |
| font      | pointer to an in memory font |

You can load an SSFN file and pass it's address, or you can also use `ld -b binary` to convert an SSFN file
into an object and link that with your code. In this case you'll have a `_binary_(filename)_start` label.

You can also pass an SSFN font collection to this function, in which case all fonts within the collection
will be loaded into the context at once.

### Return value

Error code. `SSFN_ERR_BADFILE` means bad (incorrect or inconsistent) SSFN format. Hint: use `sfn2asc -d` to
debug what's wrong with it.

## Select Face

```c
int ssfn_select(ssfn_t *ctx, int family, char *name, int style, int size, int mode);
```

Sets up renderer context to use a particular font family, style, size and rendering mode.

In `SSFN_MODE_OUTLINE`, the font's grid is scaled to `size`. In other modes the difference
of bounding box and the baseline is scaled. In other words, if you set `size` to 32, then
you'll get an exatly 32 pixel tall 'A', see Font Metrics above. Passing `SSFN_STYLE_ABS_SIZE`
will scale the glyph's height to `size`. This is also the default for monospace fonts.

### Parameters

| Parameter | Description |
| --------- | ----------- |
| ctx       | pointer to the renderer's context |
| family    | font family group, see below |
| name      | pointer to an UTF-8 string with font's unique name if family is `SSFN_FAMILY_BYNAME` |
| style     | one or more font style defines OR'd together, see below |
| size      | rendered font's size in pixels, from 8 to 255 (sizes smaller than 16 are not readable very much) |
| mode      | rendering mode, see below |

Parameter defines:

| family                  | Description |
| ----------------------- | ----------- |
| `SSFN_FAMILY_SERIF`     | selects Serif fonts (letters with "feet", like Times New Roman) |
| `SSFN_FAMILY_SANS`      | selects Sans Serif fonts (letters without "feet", like Helvetica) |
| `SSFN_FAMILY_DECOR`     | selects Decorative fonts (like Dingbats or Fraktur) |
| `SSFN_FAMILY_MONOSPACE` | selects Monospace fonts (like Fixed or GNU unifont) |
| `SSFN_FAMILY_HAND`      | selects Handwriting fonts (like Cursive) |
| `SSFN_FAMILY_ANY`       | don't care, pick the first font which has the glyph |
| `SSFN_FAMILY_BYNAME`    | use the `name` field to select precisely one font |

| style                   | Description |
| ----------------------- | ----------- |
| `SSFN_STYLE_REGULAR`    | regular style |
| `SSFN_STYLE_BOLD`       | bold weight. If no font found, the renderer will mimic bold |
| `SSFN_STYLE_ITALIC`     | italic or oblique slant. If no font found, the renderer will transform glyphs to make them italic |
| `SSFN_STYLE_UNDERLINE`  | under line glyphs. This is not stored in fonts, always generated |
| `SSFN_STYLE_STHROUGH`   | strike-through glyphs. Not stored either, always generated |
| `SSFN_STYLE_NOHINTING`  | don't use hinting grid, outline will be blurry but typeface correct |
| `SSFN_STYLE_ABS_SIZE`   | use absolute size (glyph's total height will be scaled to size) |

| [mode](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/modes.md) | Description |
| ----------------------- | ----------- |
| `SSFN_MODE_NONE`        | do not render the glyph, just select the font (see kerning and bounding box below) |
| `SSFN_MODE_OUTLINE`     | return the glyph's outline |
| `SSFN_MODE_BITMAP`      | rasterize glyph as bitmap, 1 bit for each pixel |
| `SSFN_MODE_ALPHA`       | rasterize glyph as an alpha channel, 1 byte for each pixel |
| `SSFN_MODE_CMAP`        | rasterize in color map mode, 1 byte for each pixel (see `SSFN_CMAP_TO_ARGB()` macro) |

### Return value

Error code. `SSFN_ERR_NOFACE` returned if no font could be found, otherwise `SSFN_ERR_BADx` refers to
the invalid argument. This function always resets glyph variant to `SSFN_VARIANT_DEFAULT`.

## Select Glyph Variant

```c
int ssfn_variant(ssfn_t *ctx, int variant);
```

This is optional, and only used in special cases. A UNICODE character may have several variants
depending on context or localization, you can choose which one to render with this function. If
a specific variant is not found, then the renderer fallbacks to `SSFN_VARIANT_DEFAULT`.

The contextual version variants are currently unused and no SSFN fonts are generated with such
variants because UNICODE provides different code points for those glyphs. This feature can be used
by special applications if needed. There's no isolated variant because default should contain those
in the first place.

Also note hese can be used for localization too. Currently there's only one such case, the
`SSFN_VARIANT_INITIAL` (or `SSFN_VARIANT_LOCAL1`) is used to store Serbian/Macedonian
B D G P T letter variations.

### Parameters

| Parameter | Description |
| --------- | ----------- |
| ctx       | pointer to the renderer's context |
| variant   | one of the variant defines, see below |

Parameter defines:

| variant                 | Description |
| ----------------------- | ----------- |
| `SSFN_VARIANT_DEFAULT`  | selects the default (isolated) glyph variant |
| `SSFN_VARIANT_LOCAL0`   | same as default |
| `SSFN_VARIANT_INITIAL`  | selects the version when the glyph is at the beginning |
| `SSFN_VARIANT_LOCAL1`   | same as initial |
| `SSFN_VARIANT_MEDIAL`   | selects the version when the glyph is in the middle |
| `SSFN_VARIANT_LOCAL2`   | same as medial |
| `SSFN_VARIANT_FINAL`    | selects the version when the glyph is at the end |
| `SSFN_VARIANT_LOCAL3`   | same as final |
| `SSFN_VARIANT_LOCAL4`   | unused for now |
| `SSFN_VARIANT_LOCAL5`   | unused for now |
| `SSFN_VARIANT_LOCAL6`   | unused for now |

### Return value

Error code. `SSFN_ERR_INVINP` returned if wrong parameters supplied, otherwise `SSFN_OK`.

## Convert UTF-8 to UNICODE

```c
uint32_t ssfn_utf8(char **str);
```

Decodes an UTF-8 multibyte character. Be careful, this function does not check its input, expects that pointer is
valid and points to a string with only valid UTF-8 sequences. The behaviour with invalid input is undefined. All the
other functions check for valid input, this is an exception because it's expected to be called repeatedly many times.

This function does not take code point combinations into consideration either. That's the caller responsibility
to generate and use the proper glyphs for UNICODE Variation Selectors and other special cases (see `ssfn_variant()`).
For Variation Selectors I would rather recommend to use separate fonts, one for each VSx, and render the normal UNICODE
code point with those special fonts.

### Parameters

| Parameter | Description |
| --------- | ----------- |
| str       | pointer to a UTF-8 string pointer |

### Return value

UNICODE code point, and `str` pointer adjusted to the next multibyte sequence.

## Render a Glyph

```c
ssfn_glyph_t *ssfn_render(ssfn_t *ctx, uint32_t unicode);
```

Render a glyph. It is possible to load more fonts with different UNICODE code range coverage, the
renderer will pick the ones which have a glyph defined for `unicode`. If more fonts have glyphs for this
character, then the renderer will look for the best variant and style match to figure out which font to
use, unless you've asked for a specific font with `SSFN_FAMILY_BYNAME`. If there's no font for the
requested style, then the renderer will mimic bold or italic.

If you have called `ssfn_variant()` before this call, then glyph variant will also participate in
font selection, and takes precedence over style.

### Parameters

| Parameter   | Description |
| ----------- | ----------- |
| ctx         | pointer to the renderer's context |
| unicode     | UNICODE code point of the character to be rendered (see `ssfn_utf8()`) |

### Return value

Returns `NULL` on error (and you can get the error code with `ssfn_lasterr(ctx)`. On success, it returns a newly
allocated `ssfn_glyph_t` structure.

| ssfn_glyph_t | Description |
| ------------ | ----------- |
| mode         | equals to the selected mode, specifies the format of the data buffer |
| baseline     | the baseline of the glyph in pixels (1) |
| w            | width of the rendered glyph in pixels |
| h            | height of the rendered glyph in pixels |
| adv_x        | advance x, you should add this to your cursor position after draw |
| adv_y        | advance y |
| pitch        | number of bytes per lines in the data buffer, or number of outline coordinates |
| cmap         | if mode `SSFN_MODE_CMAP`, then pointer to a color map, otherwise `NULL` |
| data         | rendered glyph outline / bitmap / pixmap data, it's `pitch` times `h` bytes |

Note(1): if advance y is not zero, then it's the vertical baseline, otherwise it's the horizontal.

If mode is `SSFN_MODE_NONE`, then it will not render anything, it just selects the font with the best match
and returns NULL. This mode is useful to select a font for kerning (see `ssfn_kern()` and `ssfn_bbox()`).

Otherwise  you should iterate on the `data` array `h` times, starting at offset 0 and adding `pitch` to the
pointer for every row. Within a row, you should extract `w` pixels for each row. The description of how to
decode the data can be found in [modes.md](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/modes.md).

I suggest to store the returned glyph in a cache. Once you've done with it, you can free the glyph with a
standard libc `free()` call, no special treatment needed.

## Kerning

```c
int ssfn_kern(ssfn_t *ctx, uint32_t unicode, uint32_t nextunicode, int *x, int *y);
```

Apply the relative advance modifier for two UNICODE code points. This will use the kerning info in
the font that was last used by `ssfn_render`, meaning you have to render at least one glyph apriori
to get valid results. For this purpose, you can pass `SSFN_MODE_NONE` to the renderer (or use
`SSFN_FAMILY_BYNAME` to select exactly one font, and then you don't have to call ssfn_render). If
the character or the kerning pair not found, `x`and `y` will be left unchanged.

If you have use `ssfn_variant()` before rendering, then the same kerning modifier will be applied to
the advance value of the given variant.

### Parameters

| Parameter   | Description |
| ----------- | ----------- |
| ctx         | pointer to the renderer's context |
| unicode     | UNICODE code point of the current character (on the left or above) |
| nextunicode | UNICODE code point of the next character (on the right or below) |
| x           | pointer to a signed integer, relative coordinate in pixels |
| y           | pointer to a singed integer, relative coordinate in pixels |

### Return value

Error code, and relative kerning offsets adjusted to `x` and `y`.

## Get Bounding Box

```c
int ssfn_bbox(ssfn_t *ctx, char *str, int usekern, int *w, int *h);
```

Returns the dimensions of a rendered text. This function handles horizontal and
vertical texts, but not mixed ones. It does not render the glyphs, and it does not
allocate extra memory either. It does calculate autohinting though, so if you have
cached the glyphs then it's much faster to sum glyph->adv_x and glyph->adv_y values
instead.

### Parameters

| Parameter   | Description |
| ----------- | ----------- |
| ctx         | pointer to the renderer's context |
| str         | pointer to an UTF-8 string |
| usekern     | set it to non-zero if you want to apply kerning when calculating sizes |
| w           | pointer to an integer, returned width in pixels |
| h           | pointer to an integer, returned height in pixels |

### Return value

Error code, and on success the bounding box size in `w`, `h`.

## Get Memory Usage

```c
int ssfn_mem(ssfn_t *ctx);
```

Returns how much memory a particular renderer context consumes. It is typically less than 32k, but strongly depends
on how complex a font is (how many contour commands it has) and how big glyphs you're rendering. When the renderer
is rasterizing a new character which requires less memory than the previous one, then no memory allocation will
take place, the existing buffers will be reused. Internal buffers can be freed with `ssfn_free()`.

The contour coordinates are internally stored on 2 bytes, with 4096 x 4096 grid system and 0 - 15 precision
(12 + 4 bits). Rasterization and hinting also use 2 bytes per pixel, but in a 256 x 256 grid and 0 - 255
precision (8 + 8 bits), and there's a separate raster line for each row.

### Parameters

| Parameter   | Description |
| ----------- | ----------- |
| ctx         | pointer to the renderer's context |

### Return value

Total memory consumed in bytes.

## Free Memory

```c
void ssfn_free(ssfn_t *ctx);
```

Destructor of the renderer context. Frees all internal buffers and clears the context.

### Parameters

| Parameter | Description |
| --------- | ----------- |
| ctx       | pointer to the renderer's context |

### Return value

None.

## Error Handling

```c
int ssfn_lasterr(ssfn_t *ctx)
```

Returns the error code for the last error occured. Passing a NULL to this function results in
an undefined behaviour, because it's implemented as a macro.

### Parameters

| Parameter | Description |
| --------- | ----------- |
| ctx       | pointer to the renderer's context |

### Return value

The error code or `SSFN_OK` if there was no error.


```c
const char *ssfn_error(int errcode)
```

Returns the human readable string equivalent of an error code in English. Also implemented as a macro.

### Parameters

| Parameter | Description |
| --------- | ----------- |
| errcode   | error code  |

### Return value

Pointer to a string constant. `SSFN_OK` returns an empty string, not NULL nor "Success".


Simple Renderer Function
------------------------

The following function is only available if the header is included with one of the `SSFN_CONSOLEBITMAP_x` defines
(independently to `SSFN_NOIMPLEMENTATION`).

This is a very specialized renderer that renders directly to the screen and does not use ssfn_t context. It operates
on one font only, can't scale or dynamically style the glyph, and it can only handle bitmap fonts. It doesn't care
about baseline, but it handles advances. For these limitations in return it does not allocate memory at all, has
exactly zero dependency and compiles to about 1 kilobyte.

## Render a Glyph

```c
int ssfn_putc(uint32_t unicode);
```

Render a character to screen. Unlike the functions in the normal renderer, this function does not check its input,
passing invalid parameters results in an undefined behaviour. This is as simple as it gets.

### Parameters

| Parameter | Description |
| --------- | ----------- |
| unicode   | UNICODE code point to render |

Configuration is passed to it in global variables (exclusive to this function, the normal renderer does not use them):

| Global Variable            | Description |
| -------------------------- | ----------- |
| `ssfn_font_t *ssfn_font`   | pointer to an SSFN font with bitmap glyphs only |
| `uint8_t *ssfn_dst_ptr`    | the destination buffer's address (probably the linear frame buffer, but could be off-screen) |
| `uint32_t ssfn_dst_pitch`  | the destination buffer's bytes per lines |
| `uint32_t ssfn_dst_w`      | the desitnation buffer's width in pixels (optional) |
| `uint32_t ssfn_dst_h`      | the desitnation buffer's height in pixels (optional) |
| `uint32_t ssfn_fg`         | the foreground color in destination buffer's native format |
| `uint32_t ssfn_bg`         | the background color (only used with `SSFN_CONSOLEBITMAP_CLEARBG`) |
| `uint32_t ssfn_x`          | the coordinate to draw to, will be modified by glyph's advance values |
| `uint32_t ssfn_y`          | the coordinate to draw to |
| `uint32_t ssfn_adv_x`      | the glyph's advance value, returned after rendering |
| `uint32_t ssfn_adv_y`      | the glyph's advance value |

The define selects the destination buffer's pixel format. `SSFN_CONSOLEBITMAP_PALETTE` selects 1 byte
(indexed), `SSFN_CONSOLEBITMAP_HICOLOR` selects 2 bytes (5-5-5 or 5-6-5 RGB) and `SSFN_CONSOLEBITMAP_TRUECOLOR`
selects 4 bytes (8-8-8-8 xRGB). For performance reasons, 3 bytes (24 bit true color) mode is not supported.

The `SSFN_CONSOLEBITMAP_CLEARBG` can be added to any of the above defines. That will turn transparency off, and
will fill the glyph's background with `ssfn_bg`.

### Return value

Either `SSFN_OK`, `SSFN_ERR_NOGLYPH` or `SSFN_ERR_INVINP` (when the character would be displayed out of screen). On
success it modifies `ssfn_dst_x`, `ssfn_dst_y`, `ssfn_adv_x` and `ssfn_adv_y`.

