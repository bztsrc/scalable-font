Scalable Screen Font File Format
================================

- File extension: `.sfn`
- Mime type: `font/x-ssfont`

(This spec is about the binary format. See [asc_format.md](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/asc_format.md)
for the human readable ASCII version, which is the "source file" format. This is
about the "compiled file" format, created by [converters](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/ecosystem.md).)

NOTE: if you want to write your own SSFN file reader / writer, then `sfn2asc -d` can
dump the SSFN binary for you, checking for inconsistencies and displaying details.

As this font format was primarily designed for screen, it differs from TrueType and
OpenType fonts in many aspects. First, it uses pixels, not points (same as when TTF
used with 72 DPI). Second, it allows smaller EM sizes (16 - 4096 points) and third,
it uses screen based Y coordinate direction: the points are stored as (0,0) is in
the upper left corner of the grid. Unlike the others, SSFN does not allow glyphs to
reach over the grid's edges, all bounding boxes and control points must fit into the
grid. So when you render a character into a (let's say) 64 x 64 bitmap, you can be
sure that the whole glyph is in it, no parts are missing. Also unlike the others,
SSFN fonts can store bitmap and pixel fonts too (even mixed with vector ones, let's
say every character has a vector contour, except for the emojis which have a colored
pixmap.)

SSFN format does not utilize flexible layout, instead
[it focuses on the smallest common dominator among font formats](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/compare.md),
and stores only the minimal required information. As such, it does not use byte
code interpreter for hinting, it relies on [autohinting](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/hinting.md)
grid (which is not as good as professional hinting code, but most fonts has crap
or mediocre hinting instructions at best anyway). Hinting grid requires lot less
bytes to store, easier to interpret by a magnitude, and it can produce very good
results.

SSFN can store everything that a TrueType or OpenType font can, except for:
 - charset encodings,
 - expressed ligatures.

As for character encoding, SSFN exclusively uses UNICODE (ISO-10464) by design. For
ligatures, I suggest to use the proper UNICODE code point for the ligature (which can
be a combined glyph), or a sequence of more code points where only the last glyph has
an advance.

The format was designed with small file size and efficient info access in mind. As
such the format specification has many IF-x-THEN conditions, because it uses
different integer sizes depending on the number of fragments, characters, kerning
groups and the grid size of course.

Unlike other scalable font formats, SSFN does not store glyphs directly. Instead it
uses a geometrical compression algorithm for sub-glyph fragments, therefore the same
font stored with the same quality (1024 EM) takes only about 40% - 60% of the file
size in SSFN format than it takes in TTF or OTF. Even better compression ratios can
be achieved when you lower the quality (which is still indistinguishable from higher
quality on screen). Please note that lower quality does not necessarily mean ugly
glyphs, as the lines and curves are still calculated with maximum precision. It only
means that the points are fitted to a less precise grid system, and with less than
128 pixel rendered bitmaps you probably will never notice the difference (which is
the most common case on screens, btw).

The basic structure of the format is as follows:

| Block      | Description |
| ---------- | ----------- |
| Header     | A fixed sized struct starting with 'SSFN' magic and basic font information |
| Strings    | Human readable UTF-8 strings, like font's name and license |
| Fragments  | Sub-glyph information table |
| Characters | UNICODE-glyph mappings |
| Kerning    | Kerning table (character combination distances, optional) |
| Colormap   | Palette for color commands and pixmap fragments (optional) |
| End magic  | Terminating 'NFSS' magic bytes |

The description of the detailed on disk format follows.

Header
------

If the file starts with the bytes 0x1f and 0x8b, then you have to inflate it first
using a gzip decompression filter. For command line, use `gzip -d`.

An SSFN file can contain more fonts. In this case the file starts with the header

| Offset | Length | Description |
| -----: | -----: | ----------- |
|      0 |      4 | magic, 'SFNC' |
|      4 |      4 | size of the font collection in bytes |

And concatenated fonts in SSFN format follows (with magic 'SSFN', see the header bellow).
The advantage is you can load all fonts at once with `ssfn_load()` (see
[API](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/API.md)).

A font collection does not have a separate extension or mime type, it is interpreted
within the context of the SSFN format. If compression filter is used, then it is applied
to the whole collection, and not on individual fonts.

The uncompressed file's header is the same as `ssfn_font_t` struct, see the
[API](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/API.md).

| Offset | Length | Description |
| -----: | -----: | ----------- |
|      0 |      4 | magic, 'SSFN' |
|      4 |      4 | size of the font in bytes |
|      8 |      1 | font family (0-Serif, 1-Sans, 2-Decorative, 3-Monospace, 4-Handwriting) |
|      9 |      1 | font style (bit 0: bold, bit 1: italic (1) |
|     10 |      1 | grid quality, 0-6 (7 and 8 are also possible, but not implemented as of yet) |
|     11 |      1 | features (2) |
|     12 |      1 | file format revision (3) |
|     13 |      3 | reserved for future use, must be zero |
|     16 |      2 | horizontal baseline in grid pixels |
|     18 |      2 | underline position in grid pixels |
|     20 |      2 | bounding box left (4) |
|     22 |      2 | bounding box top |
|     24 |      2 | bounding box right |
|     26 |      2 | bounding box bottom |
|     28 |      4 | fragments table offset (relative to magic) |
|     32 |      4 | character table offset (glyph variant 0, default also isolated) (5) |
|     36 |      4 | character table offset (glyph variant 1, initial) |
|     40 |      4 | character table offset (glyph variant 2, medial) |
|     44 |      4 | character table offset (glyph variant 3, final) |
|     48 |     12 | character table offset (glyph variants 4-6, must be zero for now) |
|     60 |      4 | kerning table offset |

Note(1): style bits 2 (underline) and 3 (strike-through) are never stored in the file, only used by the API.

Note(2): feature bits are:

| BitOffset | Description |
| --------: | ----------- |
|         0 | font has at least one bitmap fragment |
|         1 | font has at least one pixmap fragment or one color command, therefore a color map |
|         2 | font has at least one hinting fragment |
|         3 | kerning look up table has large offsets |
|         4 | kerning group table has large code points |
|         5 | kerning group table has large coordinates |
|         6 | autohinting grid has large coordinates |

Note(3): For future use, this specification is for the original format, revision 0.

Note(4): bounding box defines the biggest box that includes every points for all the glyphs.

Note(5): only the first character table (for the default glyph variants) is mandatory, the others can
be zero. Variant 1 is also used for Serbian/Macedonian Cyrillic letters, but otherwise this feature
is not utilized as contextual glyphs have their own UNICODE code points and they are stored as default
variant.

String Table
------------

Each string is a zero terminated UTF-8 string, up to 255 bytes without control characters (less than
32). If one key is not defined, then it's coded as an empty string (a single zero byte). If there's
no human readable information for the font at all, then this block is coded as 6 zero bytes.

| String | Description |
| -----: | ----------- |
|    1st | Unique name of the font |
|    2nd | Font family name |
|    3rd | Font sub-family name |
|    4th | Version or revision information of the font |
|    5th | Manufacturer or designer's name, could be an URL to their website |
|    6th | License and copyright information, could be an URL to the license |

Fragments Table
---------------

SSFN does not store glyphs directly. Instead it has fragments which are contours or
bitmap blocks with normalized coordinates. A character glyph is constructed by one
or more transformed fragments. Usually many glyphs share one or more fragments.

A fragment starts with a header, which most significant 2 - 4 bits describe it's type.
The fragment descriptors in characters table refer to these headers by their relative
offsets to the magic for fast access (see section Characters Table below).

| 1st byte | Description |
| -------- | ----------- |
| 0xxxxxxx | Contour |
| 10xxxxxx | Bitmap |
| 110xxxxx | Pixel map |
| 111xxxxx | Hinting information |

### Contour Fragment

| 1st byte | More bytes | Description |
| -------- | ---------: | ----------- |
| 00nnnnnn | 0          | Contour, up to 64 elements |
| 01NNNNNN | 1          | Contour, number of elements: (N << 8 + additional byte) + 1 |

Contour is described by a path, which consist of N+1 elements. There are six possible path
commands (move to, line to, quadratic Bezier curve to, cubic Bezier curve to, set color,
set gradient), but how they are stored with their point arguments heavily depend on the
font's quality. The "move to" and "line to" commands are encoded in 2 to 3 bytes, curves in
4 to 8 bytes, color and gradient 2 to 5 bytes. (Grid sizes 2048 and above are not implemented
yet, and probably never will be. 1024 x 1024 should be enough for a single screen letter, as a
matter of fact most fonts can be stored on 256 x 256 grid without loosing any typeface detail.)

A path always starts with a "move to" command, which is not included in the number of commands,
and that's the only "move to" command the path can have. In other words a "move to" always defines
a new path. So the contour fragment's header is followed by two coordinates:

| Quality | Grid size | Path command   | Bytes | Command format |
| ------: | --------: | -------------- | ----: | -------------- |
|     0-4 |       256 | move to        |     2 | xxxxxxxx yyyyyyyy |
|     5-8 |      4096 | move to        |     3 | YYYYXXXX xxxxxxxx yyyyyyyy |

If the path has a "color" or "gradient" command, it must come right after the "move to", and there
can be only one of them per path. The color command selects an ARGB from the color map. With gradients,
four colors are selected, which represents the four corners of the fragment's bounding box, c=top left,
d=top right, e=bottom left and f=bottom right. The glyph must be rasterized with gradients between
those. Color indeces above 239 selects the foreground color (see Pixmap fragment and Color Map).

| Quality | Grid size | Path command   | Bytes | Command format |
| ------: | --------: | -------------- | ----: | -------------- |
|     0-3 |       128 | color          |     2 | 0000000C 0ccccccc (where (uppercase<<7 + lowercase) gives the color index) |
|     0-3 |       128 | color gradient |     5 | 0000010C 0ccccccc dddddddd eeeeeeee ffffffff |
|     4-8 |      4096 | color          |     2 | 00000000 cccccccc |
|     4-8 |      4096 | color gradient |     5 | 00000100 cccccccc dddddddd eeeeeeee ffffffff |

After that other commands follow, depending on quality:

| Quality | Grid size | Path command   | Bytes | Command format |
| ------: | --------: | -------------- | ----: | -------------- |
|     0-3 |       128 | line to        |     2 | 1xxxxxxx 0yyyyyyy |
|     0-3 |       128 | quad curve to  |     4 | 0xxxxxxx 1yyyyyyy 0aaaaaaa 0bbbbbbb (where (a,b) is the control point) |
|     0-3 |       128 | cubic curve to |     6 | 1xxxxxxx 1yyyyyyy 0aaaaaaa 0bbbbbbb 0ccccccc 0ddddddd (where c1=(a,b), c2=(c,d)) |
|       - |         - | -              |     - | - |
|       4 |       256 | line to        |     3 | 00000001 xxxxxxxx yyyyyyyy |
|       4 |       256 | quad curve to  |     4 | 00000010 xxxxxxxx yyyyyyyy aaaaaaaa bbbbbbbb |
|       4 |       256 | cubic curve to |     6 | 00000011 xxxxxxxx yyyyyyyy aaaaaaaa bbbbbbbb cccccccc dddddddd |
|       - |         - | -              |     - | - |
|       5 |       512 | line to        |     3 | 0000YX01 xxxxxxxx yyyyyyyy (where (uppercase<<8 + lowercase) gives the coordinate) |
|       5 |       512 | quad curve to  |     5 | 00BAYX10 xxxxxxxx yyyyyyyy aaaaaaaa bbbbbbbb |
|       5 |       512 | cubic curve to |     7 | DCBAYX11 xxxxxxxx yyyyyyyy aaaaaaaa bbbbbbbb cccccccc dddddddd |
|       - |         - | -              |     - | - |
|       6 |      1024 | line to        |     3 | 00YYXX01 xxxxxxxx yyyyyyyy |
|       6 |      1024 | quad curve to  |     6 | 00YYXX10 xxxxxxxx yyyyyyyy 0000BBAA aaaaaaaa bbbbbbbb |
|       6 |      1024 | cubic curve to |     8 | 00YYXX11 xxxxxxxx yyyyyyyy DDCCBBAA aaaaaaaa bbbbbbbb cccccccc dddddddd |
|       - |         - | -              |     - | - |
|   7 (1) |      2048 | line to        |     3 | YYYXXX01 xxxxxxxx yyyyyyyy |
|       7 |      2048 | quad curve to  |     6 | YYYXXX10 xxxxxxxx yyyyyyyy 0BBB0AAA aaaaaaaa bbbbbbbb |
|       7 |      2048 | cubic curve to |     9 | YYYXXX11 xxxxxxxx yyyyyyyy 0BBB0AAA aaaaaaaa bbbbbbbb 0DDD0CCC cccccccc dddddddd |
|       - |         - | -              |     - | - |
|   8 (1) |      4096 | line to        |     4 | 00XXXX01 xxxxxxxx 0000YYYY yyyyyyyy |
|       8 |      4096 | quad curve to  |     7 | 00XXXX10 xxxxxxxx 0000YYYY yyyyyyyy BBBBAAAA aaaaaaaa bbbbbbbb |
|       8 |      4096 | cubic curve to |    10 | 00XXXX11 xxxxxxxx 0000YYYY yyyyyyyy BBBBAAAA aaaaaaaa bbbbbbbb DDDDCCCC cccccccc dddddddd |

Note(1): contour commands for grid size 2048 and above are specified, but not implemented yet.

### Bitmap Fragment

| 1st byte | More bytes | Description |
| -------- | ---------- | ----------- |
| 100ppppp | h          | Small bitmap (up to 256 x 256) |
| 1010PPHH | p,h        | Large bitmap (up to 1024 x 1024) |

First byte encodes type and the pitch (number of bytes per row), the second byte the number of rows (height).
Bitmap widths are always multiple of 8.

For large bitmaps, the pitch is (PP << 8 + p byte + 1), number of rows (HH << 8 + h byte + 1) (up to 1024x1024).
After the header comes the bitmap data. A set bit means foreground color (part of font face), cleared bits
means background (transparency). Least significant bit is on the left, most significant is on the right.

NOTE: currently the x coordinate is not used with bitmap fragments, only the y. This means that bitmaps
are only splitted along the y axis. This is not a limitation by the file format, just an implementation
limitation, which suits 8 or 16 bits wide bitmaps.

### Pixel Map Fragment

| 1st byte | More bytes | Description |
| -------- | ---------- | ----------- |
| 1100WWHH | w,h,s,s    | Pixel map with color palette |

For pixmaps (w + 1) x (h + 1) compressed indeces follows on (s+1) bytes. The index 240 encodes background
(transparency), other indeces (0-239) select an ARGB color from the color map and are part of the font face.
It is not possible to store separate alpha channel with this format (color map has that). If you want to
include a compressed pixmap bigger than 64k, then you're definitely doing the font thing wrong.

Compression: TGA RLE (not that bad ratio and damn easy to decode with a minimal code). Packets start
with a header byte, which encodes N (= header & 0x7F). If header bit 7 is set, then next byte should
be repeated N+1 times, if not, then the next N+1 bytes are all indeces.

NOTE: currently pixmaps are not splitted on the x axis, only on the y. A much much better compression ratio
could be achieved using both axis.

| 1st byte | More bytes | Description |
| -------- | ---------- | ----------- |
| 1101WWHH | w,h,s,s    | Pixel map with direct ARGB values |

If you need many colors or separate alpha channel. To be implemented.

### Hinting information

| 1st byte | More bytes | Description |
| -------- | ---------- | ----------- |
| 1110nnnn | 0          | number of autohint grid relative distances (up to 16) |
| 1111NNNN | 1          | number of autohint grid relative distances |

After that cames N+1 relative coordinates in 1 or 2 bytes each. The size depends on header feature bit 6, it's
a unsigned char (1 byte) if feature bit is unset, othervise it's a unsigned short int (2 bytes). Those are relative
grid system distances to identify grid partitions in [hinting](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/hinting.md).

Character Mappings
------------------

SSFN describes the vast UNICODE code point space in runs. There are two different kind of
runs, one if there's a glyph defined for the code point, and one if not (skip runs). The
table always describes the entire 0 .. 0x10FFFF UNICODE range. If UNICODE Inc. decides to
increase that range, then file revision must be incremented.

There's a Character Lookup Table for each glyph variants, but only the first table is
mandatory.

| 1st byte | More bytes | Description |
| -------- | ---------- | ----------- |
| 0nnnnnnn | 9+x        | There's a glyph for this character, which consist of N fragments |
| 10nnnnnn | 0          | Skip N+1 code points (up to 64) |
| 11NNNNNN | 1          | Skip N+1 code points (N << 8 + additional byte, up to 16384) |

In edge case, when the variant does not encode any characters (shouldn't happen, at least
one character required), then the characters table would contain 68 skip runs to cover the
entire UNICODE range, which would need 136 bytes in the file (if the table is empty, then
the table's offset in font header is zero, and no runs are stored at all).

A TrueType font usually contains 2 or 3 tops composite glyphs. SSFN can store more than a
hundred compositions, and it compresses the glyphs by storing normalized glyph fragments.

The glyph for the 0th character of the first (default) variant is interpreted as a not defined
glyph, which can be shown for every character that is skipped. If no glyph is defined for the
0th character, then the application should render an empty box (it can also write the hex code
point inside the box).

### Glyph Header

If the characters table describe a glyph at a given code point position, then it starts with
a 10 bytes long header.

| Offset | Length | Description |
| -----: | -----: | ----------- |
|      0 |      1 | number of fragments (up to 127) |
|      1 |      1 | most significant bits for dimensions, HHHHWWWW |
|      2 |      1 | most significant bits for advances, YYYYXXXX |
|      3 |      1 | most significant bits for bearing, TTTTLLLL |
|      4 |      1 | glyph width (max x coordinate, including left bearing) |
|      5 |      1 | glyph height (max y coordinate, including top bearing) |
|      6 |      1 | advance x (pen movement after the glyph is drawn) |
|      7 |      1 | advance y |
|      8 |      1 | bearing left |
|      9 |      1 | bearing top |

This header is followed by fragment descriptors. It is possible that a character does not
have any fragments (like the space). Descriptors are 4 to 6 bytes long, depending on font
quality and characters table offset within the file. Within a font, the descriptor size
is constant.

IMPORTANT: if there's hinting information associated with the character's glyph, then the
hinting fragment descriptor must be the first.

### Fragment Descriptors

If characters table offset in header is less than 65536 and quality is less than 5, then
one fragment descriptor looks like:

| Offset | Length | Description |
| -----: | -----: | ----------- |
|      0 |      2 | fragment offset |
|      2 |      1 | x offset |
|      3 |      1 | y offset |

If characters table offset in header is bigger than 65535 and less than 1048576 or
quality is 5 or 6:

| Offset | Length | Description |
| -----: | -----: | ----------- |
|      0 |      2 | fragment offset |
|      2 |      1 | most significant bits YYXXFFFF |
|      3 |      1 | x offset |
|      4 |      1 | y offset |

Otherwise (offset is above 1048576 or quality is above 6):

| Offset | Length | Description |
| -----: | -----: | ----------- |
|      0 |      3 | fragment offset |
|      3 |      1 | most significant bits YYYYXXXX |
|      4 |      1 | x offset |
|      5 |      1 | y offset |

The fragment offset is relative to the magic, and coordinate offsets are relative to character's
bearing. For hinting grid, the offset is one bigger, therefore if bearing is zero, x=1 and y=0
encodes horizontal grid, and x=0 and y=1 vertical grid hints.

Kerning Table (optional)
------------------------

If there's no kerning information associated with the font, then the kerning table offset
in the font header is zero. Otherwise it has two sub-tables, and starts with the Kerning
Lookup Table.

### Look Up

This is very similar to the Character table, it also stores two different kind of runs. The
skip runs are encoded exactly the same way.

| 1st byte | More bytes | Description |
| -------- | ---------- | ----------- |
| 0nnnnnnn | 2/3        | There's a kerning info for this character and next N characters (up to 127) |
| 10nnnnnn | 0          | Skip N+1 code points (up to 64) |
| 11NNNNNN | 1          | Skip N+1 code points (N << 8 + additional byte, up to 16384) |

If there's a kerning info, then the first byte is followed by 2 (or 3 if header feature bit 3 is set)
bytes, which is a relative offset from the kerning table start (and not relative to magic) and selects
a kerning group. If more contiguous characters share the same kerning group, then N is non-zero. The
look up table represents the left character in the kerning relation.

### Kerning Groups

After the look up table cames the second sub-table, the Kerning Groups Table. This table represents
the right side of the kerning relation.

#### Group Header

Each group starts with a 1 or 2 bytes header, which encodes the number of elements in this group. The
offsets in the look up table point to these group headers (relative to the kerning look up table start).

| 1st byte | More bytes | Description |
| -------- | ---------- | ----------- |
| 0nnnnnnn | 0          | N+1 elements in the group (up to 128) |
| 1NNNNNNN | 1          | N+1 elements in the group (N << 8 + additional byte, up to 32768) |

#### Group Elements

Each element contains a UNICODE - coordinate kerning pair. For each, first cames the right side code point,
which is encoded as follows if font header feature bit 4 is clear (largest code point is smaller than 32768):

| Offset | Length | Description |
| -----: | -----: | ----------- |
|      0 |      1 | right side code point, less significant part |
|      1 |      1 | right side code point, most significant 7 bits, bit 7: 0=horizontal coordinate, 1=vertical |

With large codepoints:

| Offset | Length | Description |
| -----: | -----: | ----------- |
|      0 |      2 | right side code point, less significant part |
|      2 |      1 | right side code point, most significant 7 bits, bit 7: 0=horizontal coordinate, 1=vertical |

This is followed by the relative horizontal or vertical coordinate offset stored in a signed byte or signed
short int (depends on font header feature bit 5).

| Offset | Length | Description |
| -----: | -----: | ----------- |
|    2/3 |    1/2 | relative coordinate |

Note that feature bit 5 is independent from the grid size. It is possible that a 1024 grid font does
not use kerning offsets bigger than -128 .. 127 pixels, and thus does not need large coordinates in
the kerning table. On the other hand, 128 grid size will never use large coordinates for sure.

There are as many right side blocks as the number of pairs indicated in the group header.

Color Map (optional)
--------------------

Color map only exists if there's at least one pixmap fragment or color command in the font and therefore
the font header feature bit 1 is set. The offset of the color map is not stored in the font header, as
it can be easily calculated by `font size - 964`. Color map is always 960 bytes long, and stores four
A,R,G,B bytes for each color index 0-239. Values for indeces 240-255 are *not* stored, as those encode
a 16 step alpha transition in pixmaps.

End Magic
---------

The SSFN file is closed by the magic in big endian format: 'NFSS'.

That's all folks!
