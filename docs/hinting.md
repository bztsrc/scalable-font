Scalable Screen Font Autohinting Grid
=====================================

Hinting is one of the most important aspects of vector font rasterization, because it ensures that
the pixelized glyphs became readable. SSFN does not want to take sides on the "should the font designer's
typeface respected or should the user be served instead with more readable fonts" battle. Therefore with
SSFN you can do both: SSFN by default stretches the glyphs to pixel fitting important coordinates for
improved readability, which you can turn off any time (see `SSFN_STYLE_NOHINTING` in [API](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/API.md)
documentation on `ssfn_select()`).

The Problem When We Respect the Font Designer
---------------------------------------------

Normally the glyphs are stored in a 4096 x 4096 grid with 4 bits precision. When you rasterize it to
a smaller pixel coordinate system using a linear transformation, those grid coordinates are scaled to
sub-pixel coordinates:
```
pixel coordinates
  ^
  |                                   xxxx
2 |                               xxxx
  |...........................xxxx
  |                       xxxx :
  |...................xxxx     :
1 |               xxxx :       :
  |           xxxx     :       :
  |       xxxx         :       :
  |...xxxx             :       :
0 +----o---------------o-------o-----------> grid coordinates
```

When the grid coordinate is translated to 1.4 or 1.6 pixel coordinate, the antialiasing code will generate
pixels at discrete coordinate 1 and 2 with 40% alpha (for both as their distance to the next discrete coordinate
is 0.4), which makes the outline blurry and the glyph hard to read. Even worse when one alpha cames
closer to 100%, then it would seem that the glyph has different coloumn widths. In the following
example ',' (which represents 10% alpha) barely noticable, while 'Y' (90%) almost similar to 'X' (100%).

```
............                 ............
..,X...XY...                 ...X...XX...
..,X...XY...   looks like    ...X...XX...
..,XXXXXY...  ------------>  ...XXXXXX...
..,X...XY...                 ...X...XX...
..,X...XY...                 ...X...XX...
............                 ............
```

Hinting bytecode
----------------

TrueType and OpenType fonts tries to solve this by introducing a bytecode interpreter and a geometric
instruction set. Then every glyph has a "program" that walks trough the contour and rounds the grid
coordinate to pixel coordinate. This is just brainfuck. Seriously, what a lunatic come up with such
a bad and disgusting solution to pixel fit a few coordinates?

The disadvatanges are numerous. You have to rewrite that "program" every time you change a coordinate
in the glyph. Also there are very few tools to regenerate such a bytecode, most of them are proprietary
and a piece of shit. Manually writing the bytecode on the other hand is possible, but painful like Hell.
Not to mention that the size of bytecode grows font file's size considerably, and makes rasterization
extremely difficult and slow.

So hinting bytecode is bad for font designers, complicates the life of programmers who're writing
rasterizers, and also bad for end users who suffer the cost from the slowdown of interpreting the
bytecode every time a glyph is rendered. Auch.

Autohinting Grid
----------------

SSFN uses a fundamentally different approach. Instead of a single linear transformation, it uses a
series of contiguous partitions each with it's own linear transformation.

```
pixel coordinates
  ^
  |                                xxxx
2 |............................xxxx
  |                          xx:
  |                        xx  :
  |                      xx    :
1 |....................xx      :
  |               xxxxx:       :
  |          xxxxx     :       :
  |    xxxxxx          :       :
0 +----o---------------o-------o------------> grid coordinates
```

Here every important grid points (on partition edges) are pixel fitted. This guarantees that specific
grid coordinates are transformed into a discrete pixel coordinates, so antialiasing will render them
sharp. To achieve pixel fitting, SSFN does the following steps:

 1. identify important coordinates on the X axis
 2. calculate the relative difference between those coordinates
 3. create discrete pixel coordinates using the same distances but on pixel rasterization scale
 4. fill in the gaps with linear sub-pixel transformations

If the SSFN font provides hinting grid information, then the first 2 steps are skipped, as the grid
coordinate distances are read from the font file. You can [generate such an info](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/ecosystem.md)
using `ttf2sfn -g` or `bit2sfn -g` (where `-g` stands for grid information). Be aware, this will
considerably increase the font file's size.

I'm unsure how ClearType works, as that's a proprietary solution with very little documentation
on the net. But I bet this solution beats it in speed and in accuracy!

Autohinting can be unsuccessful if the font designers are lazy or uncareful. For example in
FreeSerif the width of 't' is 184 grid pixels wide, while the coloumn of 'h' is 250 pixel wide, yet
you expect to render them with the same screen pixel width. Also the four vertical lines in
'$' have different widths, which I doubt was intentional. Another example, in FreeSans, the right
coloumn of 'H' has different widths on the top than on the bottom. This becames apparent when
the glyph is rasterized in a size when the bottom half is pixel fitted, but the upper half remains
antialiased:

```
..........
..XX..yX..
..XX..yX..
..XXXXXX..
..XX..XX..
..XX..XX..
..........
```

The current hinting implementation is very basic, and uses only one axis partitions. This has some
drawbacks when you render to very small sizes. Some curves that should be blurry fall into the
partition edge and therefore rendered with full pixels. This side effect becames less and less
noticable as the rendered size grows. This could be avoided with grid hinting boxes instead of one
axis partitions, where the two axis' partitions define boxes. The SSFN file format is capable of
storing such info, but this hasn't been implemented in the renderer yet.

```
important hint grid coordinate, starts a partition
  |
  |                +--------- for small sizes the round corner
  v                v          falls into the partition edge
.........        .........    and becames squared
..,XXXX..        ..XXXXX..
..X~  ~X.        ..X~  ~X.
..XXXXXX.        ..XXXXXX.
..X......  --->  ..X......
..X,.....        ..X,.....
..'XXXX..        ..XXXXX..
.........        .........
```

Kerning and Hinting
-------------------

Another problem is when the glyph's coordinate is modified by advance and kerning. SSFN solves this by
using independent coordinate systems for each glyphs (inertia systems for each glyph) and converting
advance and kerning to discrete pixel coordinates. This solution has a trade-off, kerning is not that
accurate, but user's won't notice, and in return applications don't have to worry or care about modified
hinting, because they are blitting only at pixel fitted coordinates. This is also benefitial to make the
rasterized glyphs cachable and reusable.

