Scalable Screen Font Editor
===========================

Command Line
------------

Very simple, there is only one argument, the name of the font file you want to edit.
```sh
$ ./sfnedit
Scalable Screen Font Editor by bzt Copyright (C) 2019 MIT license

./sfnedit <.sfn|.asc|.sfn.gz|.asc.gz>

```
That font can be in [SSFN](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/sfn_format.md) or
in [ASC](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/asc_format.md) format, and if the
editor was compiled with zlib support, then it can be gzip compressed. The editor does not care if
the filename has an additional .gz extension, it decides to use gzip depending on magic bytes.

The editor is multilingual, and autodetects the language from the environment. If you want to use
a different language than your OS' default, you can specify it in the LANG environment variable:
```sh
$ LANG=hu ./sfnedit
```

All windows are splited vertically into a tab navigation bar (on the top), and a
tab area (the rest, bigger part of the window). By clicking on the tab label in the
navigation bar, you can switch the tab area.

These keyboard shortcuts work in all windows and in all tabs.

| Key Combination | Description |
| --------------- | ----------- |
| <kbd>F1</kbd>   | Context dependent built-in help |
| <kbd>Esc</kbd>  | Close the window |
| <kbd>Ctrl</kbd>+<kbd>s</kbd> | Save the font in SSFN format |
| <kbd>Shift</kbd>+<kbd>Ctrl</kbd>+<kbd>s</kbd> | Save the font in ASC format |
| <kbd>Tab</kbd>  | Switch to next tab |
| <kbd>Shift</kbd>+<kbd>Tab</kbd>  | Switch to previous tab |

Main Window
-----------

There's only one main window per font. If you want to edit more fonts in paralell,
then you have to start more instances of `sfnedit`. If you close this window, then
all windows will be closed and the application quits. If there are unsaved modifications
on the font, then you will be asked if you want to save them.

## Properties Tab

This tab shows the properties of the font. Changes made here are not reverisible,
meaning you cannot undo them.

<img src='https://gitlab.com/bztsrc/scalable-font/raw/master/docs/sfnedit1.png'>

Changing grid size can take for a while, as it requires the recalculation of all vector
coordinates and the resize of all bitmap and pixmap glyphs. Going to a lower grid size
could loose typeface detail of the font. This is not always bad, because fonts are often
have bugs, and downgrading to 256 x 256 grid makes the misplaced points properly aligned.
Grid size below 256 x 256 often requires manual grid fitting of points to keep font
readability, and 16 x 16 is recommended only for bitmap glyphs and maybe for very special
low-detail vector glyphs.

| Key Combination | Description |
| --------------- | ----------- |
| <kbd>Down</kbd> | Switch to next field |
| <kbd>Up</kbd>   | Switch to previous field |
| <kbd>Right</kbd> | Change to next value (if applicable) |
| <kbd>Left</kbd> | Change to previous value (if applicable) |
| <kbd>Space</kbd> | Toggle check box value |

## Ranges Tab

This tab gives an overview of which UNICODE character blocks have glyph definitions
in the font.

<img src='https://gitlab.com/bztsrc/scalable-font/raw/master/docs/sfnedit2.png'>

By clicking "Uncovered ranges" then blocks with absolutely no glyphs will
be shown too. It is possible to have more than 100% coverage if glyphs are defined for
code points that are undefined by UNICODE Inc.

| Key Combination | Description |
| --------------- | ----------- |
| <kbd>Space</kbd> | Toggle "Uncovered ranges" (if search is inactive) |
| <kbd>Down</kbd> / <kbd>Up</kbd> | Scroll the list |
| <kbd>Enter</kbd> | Select a range |
| any other key    | Start a search |

You can search for UNICODE block names (both normal and short versions), and with
hex number for a range that contains that specific code point.

## Characters Tab

Here you can see the characters for which the font has glyphs.

<img src='https://gitlab.com/bztsrc/scalable-font/raw/master/docs/sfnedit3.png'>

If you have came here by selecting a range, then the list will be restricted to that range.
By clicking on tab label or switching to this tab by pressing the <kbd>Tab</kbd> key, you'll
see the whole vast UNICODE range. Glyphs with window's background color are not defined by
UNICODE. Light background with a dark, low-res bitmap glyph representation means the code
point is specified by UNICODE, but has no glyph in the font. Dark background with light
glyphs are generated from the font, and are real-time representations of all modifications
you may have done on the font.

| Key Combination | Description |
| --------------- | ----------- |
| <kbd>Down</kbd> / <kbd>Up</kbd>   | Scroll the list |
| <kbd>Left</kbd> / <kbd>Right</kbd> | Select a glyph variant table |
| <kbd>Shift</kbd> + left click and move | Select a range |
| <kbd>Ctrl</kbd> + <kbd>c</kbd> | Copy the selected range to clipboard |
| <kbd>Ctrl</kbd> + <kbd>x</kbd> | Cut out the selected range and store it to clipboard |
| <kbd>Ctrl</kbd> + <kbd>p</kbd> | Paste a range from clipboard to cursor position |
| left click    | Open glyph window for the character |
| any other key | Start a search |

You can search for a UNICODE code point name, for an UTF-8 character, or with a hex
number for a particular code point.

Glyph Windows
-------------

You can have as many of these windows as you want, but only one per character. In this
window you can edit and modify the properties of a glyph. Modification history is
accessible until you close this window, meaning you can undo everything up to the point
when you opened the window.

## Edit Tab

This is the main editing tab. On the left, there's the toolbar, and on the right (which
occupies the rest of the window) is the main edit area.

<img src='https://gitlab.com/bztsrc/scalable-font/raw/master/docs/sfnedit5.png'>

### Toolbar

You can select an icon from the toolbar by clicking, the icons are in order:

 - Change horizontal advance
 - Change vertical advance
 - Set up hinting grid points
 - Delete the entire glyph
 - Remove glyph layer
 - Edit glyph layer
 - Add vector glyph layer

Note that advances are exclusive. It is either horizontal or vertical, but never both.

If you select "Delete the entire glyph", then you'll be asked for confirmation. If you
delete layers accidentally with "Remove glyph layer", don't be alarmed, just press undo.

If you click and hold down left mouse button on the "Add vector glyph layer" icon, then two
more icons will show up. You can select those by moving the pointer above them, and release the
mouse button there. They are "Add bitmap glyph layer" and "Add pixmap glyph layer". If you
already have a bitmap or pixmap layer, then no new layer will be added, but the appropriate
layer will be activated. For vector layers a new layer is always generated.

### Layers

Beneath the icon toolbar, you can see the available layers. There's only one bitmap and
pixmap layer, but you can have as many vector layers as you want. Moving the mouse pointer
above the layers and using the wheel you can scroll them. By clicking on one layer it will
became active, and the toolbar will switch to the "Edit glyph layer" icon.

By pressing <kbd>Ctrl</kbd>+<kbd>a</kbd> you can select all layers at once. In this mode
only a limited set of tools are available (like moving or copying the layers).

### Color Selector

If you have choosen a vector layer, then you'll see 4 color boxes. Clicking on one of those
will bring up the palette and the color selector. With these you can specify the color gradients
to be used for that glyph layer. Please note that colored vector glyphs are only supported by
the editor for now. The normal renderer in ssfn.h ignores the color command in fonts because it
renders to alpha channels only.

If you have choosen the pixmap layer, then you'll see only one color box. Clicking on it will
bring up the palette, and also by scrolling vertically or using the mouse wheel with holding
<kbd>Shift</kbd> down, you can cycle through the palette. When you left click on the edit area,
the pixel under the pointer will change to this color. The normal renderer in ssfn.h supports
colored pixmaps in CMAP mode, otherwise it generates a grayscale alpha channel of them.

<img src='https://gitlab.com/bztsrc/scalable-font/raw/master/docs/sfnedit6.png'>

To select a color (for gradient or pixmap), you have to click on the palette. At first, when your
palette is empty, all colors will be transparent. After selecting the palette index, the color
selector will show up. You can specify the color for that particular palette index there. The last
color, the 240th, which is in it's own line, is for selecting the background. That one is always
transparent and it does not bring up the color selector either.

### Coordinates

Whenever your mouse pointer is above the edit area, you'll see here on the bottom left corner
the coordinates in font's grid resolution, X axis above, and Y below.

### Edit Area

| Key Combination | Description |
| --------------- | ----------- |
| <kbd>g</kbd>    | Toggle grid |
| <kbd>l</kbd>    | Toggle lines for control points |
| <kbd>r</kbd>    | Toggle right-to-left flag on glyph. This does not influence the font, only the editor |
| <kbd>Shift</kbd> + left click and move | Select portion of the glyph |
| <kbd>Ctrl</kbd> + <kbd>c</kbd> | Copy the selected portion to clipboard |
| <kbd>Ctrl</kbd> + <kbd>x</kbd> | Cut out the selected portion and store it to clipboard |
| <kbd>Ctrl</kbd> + <kbd>p</kbd> | Paste a portion from clipboard to cursor position |
| <kbd>Ctrl</kbd> + <kbd>z</kbd> | Undo |
| <kbd>Ctrl</kbd> + <kbd>y</kbd> | Redo |
| left click      | Select a point, or set foreground for bitmap and pixmap layers |
| left click + move | Without a selected point, move the canvas |
| left click + move | With a selected point (either on-curve or control), move that point |
| <kbd>Ctrl</kbd> + left click | Add a line to contour |
| <kbd>GUI</kbd> + left click | Add a cubic Bezier curve to contour |
| <kbd>Alt</kbd> + left click | Add a quadratic Bezier curve to contour |
| <kbd>Ctrl</kbd> + <kbd>GUI</kbd> + left click | Add a quadratic Bezier curve to contour |
| right click     | Remove last contour, or clear to transparency for bitmap and pixmap layers |
| wheel           | zooming in or out |

Selecting a portion of a vector glyph can only work if the path in the selection is contiguous.

Grid lines are only shown if there's at least 2 pixels between grid coordinates. This means for
vector glyphs with large grids, you'll have to zoom in a lot until the grid finally shows up.

Control points (green X) cannot be moved beyond the bounding box of the layer, defined by the
on-curve points (blue dots). The editor tries to force this intelligently, not only when you move
a control point, but also when you move an on-curve point which defines one or more sides of the
bounding box. In the latter case, if you press undo, then not only the on-curve point, but all
modified control points will be restored too.

## Kerning

In this tab you can set up relative character pair offsets. Unlike advances, you can set
up both horizontal and vertical offsets for the same pair. This is very handy in glyph
composition, when you need to place an accent above or below a base glyph.

<img src='https://gitlab.com/bztsrc/scalable-font/raw/master/docs/sfnedit7.png'>

On the top, there are 7 checkboxes for displaying glyph variants to help you with kerning.
All glyph variants share the same relative kerning offsets, but they have different advance
values, therefore aligned to the red advance line differently. The last checkbox is for
toggling right-to-left direction (more on this later).

On the left, you can see a list of characters paired with the character you're editing.
In the searchbox, you can search for hex code point, UTF-8 character match and UNICODE
character names. If there are no matches for paired characters, then the search will continue
on all code points which have a glyph defined. By pressing <kbd>Enter</kbd> or <kbd>Down</kbd>,
you'll switch to the right-side character list. Selecting a previously undefined pair will add
offsets of 0 and 0 which you can then modify. To delete a kerning pair, simply reset the offsets
to 0 and 0 and that pair won't be stored into the font file upon save.

On the right, in the big area, you'll see the choosen character pair. Depending whether
you have choosen vertical or horizontal advance on the Edit tab, the pair is shown
horizontally or vertically. You should not mix advance directions for glyph variants
for the same character, and kerning area only respects the default variant's direction.
Most scripts (including Latin, Cyrillic, Greek, etc.) are left-to-right horizontal. This
distinction between horizontal and vertical is stored within the font file.

<img src='https://gitlab.com/bztsrc/scalable-font/raw/master/docs/sfnedit8.png'>

With an Y advance, kerning looks like this (I've used Mahjongg tiles because they are
kind of CJK and most CJK scripts are up-to-down vertical).

Unfortunatelly just by the UNICODE code point, you can't always tell if the glyph
has to be written in right-to-left direction. Therefore sfnedit tries to figure it
out, but lets the user change that by clicking on the "RTL" checkbox or pressing
<kbd>r</kbd>. This distinction between left-to-right and right-to-left is NOT stored
within the font file, and requires a BiDi state machine to be applied correctly when
rendering the font. Read more on this in the [API](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/API.md)
documentation.

<img src='https://gitlab.com/bztsrc/scalable-font/raw/master/docs/sfnedit9.png'>

| Key Combination | Description |
| --------------- | ----------- |
| <kbd>r</kbd>    | Toggle right-to-left flag on glyph. This does not influence the font, only the editor |
| <kbd>Up</kbd> / <kbd>Down</kbd> | Switch input field (if neither kerning editor area nor right character list is active) |
| <kbd>Up</kbd> / <kbd>Down</kbd> | Select right-side character (if right character list is active) |
| <kbd>Up</kbd> / <kbd>Down</kbd> / <kbd>Left</kbd> / <kbd>Right</kbd> | Change kerning (if kerning editor area is active) |
