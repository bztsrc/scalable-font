Scalable Screen Font Modes
==========================

Outline Mode
------------

If mode is `SSFN_MODE_OUTLINE`, then the glyph's outline will be returned in `data`. It will contain `pitch`
coordinates, which must be interpreted in x,y pairs. The first pair is a fixed point, the others are lines from
the previous pair to the current one. An outline ends at the end of data or with a 255,255 pair, which is followed
by a new fixed point. The outline does not contain curves or splines, those are converted to a series of lines.
Outlines are ordered by the total size they cover, biggest first.

Bitmap Mode
-----------

If mode is `SSFN_MODE_BITMAP`, then data contains a bitmap, where bits set belong to the font face (foreground),
and bits cleared encode transparency (background). Less significant bit is on the left, the most significant is
on the right.

Alpha Channel Mode
------------------

If mode is `SSFN_MODE_ALPHA`, then data contains an alpha channel, one byte for each pixel. There 255 means opaque
(foreground), and 0 transparent (background). Other values are transitions between the font face and the background.

Color Map Mode
--------------

Finally, if mode is `SSFN_MODE_CMAP`, then data contains one byte for each pixel. There values 0-239 select a
color from the color map provided in `cmap`. Values 240-255 encode a 16 steps alpha channel, where 240 means
transparency (alpha 0, background) and 255 means opaque (alpha 15, foreground). The color map contains 4 bytes
for each index, A,R,G,B values in order. You can decode this information with the `SSFN_CMAP_TO_ARGB()` macro,
which receives three arguments: a cmap mode pixel, pointer to the color map and a foreground color.

__Example Code__

Here's a simple code on how to decode returned rasterized glyph on an SDL ARGB surface for example:

```c
#define SDL_PIXEL ((uint32_t*)(surface->pixels))[(pen_y + y) * surface->pitch/4 + (pen_x + x)]

void my_draw_glyph(SDL_Surface *surface, ssfn_glyph_t *glyph, int pen_x, int pen_y, uint32_t fgcolor)
{
    int x, y, i, m;

    /* align glyph properly, we may have received a vertical letter */
    if(glyph->adv_y)
        pen_x -= glyph->baseline;
    else
        pen_y -= glyph->baseline;

    switch(glyph->mode) {
        case SSFN_MODE_OUTLINE:
            if(glyph->pitch>1) {
                x = glyph->data[0]; y = glyph->data[1];
                for(i = 0; i < glyph->pitch; i += 2) {
                    /* end of a contour? */
                    if(glyph->data[i] == 255 && glyph->data[i+1] == 255) i += 2;
                    /* no, connect this point to the previous one. You should provide your own line() */
                    else line(screen, pen_x + x, pen_y + y, pen_x + glyph->data[i], pen_y + glyph->data[i+1]);
                    x = glyph->data[i]; y = glyph->data[i+1];
                }
            }
        break;

        case SSFN_MODE_BITMAP:
            for(y = 0; y < glyph->h; y++)
                for(x = 0, i = 0, m = 1; x < glyph->w; x++, m <<= 1) {
                    if(m > 0x80) { m = 1; i++; }
                    SDL_PIXEL = (glyph->data[y * glyph->pitch + i] & m) ? 0xFF000000 | fgcolor : 0;
                }
        break;

        case SSFN_MODE_ALPHA:
            for(y = 0; y < glyph->h; y++)
                for(x = 0; x < glyph->w; x++)
                    SDL_PIXEL = (uint32_t)((glyph->data[y * glyph->pitch + x] << 24) | fgcolor);
        break;

        case SSFN_MODE_CMAP:
            for(y = 0; y < glyph->h; y++)
                for(x = 0; x < glyph->w; x++)
                    SDL_PIXEL = SSFN_CMAP_TO_ARGB(glyph->data[y * glyph->pitch + x], glyph->cmap, fgcolor);
        break;
    }
}
```

Of course this is just an example, aimed at clearity and simplicity. There's plenty of room for optimization.

