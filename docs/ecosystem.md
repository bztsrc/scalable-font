Scalable Screen Font Ecosystem
==============================

SSFN is not just a renderer. It is also a file format, which comes with a set of command line tools
to manage [SSFN font](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/sfn_format.md) files.

I'd like to point out that SSFN fonts can store vector based, bitmap and pixmap glyphs, and there's
no restriction, you can mix those within a single SSFN file. Despite of this, the conversion tools
can only produce one type of glyphs in a single output file at the moment.

Converting Vector Fonts
-----------------------

The tool `ttf2sfn` does that. It uses the [freetype2](http://www.freetype.org) library so it can convert
anything that freetype2 can read. This includes all the common formats, TrueType (.ttf) and OpenType (.otf).

SSFN uses a data-loss compression on vector fonts with user configurable quality level. See the
[comparition](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/compare.md) table on how the
quality setting influences the resulting file sizes. With quality 6 (the default) grid resolution is
about the same as a typical TTF/OTF file's (1000 EM vs. 1024 grid pixels), but usually requires half
the file size.

Converting Bitmap Fonts
-----------------------

Another tool, `bit2sfn` is for that. It has no library dependencies, and it can read the most common
bitmap formats: [PC Screen Font](https://www.win.tue.nl/~aeb/linux/kbd/font-formats-1.html) (.psfu,
Linux Console's format), [X11 Bitmap Distribution Format](https://www.x.org/docs/BDF/bdf.pdf) (.bdf) and
[GNU unifont](http://unifoundry.com/unifont/index.html) (.hex).

Because not all bitmap font formats has font name and licensing information, like vector fonts, you
can pass those as arguments on the command line.

Bitmap fonts are also compressed, but with a loss-less deduplication algorithm.

Converting Pixmap Fonts
-----------------------

Pixmaps differ to bitmaps in that they contain not bits, but pixels, so they can include colored glyphs.
This is primarly used for fancy colorful emoji icons. The `bit2sfn` tool can read [Truevision TARGA](http://www.gamers.org/dEngine/quake3/TGA.txt)
(.tga) files to generate such pixmap SSFN fonts. TGA is a very simple and common format, The Gimp can
save it, and Imagemagick can convert to it too.

In the TGA file (which can be uncompressed, compressed, indexed, true-color, turned upside-down or not)
the color #000000 (full black) reads as transparency. If you need a black color, use #000100 (which is
still quite black, no human can't tell the difference) or use alpha channel with Targa32. For
compatibility, the full black FF000000 is trancparency with alpha channel too, but you can use 99,9999%
opaque black with FE000000 (rgba(0,0,0,254) in html). The glyphs can be either stored vertically or
horizontally in the image file, and UNICODE range must be specified. If image is taller than wide, then
image width will became the glyph's width, and image height will be divided by the size of the range,
thus giving the height of each glyph. If image is wider than tall, then image height will became glyph
height and image width divided by range size will be glyph width. For example 256 characters each with
a 8 x 16 glyph can be stored in a 8 x 4096 or in an 2048 x 16 image.

Although `bit2sfn` can read true-color TGA images, I strongly suggest to convert them into indexed ones
using The Gimp: Image > Mode > Indexed... select Generate optimum palette with maximum number of 240. As
being a specific image manipulation tool, it does a lot better job in palette conversion than `bit2sfn`.
Note: if The Gimp does the aforementioned black change on its own for pixels that you want transparent,
then here's a workaround: select Color > Map > Rearrange Colormap to sort colors by hue (so that black
becames the first color in the palette). Then export as TGA. After that either open the TGA file in a hex
editor, and change 00 01 00 at offset 0x12 to 00 00 00, or execute this from command line (replace
`mypixfont.tga` with your file):
```sh
$ dd conv=notrunc if=/dev/zero of=mypixfont.tga bs=3 seek=18
```

Pixmaps fonts are also deduplicated the same way as bitmap fonts, and in addition they will be RLE
compressed too. The combination of these two algorithms usually yields better compression ratios than
PNG files for example (that's because SSFN does not store empty lines and it stores repeating image
blocks only once, while PNG compresses all occurances separately, including empty parts).

Converting SSFN ASCII Fonts
---------------------------

SSFN has a plain text, human readable "source file" format, called [ASC](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/asc_format.md).
The `bit2sfn` tool can read that and convert into a compressed binary SSFN file. The tool `sfn2asc` does
the opposite: it converts binary SSFN fonts into plain text SSFN ASCII format. If it can found [UnicodeData.txt](http://www.unicode.org/Public/UCD/latest/ucd/UnicodeData.txt)
in one of /usr/share/unicode, ~/.config/ssfn, ~/.ssfn or current working directories, then the generated ASC file
will contain UNICODE code point names too in glyph definitions (which is very handy).

SSFN ASCII format is a text editor friendly format (with UTF-8 sequences in data). It was designed in a way that
you can edit and copy'n'paste glyphs from one file to another. This works with all bitmap and pixmap glyphs. As
for the vector based glyphs, you have to make sure of it, that the source files use the same quality setting,
otherwise you'll notice strange shrinked or insanely magnified glyphs.

Merging Bitmap Fonts
--------------------

It is possible to specify more input files for `bit2sfn`, and optionally a range for each. For example to get the
default 8x16 VGA UNICODE fonts (kudos to Bolkhov), and add 16x16 Devanagari letters from GNU unifont, one could do:

```sh
$ ./bit2sfn u_vga16.bdf -r 0x900 0x97F unifont.hex console.sfn
```
Or
```sh
$ ./bit2sfn u_vga16.bdf -r U+0900 U+097F unifont.hex console.sfn
```

UTF-8 sequences are also accepted, if they are enclosed in single quotes (shell requires slashes):
```sh
$ ./bit2sfn u_vga16.bdf -r \'A\' \'Z\' unifont.hex console.sfn
```
Or you can use UNICODE block names, for example:
```sh
$ ./bit2sfn u_vga16.bdf -r "Cyrillic Extended-C" unifont.hex -r cjkunifiedideographs cjk.bdf console.sfn
```

The converter's build system autodetects if you have the zlib library installed. If compiled with
that, then you can directly convert .psfu.gz and .bdf.gz fonts, no need to uncompress them.

Merging Vector Fonts
--------------------

It is not that easy with scalable fonts. I suggest to use [FontForge](https://fontforge.github.io) which handles
copy'n'pasting glyphs and entire character ranges from one font to another. Then save the result in OTF, and use
`ttf2sfn` to convert the merged font into SSFN.

Another way is to convert both fonts to ASC, and simply copy'n'paste glyph blocks with a text editor. This
is perfectly safe if the fonts have the same quality (grid size) setting.

Creating and Extracting Font Collections
----------------------------------------

There's a tool for that, `sfn2sfn`. It is very simple, expects one or more filenames on command line. If
the first file is an SSFN font, then the rest except the last must be too, and a collection will be saved
with the last file name.

```sh
$ ./sfn2sfn VeraR.sfn VeraBd.sfn VeraIt.sfn VeraBI.sfn Vera.sfn
```

If the first file is an SSFN collection, and there are no more arguments, then it lists the contents (family
code, b for bold, i for italic and unique font name). Otherwise if there're more file name arguments, it will
extract the fonts to those. With extraction, you must provide as many filenames as fonts are in the collection.

```sh
$ ./sfn2sfn Vera.sfn
1 .. Bitstream Vera Sans
1 b. Bitstream Vera Sans Bold
1 .i Bitstream Vera Sans Oblique
1 bi Bitstream Vera Sans Bold Oblique
```
```sh
$ ./sfn2sfn Vera.sfn VeraR.sfn VeraBd.sfn VeraIt.sfn VeraBI.sfn
```

Adding Hinting Grid Manually
----------------------------

This is a bit tricky for now, as SSFN uses automatic grid [hinting](https://gitlab.com/bztsrc/scalable-font/blob/master/docs/hinting.md).
But you can save hinting info into the SSFN file from an OTF or TTF if you use `ttf2sfn -g`, then convert into
ASC. You'll see lines starting with 'H' or 'V', for horizontal and vertical grid lines respectively. Edit them,
then use `bit2sfn -g` to convert the ASC back to SSFN format (`-g` stands for grid information). That way the
SSFN file will include precalculated grid coordinate distances and the renderer won't rely on autohinting.

UNICODE Code Point Coverage Reports
-----------------------------------

You can use the `sfn2asc -r` tool to quickly check the code point range coverage in an SSFN font. This will
generate reports like this:

```sh
$ ./sfn2asc -r u_vga16.sfn
```

| Coverage | NumChar | Start  | End    | Description                                |
| -------: | ------: | ------ | ------ | ------------------------------------------ |
|    75.7% |      97 | 000000 | 00007F | Basic Latin                                |
|    75.0% |      96 | 000080 | 0000FF | Latin-1 Supplement                         |
|   100.0% |     128 | 000100 | 00017F | Latin Extended-A                           |
|    85.5% |     178 | 000180 | 00024F | Latin Extended-B                           |
|    97.9% |      94 | 000250 | 0002AF | IPA Extensions                             |
|    48.7% |      39 | 0002B0 | 0002FF | Spacing Modifier Letters                   |
|     8.9% |      10 | 000300 | 00036F | Combining Diacritical Marks                |
|    81.4% |     110 | 000370 | 0003FF | Greek and Coptic                           |
|    96.0% |     246 | 000400 | 0004FF | Cyrillic                                   |
|    33.3% |      16 | 000500 | 00052F | Cyrillic Supplement                        |
|    95.6% |      87 | 000530 | 00058F | Armenian                                   |
|    93.1% |      82 | 000590 | 0005FF | Hebrew                                     |
|    80.7% |     206 | 000600 | 0006FF | Arabic                                     |
|     5.6% |       5 | 0010A0 | 0010FF | Georgian                                   |
|    96.0% |     246 | 001E00 | 001EFF | Latin Extended Additional                  |
|     3.4% |       8 | 001F00 | 001FFF | Greek Extended                             |
|    62.1% |      69 | 002000 | 00206F | General Punctuation                        |
|    69.0% |      29 | 002070 | 00209F | Superscripts and Subscripts                |
|    50.0% |      16 | 0020A0 | 0020CF | Currency Symbols                           |
|    73.7% |      59 | 002100 | 00214F | Letterlike Symbols                         |
|    81.6% |      49 | 002150 | 00218F | Number Forms                               |
|    89.2% |     100 | 002190 | 0021FF | Arrows                                     |
|    94.5% |     242 | 002200 | 0022FF | Mathematical Operators                     |
|    19.5% |      50 | 002300 | 0023FF | Miscellaneous Technical                    |
|    20.5% |       8 | 002400 | 00243F | Control Pictures                           |
|   100.0% |     128 | 002500 | 00257F | Box Drawing                                |
|    68.7% |      22 | 002580 | 00259F | Block Elements                             |
|    91.6% |      88 | 0025A0 | 0025FF | Geometric Shapes                           |
|    29.2% |      75 | 002600 | 0026FF | Miscellaneous Symbols                      |
|    10.3% |       6 | 00FB00 | 00FB4F | Alphabetic Presentation Forms              |
|    23.4% |     143 | 00FB50 | 00FDFF | Arabic Presentation Forms-A                |
|   100.0% |      26 | 00FE50 | 00FE6F | Small Form Variants                        |
|    99.2% |     140 | 00FE70 | 00FEFF | Arabic Presentation Forms-B                |
|    20.0% |       1 | 00FFF0 | 00FFFF | Specials                                   |
| -------- | ------- | ------------------------------------------------------------ |
|    62.0% |    2899 |     = = = = = = = =   Overall Coverage   = = = = = = = =     |

Checking SSFN Font Integrity and Validity
-----------------------------------------

You can use `sfn2asc` tool to dump and debug SSFN fonts.

The flag `-h` will dump the font's header and string tables (like font name, manufacturer,
license etc.).

One `-d` will check the font for inconsistencies and dump its structures. `-dd` will
dump only the fragments table, `-ddd` only the character table, and `-dddd` only the kerning
table. If everything went well, you should see a `Font parsed OK.` message.

Finally `-D` will dump everything, even those files that otherwise fail consistency or
integrity checks (this could mess up your terminal occassionally).

For example:

```
$ ./sfn2asc -d stoneage.sfn
---Header---
magic: 'SSFN'
size: 20180
family: 2 (SSFN_FAMILY_DECOR)
style: 0 
quality: 1 (grid 32 x 32)
features: 0x02 SSFN_FEAT_HASCMAP 
revision: 0
hintunit: 0
baseline: 30
undeline: 31
bbox_left: 0
bbox_top: 0
bbox_right: 31
bbox_bottom: 31
fragments_offs: 0x00000068
characters_offs[]:
  0x000047e1 SSFN_VARIANT_LOCAL0 SSFN_VARIANT_DEFAULT
kerning_offs: 0x00000000

WARNING unlikely baseline value

---String Table---
0. name         "Stone Age"
1. family       "Dingbats"
2. subfamily    ""
3. revision     "1.0"
4. manufacturer "Eclipse"
5. license      "From the Stone Age game"

---Fragments Table---
000068: c0 1f 0d a9 00  SSFN_FRAG_PIXMAP, width 32, height 14, size  170, data 87 f0 83 06...
000117: c0 1f 05 79 00  SSFN_FRAG_PIXMAP, width 32, height  6, size  122, data 81 04 81 f0...
000196: c0 1f 07 87 00  SSFN_FRAG_PIXMAP, width 32, height  8, size  136, data 8f f0 81 15...
000223: c0 1f 1f 09 01  SSFN_FRAG_PIXMAP, width 32, height 32, size  266, data 8d f0 81 1d...
000332: c0 1f 1f 3b 02  SSFN_FRAG_PIXMAP, width 32, height 32, size  572, data 81 f0 81 18...
000573: c0 1f 1f 29 01  SSFN_FRAG_PIXMAP, width 32, height 32, size  298, data 89 f0 81 13...
0006a2: c0 1f 1f 93 01  SSFN_FRAG_PIXMAP, width 32, height 32, size  404, data 81 09 83 08...
00083b: c0 1f 1f 83 01  SSFN_FRAG_PIXMAP, width 32, height 32, size  388, data 81 0c 83 09...
    . . . (>8) . . .
00433a: c0 1f 1f c3 01  SSFN_FRAG_PIXMAP, width 32, height 32, size  452, data 83 08 95 f0...
004503: c0 1f 1f 8f 01  SSFN_FRAG_PIXMAP, width 32, height 32, size  400, data 83 01 95 f0...
004698: c0 1f 1f 43 01  SSFN_FRAG_PIXMAP, width 32, height 32, size  324, data 81 0c 83 09...

---Characters Table---
ab       skip    44 code points

01 01 00 00 00 20 20 20 00 00 12  U+00002C ',', width 32, height 32, advance x 32 y 0, bearing left 0 top 18, n=1
  68 00 00 00  fragment 000068 SSFN_FRAG_PIXMAP  at x=0, y=0

01 01 00 00 00 20 20 20 00 00 1a  U+00002D '-', width 32, height 32, advance x 32 y 0, bearing left 0 top 26, n=1
  17 01 00 00  fragment 000117 SSFN_FRAG_PIXMAP  at x=0, y=0

01 01 00 00 00 20 20 20 00 00 18  U+00002E '.', width 32, height 32, advance x 32 y 0, bearing left 0 top 24, n=1
  96 01 00 00  fragment 000196 SSFN_FRAG_PIXMAP  at x=0, y=0

01 01 00 00 00 20 20 20 00 00 00  U+00002F '/', width 32, height 32, advance x 32 y 0, bearing left 0 top 0, n=1
  23 02 00 00  fragment 000223 SSFN_FRAG_PIXMAP  at x=0, y=0

01 01 00 00 00 20 20 20 00 00 00  U+000030 '0', width 32, height 32, advance x 32 y 0, bearing left 0 top 0, n=1
  32 03 00 00  fragment 000332 SSFN_FRAG_PIXMAP  at x=0, y=0

01 01 00 00 00 20 20 20 00 00 00  U+000031 '1', width 32, height 32, advance x 32 y 0, bearing left 0 top 0, n=1
  73 05 00 00  fragment 000573 SSFN_FRAG_PIXMAP  at x=0, y=0

    . . . (>8) . . .

01 01 00 00 00 20 20 20 00 00 00  U+000059 'Y', width 32, height 32, advance x 32 y 0, bearing left 0 top 0, n=1
  03 45 00 00  fragment 004503 SSFN_FRAG_PIXMAP  at x=0, y=0

01 01 00 00 00 20 20 20 00 00 00  U+00005A 'Z', width 32, height 32, advance x 32 y 0, bearing left 0 top 0, n=1
  98 46 00 00  fragment 004698 SSFN_FRAG_PIXMAP  at x=0, y=0

ff ff  skip 16384 code points
ff ff  skip 16384 code points

    . . . (>8) . . .

ff ff  skip 16384 code points
ff a5  skip 16294 code points

---Color Map (offset 0x4b10, size 960)---
number of colors: 37

Font parsed OK.
```

